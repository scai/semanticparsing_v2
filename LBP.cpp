#include "LBP.h"
#include <fstream>
//#include "opencv2\gpu\gpu.hpp"

using namespace std;
using namespace cv;
using namespace cv::cuda;

LBPFeature::LBPFeature()
{}

LBPFeature::~LBPFeature()
{
	checkCudaErrors(cudaFree(_d_array));
}


void calculateWeight(float* h_array, int radius, int neighbors){
	for (int n = 0; n < neighbors; n++) {
		// sample points
		float x = static_cast<float>(radius)* cos(2.0 * CV_PI * n / static_cast<float>(neighbors));
		float y = static_cast<float>(radius)* -sin(2.0 * CV_PI * n / static_cast<float>(neighbors));
		// relative indices
		int fx = static_cast<int>(floor(x));
		int fy = static_cast<int>(floor(y));
		int cx = static_cast<int>(ceil(x));
		int cy = static_cast<int>(ceil(y));
		// fractional part
		float ty = y - fy;
		float tx = x - fx;
		// set interpolation weights
		float w1 = (1 - tx) * (1 - ty);
		float w2 = tx  * (1 - ty);
		float w3 = (1 - tx) *      ty;
		float w4 = tx  *      ty;
		// iterate through your data
		h_array[n * 8 + 0] = fx;
		h_array[n * 8 + 1] = fy;
		h_array[n * 8 + 2] = cx;
		h_array[n * 8 + 3] = cy;
		h_array[n * 8 + 4] = w1;
		h_array[n * 8 + 5] = w2;
		h_array[n * 8 + 6] = w3;
		h_array[n * 8 + 7] = w4;
	}
}

Mat LBPFeature::computeGPU(const GpuMat & src_d, int radius, int neighbors)
{
	GpuMat d_dst(Size(src_d.cols, src_d.rows), CV_8UC1, Scalar(0));
	float* h_array = new float[neighbors * 8];
	calculateWeight(h_array, radius, neighbors);
	your_lbp(src_d, d_dst, src_d.rows, src_d.cols,  h_array);
	free(h_array);
		
	Mat dst;
	d_dst.download(dst);
	dst.convertTo(dst, CV_32F);
	return dst;
}