#ifndef __feature
#define __feature

#include <string>
#include <vector>
#include <iostream>

#include "cv.h"
#include "cxcore.h"
#include "cvaux.h"
#include "highgui.h"
#include "ANN.h"
#include "LBP.h"
#include <omp.h>

extern "C"{
#include "dsift.h"
#include "sift.h"
}

#include "cuda.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include "opencv2/cudaarithm.hpp"
#include "opencv2/cudafilters.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/opencv.hpp"
#include "dsift_ext.h"

using namespace std;
class FeatureParameter;

/*feature name*/
const int HOG = 1;
const int FSIFT = 2;
const int TEXTON = 3;
const int SparseSIFT = 4;
const int SparseHOG = 5;
const int LBP = 6;
const int Color = 7;

/*feature type*/
const int PIXEL = 1;
const int SEGMENT = 2;

const int widthO = 1920;
const int heightO = 1080;

/* the parameter of feature*/
class FeatureParameter
{
public:
	FeatureParameter()
	{
		featType = PIXEL;
		featName = 0;
		strFeatName = "";

		saveFloderPath = "";
		loadFloderPath = "";

		featDim = -1;
		featPatchSize = -1;
		sampleStep = -1;
		numScales = 1;
		bowDim = 0;
		cluteringTree = NULL;
	}

	FeatureParameter(const FeatureParameter& f)
	{
		featName = f.featName;
		featType = f.featType;

		saveFloderPath = f.saveFloderPath;
		loadFloderPath = f.loadFloderPath;

		featDim = f.featDim;
		featPatchSize = f.featPatchSize;
		sampleStep = f.sampleStep;
		strFeatName = f.strFeatName;
		numScales = f.numScales;
		bowDim = f.bowDim;
		cluteringTree = f.cluteringTree;
	}

	~FeatureParameter();

	FeatureParameter& operator=(const FeatureParameter& f)
	{
		if (this != &f)
		{
			featName = f.featName;
			featType = f.featType;

			saveFloderPath = f.saveFloderPath;
			loadFloderPath = f.loadFloderPath;

			featDim = f.featDim;
			featPatchSize = f.featPatchSize;
			sampleStep = f.sampleStep;
			strFeatName = f.strFeatName;
			numScales = f.numScales;
			bowDim = f.bowDim;
			cluteringTree = f.cluteringTree;
		}
		return *this;
	}

public:
	int featName;            // the name of feature
	int featType;            // the type of feature: pixel or segment feature

	string strFeatName;      // the feature name
	string saveFloderPath;   // the folder to save the feature
	string loadFloderPath;   // the folder to load the feature

	int numScales;           // multi-scale feature extraction
	int featDim;             // the dimension of feature vector

	int featPatchSize;       // the patch size to extract pixel feature
	int sampleStep;          // the sampling step of extract pixel feature
	int bowDim;              // the bow projection dimension

	ANNkd_tree* cluteringTree; /*A vocabulary kd tree for bag-of-word*/
};

class FeatVector
{
public:
	FeatVector();
	FeatVector(const FeatVector& feav);
	FeatVector& operator=(const FeatVector& feav);

	float GetAttributeValue(int index) const
	{
		assert(index < data.size());
		assert(index >= 0);
		return data.at(index);
	}

	void SetAttributeValue(int index, float value)
	{
		assert(index < data.size());
		assert(index >= 0);
		data.at(index) = value;
	}

	int GetLabel() const { return labelClass; }
	void  SetLabel(int setClassLabel){ labelClass = setClassLabel; }
	int GetFeatDim() const { return data.size(); }

	vector<float>* GetDataPtr() { return &data; }
	void UpdateData(vector<float>* inputDataPtr){ data = *inputDataPtr; }
	void UpdateData(vector<float>& inputData){ data = inputData; }

	void InsertNewAttribute(float value){ data.push_back(value); }
	void ClearData(){ data.clear(); }

	void SetExtraData(int px, int py){ x = px; y = py; }
	void GetExtraData(int& px, int& py) const { px = x; py = y; }

public:
	FeatVector* spLevelFeat;
	FeatVector* imageLevelFeat;

private:
	int x;
	int y;
	vector<float> data;   // feature vector
	int labelClass;       // class label
	int index;
};

class PixelFeatureExtractor
{
private:
	static double *_d_uu, *_d_vv, *_d_kernel, *_d_hist, *_d_res;
	static bool _bCudaMalloc;

	/*static VlDsiftFilterGPU* SIFTFilter;*/
	//static double ;

public:
	PixelFeatureExtractor(vector<FeatureParameter>& setFeatParameter)
	{
		for (int i = 0; i<setFeatParameter.size(); i++)
		{
			featParameter.push_back(setFeatParameter.at(i));
		}
		//int PatchSize = 8, sampleStep = 1;
		//SIFTFilter = (VlDsiftFilterGPU*)vl_dsift_new_basic(widthO, heightO, sampleStep, PatchSize);
		//_vl_dsift_alloc_buffers_gpu(SIFTFilter);
	}

	PixelFeatureExtractor(FeatureParameter& setFeatParameter)
	{
		featParameter.push_back(setFeatParameter);
	}

	~PixelFeatureExtractor(){
		//vl_dsift_delete_gpu(SIFTFilter);
	};

	void Extraction(VlDsiftFilterGPU* SIFTFilter, vector< FeatVector* >& features, IplImage* Image, IplImage* LabelImage = NULL);

public:
	void Initialization(vector< FeatVector* >& features, IplImage* Image);

	void ExtractionTexton(vector< FeatVector* >& features, IplImage* Image, FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage = NULL);

	void ExtractionTexton(vector< FeatVector* >& features, IplImage* Image, cv::Mat& samplingMask, int sampleStep = 1, IplImage* LabelImage = NULL);

	void ExtractionTextonGPU(vector< FeatVector* >& features, IplImage* Image, cv::Mat& samplingMask, int sampleStep = 1, IplImage* LabelImage = NULL);

	//void ExtractionSIFT(vector< FeatVector* >& features, IplImage* Image, FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage, VlDsiftFilterGPU* SIFTFilter)

	void ExtractionSIFT(vector< FeatVector* >& features, IplImage* Image, FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage = NULL, VlDsiftFilterGPU* SIFTFilter = NULL);

	void ExtractionSIFT(vector< FeatVector* >& features, IplImage* Image, cv::Mat& samplingMask, int sampleStep = 1, IplImage* LabelImage = NULL);

	void ExtractionSIFTGPU(vector< FeatVector* >& features, IplImage* Image, cv::Mat& samplingMask, int sampleStep = 1, IplImage* LabelImage = NULL, VlDsiftFilterGPU* SIFTFilter= NULL);

	void ExtractionHOG(vector< FeatVector* >& features, IplImage* Image, FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage = NULL);

	void ExtractionHOG(vector< FeatVector* >& features, IplImage* Image, cv::Mat& samplingMask, int sampleStep = 1, int PatchSize = 9, IplImage* LabelImage = NULL);

	void calcHistonGPU(const cv::cuda::PtrStepSz<uchar>& matArray0, const cv::cuda::PtrStepSz<uchar>& matArray1, const cv::cuda::PtrStepSz<uchar>& matArray2, int* dims, double* uu, double* vv, double* kernel, int sbin, double* h_norm);
	
	void ExtractionHOGGPU(vector< FeatVector* >& features, IplImage* Image, cv::Mat& samplingMask, int sampleStep = 1, int PatchSize = 9, IplImage* LabelImage = NULL);

	void ExtractionSparseHOG(vector< FeatVector* >& features, IplImage* Image, FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage = NULL);

	void ExtractionSparseSIFT(vector< FeatVector* >& features, IplImage* Image, FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage = NULL);

	void ExtractionColor(vector< FeatVector* >& features, IplImage* Image, FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage = NULL);

	void ExtractionLBP(vector< FeatVector* >& features, IplImage* Image, FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage = NULL);

	void ExtractTextonShapeFilter(vector< FeatVector* >& features, IplImage* Image, FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage = NULL);

	int GetLabel(int x, int y, IplImage* LabelImage);

	//static void AssignLabel(vector< FeatVector* >& features,IplImage* LabelImage = NULL);

	static void AnalysisPCA(vector< FeatVector* >& features, string imageName, string flag);

public:
	vector<FeatureParameter> featParameter;
};

namespace TextonFeature{

	void filter(IplImage *img, vector<IplImage *> &response);

	void preprocessImage(vector<IplImage*> &responses, const IplImage *cimg, float scale);

	void resizeInPlace(IplImage **image, int height, int width, int interpolation = CV_INTER_LINEAR);
}

#endif
