#ifndef GLOBAL_H_
#define GLOBAL_H_

#include "Dataset.h"
#define mutiThread 2

extern GeneralConfiguration TestConfiguration;

extern int UNASSIGNED;
//extern int SKY;
//extern int TREE;
//extern int BUILDING;
//extern int GROUND;
//extern int VEHICLE;
//extern int OTHERS;
//extern int NONGROUND;
extern int UNCLUSTERING;
//extern int PLANEPOINT;
//extern int NOISEPOINT;

extern const int TRAINIMAGE;
extern const int TESTIMAGE;
extern const int VALIDATEIMAGE;
extern const int ALLIMAGE;

extern const int DEBUG_BOW;
extern const int DEBUG_VIEWSUPERPIXEL;
extern const int DEBUG_VIEWFEATUREPCA;
extern const int DEBUG_PATTERN;
extern const int DEBUG_STRUCT_LEARNING;
extern const int DEBUG_STRUCT_TESTING;
extern const int DEBUG_LINESEGMENT;

extern const int TypeLayerNode;
extern const int TypeUpLayerNode;
extern const int TypeSuperLayerNode;

/*the unit cost for struture learning*/
extern double UnitLossCost;

namespace LMath
{
	static const double pi = (double)3.1415926535897932384626433832795;
	static const double positiveInfinity = (double)1e50;
	static const double negativeInfinity = (double)-1e50;
	static const double almostZero = (double)1e-12;

	void SetSeed(unsigned int seed);
	unsigned int RandomInt();
	unsigned int RandomInt(unsigned int maxval);
	unsigned int RandomInt(unsigned int minval, unsigned int maxval);
	double RandomReal();
	double RandomGaussian(double mi, double var);

	double SquareEuclidianDistance(double *v1, double *v2, int size);
	double KLDivergence(double *histogram, double *referenceHistogram, int size, double threshold);
	double GetAngle(double x, double y);
};

#endif /* GLOBAL_H_ */
