#include "GraphElements.h"

NodeLink::NodeLink()
{
	linkedNode = 0;
	w = 0;
}

NodeLink::NodeLink(const NodeLink& t)
{
	linkedNode = t.linkedNode;
	w = t.w;
}

NodeLink::~NodeLink(){
	/*if (linkedNode){
		linkedNode = NULL;
		delete linkedNode;
	}*/
	//    if (linkedNode){
	//        //linkedNode = NULL;
	//        delete linkedNode;
	//    }
}


NodeLink& NodeLink::operator=(const NodeLink& t)
{
	if (this != &t)
	{
		linkedNode = t.linkedNode;
		w = t.w;
	}
	return *this;
}

GraphNode::GraphNode()
{
	index = -1;
	trueLabel = -1;
	predictLabel = -1;

	x = y = -1;
	isActive = true;
	withLabel = false;
	activeIndex = -1;
	size = 0;
}

GraphNode::GraphNode(const GraphNode& T)
{
	activeIndex = T.activeIndex;
	index = T.index;
	trueLabel = T.trueLabel;
	predictLabel = T.predictLabel;
	featVector = T.featVector;
	size = T.size;

	x = T.x;
	y = T.y;
	isActive = T.isActive;
	withLabel = T.withLabel;

	layerlinks.clear();
	for (unsigned int i = 0; i<T.layerlinks.size(); i++) layerlinks.push_back(T.layerlinks.at(i));

	upLayerlinks.clear();
	for (unsigned int i = 0; i<T.upLayerlinks.size(); i++) upLayerlinks.push_back(T.upLayerlinks.at(i));

	superLayerlinks.clear();
	for (unsigned int i = 0; i<T.superLayerlinks.size(); i++) superLayerlinks.push_back(T.superLayerlinks.at(i));

	constraintLayerlinks.clear();
	for (unsigned int i = 0; i<T.constraintLayerlinks.size(); i++)
		constraintLayerlinks.push_back(T.constraintLayerlinks.at(i));
}

GraphNode& GraphNode::operator=(const GraphNode& T)
{
	activeIndex = T.activeIndex;
	index = T.index;
	trueLabel = T.trueLabel;
	predictLabel = T.predictLabel;
	featVector = T.featVector;
	size = T.size;

	x = T.x;
	y = T.y;
	isActive = T.isActive;
	withLabel = T.withLabel;

	layerlinks.clear();
	for (unsigned int i = 0; i<T.layerlinks.size(); i++) layerlinks.push_back(T.layerlinks.at(i));

	upLayerlinks.clear();
	for (unsigned int i = 0; i<T.upLayerlinks.size(); i++) upLayerlinks.push_back(T.upLayerlinks.at(i));

	superLayerlinks.clear();
	for (unsigned int i = 0; i<T.superLayerlinks.size(); i++) superLayerlinks.push_back(T.superLayerlinks.at(i));

	constraintLayerlinks.clear();
	for (unsigned int i = 0; i<T.constraintLayerlinks.size(); i++)
		constraintLayerlinks.push_back(T.constraintLayerlinks.at(i));

	return *this;
}

void GraphNode::InsertLayerlinks(GraphNode* T, double setw)
{
	NodeLink e;
	layerlinks.push_back(e);
	int idx = layerlinks.size() - 1;
	layerlinks.at(idx).linkedNode = T;
	layerlinks.at(idx).w = setw;
}

void GraphNode::InsertUpLayerlinks(GraphNode* T, double setw)
{
	NodeLink e;
	upLayerlinks.push_back(e);
	int idx = upLayerlinks.size() - 1;
	upLayerlinks.at(idx).linkedNode = T;
	upLayerlinks.at(idx).w = setw;
}

void GraphNode::InsertSuperLayerlinks(GraphNode* T, double setw)
{
	NodeLink e;
	superLayerlinks.push_back(e);
	int idx = superLayerlinks.size() - 1;
	superLayerlinks.at(idx).linkedNode = T;
	superLayerlinks.at(idx).w = setw;
}

void GraphNode::InsertConstraintLayerlinks(GraphNode* T, double setw)
{
	NodeLink e;
	constraintLayerlinks.push_back(e);
	int idx = constraintLayerlinks.size() - 1;
	constraintLayerlinks.at(idx).linkedNode = T;
	constraintLayerlinks.at(idx).w = setw;
}
