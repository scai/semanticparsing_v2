#include "TreeDetection.h"
// SegmentationMask should be the region mask of root of a tree

float OverLappingRatio(TreePosition& p, TreePosition& q)
{
	float R = p.radius;

	float d = (q.x - p.x)*(q.x - p.x) + (q.y - p.y)*(q.y - p.y);
	d = sqrt(d + 0.0001);
	float r = q.radius;

	if (d<R - r)
	{
		return 1;
	}
	else if (d<R + r)
	{
		float t1 = (pow(d, 2) + pow(r, 2) - pow(R, 2)) / (2 * d*r);
		float t2 = (pow(d, 2) + pow(r, 2) - pow(R, 2)) / (2 * d*r);
		float A = pow(r, 2)*acos(t1) + pow(R, 2)*acos(t2) - 0.5*sqrt((-d + r + R)*(d + r - R)*(d - r + R)*(d + r + R) + 0.0001);

		return A / (3.1415*r*r);
	}
	else if (d>R + r)
	{
		return 0;
	}
}

void AerialTreeExtraction(IplImage* SegmentationMask, IplImage* InputImage, string SaveFileName)
{
	int min_radius = 6;
	int max_radius = 32;

	int width = SegmentationMask->width;
	int height = SegmentationMask->height;
	IplImage* BinaryMask = cvCreateImage(cvSize(width + 2 * max_radius, height + 2 * max_radius), 8, 1);
	cvSetZero(BinaryMask);

	int i, j, k;
	for (i = 0; i<SegmentationMask->height; i++)
	{
		for (j = 0; j<SegmentationMask->width; j++)
		{
			int b = ((unsigned char*)SegmentationMask->imageData)[i*SegmentationMask->widthStep + j*SegmentationMask->nChannels];
			int g = ((unsigned char*)SegmentationMask->imageData)[i*SegmentationMask->widthStep + j*SegmentationMask->nChannels + 1];
			int r = ((unsigned char*)SegmentationMask->imageData)[i*SegmentationMask->widthStep + j*SegmentationMask->nChannels + 2];
			if (b == 0 && g == 128 && r == 0)
			{
				BinaryMask->imageData[(i + max_radius)*BinaryMask->widthStep + j + max_radius] = 255;
			}
		}
	}

	//cvSaveImage("InputMask.png",BinaryMask);
	vector<TreePosition> TreeCandidate;
	CvPoint MaxLocation;
	vector<TreePosition> SelectedTreeCandidate;

	int x, y;

	for (i = max_radius; i >= min_radius; i = i - 4)
	{
		IplImage* CircleTemplate = cvCreateImage(cvSize(2 * i, 2 * i), 8, 1);

		for (x = 0; x<CircleTemplate->height; x++)
		{
			for (y = 0; y<CircleTemplate->width; y++)
			{
				float d = (x - CircleTemplate->width / 2)*(x - CircleTemplate->width / 2) +
					(y - CircleTemplate->height / 2)*(y - CircleTemplate->height / 2);
				if (d <= i*i)
					CircleTemplate->imageData[y*CircleTemplate->widthStep + x] = 255;
				else
					CircleTemplate->imageData[y*CircleTemplate->widthStep + x] = 0;
			}
		}

		//cvSaveImage("Template.png",CircleTemplate);
		CvMat* MatchingMask = cvCreateMat(BinaryMask->height - CircleTemplate->height + 1,
			BinaryMask->width - CircleTemplate->width + 1,
			CV_32FC1);
		cvSetZero(MatchingMask);
		cvMatchTemplate(BinaryMask, CircleTemplate, MatchingMask, CV_TM_SQDIFF_NORMED);
		//BasicTool::ViewMat(MatchingMask,"Response.png");

		int step = 4;
		for (int m = 0; m<MatchingMask->height / step; m++)
		{
			for (int n = 0; n<MatchingMask->width / step; n++)
			{
				TreePosition Candidate;
				float max_prob = 1e9;
				for (int y = m*step; y<m*step + step; y++)
				{
					for (int x = n*step; x<n*step + step; x++)
					{
						if (max_prob>MatchingMask->data.fl[y*MatchingMask->cols + x])
						{
							max_prob = MatchingMask->data.fl[y*MatchingMask->cols + x];
							MaxLocation.x = x;
							MaxLocation.y = y;
						}
					}
				}

				if (max_prob<0.3)
				{
					//cout<<max_prob<<endl;
					Candidate.x = MaxLocation.x + (CircleTemplate->width - 1) / 2 - max_radius;
					Candidate.y = MaxLocation.y + (CircleTemplate->height - 1) / 2 - max_radius;
					Candidate.radius = i;
					Candidate.probabilty = max_prob;
					TreeCandidate.push_back(Candidate);
				}
			}
		}
		cvReleaseImage(&CircleTemplate);
		cvReleaseMat(&MatchingMask);

		int num = TreeCandidate.size();
		HeapNode* BestMatching = NULL;
		HeapNode* MatchingSP = new HeapNode[num];
		FibHeap * MatchingQueue = new FibHeap();

		for (j = 0; j<TreeCandidate.size(); j++)
		{
			MatchingSP[j].x = TreeCandidate.at(j).x;
			MatchingSP[j].y = TreeCandidate.at(j).y;
			MatchingSP[j].radius = TreeCandidate.at(j).radius;
			MatchingSP[j].SetKeyValue(TreeCandidate.at(j).probabilty);
			MatchingQueue->Insert(&MatchingSP[j]);
		}

		for (j = 0; j<TreeCandidate.size(); j++)
		{
			BestMatching = (HeapNode*)MatchingQueue->ExtractMin();
			TreePosition q;
			q.x = BestMatching->x;
			q.y = BestMatching->y;
			q.radius = BestMatching->radius;

			bool InsertFlag = true;
			for (k = 0; k<SelectedTreeCandidate.size(); k++)
			{
				float r = OverLappingRatio(SelectedTreeCandidate.at(k), q);
				if (r>0.25)
				{
					InsertFlag = false;
					break;
				}
			}

			if (InsertFlag)
			{
				SelectedTreeCandidate.push_back(q);
			}
		}

		TreeCandidate.clear();

		delete[] MatchingSP;
		delete MatchingQueue;
	}

	//cout<<SelectedTreeCandidate.size()<<endl;

	for (k = 0; k<SelectedTreeCandidate.size(); k++)
	{
		int x = SelectedTreeCandidate.at(k).x;
		int y = SelectedTreeCandidate.at(k).y;
		cvCircle(InputImage, cvPoint(x, y), SelectedTreeCandidate.at(k).radius, cvScalar(0, 0, 255));
		cvCircle(InputImage, cvPoint(x, y), 1, cvScalar(255, 0, 0), -1);
	}
	cvSaveImage(SaveFileName.c_str(), InputImage);
	cvReleaseImage(&BinaryMask);
}
