#include "GraphModel.h"
#include "GCO/GCoptimization.h"
#include "BasicAPI.h"

using namespace BasicAPI;
int  GraphModel::numClass;
int  GraphModel::constraintLinkStep;
bool GraphModel::initialized;
bool GraphModel::withUnLabelRegion;
bool GraphModel::withConstraintLayer;
bool GraphModel::withRobustPn;

/*link configuration*/
bool GraphModel::withLayerUnary;
bool GraphModel::withUpLayerUnary;
bool GraphModel::withSuperLayerUnary;

bool GraphModel::withLayerPairwise;
bool GraphModel::withUpLayerPairwise;
bool GraphModel::withSuperLayerPairwise;

bool GraphModel::withUpLayer_LayerPairwise;
bool GraphModel::withSuperLayer_upLayerPairwise;
bool GraphModel::withSuperLayer_LayerPairwise;

cv::Mat*        GraphModel::constraintTable;
GraphParameters GraphModel::weights;

const float GraphModel::robustPnAllowedRatio = 0.25;
const float GraphModel::LargePenalty = 1e4;

const int Layer_Link = 0;
const int upLayer_Link = 1;
const int superLayer_Link = 2;
const int upLayer_Layer_Link = 3;
const int superLayer_Layer_Link = 4;
const int superLayer_upLayer_Link = 5;

const double almostZero = 1e-8;

/*determine the link type with the index of nodes*/
inline int GetLinkType(int node_i, int node_j, GraphModel* inputGraph)
{
	if (node_i < inputGraph->LayerSize && node_j < inputGraph->LayerSize)
		return Layer_Link;
	else if (node_i >= inputGraph->LayerSize && node_i < inputGraph->upLayerSize &&
		node_j >= inputGraph->LayerSize && node_j < inputGraph->upLayerSize)
		return upLayer_Link;
	else if (node_i >= inputGraph->upLayerSize && node_j >= inputGraph->upLayerSize)
		return superLayer_Link;
	else if (node_j >= inputGraph->LayerSize && node_j < inputGraph->upLayerSize &&
		node_i <  inputGraph->LayerSize)
		return upLayer_Layer_Link;
	else if (node_j >= inputGraph->upLayerSize && node_i < inputGraph->LayerSize)
		return superLayer_Layer_Link;
	else if (node_j >= inputGraph->upLayerSize &&
		node_i >= inputGraph->LayerSize && node_i < inputGraph->upLayerSize)
		return superLayer_upLayer_Link;
}

GCoptimization::EnergyTermType GraphModel::SmoothTermCostGeneral(int node_i, int node_j, int i, int j, void* extraData)
{
	if (i == j) return 0;
	else return 1;
}

/* linear parameters for pairwise function: w * f */
/* Two way: 1 keep the pairwise function metric
w[i]+w[j] for any label combination (i,j), i != j*/
/*          2 the pairwise function is not metric
*            w[i][j] for one label combination (i,j),i != j*/
/* How to model the interaction between parameters and feature is an open problem now*/

/* note that : node_i < node_j */
GCoptimization::EnergyTermType GraphModel::SmoothTermCost(int node_i, int node_j, int label_i, int label_j, void* extraData)
{
	GCoptimization::EnergyTermType cost = 0;
	/*exchange node_i and node_j*/
	if (node_i > node_j)
	{
		int temp = node_i;
		node_i = node_j;
		node_j = temp;

		temp = label_i;
		label_i = label_j;
		label_j = temp;
	}

	if (label_i != label_j)
	{
		GraphModel* inputGraph = static_cast<GraphModel*>(extraData);
		/*the step for constraint link must make sure that step * width > maxImageWidth*/
		/*so that we can identify different types of links easily*/
		/*layer pairwise*/

		int linkType = GetLinkType(node_i, node_j, inputGraph);
		switch (linkType)
		{
		case Layer_Link:
		{
			if (GraphModel::withConstraintLayer == true)
			{
				if (abs(inputGraph->Layer.at(node_i)->x - inputGraph->Layer.at(node_j)->x) == 1 ||
					abs(inputGraph->Layer.at(node_i)->y - inputGraph->Layer.at(node_j)->y) == 1)
				{
					if (GraphModel::weights.LayerLinksWeights.paraSizeRow == 1)
						/*p(i,j) = w[i]+w[j]*/
						cost = GraphModel::weights.LayerLinksWeights.paras.at(0).at(label_j) +
						GraphModel::weights.LayerLinksWeights.paras.at(0).at(label_i);
					else
						cost = GraphModel::weights.LayerLinksWeights.paras.at(label_i).at(label_j) +
						GraphModel::weights.LayerLinksWeights.paras.at(label_j).at(label_i);
				}
				else
				{
					if (GraphModel::weights.constraintWeights.paraSizeRow == 1)
					{
						cost = (GraphModel::weights.constraintWeights.paras.at(0).at(0) +
							GraphModel::weights.constraintWeights.paras.at(0).at(0))*
							(1 - GraphModel::constraintTable->at<int>(label_i, label_j));
					}
					else
					{
						cost = (GraphModel::weights.constraintWeights.paras.at(0).at(label_j) +
							GraphModel::weights.constraintWeights.paras.at(0).at(label_i))*
							(1 - GraphModel::constraintTable->at<int>(label_i, label_j));
					}
				}
			}
			else
			{
				if (label_j < GraphModel::numClass && label_i < GraphModel::numClass) /*not auxilary label*/
				{
					if (GraphModel::weights.LayerLinksWeights.paraSizeRow == 1)
						/*p(i,j) = w[i]+w[j]*/
						cost = GraphModel::weights.LayerLinksWeights.paras.at(0).at(label_j) +
						GraphModel::weights.LayerLinksWeights.paras.at(0).at(label_i);
					else
						cost = GraphModel::weights.LayerLinksWeights.paras.at(label_i).at(label_j) +
						GraphModel::weights.LayerLinksWeights.paras.at(label_j).at(label_i);
				}
				else cost = GraphModel::LargePenalty;
			}

			break;
		}
		case upLayer_Link:
		{
			if (GraphModel::weights.upLayerLinksWeights.paraSizeRow == 1)
				cost = GraphModel::weights.upLayerLinksWeights.paras.at(0).at(label_j) +
				GraphModel::weights.upLayerLinksWeights.paras.at(0).at(label_i);
			else
				cost = GraphModel::weights.upLayerLinksWeights.paras.at(label_i).at(label_j) +
				GraphModel::weights.upLayerLinksWeights.paras.at(label_j).at(label_i);
			break;
		}
		case superLayer_Link:
		{
			if (GraphModel::weights.superLayerLinksWeights.paraSizeRow == 1)
				cost = GraphModel::weights.superLayerLinksWeights.paras.at(0).at(label_j) +
				GraphModel::weights.superLayerLinksWeights.paras.at(0).at(label_i);
			else
				cost = GraphModel::weights.superLayerLinksWeights.paras.at(label_i).at(label_j) +
				GraphModel::weights.superLayerLinksWeights.paras.at(label_j).at(label_i);
			break;
		}
		case upLayer_Layer_Link:
		{
			/*Robust Pn Model*/
			if (GraphModel::withRobustPn == true)
			{
				if (label_j < GraphModel::numClass) /*not auxilary label*/
				{
					cost = GraphModel::weights.RobustPnWeights.paras.at(0).at(0);
				}
			}
			else /*Not a Robust Pn Model*/
			{
				if (GraphModel::weights.upLayer_LayerLinksWeights.paraSizeRow == 1)
					cost = GraphModel::weights.upLayer_LayerLinksWeights.paras.at(0).at(label_j);
				else
					cost = GraphModel::weights.upLayer_LayerLinksWeights.paras.at(label_j).at(label_i);
			}

			break;
		}
		case superLayer_Layer_Link:
		{
			if (GraphModel::weights.superLayer_LayerLinksWeights.paraSizeRow == 1)
				cost = GraphModel::weights.superLayer_LayerLinksWeights.paras.at(0).at(label_j);
			else
				cost = GraphModel::weights.superLayer_LayerLinksWeights.paras.at(label_j).at(label_i);

			break;
		}
		case superLayer_upLayer_Link:
		{
			if (GraphModel::weights.superLayer_upLayerLinksWeights.paraSizeRow == 1)
				cost = GraphModel::weights.superLayer_upLayerLinksWeights.paras.at(0).at(label_j);
			else
				cost = GraphModel::weights.superLayer_upLayerLinksWeights.paras.at(label_j).at(label_i);

			break;
		}
		}
	}

	return cost;
}

GraphModel::GraphModel()
{
	width = 0;
	height = 0;

	LayerSize = 0;
	upLayerSize = 0;
	superLayerSize = 0;
}

GraphModel::GraphModel(int  setNumClass)
{
	// TODO Auto-generated constructor stub
	numClass = setNumClass;
	width = 0;
	height = 0;

	initialEnergy = 0;
	finalEnergy = 0;

	LayerSize = 0;
	upLayerSize = 0;
	superLayerSize = 0;
}

GraphModel::~GraphModel() {
	// TODO Auto-generated destructor stub
	/*layer*/

	Release();
}

void GraphModel::Release()
{
	for (unsigned int i = 0; i<Layer.size(); i++)
	{
		if (Layer.at(i) != NULL) delete Layer.at(i);
	}

	for (unsigned int i = 0; i<upLayer.size(); i++)
	{
		if (upLayer.at(i) != NULL) delete upLayer.at(i);
	}

	for (unsigned int i = 0; i<superLayer.size(); i++)
	{
		if (superLayer.at(i) != NULL) delete superLayer.at(i);
	}

	Layer.clear();
	upLayer.clear();
	superLayer.clear();

	//if (constraintTable){
	//	delete constraintTable;
	//	//constraintTable = NULL;
	//}
}

void GraphModel::SetupLayer(PixelImage* image, cv::Mat* layerProb, cv::Mat* layerProbIndex)
{
	height = image->height;
	width = image->width;
	for (int i = 0; i<height; i++)
	{
		for (int j = 0; j<width; j++)
		{
			GraphNode* node = new GraphNode();
			node->index = i*width + j;
			node->x = j;
			node->y = i;

			/*exclude unlabled region*/
			if (image->labelMap != NULL)
			{
				node->trueLabel = image->labelMap->at<int>(i, j);
				if (image->labelMap->at<int>(i, j) <= 0)
				{
					node->withLabel = false;
					if (withUnLabelRegion == false) node->isActive = false;
				}
				else node->withLabel = true;
			}

			Layer.push_back(node);
			for (int k = 0; k<TestConfiguration.numOfClass; k++)//5 times of 0 for the current node's feature vector
			{
				Layer.at(node->index)->featVector.push_back(0);
			}
		}
	}

	/*using prediction prob to set unary potential cost*/
	if (layerProb != NULL && layerProbIndex != NULL)
	{
		for (unsigned int i = 0; i<layerProbIndex->rows; i++)
		{
			int x = layerProbIndex->at<float>(i, 0);
			int y = layerProbIndex->at<float>(i, 1);
			int idx = y*width + x;

			for (int j = 0; j<layerProb->cols; j++)
			{
				Layer.at(idx)->featVector.at(j) = 1 - layerProb->at<float>(i, j); // predicted confidence for each pixels
				//Layer.at(idx)->featVector.at(j) = -log(min(float(0.999),max(layerProb->at<float>(i,j),float(0.001))));
			}
		}
	}

	if (withLayerPairwise == true)
	{
		/*set adjacent list of pixel node in the graph*/
		// Each pixel has 4 neighbors
		for (unsigned int i = 0; i<Layer.size(); i++)
		{
			int x = Layer.at(i)->x;
			int y = Layer.at(i)->y;

			if (x + 1<width)  Layer.at(i)->InsertLayerlinks(Layer.at(y*width + x + 1));
			if (y + 1<height) Layer.at(i)->InsertLayerlinks(Layer.at((y + 1)*width + x));
			if (x - 1 >= 0)     Layer.at(i)->InsertLayerlinks(Layer.at(y*width + x - 1));
			if (y - 1 >= 0)     Layer.at(i)->InsertLayerlinks(Layer.at((y - 1)*width + x));
		}

		float average_color_differnece = 16;
		/*color different between neighbors*/
		for (unsigned int i = 0; i<Layer.size(); i++)
		{
			int x = Layer.at(i)->x;
			int y = Layer.at(i)->y;
			CvScalar s = cvGet2D(image->ptImage, y, x);
			for (int j = 0; j<Layer.at(i)->layerlinks.size(); j++)//to adjacent neighbors
			{
				int adjx = Layer.at(i)->layerlinks.at(j).linkedNode->x;
				int adjy = Layer.at(i)->layerlinks.at(j).linkedNode->y;
				CvScalar adjs = cvGet2D(image->ptImage, adjy, adjx);

				double colorDifference = 0;
				//double colorDifference = INT_MAX;//////?
				for (int k = 0; k<image->ptImage->nChannels; k++)
				{
					if (colorDifference < abs(s.val[k] - adjs.val[k]))
					{
						colorDifference = abs(s.val[k] - adjs.val[k]);//get max val in each channel and get the maximum one
					}
				}
				colorDifference /= average_color_differnece;
				Layer.at(i)->layerlinks.at(j).w = 1 / (colorDifference + 1.0);
			}
		}
	}
}

void GraphModel::SetupLayer(SuperPixelImage* image, cv::Mat* layerProb, cv::Mat* layerProbIndex)
{
	SetupLayer((PixelImage*)(image), layerProb, layerProbIndex);
}

void GraphModel::SetupUpLayer(SuperPixelImage* image,
	cv::Mat* upLayerProb, cv::Mat* upLayerProbIndex)
{
	/*set superpixel node in the graph*/
	for (unsigned int i = 0; i<image->superPixels.size(); i++)
	{
		GraphNode* node = new GraphNode();
		node->index = Layer.size() + i;

		/*exclude unlabled region*/
		node->trueLabel = image->superPixels.at(i).label;
		if (node->trueLabel <= 0)
		{
			node->withLabel = false;
			if (withUnLabelRegion == false) node->isActive = false;
		}
		else node->withLabel = true;

		node->size = image->superPixels.at(i).pixels.size();

		upLayer.push_back(node);
		for (int k = 0; k<TestConfiguration.numOfClass; k++)
		{
			upLayer.at(i)->featVector.push_back(0);
		}
	}

	if (upLayerProb != NULL && upLayerProbIndex != NULL)
	{
		for (unsigned int i = 0; i<upLayerProb->rows; i++)
		{
			int idx = upLayerProbIndex->at<int>(i, 0) - 1;
			for (int j = 0; j<TestConfiguration.numOfClass; j++)
			{
				upLayer.at(idx)->featVector.at(j) = 1 - upLayerProb->at<float>(i, j);
				//upLayer.at(i)->featVector.at(j) = -log(min(float(0.999),max(upLayerProb->at<float>(i,j),float(0.001))));
			}
		}
	}

	/*set superpixel node in the graph*/
	for (unsigned int i = 0; i<image->superPixels.size(); i++)
	{
		for (int j = 0; j<image->superPixels.at(i).pixels.size(); j++)
		{
			int x = image->superPixels.at(i).pixels.at(j).x;
			int y = image->superPixels.at(i).pixels.at(j).y;

			if (withUpLayer_LayerPairwise == true || withRobustPn == true)
				upLayer.at(i)->InsertLayerlinks(Layer.at(y*width + x), 1.0);
		}
		upLayer.at(i)->x = image->superPixels.at(i).cx;
		upLayer.at(i)->y = image->superPixels.at(i).cy;
	}

	if (withUpLayerPairwise == true)
	{
		/*set adjacent list of superpixel node in the graph*/
		for (unsigned int i = 0; i<image->superPixels.size(); i++)
		{
			for (int j = 0; j<image->superPixels.at(i).adjIndex.size(); j++)
			{
				int adjIndex = image->superPixels.at(i).adjIndex.at(j) - 1;
				double w = image->superPixels.at(i).linkWeight.at(j);
				upLayer.at(i)->InsertUpLayerlinks(upLayer.at(adjIndex), w);
			}
		}

		//		float average_color_differnece = 16;
		//		/*color different between neighbors*/
		//		for(unsigned int i=0;i<upLayer.size();i++)
		//		{
		//			CvScalar averageColor;
		//			for(int j=0;j<upLayer.at(i)->layerlinks.size();j++)
		//			{
		//				int x = upLayer.at(i)->layerlinks.at(j).linkedNode->x;
		//				int y = upLayer.at(i)->layerlinks.at(j).linkedNode->y;
		//				CvScalar s = cvGet2D(image->ptImage,y,x);
		//
		//				for(int k=0;k<image->ptImage->nChannels;k++)
		//				{
		//					averageColor.val[k] += s.val[k];
		//				}
		//			}
		//
		//			for(int j=0;j<image->ptImage->nChannels;j++)
		//			{
		//				averageColor.val[j] /= upLayer.at(i)->layerlinks.size();
		//			}
		//
		//			for(int j=0;j<upLayer.at(i)->upLayerlinks.size();j++)
		//			{
		//				CvScalar adjAverageColor;
		//				for(int k=0;k<upLayer.at(i)->upLayerlinks.at(j).linkedNode->layerlinks.size();k++)
		//				{
		//					int x = upLayer.at(i)->upLayerlinks.at(j).linkedNode->layerlinks.at(k).linkedNode->x;
		//					int y = upLayer.at(i)->upLayerlinks.at(j).linkedNode->layerlinks.at(k).linkedNode->y;
		//					CvScalar s = cvGet2D(image->ptImage,y,x);
		//
		//					for(int n=0;n<image->ptImage->nChannels;n++)
		//					{
		//						adjAverageColor.val[n] += s.val[n];
		//					}
		//				}
		//
		//				for(int k=0;k<image->ptImage->nChannels;k++)
		//				{
		//					adjAverageColor.val[k] /= upLayer.at(i)->upLayerlinks.at(j).linkedNode->layerlinks.size();
		//				}
		//
		//				double colorDifference = 0;
		//				for(int k=0;k<image->ptImage->nChannels;k++)
		//				{
		//					if(colorDifference < abs(averageColor.val[k]-adjAverageColor.val[k]))
		//					{
		//						colorDifference = abs(averageColor.val[k]-adjAverageColor.val[k]);
		//					}
		//				}
		//				colorDifference /= average_color_differnece;
		//				upLayer.at(i)->upLayerlinks.at(j).w = 1/(colorDifference+1.0);
		//			}
		//		}
	}
}

void GraphModel::SetupSuperLayer(SuperPixelImage* image,
	cv::Mat* superLayerProb, cv::Mat* superLayerProbIndex)
{


}

void GraphModel::SetupGraph(BasicImage& image)
{
	inputImage.filenameImage = image.filenameImage;             // image file name
	inputImage.filenameLabelMask = image.filenameLabelMask;         // annotation file name
	inputImage.filenameSuperPixel = image.filenameSuperPixel;
}

/*setup graph from image*/
void GraphModel::SetupGraph(PixelImage* image, cv::Mat* layerProb, cv::Mat* layerProbIndex)
{
	height = image->ptImage->height;
	width = image->ptImage->width;

	inputImage.filenameImage = image->filenameImage;             // image file name
	inputImage.filenameLabelMask = image->filenameLabelMask;         // annotation file name
	inputImage.filenameSuperPixel = image->filenameSuperPixel;

	/*To reduce memory cost, the graph structure will be constructed when inference performed*/
	SetupLayer(image, layerProb, layerProbIndex);

	int numLayerNodes = 0;
	int numUpLayerNodes = 0;
	int numSuperLayerNodes = 0;

	/*re-assign index for nodes*/
	for (unsigned int i = 0; i<Layer.size(); i++)
	{
		if (Layer.at(i)->isActive == true) numLayerNodes++;
	}

	for (unsigned int i = 0; i<upLayer.size(); i++)
	{
		if (upLayer.at(i)->isActive == true) numUpLayerNodes++;
	}

	for (unsigned int i = 0; i<superLayer.size(); i++)
	{
		if (superLayer.at(i)->isActive == true) numSuperLayerNodes++;
	}

	cout << endl << "graph size - ";
	cout << "Layer: " << numLayerNodes << " / ";
	cout << "upLayer: " << numUpLayerNodes << " / ";
	cout << "superLayer: " << numSuperLayerNodes << " ";
	cout << endl;
}

/* three types of models are supported: pixel + superpixel + segment */
//pixelCRF.SetupGraph(this, &probability, &probability_index, NULL, NULL, NULL, NULL);
void GraphModel::SetupGraph(SuperPixelImage* image,
	cv::Mat* layerProb, cv::Mat* layerProbIndex,
	cv::Mat* upLayerProb, cv::Mat* upLayerProbIndex,
	cv::Mat* superLayerProb, cv::Mat* superLayerProbIndex)
{
	height = image->ptImage->height;
	width = image->ptImage->width;

	if (withLayerUnary == true || withLayerPairwise == true)
		SetupLayer(image, layerProb, layerProbIndex);

	if (withRobustPn == true)
	{
		SetupUpLayer(image, NULL, NULL);
	}
	else
	{
		if (withUpLayerUnary == true || withUpLayerPairwise == true)
			SetupUpLayer(image, upLayerProb, upLayerProbIndex);
	}

	/* cluster */
	if (withSuperLayerUnary == true || withSuperLayerPairwise == true)
	{
		SetupSuperLayer(image, superLayerProb, superLayerProbIndex);
	}

	int numLayerNodes = 0;
	int numUpLayerNodes = 0;
	int numSuperLayerNodes = 0;

	/*re-assign index for nodes*/
	for (unsigned int i = 0; i<Layer.size(); i++)
	{
		if (Layer.at(i)->isActive == true) numLayerNodes++;
	}

	for (unsigned int i = 0; i<upLayer.size(); i++)
	{
		if (upLayer.at(i)->isActive == true) numUpLayerNodes++;
	}

	for (unsigned int i = 0; i<superLayer.size(); i++)
	{
		if (superLayer.at(i)->isActive == true) numSuperLayerNodes++;
	}

	cout << endl << "graph size - ";
	cout << "Layer: " << numLayerNodes << " / ";
	cout << "upLayer: " << numUpLayerNodes << " / ";
	cout << "superLayer: " << numSuperLayerNodes << " ";
	cout << endl;
}

/* Through introduce some extra nodes,
* we can insert some prior constraint into the graph model,
* currently, the considered constraints include categories pairwise prior and
* categories relative position prior,for example, the sky region should always
* appears above all other categories, and the building should never appears under
* the grass. To introduce this pairwise categories constraints, we use a N * N
* constraint table to renote these constraints, and each element in the table will
* be represented by a new discrete label in the labeling optimization process, if the
* categories combination should be appears, then the penalty for the corresponding
* label will be very large.*/
/* The strength of constraint will be decided by the number of introduced constrait
* nodes and links. In theory, if there exists a constraint node for each node pair, then,
* the constraint will be never voliated
* */
void GraphModel::SetupConstraintLayer()
{
	int linkCount = 0;

	assert(constraintLinkStep > 0 &&
		constraintLinkStep < width &&
		constraintLinkStep < height);

	int step = constraintLinkStep; /*should > 1*/
	if (withLayerUnary == true && Layer.size()>0)
	{
		for (unsigned int i = 0; i<Layer.size(); i++)
		{
			Layer.at(i)->constraintLayerlinks.clear();
			int x = Layer.at(i)->x;
			int y = Layer.at(i)->y;

			//if((x+step/2)%step == 0 && (y+step/2)%step == 0)
			{
				if (y >= step)
				{
					int linkIndex = (y - step)*width + x;
					Layer.at(i)->InsertConstraintLayerlinks(Layer.at(linkIndex), 1.0);
					linkCount++;
				}

				if (x >= step)
				{
					int linkIndex = y*width + x - step;
					Layer.at(i)->InsertConstraintLayerlinks(Layer.at(linkIndex), 1.0);
					linkCount++;
				}

				if (y + step<height)
				{
					int linkIndex = (y + step)*width + x;
					Layer.at(i)->InsertConstraintLayerlinks(Layer.at(linkIndex), 1.0);
					linkCount++;
				}

				if (x + step<width)
				{
					int linkIndex = y*width + x + step;
					Layer.at(i)->InsertConstraintLayerlinks(Layer.at(linkIndex), 1.0);
					linkCount++;
				}
			}
		}
	}

	cout << "number of constraint links: " << linkCount << endl;
}

/*map inference of graph with loopy belief propagation
*this algorithm is very slow for large scale graph, like images*/
//void GraphModel::MAPInferenceBP(bool withInitializaiton)
//{
//	int numLabels = numClass;
//
//    LayerSize      = Layer.size();
//    upLayerSize    = Layer.size() + upLayer.size();
//    superLayerSize = Layer.size() + upLayer.size() + superLayer.size();
//
//	int numNodes = superLayerSize + constraintLayer.size();
//	dai::FactorGraph* inferenceGraph = NULL;
//
//	if(numNodes > 0 && numLabels>0)
//	{
//	    vector<dai::Var> vars;
//	    vector<dai::Factor> factors;
//
//		vars.reserve(numNodes);
//	    /*set layer unary*/
//	    for(unsigned int i=0;i<Layer.size();i++)
//	    {
//	    	Layer.at(i).index = i;
//	    	dai::Var v(Layer.at(i).index,numLabels);
//	    	vars.push_back(v);
//	    }
//
//	    factors.reserve(numNodes);
//	    for(unsigned int i=0;i<Layer.size();i++)
//	    {
//	    	dai::Factor f(vars.at(i));
//	    	for(int j=0;j<Layer.at(i).featVector.size();j++)
//	    	{
//	    		f.set(j,float(1-Layer.at(i).featVector.at(j)));
//	    	}
//	    	factors.push_back(f);
//	    }
//
//	    /*set layer pairwise*/
//	    MRF::CostVal link_weight = 1;
//	    for(unsigned int i=0;i<Layer.size();i++)
//	    {
//	    	for(int j=0;j<Layer.at(i).Layerlinks.size();j+=2)
//	    	{
//	    		link_weight = Layer.at(i).Layerlinks.at(j).w;
//	    		if(Layer.at(i).index > Layer.at(i).Layerlinks.at(j).linkedNode->index)
//	    		{
//	    			if(link_weight>0)
//	    			{
//	    				dai::Factor pariwise(dai::VarSet(vars.at(Layer.at(i).index),
//							                             vars.at(Layer.at(i).Layerlinks.at(j).linkedNode->index)));
//
//	    				for(int m=0;m<numLabels;m++)
//	    				{
//	    					for(int n=0;n<numLabels;n++)
//	    					{
//	    						double w = 0;
//	    						if(m == n)
//	    						{
//	    							w = 1.0/numLabels;
//	    						}
//	    						pariwise.set(m*numLabels+n,w);
//	    					}
//	    				}
//	    				factors.push_back(pariwise);
//	    			}
//	    		}
//	    	}
//	    }
//
//	    inferenceGraph = new dai::FactorGraph(factors.begin(),factors.end(),
//	        		                          vars.begin(),vars.end(),
//	        		                          factors.size(),vars.size());
//	}
//	else return;
//
//    // Output some information about the factorgraph
//    cout << inferenceGraph->nrVars() << " variables" << endl;
//    cout << inferenceGraph->nrFactors() << " factors" << endl;
//
//    size_t maxiter = 10000;
//    dai::Real   tol = 1e-9;
//    size_t verb = 0;
//
//    // Store the constants in a PropertySet object
//    dai::PropertySet opts;
//    opts.set("maxiter",maxiter);  // Maximum number of iterations
//    opts.set("tol",tol);          // Tolerance for convergence
//    opts.set("verbose",verb);     // Verbosity (amount of output generated)
//    opts.set("logdomain",true);
//    opts.set("updates",string("SEQFIX"));
//
//    dai::BP bp(*inferenceGraph, opts);
//    bp.init();
//    bp.run();
//
//    vector<dai::Factor> prob = bp.beliefs();
//    /*set layer unary*/
//    for(unsigned int i=0;i<Layer.size();i++)
//    {
//    	pair<size_t,double> argmax = prob[i].p().argmax();
//    	Layer.at(i).predictLabel = argmax.first+1;
//    }
//}

void GraphModel::AssignRobustPnUnaryCost(GCoptimization::EnergyTermType* dCost,
	int numLabels,
	int startIndex,
	vector<int>& activeNodeIndexs,
	vector<GraphNode*>& inputLayer,
	int NodeType,
	int *initialLabeling)
{
	PotentialParameters* w = &weights.RobustPnWeights;
	assert(NodeType == TypeUpLayerNode);

	/*set layer unary*/
	for (unsigned int i = 0; i<activeNodeIndexs.size(); i++)
	{
		int idx = activeNodeIndexs.at(i);
		inputLayer.at(idx)->activeIndex = i;

		for (int j = 0; j<numClass; j++)
		{
			dCost[(i + startIndex)*numLabels + j] = inputLayer.at(idx)->size *
				(1 - GraphModel::robustPnAllowedRatio) * w->paras.at(0).at(0);
		}

		for (int j = numClass; j<numLabels; j++)
			dCost[(i + startIndex)*numLabels + j] = inputLayer.at(idx)->size * w->paras.at(0).at(0);

		/*initialization with ground truth*/
		if (inputLayer.at(idx)->trueLabel > 0 && initialLabeling != NULL)
		{
			initialLabeling[i] = inputLayer.at(idx)->trueLabel - 1;
		}
	}
}

void GraphModel::AssignUnaryCost(GCoptimization::EnergyTermType* dCost,
	int numLabels,
	int startIndex,
	vector<int>& activeNodeIndexs,
	vector<GraphNode*>& inputLayer,
	int NodeType,
	int *initialLabeling)
{
	PotentialParameters* w = NULL;

	if (NodeType == TypeLayerNode)          w = &weights.LayerNodesWeights;
	else if (NodeType == TypeUpLayerNode)    w = &weights.upLayerNodesWeights;
	else if (NodeType == TypeSuperLayerNode) w = &weights.superLayerNodesWeights;

	/*set layer unary*/
	for (unsigned int i = 0; i<activeNodeIndexs.size(); i++)
	{
		int idx = activeNodeIndexs.at(i);
		inputLayer.at(idx)->activeIndex = i;

		assert(w->paraSizeRow == 1);
		assert(w->paraSizeCol == 1 || w->paraSizeCol == numClass);

		if (w->paraSizeCol == numClass)
		{
			for (int j = 0; j<numClass; j++)
			{
				double cost = inputLayer.at(idx)->featVector.at(j) * w->paras.at(0).at(j);
				dCost[(i + startIndex)*numLabels + j] = cost;
			}
		}
		else if (w->paraSizeCol == 1)
		{
			for (int j = 0; j<numClass; j++)
			{
				double cost = inputLayer.at(idx)->featVector.at(j) * w->paras.at(0).at(0);
				dCost[(i + startIndex)*numLabels + j] = cost;
			}
		}

		/*Robust Pn Model : with an auxilary label*/
		for (int j = numClass; j<numLabels; j++)
			dCost[(i + startIndex)*numLabels + j] = LargePenalty;

		/*initialization with ground truth*/
		if (inputLayer.at(idx)->trueLabel > 0 && initialLabeling != NULL)
		{
			initialLabeling[i] = inputLayer.at(idx)->trueLabel - 1;
		}
	}
}

void GraphModel::AssignPairwiseCost(GCoptimizationGeneralGraph* mrf,
	int startIndex,
	vector<int>& activeNodeIndexs,
	vector<GraphNode*>& inputLayer,
	int NodeType)
{
	/*set layer pairwise*/
	GCoptimization::EnergyTermType link_weight = 1;
	for (unsigned int i = 0; i<activeNodeIndexs.size(); i++)
	{
		int idx = activeNodeIndexs.at(i);
		assert(i == inputLayer.at(idx)->activeIndex);

		vector< NodeLink >* links = NULL;
		if (NodeType == TypeLayerNode) links = &inputLayer.at(idx)->layerlinks;
		else if (NodeType == TypeUpLayerNode) links = &inputLayer.at(idx)->upLayerlinks;
		else if (NodeType == TypeSuperLayerNode) links = &inputLayer.at(idx)->superLayerlinks;

		for (int j = 0; j<links->size(); j++)
		{
			int adjIdx = links->at(j).linkedNode->activeIndex;
			link_weight = links->at(j).w;

			/*only consider active nodes*/
			if (inputLayer.at(idx)->activeIndex > adjIdx && adjIdx >= 0)
			{
				if (link_weight > almostZero)
					mrf->setNeighbors(inputLayer.at(idx)->activeIndex + startIndex,
					adjIdx + startIndex,
					link_weight);
			}
		}

		if (withConstraintLayer == true)
		{
			/*constraint link*/
			for (int j = 0; j<inputLayer.at(idx)->constraintLayerlinks.size(); j++)
			{
				int adjIdx = inputLayer.at(idx)->constraintLayerlinks.at(j).linkedNode->activeIndex;
				link_weight = inputLayer.at(idx)->constraintLayerlinks.at(j).w;

				if (inputLayer.at(idx)->activeIndex > adjIdx && adjIdx >= 0)
				{
					if (link_weight>almostZero) mrf->setNeighbors(inputLayer.at(idx)->activeIndex + startIndex,
						adjIdx + startIndex,
						link_weight);
				}
			}
		}
	}
}

void GraphModel::AssignRobustPnPairwiseCost(GCoptimizationGeneralGraph* mrf,
	int srcStartIndex,
	int dstStartIndex,
	vector<int>& activeNodeIndexs,
	vector<GraphNode*>& inputLayer,
	int LinkType)
{
	assert(LinkType == TypeLayerNode);

	/*set layer pairwise*/
	GCoptimization::EnergyTermType link_weight = GraphModel::robustPnAllowedRatio / 2;
	for (unsigned int i = 0; i<activeNodeIndexs.size(); i++)
	{
		int idx = activeNodeIndexs.at(i);
		assert(i == inputLayer.at(idx)->activeIndex);

		vector< NodeLink >* links = &inputLayer.at(idx)->layerlinks;
		for (int j = 0; j<links->size(); j++)
		{
			int adjIdx = links->at(j).linkedNode->activeIndex;

			/*only consider active nodes*/
			if (adjIdx >= 0)
			{
				if (link_weight > almostZero)
					mrf->setNeighbors(inputLayer.at(idx)->activeIndex + srcStartIndex,
					adjIdx + dstStartIndex,
					link_weight);
			}
		}
	}
}

void GraphModel::AssignAcrossLayerPairwiseCost(GCoptimizationGeneralGraph* mrf,
	int srcStartIndex,
	int dstStartIndex,
	vector<int>& activeNodeIndexs,
	vector<GraphNode*>& inputLayer,
	int LinkType)
{
	/*set layer pairwise*/
	GCoptimization::EnergyTermType link_weight = 1;
	for (unsigned int i = 0; i<activeNodeIndexs.size(); i++)
	{
		int idx = activeNodeIndexs.at(i);
		assert(i == inputLayer.at(idx)->activeIndex);

		vector< NodeLink >* links = NULL;
		if (LinkType == TypeLayerNode) links = &inputLayer.at(idx)->layerlinks;
		else if (LinkType == TypeUpLayerNode) links = &inputLayer.at(idx)->upLayerlinks;
		else if (LinkType == TypeSuperLayerNode) links = &inputLayer.at(idx)->superLayerlinks;

		for (int j = 0; j<links->size(); j++)
		{
			int adjIdx = links->at(j).linkedNode->activeIndex;
			link_weight = links->at(j).w;

			if (inputLayer.at(idx)->activeIndex > adjIdx && adjIdx >= 0)
			{
				if (link_weight>almostZero) mrf->setNeighbors(inputLayer.at(idx)->activeIndex + srcStartIndex,
					adjIdx + dstStartIndex,
					link_weight);
			}
		}
	}
}

/*assign the loss cost for inference in the structure learning*/
void GraphModel::AssignLossCost(GCoptimization::EnergyTermType* dCost,
	int numLabels,
	int startIndex,
	vector<int>& activeNodeIndexs,
	vector<GraphNode*>& inputLayer,
	int NodeType,
	int *initialLabeling)
{
	PotentialParameters* w = NULL;

	if (NodeType == TypeLayerNode)          w = &weights.LayerNodesWeights;
	else if (NodeType == TypeUpLayerNode)    w = &weights.upLayerNodesWeights;
	else if (NodeType == TypeSuperLayerNode) w = &weights.superLayerNodesWeights;

	/*set layer unary*/
	for (unsigned int i = 0; i<activeNodeIndexs.size(); i++)
	{
		int idx = activeNodeIndexs.at(i);
		inputLayer.at(idx)->activeIndex = i;

		assert(w->paraSizeRow == 1);
		assert(w->paraSizeCol == 1 || w->paraSizeCol == numClass);

		if (inputLayer.at(idx)->trueLabel > 0)
		{
			dCost[(i + startIndex)*numLabels + inputLayer.at(idx)->trueLabel - 1] -= UnitLossCost;
		}
	}
}

void GraphModel::MAPInference(bool withInitializaiton, bool withLossCost)
{
	int numLabels = numClass;
	/*with one auxilary label for robust Pn Model, label index: numLabels-1*/
	if (GraphModel::withRobustPn == true) numLabels++;

	if (withConstraintLayer == true) SetupConstraintLayer();

	initialEnergy = 0;
	finalEnergy = 0;

	/*store the index of actived nodes*/
	vector<int> activeLayerNodes;
	vector<int> activeUpLayerNodes;
	vector<int> activeSuperLayerNodes;
	vector<int> activeConstraintLayerNodes;

	/*re-assign index for nodes*/
	for (int i = 0; i<Layer.size(); i++)
	{
		if (Layer.at(i)->isActive == true) activeLayerNodes.push_back(i);
	}
	for (int i = 0; i<upLayer.size(); i++)
	{
		if (upLayer.at(i)->isActive == true) activeUpLayerNodes.push_back(i);
	}
	for (int i = 0; i<superLayer.size(); i++)
	{
		if (superLayer.at(i)->isActive == true) activeSuperLayerNodes.push_back(i);
	}

	int numLayerNodes = activeLayerNodes.size();
	int numUpLayerNodes = activeUpLayerNodes.size();
	int numSuperLayerNodes = activeSuperLayerNodes.size();
	int numConstraintLayerNodes = activeConstraintLayerNodes.size();

	LayerSize = numLayerNodes;
	upLayerSize = numLayerNodes + numUpLayerNodes;
	superLayerSize = numLayerNodes + numUpLayerNodes + numSuperLayerNodes;

	int numNodes = numLayerNodes + numUpLayerNodes +
		numSuperLayerNodes + numConstraintLayerNodes;

	GCoptimization::EnergyTermType* dCost = new GCoptimization::EnergyTermType[numLabels*numNodes];

	/*setting the initial label for optimization*/
	int *initialLabeling = NULL;
	if (withInitializaiton == true)
	{
		initialLabeling = new int[numNodes];
#pragma omp parallel for
		for (int i = 0; i<numNodes; i++) initialLabeling[i] = 0;
	}

	int startIndex = 0;
	if (GraphModel::withLayerUnary == true)
	{	/*set layer unary*/
		AssignUnaryCost(dCost, numLabels, startIndex,
			activeLayerNodes, Layer, TypeLayerNode, initialLabeling);

		/*add loss cost for structure learning*/
		if (withLossCost == true)
		{
			AssignLossCost(dCost, numLabels, startIndex,
				activeLayerNodes, Layer, TypeLayerNode, initialLabeling);
		}
	}

	startIndex = LayerSize;
	if (GraphModel::withRobustPn == true)
	{
		AssignRobustPnUnaryCost(dCost, numLabels, startIndex,
			activeUpLayerNodes, upLayer, TypeUpLayerNode, initialLabeling);
	}
	else
	{
		if (GraphModel::withUpLayerUnary == true)
			/*set uplayer unary*/
			AssignUnaryCost(dCost, numLabels, startIndex,
			activeUpLayerNodes, upLayer, TypeUpLayerNode, initialLabeling);
	}

	startIndex = upLayerSize;
	if (GraphModel::withSuperLayerUnary == true)
		/*set superlayer unary*/
		AssignUnaryCost(dCost, numLabels, startIndex,
		activeSuperLayerNodes, superLayer, TypeSuperLayerNode, initialLabeling);

	GCoptimizationGeneralGraph* mrf = new GCoptimizationGeneralGraph(numNodes, numLabels);
	mrf->setDataCost(dCost);
	mrf->setSmoothCost(GraphModel::SmoothTermCost, this);

	if (GraphModel::withLayerPairwise == true)
		/*set layer pairwise*/
		AssignPairwiseCost(mrf, 0, activeLayerNodes, Layer, TypeLayerNode);

	if (GraphModel::withUpLayerPairwise == true)
		/*set uplayer pairwise*/
		AssignPairwiseCost(mrf, LayerSize, activeUpLayerNodes, upLayer, TypeUpLayerNode);

	if (GraphModel::withSuperLayerPairwise == true)
		/*set superlayer pairwise*/
		AssignPairwiseCost(mrf, upLayerSize, activeSuperLayerNodes, superLayer, TypeSuperLayerNode);

	if (GraphModel::withRobustPn == true)
	{
		AssignRobustPnPairwiseCost(mrf, LayerSize, 0, activeUpLayerNodes, upLayer, TypeLayerNode);
	}
	else
	{
		if (GraphModel::withUpLayer_LayerPairwise == true)
			AssignAcrossLayerPairwiseCost(mrf, LayerSize, 0, activeUpLayerNodes, upLayer, TypeLayerNode);
	}

	if (GraphModel::withSuperLayer_upLayerPairwise == true)
		/*set uplayer-superlayer pairwise*/
		AssignAcrossLayerPairwiseCost(mrf, upLayerSize, LayerSize, activeSuperLayerNodes, superLayer, TypeUpLayerNode);

	if (GraphModel::withSuperLayer_LayerPairwise == true)
		/*set layer-superlayer pairwise*/
		AssignAcrossLayerPairwiseCost(mrf, upLayerSize, 0, activeSuperLayerNodes, superLayer, TypeLayerNode);

	float t;

	if (withInitializaiton == true)
	{
		mrf->initializeLabeling(initialLabeling);
		delete[] initialLabeling;
	}

	GCoptimization::EnergyTermType E_smooth = mrf->giveSmoothEnergy();
	GCoptimization::EnergyTermType E_data = mrf->giveDataEnergy();
	cout << "Energy = " << E_smooth + E_data << " (Smoothness,Data) " << "(" << E_smooth << "," << E_data << ")" << endl;
	initialEnergy = E_data + E_smooth;
	mrf->expansion(20);
	E_smooth = mrf->giveSmoothEnergy();
	E_data = mrf->giveDataEnergy();
	cout << "Energy = " << E_smooth + E_data << " (Smoothness,Data) " << "(" << E_smooth << "," << E_data << ")" << endl;
	finalEnergy = E_data + E_smooth;

#pragma omp parallel for
	for (int i = 0; i<activeLayerNodes.size(); i++)
	{
		int idx = activeLayerNodes.at(i);
		Layer.at(idx)->predictLabel = mrf->whatLabel(i) + 1;
		assert(Layer.at(idx)->predictLabel <= numClass);
	}
#pragma omp parallel for
	for (int i = 0; i<activeUpLayerNodes.size(); i++)
	{
		int idx = activeUpLayerNodes.at(i);
		upLayer.at(idx)->predictLabel = mrf->whatLabel(i + LayerSize) + 1;
	}
#pragma omp parallel for
	for (int i = 0; i<activeSuperLayerNodes.size(); i++)
	{
		int idx = activeSuperLayerNodes.at(i);
		superLayer.at(idx)->predictLabel = mrf->whatLabel(i + upLayerSize) + 1;
	}

	delete mrf;
	delete[] dCost;
}

GraphParameters::GraphParameters()
{

}

bool GraphParameters::operator==(const GraphParameters& T)
{
	if (!(LayerNodesWeights == T.LayerNodesWeights)) return false;
	if (!(upLayerNodesWeights == T.upLayerNodesWeights)) return false;
	if (!(superLayerNodesWeights == T.superLayerNodesWeights)) return false;

	if (!(LayerLinksWeights == T.LayerLinksWeights)) return false;
	if (!(upLayerLinksWeights == T.upLayerLinksWeights)) return false;
	if (!(superLayerLinksWeights == T.superLayerLinksWeights)) return false;

	if (!(upLayer_LayerLinksWeights == T.upLayer_LayerLinksWeights)) return false;
	if (!(superLayer_LayerLinksWeights == T.superLayer_LayerLinksWeights)) return false;
	if (!(superLayer_upLayerLinksWeights == T.superLayer_upLayerLinksWeights)) return false;

	if (!(RobustPnWeights == T.RobustPnWeights)) return false;
	if (!(constraintWeights == T.constraintWeights)) return false;
	return true;
}

GraphParameters& GraphParameters::operator=(const GraphParameters& T)
{
	if (this != &T)
	{
		LayerNodesWeights = T.LayerNodesWeights;        // class specific weights
		upLayerNodesWeights = T.upLayerNodesWeights;      // class specific weights
		superLayerNodesWeights = T.superLayerNodesWeights;   // class specific weights

		constraintWeights = T.constraintWeights;
		RobustPnWeights = T.RobustPnWeights;

		LayerLinksWeights = T.LayerLinksWeights;
		upLayerLinksWeights = T.upLayerLinksWeights;
		superLayerLinksWeights = T.superLayerLinksWeights;

		upLayer_LayerLinksWeights = T.upLayer_LayerLinksWeights;
		superLayer_LayerLinksWeights = T.superLayer_LayerLinksWeights;
		superLayer_upLayerLinksWeights = T.superLayer_upLayerLinksWeights;
	}
	return *this;
}

GraphParameters::GraphParameters(const GraphParameters& T)
{
	LayerNodesWeights = T.LayerNodesWeights;        // class specific weights
	upLayerNodesWeights = T.upLayerNodesWeights;      // class specific weights
	superLayerNodesWeights = T.superLayerNodesWeights;   // class specific weights

	constraintWeights = T.constraintWeights;
	RobustPnWeights = T.RobustPnWeights;

	LayerLinksWeights = T.LayerLinksWeights;
	upLayerLinksWeights = T.upLayerLinksWeights;
	superLayerLinksWeights = T.superLayerLinksWeights;

	upLayer_LayerLinksWeights = T.upLayer_LayerLinksWeights;
	superLayer_LayerLinksWeights = T.superLayer_LayerLinksWeights;
	superLayer_upLayerLinksWeights = T.superLayer_upLayerLinksWeights;
}

void GraphParameters::SetDefaultParameters()
{
	int numClass = TestConfiguration.numOfClass;
	/*unary potential weights */
	if (TestConfiguration.layerUnary != nonactive)
		LayerNodesWeights.SetParameter(1, numClass, 1.0, TestConfiguration.layerUnary);
	if (TestConfiguration.uplayerUnary != nonactive)
		upLayerNodesWeights.SetParameter(1, numClass, 1.0, TestConfiguration.uplayerUnary);
	if (TestConfiguration.superlayerUnary != nonactive)
		superLayerNodesWeights.SetParameter(1, numClass, 1.0, TestConfiguration.superlayerUnary);

	if (TestConfiguration.constraintlayer != nonactive)
		constraintWeights.SetParameter(1, 1, 0.0, TestConfiguration.constraintlayer);
	if (TestConfiguration.RobustPn != nonactive)
		RobustPnWeights.SetParameter(1, 1, 1.0, TestConfiguration.RobustPn);

	if (TestConfiguration.layerPariwise != nonactive)
		LayerLinksWeights.SetParameter(1, numClass, 0.1, TestConfiguration.layerPariwise);
	if (TestConfiguration.uplayerPariwise != nonactive)
		upLayerLinksWeights.SetParameter(1, numClass, 1e-6, TestConfiguration.uplayerPariwise);
	if (TestConfiguration.superlayerPariwise != nonactive)
		superLayerLinksWeights.SetParameter(1, numClass, 1e-6, TestConfiguration.superlayerPariwise);

	if (TestConfiguration.up_layer_Pariwise != nonactive)
		upLayer_LayerLinksWeights.SetParameter(1, numClass, 0.0, TestConfiguration.up_layer_Pariwise);
	if (TestConfiguration.super_layer_Pariwise != nonactive)
		superLayer_LayerLinksWeights.SetParameter(1, numClass, 0.0, TestConfiguration.super_layer_Pariwise);
	if (TestConfiguration.super_up_layerPariwise != nonactive)
		superLayer_upLayerLinksWeights.SetParameter(1, numClass, 0.0, TestConfiguration.super_up_layerPariwise);
}

void GraphParameters::GetParameters(vector<double>& weight, vector<weightType>& weightStatus)
{
	weight.clear();
	weightStatus.clear();
	LayerNodesWeights.GetParameters(weight, weightStatus);        // class specific weights
	upLayerNodesWeights.GetParameters(weight, weightStatus);      // class specific weights
	superLayerNodesWeights.GetParameters(weight, weightStatus);   // class specific weights

	LayerLinksWeights.GetParameters(weight, weightStatus);
	upLayerLinksWeights.GetParameters(weight, weightStatus);
	superLayerLinksWeights.GetParameters(weight, weightStatus);

	upLayer_LayerLinksWeights.GetParameters(weight, weightStatus);
	superLayer_LayerLinksWeights.GetParameters(weight, weightStatus);
	superLayer_upLayerLinksWeights.GetParameters(weight, weightStatus);

	constraintWeights.GetParameters(weight, weightStatus);
	RobustPnWeights.GetParameters(weight, weightStatus);
}

void GraphParameters::SetParameters(vector<double>& weight, vector<weightType>& weightStatus)
{
	int startIndex = 0;
	LayerNodesWeights.SetParameters(weight, weightStatus, startIndex);        // class specific weights
	upLayerNodesWeights.SetParameters(weight, weightStatus, startIndex);      // class specific weights
	superLayerNodesWeights.SetParameters(weight, weightStatus, startIndex);   // class specific weights

	LayerLinksWeights.SetParameters(weight, weightStatus, startIndex);
	upLayerLinksWeights.SetParameters(weight, weightStatus, startIndex);
	superLayerLinksWeights.SetParameters(weight, weightStatus, startIndex);

	upLayer_LayerLinksWeights.SetParameters(weight, weightStatus, startIndex);
	superLayer_LayerLinksWeights.SetParameters(weight, weightStatus, startIndex);
	superLayer_upLayerLinksWeights.SetParameters(weight, weightStatus, startIndex);

	constraintWeights.SetParameters(weight, weightStatus, startIndex);
	RobustPnWeights.SetParameters(weight, weightStatus, startIndex);
}

void GraphParameters::Print()
{
	if (LayerNodesWeights.GetSize()>0) LayerNodesWeights.Print("layer node weights");
	if (upLayerNodesWeights.GetSize()>0) upLayerNodesWeights.Print("uplayer node weights");
	if (superLayerNodesWeights.GetSize()>0) superLayerNodesWeights.Print("superlayer node weights");

	if (constraintWeights.GetSize()>0) constraintWeights.Print("constraint layer node weights");
	if (RobustPnWeights.GetSize()>0) RobustPnWeights.Print("RobustPn weights");
	if (LayerLinksWeights.GetSize()>0) LayerLinksWeights.Print("layer link weights");
	if (upLayerLinksWeights.GetSize()>0) upLayerLinksWeights.Print("uplayer link weights");
	if (superLayerLinksWeights.GetSize()>0) superLayerLinksWeights.Print("superlayer link weights");

	if (upLayer_LayerLinksWeights.GetSize()>0) upLayer_LayerLinksWeights.Print("upLayer_Layer link weights");
	if (superLayer_LayerLinksWeights.GetSize()>0) superLayer_LayerLinksWeights.Print("superLayer_Layer link weights");
	if (superLayer_upLayerLinksWeights.GetSize()>0) superLayer_upLayerLinksWeights.Print("superLayer_upLayer link weights");
}

void GraphParameters::WriteToFile(ofstream& out)
{
	if (LayerNodesWeights.GetSize()>0) LayerNodesWeights.WriteWeight(out, "layer node weights");
	if (upLayerNodesWeights.GetSize()>0) upLayerNodesWeights.WriteWeight(out, "uplayer node weights");
	if (superLayerNodesWeights.GetSize()>0) superLayerNodesWeights.WriteWeight(out, "superlayer node weights");
	if (constraintWeights.GetSize()>0) constraintWeights.WriteWeight(out, "constraint layer node weights");
	if (RobustPnWeights.GetSize()>0) RobustPnWeights.WriteWeight(out, "RobustPn weights");

	if (LayerLinksWeights.GetSize()>0) LayerLinksWeights.WriteWeight(out, "layer link weights");
	if (upLayerLinksWeights.GetSize()>0) upLayerLinksWeights.WriteWeight(out, "uplayer link weights");
	if (superLayerLinksWeights.GetSize()>0) superLayerLinksWeights.WriteWeight(out, "superlayer link weights");

	if (upLayer_LayerLinksWeights.GetSize()>0) upLayer_LayerLinksWeights.WriteWeight(out, "upLayer_Layer link weights");
	if (superLayer_LayerLinksWeights.GetSize()>0) superLayer_LayerLinksWeights.WriteWeight(out, "superLayer_Layer link weights");
	if (superLayer_upLayerLinksWeights.GetSize()>0) superLayer_upLayerLinksWeights.WriteWeight(out, "superLayer_upLayer link weights");
}

void GraphParameters::LoadFromFile(ifstream& in)
{
	char temp[1024 * 10];
	while (!in.eof())
	{
		in.getline(temp, 1024 * 10);
		string dataLine(temp);
		if (dataLine == "& layer node weights")
		{
			LayerNodesWeights.LoadWeight(in);
		}
		else if (dataLine == "& uplayer node weights")
		{
			upLayerNodesWeights.LoadWeight(in);
		}
		else if (dataLine == "& superlayer node weights")
		{
			superLayerNodesWeights.LoadWeight(in);
		}
		else if (dataLine == "& constraint layer node weights")
		{
			constraintWeights.LoadWeight(in);
		}
		else if (dataLine == "& layer link weights")
		{
			LayerLinksWeights.LoadWeight(in);
		}
		else if (dataLine == "& uplayer link weights")
		{
			upLayerLinksWeights.LoadWeight(in);
		}
		else if (dataLine == "& superlayer link weights")
		{
			superLayerLinksWeights.LoadWeight(in);
		}
		else if (dataLine == "& upLayer_Layer link weights")
		{
			upLayer_LayerLinksWeights.LoadWeight(in);
		}
		else if (dataLine == "& superLayer_Layer link weights")
		{
			superLayer_LayerLinksWeights.LoadWeight(in);
		}
		else if (dataLine == "& superLayer_upLayer link weights")
		{
			superLayer_upLayerLinksWeights.LoadWeight(in);
		}
		else if (dataLine == "& RobustPn weights")
		{
			RobustPnWeights.LoadWeight(in);
		}
	}
}

PotentialParameters::PotentialParameters()
{
	paraSizeRow = 0;
	paraSizeCol = 0;
}

PotentialParameters::PotentialParameters(int setParaSizeRow, int setParaSizeCol, double intialValue, weightType status)
{
	paraSizeRow = setParaSizeRow;
	paraSizeCol = setParaSizeCol;

	for (unsigned int i = 0; i<paraSizeRow; i++)
	{
		vector<double> parasRow;
		vector<weightType> parasStatusRow;
		for (int j = 0; j<paraSizeCol; j++)
		{
			parasRow.push_back(intialValue);
			parasStatusRow.push_back(status);
		}
		paras.push_back(parasRow);
		parasStatus.push_back(parasStatusRow);
	}
}

PotentialParameters& PotentialParameters::operator=(const PotentialParameters& T)
{
	paraSizeRow = T.paraSizeRow;
	paraSizeCol = T.paraSizeCol;
	paras.clear();
	parasStatus.clear();
	for (unsigned int i = 0; i<paraSizeRow; i++)
	{
		paras.push_back(T.paras.at(i));
		parasStatus.push_back(T.parasStatus.at(i));
	}

	return *this;
}

void PotentialParameters::SetParameter(int setParaSizeRow, int setParaSizeCol,
	double intialValue, weightType status)
{
	paraSizeRow = setParaSizeRow;
	paraSizeCol = setParaSizeCol;
	paras.clear();
	parasStatus.clear();
	for (unsigned int i = 0; i<paraSizeRow; i++)
	{
		vector<double> parasRow;
		vector<weightType> parasStatusRow;
		for (int j = 0; j<paraSizeCol; j++)
		{
			parasRow.push_back(intialValue);
			parasStatusRow.push_back(status);
		}
		paras.push_back(parasRow);
		parasStatus.push_back(parasStatusRow);
	}
}

void PotentialParameters::SetStatus(weightType status)
{
	for (unsigned int i = 0; i<paraSizeRow; i++)
	{
		for (int j = 0; j<paraSizeCol; j++)
		{
			parasStatus.at(i).at(j) = status;
		}
	}
}

void PotentialParameters::PrintWeight()
{
	for (unsigned int i = 0; i<paraSizeRow; i++)
	{
		for (int j = 0; j<paraSizeCol; j++)
		{
			cout << paras.at(i).at(j) << " ";
		}
		cout << endl;
	}
}

void PotentialParameters::WriteWeight(ofstream& out, string potentialType)
{
	out << "& " << potentialType << endl;
	for (unsigned int i = 0; i<paraSizeRow; i++)
	{
		for (int j = 0; j<paraSizeCol; j++)
		{
			out << paras.at(i).at(j) << " ";
		}
		out << endl;
	}
}

void PotentialParameters::LoadWeight(ifstream& in)
{
	char temp[1024 * 10];
	if (paraSizeRow*paraSizeCol>0)
	{
		for (unsigned int i = 0; i<paraSizeRow; i++)
		{
			in.getline(temp, 1024 * 10);
			string dataLine(temp);

			vector<string> textElement;
			BasicAPI::TextlineSplit(dataLine, textElement, ' ');
			for (int j = 0; j<paraSizeCol; j++)
			{
				std::istringstream stm1;
				stm1.str(textElement.at(j).c_str());
				stm1 >> paras.at(i).at(j);
			}
		}
	}
}

void PotentialParameters::PrintStatus(string potentialType)
{
	cout << potentialType << endl;
	for (unsigned int i = 0; i<paraSizeRow; i++)
	{
		for (int j = 0; j<paraSizeCol; j++)
		{
			if (parasStatus.at(i).at(j) == staticType)
				cout << "s ";
			else if (parasStatus.at(i).at(j) == active)
				cout << "a ";
			else
				cout << "n ";
		}
		cout << endl;
	}
}

void PotentialParameters::Print(string potentialType)
{
	PrintStatus(potentialType);
	PrintWeight();
}

bool PotentialParameters::operator==(const PotentialParameters& T)
{
	double eps = 1e-4;
	for (unsigned int i = 0; i<paraSizeRow; i++)
	{
		for (int j = 0; j<paraSizeCol; j++)
		{
			if (abs(paras.at(i).at(j) - T.paras.at(i).at(j))>eps)
			{
				return false;
			}
		}
	}

	return true;
}

void PotentialParameters::GetParameters(vector<double>& weight, vector<weightType>& weightStatus)
{
	for (unsigned int i = 0; i<paraSizeRow; i++)
	{
		for (int j = 0; j<paraSizeCol; j++)
		{
			weight.push_back(paras.at(i).at(j));
			weightStatus.push_back(parasStatus.at(i).at(j));
		}
	}
}

void PotentialParameters::SetParameters(vector<double>& weight, vector<weightType>& weightStatus, int& startIndex)
{
	for (unsigned int i = 0; i<paraSizeRow; i++)
	{
		for (int j = 0; j<paraSizeCol; j++)
		{
			paras.at(i).at(j) = weight.at(startIndex + i*paraSizeCol + j);
			parasStatus.at(i).at(j) = weightStatus.at(startIndex + i*paraSizeCol + j);
		}
	}

	startIndex += paraSizeRow*paraSizeCol;
}

int PotentialParameters::GetSize()
{
	return paraSizeRow*paraSizeCol;
}
