#pragma once
#include "dsift.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/cuda.hpp"

using cv::Mat;
using cv::cuda::GpuMat;

/** @brief Dense SIFT filter */
typedef struct VlDsiftFilterGPU_ : VlDsiftFilter_
{
	float *img_d;
	float *descrs_d;
	float *grads_d;          /**< gradient buffer */
	float *convTmp_d;        /**< temporary buffer */
}  VlDsiftFilterGPU;

void _vl_dsift_alloc_buffers_gpu(VlDsiftFilterGPU* self);

void vl_dsift_process_gpu(VlDsiftFilterGPU *self, float const* im);

// calculate gradients for dsift
void calculate_dsift_grads(const GpuMat & dst_d, GpuMat & grads_d, int numBinT);

// parse gradients to descriptor
void parse_dsift_convolution_result(Mat & m_dst, const Mat & m_conv, int stepX, int stepY, int binx, int biny, int bint, int numBinX, int numBinT,
	int binSizeX, int binSizeY);
void parse_dsift_convolution_result(GpuMat & dst_d, const GpuMat & conv_d, int stepX, int stepY, int binx, int biny, int bint, int numBinX, int numBinT,
	int binSizeX, int binSizeY);

// 2d filtering on gpu
void filter2D_gpu(const GpuMat & m_grads_d, const GpuMat & m_kernal_d, GpuMat & m_conv_d);

// free gpu memory
void vl_dsift_delete_gpu(VlDsiftFilterGPU *self);

