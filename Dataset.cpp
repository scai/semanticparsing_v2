#include "Dataset.h"
#include "BasicAPI.h"
#include "Global.h"
#include "GraphModel.h"
using namespace BasicAPI;

int DataSampleSet::sampleDataLength;

GeneralConfiguration::~GeneralConfiguration() {
	// TODO Auto-generated destructor stub
	//if (graphParameters != NULL){
		//delete graphParameters;
		//graphParameters = NULL;
	//}
}

void ColorCategoryTable::LoadFromFile(string colorTableFile)
{
	cout << "Loading color table..." << endl;
	colorCategorySet.clear();//vector<ColorToCategory>
	colorCategoryMapping.clear();//unordered_map<int, int>

	int filelength = 1024;
	ifstream inputFile;
	inputFile.open(colorTableFile.c_str());

	if (!inputFile)
	{
		cout << "loading failed ... " << endl;
		return;
	}

	vector< pair<int, int> > labelOriginalNewSet;

	char temp[1024];
	while (!inputFile.eof())
	{
		inputFile.getline(temp, filelength);
		string textLine(temp);
		vector<string> textElement;
		TextlineSplit(textLine, textElement, ' ');
		if (textElement.size()>4)
		{
			ColorToCategory cc;
			int classLabel = atoi(textElement.at(0).c_str());
			if (classLabel > 0)
			{
				cc.r = atoi(textElement.at(1).c_str());
				cc.g = atoi(textElement.at(2).c_str());
				cc.b = atoi(textElement.at(3).c_str());

				if (textElement.at(4).at(textElement[4].size() - 1) == '\n')
				{
					cc.className = textElement[4].substr(0, textElement.at(4).size() - 2);
				}
				else cc.className = textElement[4];

				cout << "class " << classLabel << " RGB(" << cc.r << "," << cc.g << "," << cc.b << ")" << " " << cc.className << endl;
				int rgb = cc.r*1e6 + cc.g*1e3 + cc.b;

				/*make the class label unique*/
				cc.classLabel = colorCategorySet.size() + 1;
				pair<int, int> projection(rgb, cc.classLabel);

				colorCategorySet.push_back(cc);
				colorCategoryMapping.insert(projection);
			}

			pair<int, int> p;
			p.first = classLabel;
			p.second = cc.classLabel;
			labelOriginalNewSet.push_back(p);
		}
	}
	inputFile.close();

	/*verification*/
	for (int i = 0; i<colorCategorySet.size(); i++)
	{
		assert(i == colorCategorySet[i].classLabel - 1);
		categoryGrouping.push_back(list<int>());

		int originalLabel = -1;
		for (int j = 0; j<labelOriginalNewSet.size(); j++)
		{
			if (labelOriginalNewSet[j].second == i + 1)
			{
				originalLabel = labelOriginalNewSet[j].first;
				break;
			}
		}

		if (originalLabel > 0)
		{
			for (int j = 0; j<labelOriginalNewSet.size(); j++)
			{
				if (labelOriginalNewSet[j].first == originalLabel)
				{
					categoryGrouping[i].push_back(labelOriginalNewSet[j].second);
				}
			}
			categoryGrouping[i].sort();
			categoryGrouping[i].unique();
		}
		//cout<<"label = "<<UniqueLabel(i+1)<<endl;
	}
}

//void ColorCategoryTable::LoadFromFile2(string colorTableFile)
//{
//	cout << "Loading color table..." << endl;
//	colorCategorySet.clear();//vector<ColorToCategory>
//	colorCategoryMapping.clear();//unordered_map<int, int>
//
//	int filelength = 1024;
//	ifstream inputFile;
//	inputFile.open(colorTableFile.c_str());
//
//	if (!inputFile)
//	{
//		cout << "loading failed ... " << endl;
//		return;
//	}
//
//	vector< pair<int, int> > labelOriginalNewSet;
//
//	char temp[1024];
//	while (!inputFile.eof())
//	{
//		inputFile.getline(temp, filelength);
//		string textLine(temp);
//		vector<string> textElement;
//		TextlineSplit(textLine, textElement, ' ');
//		if (textElement.size()>4)
//		{
//			ColorToCategory cc;
//			int classLabel = atoi(textElement.at(0).c_str());
//			if (classLabel > 0)
//			{
//				cc.r = atoi(textElement.at(1).c_str());
//				cc.g = atoi(textElement.at(2).c_str());
//				cc.b = atoi(textElement.at(3).c_str());
//
//				if (textElement.at(4).at(textElement[4].size() - 1) == '\n')
//				{
//					cc.className = textElement[4].substr(0, textElement.at(4).size() - 2);
//				}
//				else cc.className = textElement[4];
//
//				cout << "class " << classLabel << " RGB(" << cc.r << "," << cc.g << "," << cc.b << ")" << " " << cc.className << endl;
//				int rgb = cc.r*1e6 + cc.g*1e3 + cc.b;
//
//				/*make the class label unique*/
//				cc.classLabel = colorCategorySet.size() + 1;
//				pair<int, int> projection(rgb, cc.classLabel);
//
//				colorCategorySet.push_back(cc);
//				colorCategoryMapping.insert(projection);
//			}
//
//			pair<int, int> p;
//			p.first = classLabel;
//			p.second = cc.classLabel;
//			labelOriginalNewSet.push_back(p);
//		}
//	}
//	inputFile.close();
//
//	/*verification*/
//	for (int i = 0; i<colorCategorySet.size(); i++)
//	{
//		assert(i == colorCategorySet[i].classLabel - 1);
//		categoryGrouping.push_back(list<int>());
//
//		int originalLabel = -1;
//		for (int j = 0; j<labelOriginalNewSet.size(); j++)
//		{
//			if (labelOriginalNewSet[j].second == i + 1)
//			{
//				originalLabel = labelOriginalNewSet[j].first;
//				break;
//			}
//		}
//
//		if (originalLabel > 0)
//		{
//			for (int j = 0; j<labelOriginalNewSet.size(); j++)
//			{
//				if (labelOriginalNewSet[j].first == originalLabel)
//				{
//					categoryGrouping[i].push_back(labelOriginalNewSet[j].second);
//				}
//			}
//			categoryGrouping[i].sort();
//			categoryGrouping[i].unique();
//		}
//		//cout<<"label = "<<UniqueLabel(i+1)<<endl;
//	}
//}
//

void GeneralConfiguration::LoadColorTable()
{
	colorTable.LoadFromFile(colorTableFile);
	numOfClass = colorTable.GetNumberOfCategory();
	cout <<"numOfClass: "<< numOfClass << endl;
	confusionMatrix = cv::Mat(numOfClass, numOfClass, CV_32FC1, cv::Scalar(0));
	if (numOfClass == 2) classifierType = "multiClass";
}

//void GeneralConfiguration::LoadColorTableArray(){
//	colorTable.LoadFromFile2(colorTableFile);
//	numOfClass = colorTable.GetNumberOfCategory();
//	confusionMatrix = cv::Mat(numOfClass, numOfClass, CV_32FC1, cv::Scalar(0));
//	if (numOfClass == 2) classifierType = "multiClass";
//}

void GeneralConfiguration::LoadParameter()
{
	ifstream inputfile;
	int filelength = 1024;

	char temp[1024];
	cout << "Parameter setting: " << rootpath << endl;

	/*feature extraction parameters*/
	int featPatchSize = -1;
	int BowfeatPatchSize = -1;
	int sampleStep = -1;
	int BowsampleStep = -1;

	if (FileExistCheck(parameterFile.c_str(), inputfile))
	{
		while (!inputfile.eof())
		{
			inputfile.getline(temp, filelength);
			string textLine(temp);
			vector<string> textElement;
			TextlineSplit(textLine, textElement, ' ');
			if (textElement.size()>1)
			{
				string prefix = textElement.at(0);
				string valuestr = textElement.at(1);
				cout << prefix << " ";
				for (int j = 1; j<textElement.size(); j++)
				{
					cout << textElement.at(j) << " ";
				}
				cout << endl;

				if (prefix == "-operation")
				{
					operation = atoi(valuestr.c_str());
				}
				else if (prefix == "-dataset")
				{
					datasetName = valuestr;
				}
				else if (prefix == "-classifierName")
				{
					classifierName = valuestr;
				}
				else if (prefix == "-loadConfidenceMap")
				{
					if (atoi(valuestr.c_str()) == 1) loadConfidenceMap = true;
					else loadConfidenceMap = false;
				}
				else if (prefix == "-saveConfidenceMap")
				{
					if (atoi(valuestr.c_str()) == 1) saveConfidenceMap = true;
					else saveConfidenceMap = false;
				}
				else if (prefix == "-numofMaxTrainSamples")
				{
					numofMaxTrainSamples = atoi(valuestr.c_str());
				}
				else if (prefix == "-numofMaxClusteringSamples")
				{
					numofMaxClusteringSamples = atoi(valuestr.c_str());
				}
				else if (prefix == "-patchsize")
				{
					featPatchSize = atoi(valuestr.c_str());
				}
				else if (prefix == "-Bowpatchsize")
				{
					BowfeatPatchSize = atoi(valuestr.c_str());
				}
				else if (prefix == "-rootpath")
				{
					rootpath = valuestr;
				}
				else if (prefix == "-with3dfeature")
				{
					if (atoi(valuestr.c_str()) == 1) with3dFeature = true;
					else with3dFeature = false;
				}
				else if (prefix == "-numThreads")
				{
					numThreads = atoi(valuestr.c_str());
				}
				else if (prefix == "-localGraphTraining")
				{
					if (atoi(valuestr.c_str()) == 1) localGraphTraining = true;
					else localGraphTraining = false;
				}
				else if (prefix == "-features")
				{
					if (textElement.size() >= 2)
					{
						int numScales = atoi(textElement.at(1).c_str());
						featureFlag = textElement.at(1);

						for (int j = 2; j<textElement.size(); j++)
						{
							FeatureParameter featParameter;
							if (textElement.at(j) == "HOG"){
								featParameter.featName = HOG;
								featParameter.strFeatName = textElement.at(j);
								featParameter.numScales = numScales;
								featParameter.featDim = 32 * numScales;
							}
							if (textElement.at(j) == "SIFT"){
								featParameter.featName = FSIFT;
								featParameter.strFeatName = textElement.at(j);
								featParameter.numScales = numScales;
								featParameter.featDim = 128 * numScales;
							}
							if (textElement.at(j) == "Texton"){
								featParameter.featName = TEXTON;
								featParameter.strFeatName = textElement.at(j);
								featParameter.numScales = numScales;
								featParameter.featDim = 17 * numScales;
							}
							if (textElement.at(j) == "SparseSIFT"){
								featParameter.featName = SparseSIFT;
								featParameter.strFeatName = textElement.at(j);
								featParameter.featDim = 128;
							}
							if (textElement.at(j) == "SparseHOG"){
								featParameter.featName = SparseHOG;
								featParameter.strFeatName = textElement.at(j);
								featParameter.featDim = 32;
							}
							if (textElement.at(j) == "LBP"){
								featParameter.featName = LBP;
								featParameter.strFeatName = textElement.at(j);
								featParameter.featDim = 256;
							}
							if (textElement.at(j) == "Color"){
								featParameter.featName = Color;
								featParameter.strFeatName = textElement.at(j);
								featParameter.featDim = 9;
							}
							featParameters.push_back(featParameter);
							featureFlag = featureFlag + "_" + textElement.at(j);
						}
					}
				}
				else if (prefix == "-Bowfeatures")
				{
					if (textElement.size() >= 2)
					{
						int numScales = atoi(textElement.at(1).c_str());
						bowFeatureFlag = textElement.at(1);

						for (int j = 2; j<textElement.size(); j++)
						{
							FeatureParameter featParameter;
							if (textElement.at(j) == "HOG"){
								featParameter.featName = HOG;
								featParameter.strFeatName = textElement.at(j);
								featParameter.numScales = numScales;
								featParameter.featDim = 32 * numScales;
							}
							if (textElement.at(j) == "SIFT"){
								featParameter.featName = FSIFT;
								featParameter.strFeatName = textElement.at(j);
								featParameter.numScales = numScales;
								featParameter.featDim = 128 * numScales;
							}
							if (textElement.at(j) == "Texton"){
								featParameter.featName = TEXTON;
								featParameter.strFeatName = textElement.at(j);
								featParameter.numScales = numScales;
								featParameter.featDim = 17 * numScales;
							}
							if (textElement.at(j) == "SparseSIFT"){
								featParameter.featName = SparseSIFT;
								featParameter.strFeatName = textElement.at(j);
								featParameter.featDim = 128;
							}
							if (textElement.at(j) == "SparseHOG"){
								featParameter.featName = SparseHOG;
								featParameter.strFeatName = textElement.at(j);
								featParameter.featDim = 32;
							}
							if (textElement.at(j) == "LBP"){
								featParameter.featName = LBP;
								featParameter.strFeatName = textElement.at(j);
								featParameter.featDim = 256;
							}
							if (textElement.at(j) == "Color"){
								featParameter.featName = Color;
								featParameter.strFeatName = textElement.at(j);
								featParameter.featDim = 9;
							}
							BowFeatParameters.push_back(featParameter);
							bowFeatureFlag = bowFeatureFlag + "_" + textElement.at(j);
						}
					}
				}
				else if (prefix == "-withPixelLevelfeatures")
				{
					if (atoi(valuestr.c_str()) == 1) withPixelLevelfeatures = true;
					else withPixelLevelfeatures = false;
				}
				else if (prefix == "-withSPLevelfeatures")
				{
					if (atoi(valuestr.c_str()) == 1) withSPLevelfeatures = true;
					else withSPLevelfeatures = false;
				}
				else if (prefix == "-withImageLevelfeatures")
				{
					if (atoi(valuestr.c_str()) == 1) withImageLevelfeatures = true;
					else withImageLevelfeatures = false;
				}
				else if (prefix == "-classifierFlag")
				{
					classifierFlag = valuestr;
				}
				else if (prefix == "-classifierType")
				{
					classifierType = valuestr;
				}
				else if (prefix == "-withpositionfeature")
				{
					if (atoi(valuestr.c_str()) == 1) withPositionFeature = true;
					else withPositionFeature = false;
				}
				else if (prefix == "-samplestep")
				{
					sampleStep = atoi(valuestr.c_str());
				}
				else if (prefix == "-Bowsamplestep")
				{
					BowsampleStep = atoi(valuestr.c_str());
				}
				else if (prefix == "-testLevel")
				{
					testLevel = valuestr;
				}
				else if (prefix == "-layerUnary")
				{
					if (valuestr == "s") layerUnary = staticType;
					else if (valuestr == "a") layerUnary = active;
					else layerUnary = nonactive;
				}
				else if (prefix == "-layerPariwise")
				{
					if (valuestr == "s") layerPariwise = staticType;
					else if (valuestr == "a") layerPariwise = active;
					else layerPariwise = nonactive;
				}
				else if (prefix == "-uplayerUnary")
				{
					if (valuestr == "s") uplayerUnary = staticType;
					else if (valuestr == "a") uplayerUnary = active;
					else uplayerUnary = nonactive;
				}
				else if (prefix == "-uplayerPariwise")
				{
					if (valuestr == "s") uplayerPariwise = staticType;
					else if (valuestr == "a") uplayerPariwise = active;
					else uplayerPariwise = nonactive;
				}
				else if (prefix == "-superlayerUnary")
				{
					if (valuestr == "s") superlayerUnary = staticType;
					else if (valuestr == "a") superlayerUnary = active;
					else superlayerUnary = nonactive;
				}
				else if (prefix == "-superlayerPariwise")
				{
					if (valuestr == "s") superlayerPariwise = staticType;
					else if (valuestr == "a") superlayerPariwise = active;
					else superlayerPariwise = nonactive;
				}
				else if (prefix == "-constraintlayer")
				{
					if (valuestr == "s") constraintlayer = staticType;
					else if (valuestr == "a") constraintlayer = active;
					else constraintlayer = nonactive;
				}
				else if (prefix == "-RobustPn")
				{
					if (valuestr == "s") RobustPn = staticType;
					else if (valuestr == "a") RobustPn = active;
					else RobustPn = nonactive;
				}
				else if (prefix == "-up_layer_Pariwise")
				{
					if (valuestr == "s") up_layer_Pariwise = staticType;
					else if (valuestr == "a") up_layer_Pariwise = active;
					else up_layer_Pariwise = nonactive;
				}
				else if (prefix == "-super_up_layerPariwise")
				{
					if (valuestr == "s") super_up_layerPariwise = staticType;
					else if (valuestr == "a") super_up_layerPariwise = active;
					else super_up_layerPariwise = nonactive;
				}
				else if (prefix == "-super_layer_Pariwise")
				{
					if (valuestr == "s") super_layer_Pariwise = staticType;
					else if (valuestr == "a") super_layer_Pariwise = active;
					else super_layer_Pariwise = nonactive;
				}
				else if (prefix == "-randomInitialization")
				{
					if (atoi(valuestr.c_str()) == 1) randomInitialization = true;
					else randomInitialization = false;
				}
				else if (prefix == "-SubGradient_stepsize")
				{
					SubGradient_stepsize = atof(valuestr.c_str());
				}
				else if (prefix == "-SubGradient_C")
				{
					SubGradient_C = atof(valuestr.c_str());
				}
				else if (prefix == "-OnlinePA_C")
				{
					OnlinePA_C = atof(valuestr.c_str());
				}
				else if (prefix == "-StructSVM_C")
				{
					StructSVM_C = atof(valuestr.c_str());
				}
				else if (prefix == "-maxIterations")
				{
					maxIterations = atoi(valuestr.c_str());
				}
				else if (prefix == "-optimize_averagerecall")
				{
					if (atoi(valuestr.c_str()) == 1) optimizeAverageRecall = true;
					else optimizeAverageRecall = false;
				}
				else if (prefix == "-useShapeFilter")
				{
					if (atoi(valuestr.c_str()) == 1) useShapeFilter = true;
					else useShapeFilter = false;
				}
				else if (prefix == "-withUnlabelRegion")
				{
					if (atoi(valuestr.c_str()) == 1) withUnLabelRegion = true;
					else withUnLabelRegion = false;
				}
				else if (prefix == "-loadLearnedWeight")
				{
					if (atoi(valuestr.c_str()) == 1) loadLearnedWeight = true;
					else loadLearnedWeight = false;
				}
				else if (prefix == "-parallel")
				{
					if (atoi(valuestr.c_str()) == 1) parallel = true;
					else parallel = false;
				}
				else if (prefix == "-GraphModelTrainer")
				{
					if (valuestr == "OnlinePA")
						GraphModelTrainerType = TypeOnlinePA;
					else if (valuestr == "StructSVM")
						GraphModelTrainerType = TypeStructSVM;
					else if (valuestr == "SubGradient")
						GraphModelTrainerType = TypeSubGradient;
				}
				else if (prefix == "-scaleFactor")
				{
					imageScaleFactor = atoi(valuestr.c_str());
				}
			}
		}

		/*when need to 3D feature, check the test level*/
		if (TestConfiguration.withPositionFeature == true) featureFlag = featureFlag + "_pos";

		/*when need to use SP Level BOW feature, check the test level*/
		if (TestConfiguration.withSPLevelfeatures == true && TestConfiguration.testLevel == "pixel")
		{
			TestConfiguration.testLevel = "superpixel";
		}

		colorTableFile = filedir + datasetName + "_color_table.txt";
		clusteringTreeFile = modeldir + datasetName + "_clustering_" + featureFlag + ".model";
		cout << endl;

		validateList = filedir + datasetName + "_validatelist.txt";      // training list
		trainList = filedir + datasetName + "_trainlist.txt";         // training list
		testList = filedir + datasetName + "_testlist.txt";         // testing list
		imageLevelFeaturesFilename = filedir + datasetName + "_GIST.txt"; // GIST list

		for (unsigned int j = 0; j<featParameters.size(); j++)
		{
			featParameters.at(j).featPatchSize = featPatchSize;
			featParameters.at(j).sampleStep = sampleStep;

			if (featParameters.at(j).featName == HOG)
			{
				featParameters.at(j).featPatchSize = 8;
			}

			if (featParameters.at(j).featName == FSIFT)
			{
				featParameters.at(j).featPatchSize = 8;
			}
		}

		for (unsigned int j = 0; j<BowFeatParameters.size(); j++)
		{
			BowFeatParameters.at(j).featPatchSize = BowfeatPatchSize;
			BowFeatParameters.at(j).sampleStep = BowsampleStep;
		}

		GraphModel::initialized = false;

		//	    /*adjust the sample step to minimum 4*/
		//	    for(unsigned int i=0;i<TestConfiguration.BowFeatParameters.size();i++)
		//	    {
		//	    	if(TestConfiguration.BowFeatParameters.at(i).sampleStep < 4)
		//	    	{
		//	    		cout<<TestConfiguration.BowFeatParameters.at(i).strFeatName<<endl;
		//	    		cout<<"BOW sample step is invalid for bag-of-word feature ... ";
		//	    		cout<<"adjust from "<<TestConfiguration.BowFeatParameters.at(i).sampleStep<<" to 4";
		//	    		cout<<endl;
		//
		//	    		TestConfiguration.BowFeatParameters.at(i).sampleStep = 4;
		//	    	}
		//	    }

		if (numThreads == 1) parallel = true;

		resultdir = rootpath + dirend + "Result" + dirend;           // segmentation result
		interresultdir = rootpath + dirend + "interresult" + dirend;      // some step result
		filedir = rootpath + dirend + "File" + dirend;             // the data list file and color table file
		featurePCAdir = rootpath + dirend + "FeaturePCA" + dirend;       // feature PCA analysis for visualization
		modeldir = rootpath + dirend + "classifier" + dirend;       // clustering tree and matching model
		configuredir = rootpath + dirend + "configure" + dirend;        // training and test configuration for extreme random forest
		parameterFile = configuredir + "ParameterSetting.txt"; // the parameter file
		confidencedir = resultdir + "confidence" + dirend;

		inputfile.close();
		return;
	}
	else return;
}

void GeneralConfiguration::LoadGistFeature()
{
	ifstream inputfile;
	const int filelength = 1024 * 32;

	char temp[filelength];
	if (FileExistCheck(imageLevelFeaturesFilename.c_str(), inputfile))
	{
		while (!inputfile.eof())
		{
			inputfile.getline(temp, filelength);
			string textLine(temp);
			vector<string> textElement;
			TextlineSplit(textLine, textElement, ' ');

			if (textElement.size()>1)
			{
				pair<string, vector<float> > imageFeature;
				imageFeature.first = textElement.at(0);

				for (unsigned int j = 1; j<textElement.size(); j++)
				{
					imageFeature.second.push_back(atof(textElement.at(j).c_str()));
					//cout<<imageFeature.second.at(j-1)<<" ";
				}
				//cout<<endl;
				ImageLevelFeatures.insert(imageFeature);
			}
		}
	}
}

void GeneralConfiguration::LoadImageList(string dataList, const int IMAGETYPE)
{
	cout << dataList << endl;
	/*load all string items from the file*/

	const int filelength = 256;
	ifstream inputFile;

	inputFile.open(dataList.c_str());

	if (!inputFile)
	{
		cout << "loading failed ... " << endl;
		return;
	}

	if (IMAGETYPE == TRAINIMAGE)
	{
		cout << "loading training image list..." << endl;
	}
	else if (IMAGETYPE == TESTIMAGE)
	{
		cout << "loading testing image list..." << endl;
	}
	else if (IMAGETYPE == VALIDATEIMAGE)
	{
		cout << "loading validation image list..." << endl;
	}
	else if (IMAGETYPE == ALLIMAGE)
	{
		cout << "loading all image list..." << endl;
	}

	BasicImage Image;
	vector<BasicImage> ImageSets;

	char temp[filelength];
	while (!inputFile.eof())
	{
		inputFile.getline(temp, filelength);
		if (temp[0] != '\n')
		{
			string textLine(temp);
			vector<string> textElement;
			TextlineSplit(textLine, textElement, ' ');
			//TextlineSplitW(textLine, textElement);

			if (textElement.size() > 1)
			{
				if (textElement[0] == "image")
				{
					Image.filenameImage = textElement[1];
					Image.filenameLabelMask = "";
					Image.filenameSuperPixel = "";
					ImageSets.push_back(Image);
				}
				else if (textElement[0] == "label")
				{
					ImageSets[ImageSets.size() - 1].filenameLabelMask = textElement[1];
				}
				else if (textElement[0] == "validation")
				{
					ImageSets[ImageSets.size() - 1].filenameLabelMask = textElement[1];
				}
				else if (textElement[0] == "feature3d")
				{
					ImageSets[ImageSets.size() - 1].filenameFeature3d = textElement[1];
				}
			}
		}
	}

	inputFile.close();

	for (int i = 0; i<ImageSets.size(); i++)
	{
		if (IMAGETYPE == TRAINIMAGE)
		{
			//cout<<ImageSets.at(i).filenameImage<<endl;
			//cout<<ImageSets.at(i).filenameLabelMask.size()<<endl;
			if (ImageSets[i].filenameImage.size() > 4 && ImageSets[i].filenameLabelMask.size() > 4)
			{
				trainImages.push_back(ImageSets[i]);
			}
		}
		else if (IMAGETYPE == TESTIMAGE)
		{
			testImages.push_back(ImageSets[i]);
		}
		else if (IMAGETYPE == VALIDATEIMAGE)
		{
			if (ImageSets[i].filenameImage.size() > 4 && ImageSets[i].filenameLabelMask.size() > 4)
			{
				validateImages.push_back(ImageSets[i]);
			}
		}
		else if (IMAGETYPE == ALLIMAGE)
		{
			allImages.push_back(ImageSets[i]);
		}
	}

	cout << "Number of Images: ";
	if (IMAGETYPE == TRAINIMAGE) cout << trainImages.size() << endl;
	else if (IMAGETYPE == TESTIMAGE) cout << testImages.size() << endl;
	else if (IMAGETYPE == VALIDATEIMAGE) cout << validateImages.size() << endl;
}

void GeneralConfiguration::RegularizeMask(){
	int diff = 0;//set the threshold for color difference tolerance
	for (int k = 0; k < allImages.size(); k++){
		cv::Mat newmask = cv::imread(allImages[k].filenameLabelMask);
		//cout << allImages[k].filenameLabelMask << endl;
		//cout << newmask.type() << endl;
		int height = newmask.rows;
		int width = newmask.cols;
		if (!newmask.empty()){
			for (int i = 0; i < height; i++){
				for (int j = 0; j < width; j++){
						cv::Vec3b v = newmask.at<cv::Vec3b>(i, j);
						int r = v(2), g = v(1), b = v(0);
						bool labelclass = false;
						for (int l = 0; l < colorTable.GetNumberOfCategory(); l++){
							cv::Scalar t = colorTable.GetCategoryColor(colorTable.GetCategoryLabel(l));
							if (abs(r - t.val[2]) <= diff && abs(g - t.val[1]) <= diff && abs(b - t.val[0]) <= diff){
								newmask.at<cv::Vec3b>(i, j)[0] = t[0];
								newmask.at<cv::Vec3b>(i, j)[1] = t[1];
								newmask.at<cv::Vec3b>(i, j)[2] = t[2];
								labelclass = true;
								break;
							}
						}
						if (!labelclass)
							newmask.at<cv::Vec3b>(i, j) = cv::Vec3b::all(0);
					}
			}
			int index = allImages[k].filenameLabelMask.find(".");
			string newfilename = allImages[k].filenameLabelMask.substr(0, index) + "_copy.png";
			cout << newfilename << endl;
			imwrite(newfilename, newmask);
		}
	}
}

string GeneralConfiguration::GetClassifierName(bool with3DFlag)
{
	string typeFlag = GetFeatureTag(with3DFlag);

	string model_file_name = TestConfiguration.modeldir +
		TestConfiguration.datasetName + "_" +
		TestConfiguration.classifierName + "_" + typeFlag + "_" +
		TestConfiguration.classifierType + ".model";

	return model_file_name;
}

string GeneralConfiguration::GetFeatureTag(bool with3DFlag)
{
	string typeFlag;
	typeFlag = "2d_";
	if (with3DFlag == true) typeFlag = "2d3d_";

	if (TestConfiguration.withPixelLevelfeatures == true)
	{
		if (useShapeFilter == true) typeFlag = typeFlag + "ShapeFilter_";
		typeFlag = typeFlag + TestConfiguration.featureFlag;
	}

	if (TestConfiguration.withSPLevelfeatures == true)
	{
		typeFlag = typeFlag + "BOW_SP_";
		if (TestConfiguration.withImageLevelfeatures == true)
		{
			typeFlag = typeFlag + "IMAGE_";
		}
		typeFlag = typeFlag + TestConfiguration.bowFeatureFlag;
	}

	return typeFlag;
}

void GeneralConfiguration::SetRootPath(string newRootPath)
{
	rootpath = newRootPath;
	dirend = "/";                                                                 // system directory symbol
	resultdir = rootpath + dirend + "Result" + dirend;           // segmentation result
	interresultdir = rootpath + dirend + "interresult" + dirend;  // some step result
	filedir = rootpath + dirend + "File" + dirend;                  // the data list file and color table file
	featurePCAdir = rootpath + dirend + "FeaturePCA" + dirend;       // feature PCA analysis for visualization
	modeldir = rootpath + dirend + "classifier" + dirend;       // clustering tree and matching model
	configuredir = rootpath + dirend + "configure" + dirend;     // training and test configuration for extreme random forest
	parameterFile = configuredir + "ParameterSetting.txt";       // the parameter file
	confidencedir = resultdir + "confidence" + dirend;
}

void GeneralConfiguration::Configuration()
{
	LoadParameter();
	LoadColorTable();

	LoadImageList(trainList, TRAINIMAGE);
	LoadImageList(validateList, VALIDATEIMAGE);
	LoadImageList(testList, TESTIMAGE);

	for (unsigned int i = 0; i<trainImages.size(); i++)
	{
		allImages.push_back(trainImages[i]);
	}

	for (unsigned int i = 0; i<testImages.size(); i++)
	{
		allImages.push_back(testImages[i]);
	}

	for (unsigned int i = 0; i<validateImages.size(); i++)
	{
		allImages.push_back(validateImages[i]);
	}
}

// save test result to file
void GeneralConfiguration::SaveTestingResult(string resultfile)
{
	ofstream result;
	result.open(resultfile.c_str(), ios::app);
	result << endl;
	result << endl;
	result << datasetName << endl;
	result << "number of testing images: " << testImages.size() << endl;
	result << "average process time: " << timeCost / testImages.size() << endl;

	result << "parameter setting(classifierFlag): " << classifierFlag << endl;
	result << "parameter setting(withpositionfeature): " << withPositionFeature << endl;

	result << "parameter setting(feature): " << endl;
	for (int j = 0; j<featParameters.size(); j++)
	{
		result << featParameters.at(j).strFeatName << " ";
		result << "patchsize/" << featParameters.at(j).featPatchSize << " ";
		result << "samplestep/" << featParameters.at(j).sampleStep << " ";
		result << endl;
	}

	result << "struct learner: ";
	if (TestConfiguration.GraphModelTrainerType == TypeOnlinePA){
		result << "OnlinePA";
		result << " C = " << TestConfiguration.OnlinePA_C;
	}
	else if (TestConfiguration.GraphModelTrainerType == TypeStructSVM){
		result << "StructSVM";
		result << " C = " << TestConfiguration.StructSVM_C;
	}
	else if (TestConfiguration.GraphModelTrainerType == TypeSubGradient){
		result << "SubGradient";
		result << " C = " << TestConfiguration.SubGradient_C;
		result << " stepsize = " << TestConfiguration.SubGradient_stepsize;
	}
	result << endl;
	result << "max iterations =" << TestConfiguration.maxIterations;
	result << endl;

	result << "struct learner training type: ";
	if (TestConfiguration.localGraphTraining == true) result << "local training";
	else result << "global training";
	result << endl;

	result << "unlabel region treatment: ";
	if (TestConfiguration.withUnLabelRegion == true) result << "include unlabel region";
	else result << "disgard unlabel region";
	result << endl;

	result << "graphical model Type: ";
	GraphModel::weights.WriteToFile(result);

	result << "confusion matrix: " << endl;

	int i, j, k;
	result << setw(10) << " ";
	for (i = 0; i<numOfClass; i++)
	{
		result << setw(10) << left << colorTable.GetCategoryName(i);
	}
	result << endl;

	double average_class_correct_ratio = 0;
	k = 0;
	for (i = 0; i<numOfClass; i++)
	{
		int count = 0;
		for (j = 0; j<numOfClass; j++) count += confusionMatrix.at<float>(i, j);

		result.precision(3);
		result << setw(10) << left << colorTable.GetCategoryName(i);
		for (j = 0; j<numOfClass; j++)
		{
			if (count>0)
				result << setw(10) << left << confusionMatrix.at<float>(i, j);
			else
				result << setw(10) << left << 0;
		}
		result << endl;
		if (count>0)
		{
			average_class_correct_ratio += float(confusionMatrix.at<float>(i, i)) / count;
			k++;
		}
	}

	result << endl;
	result << "average category accuracy: " << average_class_correct_ratio / numOfClass << endl;

	int total_count = 0;
	int correct_count = 0;
	for (i = 0; i<numOfClass; i++)
	{
		for (j = 0; j<numOfClass; j++)
		{
			total_count += confusionMatrix.at<float>(i, j);
		}
		correct_count += confusionMatrix.at<float>(i, i);
	}

	float AverageCorrectRate = float(correct_count) / float(total_count);
	result << endl;
	result << "overall accuracy: " << AverageCorrectRate << endl;

	//precision-recall
	result << "precision-recall:" << endl;
	for (i = 0; i<numOfClass; i++)
	{
		result << colorTable.GetCategoryName(i) << ": ";

		int col_total_count = 0;
		for (j = 0; j<numOfClass; j++) col_total_count += confusionMatrix.at<float>(j, i);
		int row_total_count = 0;
		for (j = 0; j<numOfClass; j++) row_total_count += confusionMatrix.at<float>(i, j);

		if (row_total_count>0)
		{
			double precision = float(confusionMatrix.at<float>(i, i)) / float(col_total_count);
			result << precision << " ";
			double recall = float(confusionMatrix.at<float>(i, i)) / float(row_total_count);
			result << recall << " ";
		}
		else
		{
			result << "not exist" << endl;
		}
		result << endl;
	}

	result.close();
}

DataSample::DataSample(int setSampleIndex, int setLabel)
{
	label = setLabel;
	sampleIndex = setSampleIndex;
}

DataSample::~DataSample() {
	// TODO Auto-generated destructor stub
	//if (feat != NULL){
	//	delete feat;
	//	//feat = NULL;
	//}
}

DataSampleSet::DataSampleSet()
{
	labelMapping = NULL;
	categoryDist = NULL;
}

DataSampleSet::DataSampleSet(int setSampleDataLength,
	int setDatasetType,
	vector<int>& setLabelSet)
{
	sampleDataLength = setSampleDataLength;
	datasetType = setDatasetType;
	labelSet = setLabelSet;
	labelMapping = NULL;
	categoryDist = NULL;
}

DataSampleSet::DataSampleSet(DataSampleSet* inputDataset)
{
	sampleDataLength = inputDataset->sampleDataLength;
	datasetType = inputDataset->datasetType;
	labelSet = inputDataset->labelSet;
	labelMapping = NULL;
	categoryDist = NULL;
}

void DataSampleSet::ReleaseSample()
{
	for (int i = 0; i<samples.size(); i++)
	{
		if (samples.at(i) != NULL) delete samples.at(i);
	}
}

DataSampleSet::~DataSampleSet()
{

	// TODO Auto-generated destructor stub
	//if (categoryDist != NULL){
	//	delete[] categoryDist;
	//	categoryDist = NULL;
	//}
	//for (int i = 0; i < samples.size(); i++){
	//	if (samples[i] != NULL){
	//		delete samples[i];
	//		samples[i] = NULL;
	//	}
	//}
	//samples.clear();
	//if (labelMapping != NULL){
	//	delete labelMapping;
	//	labelMapping = NULL;
	//}

	if (labelMapping != NULL)  delete labelMapping;
	if (categoryDist != NULL) delete[] categoryDist;

	//for(int i = 0;i<samples.size();i++) delete samples.at(i);
	//samples.clear();
}

void DataSampleSet::EstimatePurity(bool update)
{
	if (categoryDist == NULL) update = true;

	if (update == true)
	{
		int numLabels = labelSet.size();
		if (categoryDist == NULL) categoryDist = new double[numLabels];
		for (int i = 0; i<numLabels; i++) categoryDist[i] = 0;

		for (int i = 0; i<samples.size(); i++)
		{
			int label = GetSample(i)->GetLabel();
			categoryDist[GetLabelIndex(label)]++;
		}

		int totalNumSamples = samples.size();

		//cout<<endl<<"sample size        : "<<samples.size()<<endl;
		//cout<<"sample distribution: ";
		for (int i = 0; i<numLabels; i++)
		{
			categoryDist[i] /= double(totalNumSamples);
			//cout<<categoryDist[i]<<" ";
		}
		//cout<<endl;
	}
}

double DataSampleSet::EstimateEntropy(bool updateDistribution)
{
	EstimatePurity(updateDistribution);
	double entropy = 0;
	for (int i = 0; i<GetNumLabels(); i++)
	{
		if (categoryDist[i]>0) entropy += (-categoryDist[i] * log2(categoryDist[i]));
	}

	return entropy;
}

bool DataSampleSet::LoadSamples(string filename)
{
	const int filelength = 1024 * 10;
	char temp[filelength];

	vector<string> datalines;
	ifstream inputfile;
	inputfile.open(filename.c_str());
	while (!inputfile.eof())
	{
		inputfile.getline(temp, filelength);
		string textLine(temp);
		if (textLine.size() >= 5)
			datalines.push_back(textLine);
	}

	int maxIndex = 0;
	list<int>      tempLabelList;
	vector<int>    labels;
	vector< vector<int> >    dataIndexSet;
	vector< vector<double> > dataSet;
	for (int i = 0; i<datalines.size(); i++)
	{
		vector<string> textElement;
		TextlineSplit(datalines.at(i), textElement, ' ');

		int label = atoi(textElement.at(0).c_str());
		tempLabelList.push_back(label);
		labels.push_back(label);

		//cout<<label<<" ";

		vector<int> dataIndexs;
		vector<double> datas;

		for (int j = 1; j<textElement.size(); j++)
		{
			vector<string> dataElement;
			TextlineSplit(textElement.at(j), dataElement, ':');
			int index = atoi(dataElement.at(0).c_str());
			double v = 0;
			std::istringstream stm;
			stm.str(dataElement.at(1).c_str());
			stm >> v;

			dataIndexs.push_back(index);
			datas.push_back(v);

			//cout<<index<<":"<<v<<" ";

			if (maxIndex < index) maxIndex = index;
		}
		//cout<<endl;

		dataIndexSet.push_back(dataIndexs);
		dataSet.push_back(datas);
	}
	tempLabelList.sort();
	tempLabelList.unique();

	labelSet.clear();
	for (list<int>::iterator iter = tempLabelList.begin(); iter != tempLabelList.end(); iter++)
	{
		labelSet.push_back(*iter);
	}

	int numSamples = datalines.size();
	sampleDataLength = maxIndex;

	for (int i = 0; i<samples.size(); i++)
	{
		if (samples.at(i) != NULL) delete samples.at(i);
	}
	//samples.clear();
	for (int i = 0; i<numSamples; i++)
	{
		int sampleLabel = labels.at(i);
		int sampleIndex = i;
		DataSample* sample = new DataSample(sampleIndex, sampleLabel);

		for (int j = 0; j<dataIndexSet.at(i).size(); j++)
		{
			int index = dataIndexSet.at(i).at(j) - 1;
			sample->SetData(index, dataSet.at(i).at(j));
		}
		samples.push_back(sample);
	}

	return true;
}

void DataSampleSet::RandomSample(DataSampleSet*& sampledDataset, double sampleRatio)
{
	sampledDataset = new DataSampleSet(sampleDataLength, GetDatasetType(), labelSet);

	vector<int> sampledSet;
	BasicAPI::GenerateRandomSequence(0, samples.size() - 1, sampledSet, int(samples.size()*sampleRatio));

	for (int i = 0; i<sampledSet.size(); i++)
	{
		sampledDataset->samples.push_back(samples.at(sampledSet.at(i)));
	}
}

void DataSampleSet::BuildAttributesRange(vector<int>& attributeIndexs, vector< pair<double, double> >& featValuesRange)
{
	featValuesRange.clear();
	for (int j = 0; j<attributeIndexs.size(); j++)
	{
		pair<double, double> attributeRange;
		featValuesRange.push_back(attributeRange);
		featValuesRange.at(j).first = 1e8;
		featValuesRange.at(j).second = -1e8;

		for (int i = 0; i<samples.size(); i++)
		{
			if (featValuesRange.at(j).first > samples.at(i)->GetData(attributeIndexs.at(j)))
			{
				featValuesRange.at(j).first = samples.at(i)->GetData(attributeIndexs.at(j));
			}

			if (featValuesRange.at(j).second < samples.at(i)->GetData(attributeIndexs.at(j)))
			{
				featValuesRange.at(j).second = samples.at(i)->GetData(attributeIndexs.at(j));
			}
		}
	}
}

void DataSampleSet::SearchActiveAttributes(vector<int>& activeAttributesIndex)
{
	int featDim = GetSampleDataLength();
	double* vmax = new double[featDim];
	double* vmin = new double[featDim];

	for (int i = 0; i<featDim; i++)
	{
		vmax[i] = -1e8;
		vmin[i] = 1e8;
	}

	for (int i = 0; i<GetNumSamples(); i++)
	{
		for (int j = 0; j<featDim; j++)
		{
			if (samples.at(i)->GetData(j) > vmax[j])
				vmax[j] = samples.at(i)->GetData(j);

			if (samples.at(i)->GetData(j) < vmin[j])
				vmin[j] = samples.at(i)->GetData(j);
		}
	}

	featValuesRange.clear();
	activeAttributesIndex.clear();
	for (int j = 0; j<featDim; j++)
	{
		if (vmax[j] - vmin[j] > 1e-6)
		{
			activeAttributesIndex.push_back(j);
		}

		pair<double, double> attributeRange;
		featValuesRange.push_back(attributeRange);
		featValuesRange.at(j).first = vmin[j];
		featValuesRange.at(j).second = vmax[j];
	}

	delete[] vmax;
	delete[] vmin;

	cout << "Number of active attributes: " << activeAttributesIndex.size() << "/" << featDim << endl;
}
