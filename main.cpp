#include "Global.h"
#include "ImageSequence.h"
#include "ClassificationModel.h"
#include "StructureParsingLearning.h"
#include "MapEdgeAnalysis.h"

int main(int argc, char *argv[])
{
	cout << "start" << endl;
	/*change basic path*/
	if (argc == 2)
	{
		TestConfiguration.SetRootPath(string(argv[1]));
	}

	TestConfiguration.Configuration();
	ImageParser RFClassifier;
	ImageSequence seq;

	switch (TestConfiguration.operation)
	{
	case 1: // 2D image features for classifier training
	{
		cout << "2d feature classifier learning..." << endl;
		RFClassifier.Train(TestConfiguration.trainImages);
		break;
	}
	case 2: // testing image segmentation, could use different features
	{
		cout << "testing classifiers for segmentation";
		cout << " using: 2d features..." << endl;
		if (TestConfiguration.testImages.size()>0) RFClassifier.Test(TestConfiguration.testImages);
		if (TestConfiguration.validateImages.size()>0) RFClassifier.Test(TestConfiguration.validateImages);
		break;
	}
	case 4:
	{
		cout << "view super pixel ..." << endl;
		seq.DebugTesting(DEBUG_VIEWSUPERPIXEL);
		break;
	}
	case 5:
	{
		cout << "view line segment detection ..." << endl;
		seq.DebugTesting(DEBUG_LINESEGMENT);
		break;
	}
	case 9:
	{
		cout << "test pattern..." << endl;
		seq.DebugTesting(DEBUG_VIEWFEATUREPCA);
		break;
	}
	case 10:
	{
		cout << "test BOW projection..." << endl;
		seq.DebugTesting(DEBUG_BOW);
		break;
	}
	case 11:
	{
		cout << "structured prediction testing..." << endl;
		seq.DebugTesting(DEBUG_STRUCT_LEARNING);
		break;
	}
	case 12:
	{
		cout << "structured prediction testing..." << endl;
		seq.DebugTesting(DEBUG_STRUCT_TESTING);
		break;
	}
	case 15:
	{
		cout << endl << "structure parsing training...with ";
		if (TestConfiguration.randomInitialization == true)
			cout << "random initialization" << endl;
		else
			cout << "normal initialization" << endl;
		StructureParsing parser;
		parser.Train(TestConfiguration.validateImages);
		break;
	}
	case 16:
	{
		cout << endl << "structure parsing testing..." << endl;
		StructureParsing parser;
		parser.Test(TestConfiguration.testImages);
		break;
	}
	case 21:{//fill the mask with all labeled colors or black color and make the similar color to mask color
		TestConfiguration.RegularizeMask();
		break;
	}
	default:
		cout << "No operation is performed" << endl;
		break;
	}
	return 0;
}
