#ifndef LBP_H
#define LBP_H
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"
#include "utils.h"
#include <iostream>
#include <string>
#include <math.h>
#include <cmath>
#include "opencv2/cudaarithm.hpp"


class LBPFeature
{
public:
	LBPFeature();
	~LBPFeature();

	//Mat computeCPU(const Mat &, int, int);
	cv::Mat computeGPU(const cv::cuda::GpuMat &, int, int);

private:
	float* _d_array;

	void your_lbp(const cv::cuda::PtrStepSz<uchar>& d_src, cv::cuda::PtrStepSz<uchar> d_dst, const size_t numRows, const size_t numCols, float* h_array);//, cudaStream_t s = gpu::StreamAccessor::getStream(gpu::Stream::Null()));
};

#endif