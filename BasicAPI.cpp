﻿#include "Global.h"
#include "BasicAPI.h"
using namespace BasicAPI;

void BasicAPI::Normalization(vector<float>& vec, string type)
{
	vector<float> vec_temp = vec;

	if (type != "L1")
	{
		float total_sum = 0;
		for (unsigned int i = 0; i<vec_temp.size(); i++)
			total_sum += (vec_temp.at(i)*vec_temp.at(i));

		if (abs(total_sum)<1e-6)
		{
			for (unsigned int i = 0; i<vec.size(); i++) vec.at(i) = 0;
		}
		else
		{
			total_sum = sqrt(total_sum);
			for (unsigned int i = 0; i<vec.size(); i++) vec.at(i) /= total_sum;
		}
	}
	else
	{
		float total_sum = 0;
		for (unsigned int i = 0; i<vec_temp.size(); i++)
			total_sum += abs(vec_temp.at(i));

		if (abs(total_sum)<1e-6)
		{
			for (unsigned int i = 0; i<vec.size(); i++) vec.at(i) = 0;
		}
		else
		{
			for (unsigned int i = 0; i<vec.size(); i++) vec.at(i) /= total_sum;
		}
	}
};

void BasicAPI::ViewMat(cv::Mat& Matrix, string SaveFileName)
{
	if (Matrix.type() != CV_32FC1 &&
		Matrix.type() != CV_64FC1 &&
		Matrix.type() != CV_32SC1)
	{
		cout << "matrix viewing with invalid matrix type ... " << endl;
		return;
	}

	IplImage* testView = cvCreateImage(cvSize(Matrix.cols, Matrix.rows), 8, 3);
	cvSetZero(testView);
	float max_value = -1e8;
	float min_value = 1e8;
	int i, j;
	for (i = 0; i<Matrix.rows; i++)
	{
		for (j = 0; j<Matrix.cols; j++)
		{
			if (Matrix.type() == CV_32FC1)
			{
				if (Matrix.at<float>(i, j)>max_value) max_value = Matrix.at<float>(i, j);
				if (Matrix.at<float>(i, j)<min_value) min_value = Matrix.at<float>(i, j);
			}
			else if (Matrix.type() == CV_64FC1)
			{
				if (Matrix.at<double>(i, j)>max_value) max_value = Matrix.at<double>(i, j);
				if (Matrix.at<double>(i, j)<min_value) min_value = Matrix.at<double>(i, j);
			}
			else if (Matrix.type() == CV_32SC1)
			{
				if (Matrix.at<int>(i, j)>max_value) max_value = Matrix.at<int>(i, j);
				if (Matrix.at<int>(i, j)<min_value) min_value = Matrix.at<int>(i, j);
			}
		}
	}

	for (i = 0; i<Matrix.rows; i++)
	{
		for (j = 0; j<Matrix.cols; j++)
		{
			float v = 0;
			if (max_value - min_value > 0)
			{
				if (Matrix.type() == CV_32FC1)
				{
					v = (Matrix.at<float>(i, j) - min_value) / (max_value - min_value);
				}
				else if (Matrix.type() == CV_64FC1)
				{
					v = (Matrix.at<double>(i, j) - min_value) / (max_value - min_value);
				}
				else if (Matrix.type() == CV_32SC1)
				{
					v = (Matrix.at<int>(i, j) - min_value) / (max_value - min_value);
				}
			}
			testView->imageData[i*testView->widthStep + j*testView->nChannels + 2] = int(v * 255);
		}
	}
	cvSaveImage(SaveFileName.c_str(), testView);
	cvReleaseImage(&testView);
}

void BasicAPI::ViewMatJetColor(cv::Mat& Matrix, string SaveFileName, cv::Mat* Mask)
{
	if (Matrix.type() != CV_32FC1 &&
		Matrix.type() != CV_64FC1 &&
		Matrix.type() != CV_32SC1)
	{
		cout << "matrix viewing with invalid matrix type ... " << endl;
		return;
	}

	if (Mask != NULL)
	{
		if (Mask->type() != CV_32SC1)
		{
			cout << "matrix viewing mask with invalid matrix type ... " << endl;
			return;
		}
	}

	IplImage* testView = cvCreateImage(cvSize(Matrix.cols, Matrix.rows), 8, 3);
	cvSetZero(testView);
	float max_value = -1e8;
	float min_value = 1e8;
	int i, j;
	for (i = 0; i<Matrix.rows; i++)
	{
		for (j = 0; j<Matrix.cols; j++)
		{
			if (Mask != NULL)
			{
				if (Mask->at<int>(i, j) > 0)
				{
					if (Matrix.type() == CV_32FC1)
					{
						if (Matrix.at<float>(i, j)>max_value) max_value = Matrix.at<float>(i, j);
						if (Matrix.at<float>(i, j)<min_value) min_value = Matrix.at<float>(i, j);
					}
					else if (Matrix.type() == CV_64FC1)
					{
						if (Matrix.at<double>(i, j)>max_value) max_value = Matrix.at<double>(i, j);
						if (Matrix.at<double>(i, j)<min_value) min_value = Matrix.at<double>(i, j);
					}
					else if (Matrix.type() == CV_32SC1)
					{
						if (Matrix.at<int>(i, j)>max_value) max_value = Matrix.at<int>(i, j);
						if (Matrix.at<int>(i, j)<min_value) min_value = Matrix.at<int>(i, j);
					}
				}
			}
			else
			{
				if (Matrix.type() == CV_32FC1)
				{
					if (Matrix.at<float>(i, j)>max_value) max_value = Matrix.at<float>(i, j);
					if (Matrix.at<float>(i, j)<min_value) min_value = Matrix.at<float>(i, j);
				}
				else if (Matrix.type() == CV_64FC1)
				{
					if (Matrix.at<double>(i, j)>max_value) max_value = Matrix.at<double>(i, j);
					if (Matrix.at<double>(i, j)<min_value) min_value = Matrix.at<double>(i, j);
				}
				else if (Matrix.type() == CV_32SC1)
				{
					if (Matrix.at<int>(i, j)>max_value) max_value = Matrix.at<int>(i, j);
					if (Matrix.at<int>(i, j)<min_value) min_value = Matrix.at<int>(i, j);
				}
			}
		}
	}

	for (i = 0; i<Matrix.rows; i++)
	{
		for (j = 0; j<Matrix.cols; j++)
		{
			float value = 0;
			if (max_value - min_value > 0)
			{
				if (Matrix.type() == CV_32FC1)
				{
					value = (Matrix.at<float>(i, j) - min_value) / (max_value - min_value);
				}
				else if (Matrix.type() == CV_64FC1)
				{
					value = (Matrix.at<double>(i, j) - min_value) / (max_value - min_value);
				}
				else if (Matrix.type() == CV_32SC1)
				{
					value = (Matrix.at<int>(i, j) - min_value) / (max_value - min_value);
				}
			}

			float r, g, b;
			if (value <= 0)
			{
				r = 0;
				g = 0;
				b = 1.0;
			}
			else if (value > 0 && value <= 0.25)
			{
				r = 0;
				g = value * 4;
				b = 1.0;
			}
			else if (value > 0.25 && value <= 0.5)
			{
				r = 0.0;
				g = 1.0;
				b = 1.0 - 4 * (value - 0.25);
			}
			else if (value > 0.5 && value <= 0.75)
			{
				r = 4 * (value - 0.5);
				g = 1.0;
				b = 0.0;
			}
			else if (value > 0.75 && value <= 1.0)
			{
				r = 1.0;
				g = 1 - 4 * (value - 0.75);
				b = 0.0;
			}
			else
			{
				r = 1.0;
				g = 0.0;
				b = 0.0;
			}

			if (Mask != NULL)
			{
				if (Mask->at<int>(i, j) <= 0)
				{
					r = g = b = 0.5;
				}
			}

			testView->imageData[i*testView->widthStep + j*testView->nChannels + 0] = int(b * 255);
			testView->imageData[i*testView->widthStep + j*testView->nChannels + 1] = int(g * 255);
			testView->imageData[i*testView->widthStep + j*testView->nChannels + 2] = int(r * 255);
		}
	}
	cvSaveImage(SaveFileName.c_str(), testView);
	cvReleaseImage(&testView);
}

void BasicAPI::SavingMatrixToFile(cv::Mat& Matrix, const char* filename)
{
	ofstream out;
	out.open(filename);
	for (unsigned int i = 0; i<Matrix.rows; i++)
	{
		for (int j = 0; j<Matrix.cols; j++)
		{
			if (Matrix.type() == CV_32SC1) out << Matrix.at<int>(i, j) << " ";
			else if (Matrix.type() == CV_32FC1) out << Matrix.at<float>(i, j) << " ";
			else if (Matrix.type() == CV_64FC1) out << Matrix.at<double>(i, j) << " ";
		}
		out << endl;
	}
	out.close();
}

bool BasicAPI::LoadMatrixFromFile(cv::Mat& Matrix, const char* filename)
{
	ifstream inputfile;
	const int filelength = 10 * 4096;
	char temp[filelength];
	if (FileExistCheck(filename, inputfile))
	{
		int index = 0;
		Matrix = cv::Scalar(0);
		vector< vector<string> > MatrixText;
		while (!inputfile.eof())
		{
			inputfile.getline(temp, filelength);
			string textLine(temp);
			if (textLine.size()>1)
			{
				vector<string> textElement;
				TextlineSplit(textLine, textElement, ' ');
				MatrixText.push_back(textElement);
			}
		}
		inputfile.close();

		if (MatrixText.size() == Matrix.rows)
		{
			for (unsigned int i = 0; i<MatrixText.size(); i++)
			{
				if (MatrixText.at(i).size() == Matrix.cols)
				{
					for (int j = 0; j<MatrixText.at(i).size(); j++)
					{
						string data = MatrixText.at(i).at(j);
						if (Matrix.type() == CV_32SC1)
						{
							Matrix.at<int>(i, j) = atoi(data.c_str());
						}
						else if (Matrix.type() == CV_32FC1)
						{
							Matrix.at<float>(i, j) = atof(data.c_str());
						}
						else if (Matrix.type() == CV_64FC1)
						{
							Matrix.at<double>(i, j) = atof(data.c_str());
						}
						else
						{
							cout << "matrix data loading error ... invalid data type" << endl;
							return false;
						}
					}
				}
				else
				{
					cout << "matrix data loading error ... inconsistent size" << endl;
					return false;
				}
			}
		}
		else
		{
			cout << "matrix data loading error ... inconsistent size" << endl;
			return false;
		}
		return true;
	}
	else return false;
}

void BasicAPI::ComputeImageGradient(IplImage* Image, cv::Mat& Gradient_x, cv::Mat& Gradient_y, bool normalization)
{
	if (Image == NULL)
	{
		cout << "invalid image input..." << endl;
		return;
	}

	assert(Gradient_x.rows == Image->height);
	assert(Gradient_x.cols == Image->width);
	assert(Gradient_y.rows == Image->height);
	assert(Gradient_y.cols == Image->width);

	IplImage* Image_Gray = cvCreateImage(cvGetSize(Image), 8, 1);
	if (Image->nChannels == 3)
	{
		cvCvtColor(Image, Image_Gray, CV_RGB2GRAY);
	}
	else cvCopy(Image, Image_Gray);

	IplImage* Image_float = cvCreateImage(cvGetSize(Image_Gray), IPL_DEPTH_32F, Image_Gray->nChannels);
	IplImage* GradientImageX = cvCreateImage(cvGetSize(Image_Gray), IPL_DEPTH_32F, Image_Gray->nChannels);
	cvSetZero(GradientImageX);
	IplImage* GradientImageY = cvCreateImage(cvGetSize(Image_Gray), IPL_DEPTH_32F, Image_Gray->nChannels);
	cvSetZero(GradientImageY);

	int step = Image_float->widthStep / sizeof(float);
	int i, j;
	for (i = 0; i<Image_Gray->height; i++)
	{
		for (j = 0; j<Image_Gray->width; j++)
		{
			((float *)Image_float->imageData)[i*step + j] =
				int(((unsigned char *)Image_Gray->imageData)[i*Image_Gray->widthStep + j]);
		}
	}

	cvSobel(Image_float, GradientImageX, 1, 0);
	cvSobel(Image_float, GradientImageY, 0, 1);

	float dx, dy;
	for (i = 0; i<Image_Gray->height; i++)
	{
		for (j = 0; j<Image_Gray->width; j++)
		{
			dx = ((float *)GradientImageX->imageData)[i*step + j];
			dy = ((float *)GradientImageY->imageData)[i*step + j];
			Gradient_x.at<float>(i, j) = abs(dx);
			Gradient_y.at<float>(i, j) = abs(dy);
		}
	}

	if (normalization == true)
	{
		float max_value_x = 0;
		float max_value_y = 0;
		for (i = 0; i<Image_Gray->height; i++)
		{
			for (j = 0; j<Image_Gray->width; j++)
			{
				if (Gradient_x.at<float>(i, j)>max_value_x)
				{
					max_value_x = Gradient_x.at<float>(i, j);
				}
				if (Gradient_y.at<float>(i, j)>max_value_y)
				{
					max_value_y = Gradient_y.at<float>(i, j);
				}
			}
		}

		for (i = 0; i<Image_Gray->height; i++)
		{
			for (j = 0; j<Image_Gray->width; j++)
			{
				Gradient_x.at<float>(i, j) /= max_value_x;
				Gradient_y.at<float>(i, j) /= max_value_y;
			}
		}
	}

	cvReleaseImage(&GradientImageX);
	cvReleaseImage(&GradientImageY);
	cvReleaseImage(&Image_float);
	cvReleaseImage(&Image_Gray);
}

void BasicAPI::ComputeImageGradient(IplImage* Image, cv::Mat& Gradient, bool normalization)
{
	if (Image == NULL)
	{
		cout << "invalid image input..." << endl;
		return;
	}

	assert(Gradient.rows == Image->height);
	assert(Gradient.cols == Image->width);

	IplImage* Image_Gray = cvCreateImage(cvGetSize(Image), 8, 1);
	if (Image->nChannels == 3)
	{
		cvCvtColor(Image, Image_Gray, CV_RGB2GRAY);
	}
	else
		cvCopy(Image, Image_Gray);

	IplImage* Image_float = cvCreateImage(cvGetSize(Image_Gray), IPL_DEPTH_32F, Image_Gray->nChannels);
	IplImage* GradientImageX = cvCreateImage(cvGetSize(Image_Gray), IPL_DEPTH_32F, Image_Gray->nChannels);
	cvSetZero(GradientImageX);
	IplImage* GradientImageY = cvCreateImage(cvGetSize(Image_Gray), IPL_DEPTH_32F, Image_Gray->nChannels);
	cvSetZero(GradientImageY);

	int step = Image_float->widthStep / sizeof(float);
	int i, j;
	for (i = 0; i<Image_Gray->height; i++)
	{
		for (j = 0; j<Image_Gray->width; j++)
		{
			((float *)Image_float->imageData)[i*step + j] =
				int(((unsigned char *)Image_Gray->imageData)[i*Image_Gray->widthStep + j]);
		}
	}

	cvSobel(Image_float, GradientImageX, 1, 0);
	cvSobel(Image_float, GradientImageY, 0, 1);

	float dx, dy;
	for (i = 0; i<Image_Gray->height; i++)
	{
		for (j = 0; j<Image_Gray->width; j++)
		{
			dx = ((float *)GradientImageX->imageData)[i*step + j];
			dy = ((float *)GradientImageY->imageData)[i*step + j];
			Gradient.at<float>(i, j) = sqrt(dx*dx + dy*dy + 1e-8);
		}
	}

	if (normalization == true)
	{
		float max_value = 0;
		for (i = 0; i<Image_Gray->height; i++)
		{
			for (j = 0; j<Image_Gray->width; j++)
			{
				if (Gradient.at<float>(i, j)>max_value)
				{
					max_value = Gradient.at<float>(i, j);
				}
			}
		}

		for (i = 0; i<Image_Gray->height; i++)
		{
			for (j = 0; j<Image_Gray->width; j++)
			{
				Gradient.at<float>(i, j) /= max_value;
			}
		}
	}

	cvReleaseImage(&GradientImageX);
	cvReleaseImage(&GradientImageY);
	cvReleaseImage(&Image_float);
	cvReleaseImage(&Image_Gray);
}

string BasicAPI::ExtractPath(string filename)
{
	string result;
	unsigned int pos = filename.rfind("/");
	if (pos == string::npos) result = "";
	else result = filename.substr(0, pos);

	return result;
}

string BasicAPI::ExtractFileExtention(std::string filename)
{
	string result = "";
	int pos = filename.rfind(".");
	if (pos != string::npos)
		result = filename.substr(pos + 1, filename.size() - 1);
	return result;
}

string BasicAPI::ExtractFileName(std::string filename, bool withExtension = true)
{
	string result;
	unsigned int pos = filename.rfind("/");
	if (pos == string::npos)
		result = filename;
	else {
		result = filename.substr(pos + 1);
	}
	if (!withExtension) {
		pos = result.rfind(".");
		if (pos != string::npos)
			result = result.substr(0, pos);
	}
	return result;
}

bool BasicAPI::FileExistCheck(const char* filename, ifstream& inputfile)
{
	bool flag = true;
	inputfile.open(filename);
	if (!inputfile)
	{
		std::cout << "Loading file failed: " << filename << endl;
		flag = false;
		inputfile.close();
	}
	return flag;
}

bool BasicAPI::FileExistCheck(const char* filename)
{
	ifstream inputfile;
	bool flag = true;
	inputfile.open(filename);
	if (!inputfile)
	{
		std::cout << "Loading file failed: " << filename << endl;
		flag = false;
	}
	inputfile.close();
	return flag;
}

string BasicAPI::NumberToString(int v)
{
	char temp[128];
	sprintf(temp, "%d", v);
	string s(temp);
	return s;
}

string BasicAPI::NumberToString(float v)
{
	char temp[128];
	sprintf(temp, "%.6f", v);

	string s(temp);
	return s;
}

string BasicAPI::NumberToString(double v)
{
	char temp[128];
	sprintf(temp, "%.8f", v);

	string s(temp);
	return s;
}

void BasicAPI::TextlineSplit(string textLine, vector<string>& textElement, char splitFlag)
{
	textElement.clear();
	if (textLine.size()>0)
	{
		int start = 0;
		int end = 0;

		if (textLine.at(0) != '\0' && textLine.at(0) != '#' && textLine.at(0) != '\n')
		{
			bool is_end = false;
			while (start<textLine.size() && is_end == false)
			{
				while (textLine.at(start) == splitFlag)
				{
					start++;
					if (start >= textLine.size())
					{
						is_end = true;
						break;
					}
				}
				end = start;
				while (textLine.at(end) != splitFlag)
				{
					end++;
					if (end >= textLine.size())
					{
						is_end = true;
						break;
					}
				}
				string data = textLine.substr(start, end - start);

				textElement.push_back(data);
				start = end + 1;
			}
		}
	}
}

//void BasicAPI::TextlineSplitW(string textLine, vector<string>& textElement)
//{
//	textElement.clear();
//	if (textLine.size()>0)
//	{
//		int pos = textLine.find(" ");
//		textElement.push_back(textLine.substr(0, pos));
//		textElement.push_back(textLine.substr(pos + 1));
//	}
//	//cout << textElement[1] << endl;
//}

void BasicAPI::AnalysisConfusionMap(cv::Mat& confusionMatrix)
{
	int i, j;
	int numclass = confusionMatrix.rows;
	cout << endl;
	cout << "global confusion matrix: " << endl;

	cout << setw(10) << " ";
	for (i = 0; i<numclass; i++)
	{
		cout << setw(10) << left << TestConfiguration.colorTable.GetCategoryName(i);
	}
	cout << endl;

	for (i = 0; i<numclass; i++)
	{
		int count = 0;
		for (j = 0; j<numclass; j++) count += confusionMatrix.at<float>(i, j);

		cout.precision(3);
		cout << setw(10) << left << TestConfiguration.colorTable.GetCategoryName(i);
		for (j = 0; j<numclass; j++)
		{
			if (count>0)
				cout << setw(10) << left << float(confusionMatrix.at<float>(i, j));
			else
				cout << setw(10) << left << 0;
		}
		cout << endl;
	}

	int total_count = 0;
	int correct_count = 0;
	for (i = 0; i<numclass; i++)
	{
		for (j = 0; j<numclass; j++)
			total_count += confusionMatrix.at<float>(i, j);
		correct_count += confusionMatrix.at<float>(i, i);
	}

	double AverageCorrectRate = float(correct_count) / float(total_count);
	cout << endl;
	cout << "overall accuracy: " << AverageCorrectRate << "(" << correct_count << "," << total_count << ")" << endl;

	//precision-recall
	cout << "recall-precision:" << endl;
	for (i = 0; i<numclass; i++)
	{
		cout << TestConfiguration.colorTable.GetCategoryName(i) << ": ";
		int row_total_count = 0;
		int col_total_count = 0;

		for (j = 0; j<numclass; j++)
		{
			row_total_count += confusionMatrix.at<float>(i, j);
			col_total_count += confusionMatrix.at<float>(j, i);
		}

		if (row_total_count>0)
		{
			double recall = float(confusionMatrix.at<float>(i, i)) / float(row_total_count);
			cout << recall << " ";
			double precision = float(confusionMatrix.at<float>(i, i)) / float(col_total_count);
			cout << precision << " ";
		}
		cout << endl;
	}
}

void BasicAPI::GenerateRandomColorSet(int numColors, vector<cv::Scalar>& colorset)
{
	int bit_per_channle = ceil(log2(4096) / 3.0);
	int bits_total = 3 * bit_per_channle;
	int color_num = 1 << bits_total;

	int k = 4096;
	while (numColors > color_num)
	{
		bit_per_channle = ceil(log2(k) / 3.0);
		bits_total = 3 * bit_per_channle;
		color_num = 1 << bits_total;
		k *= 2;
	}

	for (int c = 0; c < color_num; c++)
	{
		int r = 0;
		int g = 0;
		int b = 0;
		for (int k = 0; k < bits_total;)
		{
			b = (b << 1) + ((c >> k++) & 1);
			g = (g << 1) + ((c >> k++) & 1);
			r = (r << 1) + ((c >> k++) & 1);
		}
		r = r << (8 - bit_per_channle);
		g = g << (8 - bit_per_channle);
		b = b << (8 - bit_per_channle);

		cv::Scalar t;
		t.val[0] = b;
		t.val[1] = g;
		t.val[2] = r;
		if (b != 0 || g != 0 || r != 0)
		{
			colorset.push_back(t);
		}
	}
}

void BasicAPI::GenerateRandomSequence(int s, int e, vector<int>& seq, int length)
{
	if (s < e && length > 0)
	{
		int maxLength = e - s + 1;
		if (maxLength < length)
		{
			cout << "sequence length smaller than request sequence length... ";
			return;
		}

		seq.clear();
		if (maxLength == length)
		{
			for (int i = s; i <= e; i++) seq.push_back(i);
		}
		else
		{
			vector<int> candidates;
			for (int i = s; i <= e; i++) candidates.push_back(i);

			for (int i = 0; i<length; i++)
			{
				int idx = rand() % candidates.size();
				seq.push_back(candidates.at(idx));
				candidates.erase(candidates.begin() + idx);
			}
		}
	}
}

void BasicAPI::ExtractConnectedComponent(cv::Mat& inputImage, int markIndex, vector< vector<cv::Point> >& connectedComponents, bool checkBoundaryOnly)
{
	cv::Mat flagMask(inputImage.rows, inputImage.cols, CV_32SC1, cv::Scalar(0));
	int radius = 1;

	connectedComponents.clear();
	assert(inputImage.type() == CV_32SC1);

	for (size_t y = 0; y<inputImage.rows; y++)
	{
		for (size_t x = 0; x<inputImage.cols; x++)
		{
			int idx = inputImage.at<int>(y, x);
			if (flagMask.at<int>(y, x) == 0 && idx != markIndex)
			{
				connectedComponents.push_back(vector<cv::Point>());
				vector<cv::Point> stack;
				stack.push_back(cv::Point(x, y));
				flagMask.at<int>(y, x) = 1;
				while (stack.size() > 0)
				{
					cv::Point p = stack.back();
					stack.pop_back();
					connectedComponents.back().push_back(p);
					for (int m = -radius; m <= radius; m++)
					{
						for (int n = -radius; n <= radius; n++)
						{
							if (p.y + m >= 0 && p.y + m < inputImage.rows && p.x + n >= 0 && p.x + n < inputImage.cols &&
								flagMask.at<int>(p.y + m, p.x + n) == 0)
							{
								int newIdx = inputImage.at<int>(p.y + m, p.x + n);
								if (checkBoundaryOnly == true)
								{
									if (newIdx != markIndex)
									{
										stack.push_back(cv::Point(p.x + n, p.y + m));
										flagMask.at<int>(p.y + m, p.x + n) = 1;
									}
								}
								else
								{
									if (idx == newIdx)
									{
										stack.push_back(cv::Point(p.x + n, p.y + m));
										flagMask.at<int>(p.y + m, p.x + n) = 1;
									}
								}

								//flagMask.at<int>(p.y+m,p.x+n) = 1;
							}
						}
					}
				}
			}
		}
	}
}

void BasicAPI::ExtractConnectedComponent(IplImage* ptInputImage, cv::Scalar const& markColor, vector< vector<cv::Point> >& connectedComponents, bool checkBoundaryOnly)
{
	cv::Mat flagMask(ptInputImage->height, ptInputImage->width, CV_32SC1, cv::Scalar(0));
	int radius = 1;

	connectedComponents.clear();

	for (size_t y = 0; y<ptInputImage->height; y++)
	{
		for (size_t x = 0; x<ptInputImage->width; x++)
		{
			if (flagMask.at<int>(y, x) == 0)
			{
				cv::Scalar s = cvGet2D(ptInputImage, y, x);
				bool isMarkColor = false;
				if (checkBoundaryOnly == true)
				{
					if (ptInputImage->nChannels == 1)
					{
						isMarkColor = (int(s.val[0]) != int(markColor.val[0]));
					}
					else if (ptInputImage->nChannels == 3)
					{
						isMarkColor = (int(s.val[0]) != int(markColor.val[0]) ||
							int(s.val[1]) != int(markColor.val[1]) ||
							int(s.val[2]) != int(markColor.val[2]));
					}
				}

				if (isMarkColor == false)
				{
					connectedComponents.push_back(vector<cv::Point>());
					vector<cv::Point> stack;
					stack.push_back(cv::Point(x, y));
					flagMask.at<int>(y, x) = 1;
					while (stack.size() > 0)
					{
						cv::Point p = stack.back();
						stack.pop_back();
						connectedComponents.back().push_back(p);
						for (int m = -radius; m <= radius; m++)
						{
							for (int n = -radius; n <= radius; n++)
							{
								if (p.y + m >= 0 && p.y + m < ptInputImage->height && p.x + n >= 0 && p.x + n < ptInputImage->width &&
									flagMask.at<int>(p.y + m, p.x + n) == 0)
								{
									cv::Scalar s1 = cvGet2D(ptInputImage, p.y + m, p.x + n);
									if (checkBoundaryOnly == true)
									{
										if (int(s1.val[0]) != int(markColor.val[0]) ||
											int(s1.val[1]) != int(markColor.val[1]) ||
											int(s1.val[2]) != int(markColor.val[2]))
										{
											stack.push_back(cv::Point(p.x + n, p.y + m));
											flagMask.at<int>(p.y + m, p.x + n) = 1;
										}
									}
									else
									{
										if (int(s1.val[0]) == int(s.val[0]) &&
											int(s1.val[1]) == int(s.val[1]) &&
											int(s1.val[2]) == int(s.val[2]))
										{
											stack.push_back(cv::Point(p.x + n, p.y + m));
											flagMask.at<int>(p.y + m, p.x + n) = 1;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
