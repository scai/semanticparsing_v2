#include "feature.h"
#include "cuda_runtime.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/cudafilters.hpp"
#include "opencv2/cudaimgproc.hpp"

#include <fstream>

using namespace std;
using namespace cv;
using namespace cv::cuda;

double * PixelFeatureExtractor::_d_uu;
double * PixelFeatureExtractor::_d_vv;
double * PixelFeatureExtractor::_d_kernel;
double * PixelFeatureExtractor::_d_hist;
double * PixelFeatureExtractor::_d_res;
bool PixelFeatureExtractor::_bCudaMalloc;

__device__ double atomicAdd(double* address, double val)
{
	unsigned long long int* address_as_ull =
		(unsigned long long int*)address;
	unsigned long long int old = *address_as_ull, assumed;
	do {
		assumed = old;
		old = atomicCAS(address_as_ull, assumed,
			__double_as_longlong(val +
			__longlong_as_double(assumed)));
	} while (assumed != old);
	return __longlong_as_double(old);
}

__global__ void calHist(const PtrStepSz<uchar> bframe, const PtrStepSz<uchar> gframe, const PtrStepSz<uchar> rframe, double* hist, double* d_uu, double* d_vv, double* d_kernel, int numRows, int numCols, int sbin){
	const int c = blockIdx.x * blockDim.x + threadIdx.x;
	const int r = blockIdx.y * blockDim.y + threadIdx.y;

	if (c >= numCols - 1 || r >= numRows - 1 || c < 1 || r < 1){
		
		return;
	}
	

	double dx = bframe(r, c + 1) - bframe(r, c - 1);
	double dy = bframe(r + 1, c) - bframe(r - 1, c);
	double v = dx * dx + dy * dy;


	double dx2 = gframe(r, c + 1) - gframe(r, c - 1);
	double dy2 = gframe(r + 1, c) - gframe(r - 1, c);
	double v2 = dx2 * dx2 + dy2 * dy2;

	double dx3 = rframe(r, c + 1) - rframe(r, c - 1);
	double dy3 = rframe(r + 1, c) - rframe(r - 1, c);
	double v3 = dx3 * dx3 + dy3 * dy3;

	if (v2 > v){
		v = v2;
		dx = dx2;
		dy = dy2;
	}
	if (v3 > v){
		v = v3;
		dx = dx3;
		dy = dy3;
	}

	double best_dot = 0;
	int best_o = 17;

	for (int o = 0; o < 9; o++){
		double dot = d_uu[o] * dx + d_vv[o] * dy;
		if (dot > best_dot){
			best_dot = dot;
			best_o = o;
		}
		else if (-dot > best_dot){
			best_dot = -dot;
			best_o = o + 9;
		}
	}
	

	v = sqrt(v);
	for (int dx = -(sbin - 1); dx <= sbin - 1; dx++){
		int xdx = c + dx;
		if (sbin <= xdx && xdx < numCols - sbin){
			int adx = abs(dx);
			xdx = xdx - sbin;
			for (int dy = -(sbin - 1); dy <= sbin - 1; dy++){
				int ydy = r + dy;
				if (sbin <= ydy && ydy < numRows - sbin){
					int ady = abs(dy);
					ydy = ydy - sbin;
					//hist[17 * numRows * numCols] = 0;
					//atomicAdd((double*)hist+ xdx*(numRows-2*sbin)+(ydy-2*sbin)+best_o(r,c)*(numRows-2*sbin)*(numCols-2*sbin), (double)v * d_kernel[adx*sbin + ady]);
					atomicAdd((double*)hist + xdx*(numRows-2*sbin) + (ydy) + best_o*(numRows - 2* sbin)*(numCols-2*sbin), (double)v * d_kernel[adx*sbin + ady]);
				}
			}
		}
	}
}

__global__ void calFeature(PtrStepSz<double> norm, double* hist, int numRows, int numCols, int sbin){//numRows = block0, numCols = block1
	const int c = blockIdx.x * blockDim.x + threadIdx.x;
	const int r = blockIdx.y * blockDim.y + threadIdx.y;

	if (c >= numCols || r >= numRows || c < 0 || r < 0){
		return;
	}
	
	for (int o = 0; o < 9; o++){
		double temp1 = *(hist + o*numRows*numCols+ c*numRows + r);
		double temp2 = *(hist + (o+9)*numRows*numCols+c*numRows + r);
		norm(r, c) += (double)(temp1 + temp2)*(temp1 + temp2);
	}
}


__global__ void calRes(PtrStepSz<double> norm, double* hist, int numRows, int numCols, int sbin, double *res){//numRows = block0, numCols = block1
	const int c = blockIdx.x * blockDim.x + threadIdx.x;
	const int r = blockIdx.y * blockDim.y + threadIdx.y;

	int out0 = numRows - 2 * sbin, out1 = numCols - 2 * sbin;


	if (c >= out1 - sbin  || r >= out0 - sbin || c < 0 || r < 0){
		return;
	}

	double eps = 0.0001;
	double n4 = 1.0/sqrt(norm(r, c) + norm(r + sbin, c) + norm(r, c + sbin) + norm(r + sbin, c + sbin) + eps);
	double n3 = 1.0/sqrt(norm(r + sbin, c) + norm(r + 2 * sbin, c) + norm(r + sbin, c+sbin) + norm(r + 2 * sbin, c+sbin)+eps);
	double n1 = 1.0/sqrt(norm(r + sbin, c + sbin) + norm(r + 2 * sbin, c + sbin) + norm(r + sbin, c + 2 * sbin) + norm(r + 2 * sbin, c + 2 * sbin) + eps);
	double n2 = 1.0/sqrt(norm(r, c + sbin) + norm(r + sbin, c + sbin) + norm(r, c + 2 * sbin) + norm(r + sbin, c +2 * sbin) + eps);

	double t1 = 0;
	double t2 = 0;
	double t3 = 0;
	double t4 = 0;
	
	for (int o = 0; o < 18; o++){
		double src = *(hist + (c + sbin)*numRows + r + sbin + o * numRows * numCols);
		double h1 = min(src * n1, 0.2);
		double h2 = min(src * n2, 0.2);
		double h3 = min(src * n3, 0.2);
		double h4 = min(src * n4, 0.2);
		*(res + r + c * out0 + out0 * out1 * o) = 0.5*(h1 + h2 + h3 + h4);

		t1 += h1;
		t2 += h2;
		t3 += h3;
		t4 += h4;
	}

	for (int o = 0; o < 9; o++){
		double src = *(hist + (c + sbin)*numRows + (r + sbin)+o*numRows*numCols);
		double src2 = *(hist + (c + sbin)*numRows + (r + sbin)+(o+9)*numRows*numCols);
		double sum = src + src2;
		double h1 = min(sum*n1, 0.2);
		double h2 = min(sum*n2, 0.2);
		double h3 = min(sum*n3, 0.2);
		double h4 = min(sum*n4, 0.2);
		*(res + r + c*out0 + out0*out1*(o + 18)) = 0.5*(h1 + h2 + h3 + h4);
	}
	
	*(res + r + c*out0 + out0*out1* 27) = 0.2357*t1;
	*(res + r + c*out0 + out0*out1 * 28) = 0.2357*t2;
	*(res + r + c*out0 + out0*out1 * 29) = 0.2357*t3;
	*(res + r + c*out0 + out0*out1 * 30) = 0.2357*t4;
}

void PixelFeatureExtractor::calcHistonGPU(const PtrStepSz<uchar>& matArray0, const PtrStepSz<uchar>& matArray1, const PtrStepSz<uchar>& matArray2, int* dims, double* uu, double* vv, double* kernel, int sbin,  double* h_res){

	//int64 t0 = getTickCount();
	const dim3 blockSize(16, 16);
	dim3 gridSize((dims[1] + blockSize.x - 1) / blockSize.x, (dims[0] + blockSize.y - 1) / blockSize.y);

	if (!_bCudaMalloc)
	{
		checkCudaErrors(cudaMalloc(&_d_uu, sizeof(double) * 9));
		checkCudaErrors(cudaMalloc(&_d_vv, sizeof(double) * 9));
		checkCudaErrors(cudaMalloc(&_d_kernel, sizeof(double)*sbin*sbin));
		checkCudaErrors(cudaMalloc(&_d_hist, sizeof(double)*((dims[0] - 2 * sbin) * (dims[1] - 2 * sbin) * 18)));
		checkCudaErrors(cudaMalloc(&_d_res, sizeof(double)*((dims[0] - 4 * sbin) * (dims[1] - 4 * sbin) * 31)));
		checkCudaErrors(cudaMemcpy(_d_res, h_res, sizeof(double)*((dims[0] - 4 * sbin) * (dims[1] - 4 * sbin) * 31), cudaMemcpyHostToDevice));
		checkCudaErrors(cudaMemcpy(_d_uu, uu, sizeof(double) * 9, cudaMemcpyHostToDevice));
		checkCudaErrors(cudaMemcpy(_d_vv, vv, sizeof(double) * 9, cudaMemcpyHostToDevice));
		checkCudaErrors(cudaMemcpy(_d_kernel, kernel, sizeof(double)*sbin*sbin, cudaMemcpyHostToDevice));
		/*checkCudaErrors(cudaMemcpy(_d_hist, h_hist, sizeof(double)*((dims[0] - 2 * sbin) * (dims[1] - 2 * sbin) * 18), cudaMemcpyHostToDevice));*/
		_bCudaMalloc = true;
	}

	//cout << "cuda allocate " << (getTickCount() - t0) / getTickFrequency() * 1000 << "ms. " << endl;

	//t0 = getTickCount();
	calHist << <gridSize, blockSize >> >(matArray0, matArray1, matArray2, _d_hist, _d_uu, _d_vv, _d_kernel, dims[0], dims[1], sbin);
	cudaDeviceSynchronize();
	checkCudaErrors(cudaGetLastError());
	//cout << "calhist " << (getTickCount() - t0) / getTickFrequency() * 1000 << "ms. " << endl;

	/*checkCudaErrors(cudaMemcpy(h_hist, _d_hist, sizeof(double)*(dims[0] - 2 * sbin)*(dims[1] - 2  * sbin) * 18, cudaMemcpyDeviceToHost));
	cudaDeviceSynchronize();*/
	/*const char *err = cudaGetErrorString(cudaGetLastError());
	printf("%s\n", err);
	*/
	//t0 = getTickCount();
	GpuMat d_norm(dims[0] - 2 * sbin, dims[1] - 2 * sbin, CV_64FC1);
	calFeature<<<gridSize, blockSize>>>(d_norm, _d_hist, dims[0] - 2 * sbin, dims[1] - 2 * sbin, sbin);
	cudaDeviceSynchronize();
	checkCudaErrors(cudaGetLastError());
	//cout << "calnorm " << (getTickCount() - t0) / getTickFrequency() * 1000 << "ms. " << endl;

	//t0 = getTickCount();
	//double* d_res;
	

	calRes<<<gridSize,blockSize>>>(d_norm, _d_hist, dims[0] - 2 * sbin, dims[1] - 2 * sbin, sbin, _d_res);
	cudaDeviceSynchronize();
	checkCudaErrors(cudaGetLastError());
	//cout << "calres " << (getTickCount() - t0) / getTickFrequency() * 1000 << "ms. " << endl;


	checkCudaErrors(cudaMemcpy(h_res, _d_res, sizeof(double)*(dims[0] - 4 * sbin)*(dims[1] - 4 * sbin) * 31, cudaMemcpyDeviceToHost));
	cudaDeviceSynchronize();

	d_norm.release();
}