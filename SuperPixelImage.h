#ifndef SUPERPIXELIMAGE_H_
#define SUPERPIXELIMAGE_H_

#include <vector>
#include <iostream>
#include <string>
#include <list>

#include "Image.h"
#include "feature.h"
#include "SLIC.h"
#include "SeedsRevised.h"
#include "GCO/GCoptimization.h"

using namespace std;

struct SuperPixel
{
	int index;                               // the index of super-pixel start from 1 to max index
	int label;                               // the assigned label of super-pixel,for graph-cut
	vector< CvPoint > pixels;                // the pixels of super-pixel
	vector<int> adjIndex;                    // the index of adjacent super-pixel
	vector<float> linkWeight;

	FeatVector feature_2d;
	vector<float> confidenceValue;           // the confidence value for assign the label of this super-pixel

	int cx, cy;
	SuperPixel()
	{
		index = 0;
		label = 0;
		cx = cy = 0;
	}

	~SuperPixel()
	{
	}

	SuperPixel(const SuperPixel& s)
	{
		index = s.index;
		label = s.label;
		pixels = s.pixels;
		adjIndex = s.adjIndex;
		feature_2d = s.feature_2d;
		confidenceValue = s.confidenceValue;
		cx = s.cx;
		cy = s.cy;
	}

	void ComputeCenter()
	{
		cx = cy = 0;
		for (unsigned int j = 0; j<pixels.size(); j++)
		{
			int x = pixels.at(j).x;
			int y = pixels.at(j).y;
			cx += x;
			cy += y;
		}

		cx = float(cx) / pixels.size();
		cy = float(cy) / pixels.size();
	}
};

struct GraphLink
{
	int imageStartIndex;  // link across images
	int imageEndIndex;
	int linkStartIndex;   // link within image
	int linkEndIndex;
	float weight;         // link weight
	int id;

	GraphLink()
	{
		imageStartIndex = -1;   // link across images
		imageEndIndex = -1;
		linkStartIndex = -1;   // link within image
		linkEndIndex = -1;
		weight = 0;    // link weight
		id = -1;
	}

	GraphLink(const GraphLink& g)
	{
		imageStartIndex = g.imageStartIndex;  // link across images
		imageEndIndex = g.imageEndIndex;
		linkStartIndex = g.linkStartIndex;   // link within image
		linkEndIndex = g.linkEndIndex;
		weight = g.weight;           // link weight
		id = g.id;
	}
};

class SuperPixelImage : public PixelImage
{
public:

	SuperPixelImage();
	SuperPixelImage(IplImage* Image) :PixelImage(Image)
	{
		maxIndex = 0;
		numClassLabel = 0;
		numSuperPixel = 0;
		superPixelMap = NULL;
		activeMask = NULL;
	}

	SuperPixelImage(string filenameInput) :PixelImage(filenameInput)
	{
		maxIndex = 0;
		numClassLabel = 0;
		numSuperPixel = 0;
		superPixelMap = NULL;
		activeMask = NULL;
	}
	virtual ~SuperPixelImage();

public:

	// Initialization of super pixel matrix
	bool Initialization(cv::Mat* superpixelMat);
	bool Initialization(BasicImage* Image, string testLevel = "pixel");

	void PixelMRFInference(cv::Mat& probability, cv::Mat& probability_index);
	void PnModelMRFInference(cv::Mat& probability, cv::Mat& probability_index);
	bool SegmentMRFinference();
	void MRFParsing(vector<int>& label, bool saveResult = true);

	void LabelSuperPixel();
	void SaveConfidenceMapSP();  // confidence map with basic unit : super pixel
	bool LoadConfidenceMapSP();  // confidence map with basic unit : super pixel
	bool LoadSuperPixels();

	/*if extractType = train, then only features with label are extracted from images*/
	void FeatureExtraction(VlDsiftFilterGPU* SIFTFilter = NULL, string extractType = "train");
	void CleanLabelMaskBoundary(IplImage* mask);

	void ConfidenceEstimation(cv::Mat& probability, cv::Mat& probability_index);

	/*release memory*/
	void ReleaseContent();
	void SetActivemask();

	/*post processing of segmentation result*/
	int  SegmentationPostProcessing(vector<int>& sizeConstraint);
	void SegmentationEdgeRefine();
	void ShrinkEdge(cv::Mat& segmentMap, int bandW);
	void RefineEdge(cv::Mat& imageInput, cv::Mat& segmentMap);

	/*debug*/
	void ViewSegmentConfidenceMap();
	void ViewSuperPixel();

public:
	cv::Mat* superPixelMap;                  // super pixel map
	cv::Mat* activeMask;                     // active region for testing and training

	vector<cv::Mat*> priorMaps;              // the prior map of different categories

	int maxIndex;                            // max index of super pixel
	int numSuperPixel;                       // number of super pixels
	int numClassLabel;                       // number of class label

	vector<SuperPixel>   superPixels;        // super pixel unit
	vector<GraphLink>    superPixelLink;     // link between super pixel nodes in the graph
};

#endif /* SUPERPIXELIMAGE_H_ */
