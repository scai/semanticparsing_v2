#include "Global.h"
GeneralConfiguration TestConfiguration;

int UNASSIGNED = -1;
//int SKY            = 0;
//int TREE           = 1;
//int BUILDING       = 2;
//int GROUND         = 3;
//int VEHICLE        = 4;
//int OTHERS         = 5;
//int NONGROUND      = 6;
int UNCLUSTERING = -1;
//int PLANEPOINT     = 7;
//int NOISEPOINT     = 8;

const int TRAINIMAGE = 1;
const int TESTIMAGE = 2;
const int VALIDATEIMAGE = 3;
const int ALLIMAGE = 4;

const int DEBUG_VIEWSUPERPIXEL = 1;
const int DEBUG_VIEWFEATUREPCA = 2;
const int DEBUG_VIEW3DPROJECTION = 3;
const int DEBUG_VIEW3DFEATURE = 4;
const int DEBUG_LINESEGMENT = 5;
const int DEBUG_PATTERN = 6;
const int DEBUG_BOW = 7;;
const int DEBUG_STRUCT_LEARNING = 8;
const int DEBUG_STRUCT_TESTING = 9;

const int TypeLayerNode = 0;
const int TypeUpLayerNode = 1;
const int TypeSuperLayerNode = 2;

double UnitLossCost = 0.1;

void LMath::SetSeed(unsigned int seed)
{
	srand(seed);
}

unsigned int LMath::RandomInt()
{
	return(rand());
}

unsigned int LMath::RandomInt(unsigned int maxval)
{
	return(rand() % maxval);
}

unsigned int LMath::RandomInt(unsigned int minval, unsigned int maxval)
{
	return(minval + RandomInt(maxval - minval));
}

double LMath::RandomReal()
{
	return(rand() / (double)RAND_MAX);
}

double LMath::RandomGaussian(double mi, double var)
{
	double sum = 0;
	int count = 100;
	for (unsigned int i = 0; i < count; i++) sum += RandomReal();
	return(mi + sqrt(3 * var / count) * (2 * sum - count));
}

double LMath::SquareEuclidianDistance(double *v1, double *v2, int size)
{
	double dist = 0;
	for (unsigned int i = 0; i < size; i++) dist += (v1[i] - v2[i]) * (v1[i] - v2[i]);
	return(dist);
}

double LMath::KLDivergence(double *histogram, double *referenceHistogram, int size, double threshold)
{
	int i;
	double sum1 = (double)0, sum2 = (double)0;
	for (i = 0; i < size; i++) sum1 += histogram[i], sum2 += referenceHistogram[i];
	if (sum1 < almostZero) sum1 = (double)1.0;
	if (sum2 < almostZero) sum2 = (double)1.0;

	double value = (double)0;
	for (i = 0; i < size; i++) value += (threshold + ((double)1.0 - threshold) * histogram[i] / sum1) * log((threshold + ((double)1.0 - threshold) * histogram[i] / sum1) / (threshold + ((double)1.0 - threshold) * referenceHistogram[i] / sum2));
	return(value);
}

double LMath::GetAngle(double x, double y)
{
	double r = sqrt(x * x + y * y);
	double angle = (r < almostZero) ? 0 : acos(x / r);
	if (y < 0) angle = 2 * pi - angle;
	return(angle);
}
