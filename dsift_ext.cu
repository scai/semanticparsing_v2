#include "dsift_ext.h"
#include "opencv2\core\cuda.hpp"
#include "mathop.h"
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>

using namespace std;
using namespace cv;
using namespace cv::cuda;

__device__ void fast_atan2_f(float y, float x, float *ang)
{
	float angle, r;
	float const c3 = 0.1821F;
	float const c1 = 0.9675F;
	float abs_y = fabsf(y) + VL_EPSILON_F;

	if (x >= 0) {
		r = (x - abs_y) / (x + abs_y);
		angle = (float)(VL_PI / 4);
	}
	else {
		r = (x + abs_y) / (abs_y - x);
		angle = (float)(3 * VL_PI / 4);
	}
	angle += (c3*r*r - c1) * r;

	*ang = (y < 0) ? -angle : angle;
}

__global__ void calculate_dsift_grads_(const PtrStepSz<float> src, PtrStepSz<float> dst, int numBinT)
{
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;
	if (x < 0 || y < 0 || x > src.cols - 1 || y > src.rows - 1)
		return;

	float gx, gy;
	float angle, mod, nt, rbint;
	int bint;

	/* y derivative */
	if (y == 0) {
		gy = src(y + 1, x) - src(y, x);
	}
	else if (y == src.rows - 1) {
		gy = src(y, x) - src(y - 1, x);
	}
	else {
		gy = 0.5f * (src(y + 1, x) - src(y - 1, x));
	}

	/* x derivative */
	if (x == 0) {
		gx = src(y, x + 1) - src(y, x);
	}
	else if (x == src.cols - 1) {
		gx = src(y, x) - src(y, x - 1);
	}
	else {
		gx = 0.5F * (src(y, x + 1) - src(y, x - 1));
	}

	/* angle and modulus */
	fast_atan2_f(gy, gx, &angle);
	mod = sqrtf(gx*gx + gy*gy);

	/* quantize angle */
	// vl_mod_2pi_f
	while (angle > (float)(2 * VL_PI)) angle -= (float)(2 * VL_PI);
	while (angle < 0.0F) angle += (float)(2 * VL_PI);

	nt = angle * (numBinT / (2 * VL_PI));
	bint = (int)floorf(nt);
	rbint = nt - bint;

	/* write it back */
	//grads[(bint) % numBinT].ptr<float>(y)[x] = (1 - rbint) * mod;
	//grads[(bint + 1) % self->geom.numBinT].ptr<float>(y)[x] = (rbint)* mod;

	*(dst + ((bint) % numBinT) * src.cols * src.rows + y * src.cols + x) = (1 - rbint) * mod;
	*(dst + ((bint + 1) % numBinT) * src.cols * src.rows + y * src.cols + x) = (rbint)* mod;
}

void calculate_dsift_grads(const GpuMat & dst_d, GpuMat & grads_d, int numBinT)
{
	dim3 block(32, 16);
	dim3 grid(dst_d.cols / block.x + 1, dst_d.rows / block.y + 1);
	calculate_dsift_grads_ << <grid, block >> >(dst_d, grads_d, numBinT);
	cudaDeviceSynchronize();
}

__global__ void parse_gpumat_1_dimension(PtrStepSz<float> dst, PtrStepSz<float> src, int bin_idx, int bin_size, int stepX, int stepY, int x_shift, int y_shift)
{
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;
	if (x < 0 || y < 0 || x > dst.cols - 1 || y > dst.rows - 1)
		return;

	float *p_dst = dst + y * dst.cols * bin_size + bin_size * x + bin_idx;

	int src_x = x * stepX, src_y = y * stepY;
	*p_dst = src(src_y + y_shift, src_x + x_shift);
}

void parse_dsift_convolution_result(Mat & m_dst, const Mat & m_conv, int stepX, int stepY, int binx, int biny, int bint, int numBinX, int numBinT,
	int binSizeX, int binSizeY)
{
	int bin_idx = bint + binx * numBinT + biny * (numBinX * numBinT);
	int x_shift = binx * binSizeX, y_shift = biny * binSizeY;

	// cpu version
	for (int dsty = 0; dsty < m_dst.rows; dsty++) {
		for (int dstx = 0; dstx < m_dst.cols; dstx++) {
			int framex = dstx * stepX;
			int framey = dsty * stepY;

			m_dst.ptr<Vec<float, 128>>(dsty)[dstx][bin_idx]
				= m_conv.ptr<float>(framey + y_shift)[framex + x_shift];

		}
	}
}

void parse_dsift_convolution_result(GpuMat & dst_d, const GpuMat & conv_d, int stepX, int stepY, int binx, int biny, int bint, int numBinX, int numBinT,
	int binSizeX, int binSizeY)
{
	int bin_idx = bint + binx * numBinT + biny * (numBinX * numBinT);
	int x_shift = binx * binSizeX, y_shift = biny * binSizeY;

	// cuda version
	dim3 block(32, 16);
	dim3 grid(dst_d.cols / block.x + 1, dst_d.rows / block.y + 1);
	parse_gpumat_1_dimension << <grid, block >> >(dst_d, conv_d, bin_idx, 128, stepX, stepY, x_shift, y_shift);
	cudaDeviceSynchronize();

	cudaError err = cudaGetLastError();
	if (cudaSuccess != err) {
		printf("%s\n", err);
	}
}

__global__ void filter2D_(const PtrStepSz<float> src, const PtrStepSz<float> kernal, PtrStepSz<float> dst, int anchor_x, int anchor_y, int border_type)
{
	int c = blockIdx.x * blockDim.x + threadIdx.x;
	int r = blockIdx.y * blockDim.y + threadIdx.y;
	if (c < 0 || r < 0 || c > src.cols - 1 || r > src.rows - 1)
		return;

	int src_x, src_y;
	float src_v, sum = 0;
	for (int x = 0; x < kernal.cols; x++) {
		for (int y = 0; y < kernal.rows; y++) {
			src_x = c + x - anchor_x;
			src_y = r + y - anchor_y;

			if (border_type == BORDER_REPLICATE) {
				if (src_x < 0)
					src_x = 0;
				else if (src_x > src.cols - 1)
					src_x = src.cols - 1;

				if (src_y < 0)
					src_y = 0;
				else if (src_y > src.rows - 1)
					src_y = src.rows - 1;

				src_v = src(src_y, src_x);
			}

			sum += src_v * kernal(y, x);
		}
	}

	dst(r, c) = sum;
}

void filter2D_gpu(const GpuMat & m_grads_d, const GpuMat & m_kernal_d, GpuMat & m_conv_d)
{
	if (m_conv_d.cols != m_grads_d.cols || m_conv_d.rows != m_grads_d.rows || m_conv_d.depth() != m_grads_d.depth())
		m_conv_d = m_grads_d.clone();

	int anchor_x = m_kernal_d.cols / 2;
	int anchor_y = m_kernal_d.rows / 2;

	int border_type = BORDER_REPLICATE;

	dim3 block(32, 32);
	dim3 grid(m_grads_d.cols / block.x + 1, m_grads_d.rows / block.y + 1);
	filter2D_ << <grid, block >> >(m_grads_d, m_kernal_d, m_conv_d, anchor_x, anchor_y, border_type);
	cudaDeviceSynchronize();

	cudaError err = cudaGetLastError();
	if (cudaSuccess != err) {
		printf("%s\n", err);
	}
}

