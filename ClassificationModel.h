#ifndef CLASSIFICATIONMODEL_H_
#define CLASSIFICATIONMODEL_H_

/*struct definition*/
#include "Global.h"
/*feature extraction from image API*/
#include "feature.h"
/*opencv library*/
#include "cv.h"
#include "cxcore.h"
#include "highgui.h"

/*graph model module*/
#include "GCO/GCoptimization.h"
#include "Image.h"

#include <cmath>
#include <vector>
#include "SuperPixelImage.h"
#include "RandomForest.h"
#include "TreeDetection.h"

#include "opencv2/cudaarithm.hpp"
#include "opencv2/cudafilters.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/opencv.hpp"
#include "dsift_ext.h"

using namespace std;

class DataExtractor
{
public:
	DataExtractor();
	virtual ~DataExtractor();

public:

	/*training random forest with training feature vectors*/
	//virtual void Train(vector<BasicImage>& TrainImages);

	/*training:
	* ImageObjectis the struct for image organization
	* Feature is the struct for extracted features*/
	void FeatureExtractionTraining(vector<FeatVector>& SampleFeatureSet,
		vector<BasicImage>& TrainImages);

	/*sample feature vectors for classifier training*/
	void FeatureExtractionTraining(vector<FeatVector>& sampleFeatureSet,
		SuperPixelImage* trainImage,
		int* trainSampleStat,
		int numFeatures);

	/* extract the valid feature set for the image*/
	/* set withLabel for training*/
	/* set isPixelFeature for pixel level feature or
	* super pixel level feature*/
	void ExtractFeatureSamples(SuperPixelImage* testImage,
		vector<int>& SampleFeatureSet,
		bool isPixelFeature = true,
		bool withLabel = true);

	/*API for image sequence testing*/
	//virtual void Test(vector<BasicImage>& TestImages);
};

/*Random forest API for Image Parsing*/
class ImageParser
{
public:
	ImageParser();
	virtual ~ImageParser();

	/*training random forest with training feature vectors*/
	void Train(vector<BasicImage>& TrainImages);

	/*API for image sequence testing*/
	void Test(vector<BasicImage>& TestImages);

	void TrainClassifier(vector<FeatVector>& sampleFeatureSet);

	void TestBasicClassifiers(BasicImage& Image, cv::Mat& confusionMatrix);

	/*for pixel level feature, the estimated prob is stored in probability & probability_index*/
	/*format: probability row [p1,p2,...,pN]
	* probability_index row <x,y>*/
	/*for super pixel level feature, the estimated prob is stored in the confidence of superpixel*/
	void TestBasicClassifiers(SuperPixelImage* testImage, cv::Mat*& probability, cv::Mat*& probability_index);

	/*testing initialization: load classifier and related information*/
	void TestInitialization();

	bool LoadRandomForest(RandomForest*& RTrees, string modelFileName);

	void UpdateGraphModel(BasicImage& TestImages);

public:
	RandomForest* pixelRTrees;
	RandomForest* superpixelRTrees;

	DataExtractor extractor;
	string RTFileName;

	bool classifierLoaded; /*classifier loaded or not*/
};
#endif /* CLASSIFICATIONMODEL_H_ */
