#include "feature.h"
#include <iostream>
#include "Global.h"
#include "BasicAPI.h"
#include <windows.h>

using namespace TextonFeature;
using namespace BasicAPI;
#define kernelSize(sigma) (2*(int)(sigma * 3) + 1)

#define COMPUTE_GPU
#include <sdkddkver.h>
#include <Windows.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/nonfree/gpu.hpp>
//#include <opencv2/gpu/gpu.hpp>
//#include <opencv2/gpu/stream_accessor.hpp>


//VlDsiftFilterGPU* PixelFeatureExtractor::SIFTFilter;

void NormalizeVec(vector<float>& vec)
{
	int i;
	float vecsum = 0;
	for (i = 0; i<vec.size(); i++)
	{
		vecsum += (vec.at(i)*vec.at(i));
	}

	vecsum = sqrt(vecsum + 1e-8);

	for (i = 0; i<vec.size(); i++)
	{
		if (vecsum>1e-4)
			vec.at(i) = vec.at(i) / vecsum;
		else
			vec.at(i) = 0;
		cout << vec.at(i) << " ";
	}
	cout << endl;
}

FeatVector::FeatVector()
{
	labelClass = -1;                 // class label
	x = -1;
	y = -1;
	spLevelFeat = NULL;
	imageLevelFeat = NULL;
}



FeatVector::FeatVector(const FeatVector& feav)
{
	labelClass = feav.GetLabel();
	feav.GetExtraData(x, y);

	for (int i = 0; i<feav.GetFeatDim(); i++)
		InsertNewAttribute(feav.GetAttributeValue(i));

	spLevelFeat = feav.spLevelFeat;
	imageLevelFeat = feav.imageLevelFeat;
}

FeatVector& FeatVector::operator=(const FeatVector& feav)
{
	if (this != &feav)
	{
		labelClass = feav.GetLabel();
		feav.GetExtraData(x, y);

		for (int i = 0; i<feav.GetFeatDim(); i++)
			InsertNewAttribute(feav.GetAttributeValue(i));

		spLevelFeat = feav.spLevelFeat;
		imageLevelFeat = feav.imageLevelFeat;
	}
	return *this;
}

void PixelFeatureExtractor::Initialization(vector< FeatVector* >& features, IplImage* Image)
{
	int i, j;
	for (i = 0; i<Image->height; i++)
	{
		for (j = 0; j<Image->width; j++)
		{
			features.push_back(NULL);
		}
	}
}

void PixelFeatureExtractor::Extraction(VlDsiftFilterGPU* SIFTFilter, vector< FeatVector* >& features, IplImage* Image, IplImage* LabelImage)
{
	cv::Mat samplingMask; /*position for sampling features*/

	features.clear();
	if (featParameter[0].featName == FSIFT)
	{
		ExtractionSIFT(features, Image, featParameter[0], samplingMask, LabelImage, SIFTFilter);
	}
	else if (featParameter[0].featName == TEXTON)
	{
		ExtractionTexton(features, Image, featParameter[0], samplingMask, LabelImage);
	}
	else if (featParameter[0].featName == HOG)
	{
		ExtractionHOG(features, Image, featParameter[0], samplingMask, LabelImage);
	}
	else if (featParameter[0].featName == SparseHOG)
	{
		ExtractionSparseHOG(features, Image, featParameter[0], samplingMask, LabelImage);
	}
	else if (featParameter[0].featName == LBP)
	{
		ExtractionLBP(features, Image, featParameter[0], samplingMask, LabelImage);
	}
	else if (featParameter[0].featName == Color)
	{
		ExtractionColor(features, Image, featParameter[0], samplingMask, LabelImage);
	}
	else
	{
		cout << "feature choice is not correct..." << endl;
	}

	for (unsigned int i = 1; i<featParameter.size(); i++)
	{
		int tempSampleStep = featParameter[i].sampleStep;
		featParameter[i].sampleStep = 1;

		vector< FeatVector* > tempFeatures;
		if (featParameter[i].featName == FSIFT)
		{
			ExtractionSIFT(tempFeatures, Image, featParameter[i], samplingMask, LabelImage);
		}
		else if (featParameter[i].featName == TEXTON)
		{
			ExtractionTexton(tempFeatures, Image, featParameter[i], samplingMask, LabelImage);
		}
		else if (featParameter[i].featName == HOG)
		{
			ExtractionHOG(tempFeatures, Image, featParameter[i], samplingMask, LabelImage);
		}
		else if (featParameter[i].featName == SparseHOG)
		{
			ExtractionSparseHOG(tempFeatures, Image, featParameter[i], samplingMask, LabelImage);
		}
		else if (featParameter[i].featName == LBP)
		{
			int64 t0 = cv::getTickCount();
			ExtractionLBP(tempFeatures, Image, featParameter[i], samplingMask, LabelImage);
			cout << "The time for extracting LBP: " << (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000 << " ms. "<< endl;
		}
		else if (featParameter[i].featName == Color)
		{
			ExtractionColor(tempFeatures, Image, featParameter[i], samplingMask, LabelImage);
		}
		else
		{
			cout << "feature choice is not correct..." << endl;
		}
		featParameter[i].sampleStep = tempSampleStep;

		for (int y = 0; y<Image->height; y++)
		{
			for (int x = 0; x<Image->width; x++)
			{
				int idx = y*Image->width + x;
				if (features[idx] != NULL && tempFeatures[idx] != NULL)
				{
					if (features[idx]->GetFeatDim()>0 && tempFeatures[idx]->GetFeatDim()>0)
					{
						for (int j = 0; j<tempFeatures[idx]->GetFeatDim(); j++)
						{
							features[idx]->InsertNewAttribute(tempFeatures[idx]->GetAttributeValue(j));
						}
					}
					else
					{
						features[idx]->ClearData();
						delete features[idx];
						features[idx] = NULL;
					}
				}
				else
				{
					if (features[idx] != NULL)
					{
						features[idx]->ClearData();
						delete features[idx];
						features[idx] = NULL;
					}
				}
			}
		}

		for (unsigned int i = 0; i<tempFeatures.size(); ++i)
		{
			if (tempFeatures[i] != NULL){ 
				delete tempFeatures[i]; 
				//tempFeatures[i] = NULL;
			}
		}
		tempFeatures.clear();
	}
}

int PixelFeatureExtractor::GetLabel(int x, int y, IplImage* LabelImage)
{
	int label = -1;
	if (LabelImage != NULL)
	{
		int r = ((unsigned char *)LabelImage->imageData)[y*LabelImage->widthStep + x*LabelImage->nChannels + 2];
		int g = ((unsigned char *)LabelImage->imageData)[y*LabelImage->widthStep + x*LabelImage->nChannels + 1];
		int b = ((unsigned char *)LabelImage->imageData)[y*LabelImage->widthStep + x*LabelImage->nChannels];
		label = TestConfiguration.colorTable.FindLabel(r, g, b);
	}

	return label;
}

void PixelFeatureExtractor::ExtractionTextonGPU(vector< FeatVector* >& features, IplImage* Image, cv::Mat& samplingMask, int sampleStep, IplImage* LabelImage)
{
	cv::Mat image = cv::cvarrToMat(Image);
	CV_Assert(!image.empty() && image.channels() == 3);
	int n = kernelSize(1.0);
	cv::GaussianBlur(image, image, cv::Size(n, n), 1);

	//cout << image.type() << " " << CV_8UC3 << endl;

	cv::cuda::GpuMat srcGPU(image);
	cv::cuda::GpuMat grayGPU;
	cv::cuda::cvtColor(srcGPU, grayGPU, CV_BGR2GRAY);

	cv::cuda::GpuMat cieGPU;
	cv::cuda::cvtColor(srcGPU, cieGPU, CV_BGR2Lab);

	int texton_dim = 17;
	vector<cv::cuda::GpuMat> response(texton_dim);
	double _kappa = 1.0;
	cv::Size ksize(n, n);
	int k = 0;

	for (double sigma = 1.0; sigma <= 4.0; sigma *= 2.0){
		cv::cuda::GpuMat temp(cieGPU.size(), CV_32FC1);
		//cv::cuda::GaussianBlur(cieGPU, temp, cv::Size(2 * (int)(_kappa*sigma) + 1, 2 * (int)(_kappa*sigma) + 1), sigma);
		cv::Ptr<cv::cuda::Filter> filter = cv::cuda::createGaussianFilter(cieGPU.type(), temp.type(), cv::Size(2 * (int)(_kappa * sigma) + 1, 2 * (int)(_kappa * sigma) + 1), 0);
		filter->apply(cieGPU, temp);

		vector<cv::cuda::GpuMat> matArray;
		cv::cuda::split(temp, matArray);
		for (int c = 0; c < 3; c++){
			response[k] = matArray[c];
			k++;
		}
	}

	for (double sigma = 2.0; sigma <= 4.0; sigma *= 2.0){
		//cv::cuda::Sobel(grayGPU, response[k], grayGPU.type(), 1, 0, 1);///////////////////////////////////////////////////////////////////////////////////
		cv::Ptr<cv::cuda::Filter> filterS = cv::cuda::createSobelFilter(grayGPU.type(), response[k].type(), 1, 0);
		filterS->apply(grayGPU, response[k]);
		//cv::cuda::GaussianBlur(response[k], response[k], cv::Size(2 * (int)(_kappa * sigma) + 1, 2 * (int)(_kappa * sigma) + 1), sigma);
		cv::Ptr<cv::cuda::Filter> filter = cv::cuda::createGaussianFilter(response[k].type(), response[k].type(), cv::Size(2 * (int)(_kappa * sigma) + 1, 2 * (int)(3.0 * _kappa * sigma) + 1), 0);
		filter->apply(response[k], response[k]);
		k++;

		//cv::cuda::Sobel(grayGPU, response[k], grayGPU.type(), 0, 1, 1);
		filterS = cv::cuda::createSobelFilter(grayGPU.type(), response[k].type(), 0, 1);
		filterS->apply(grayGPU, response[k]);
		//cv::cuda::GaussianBlur(response[k], response[k], cv::Size(2 * (int)(_kappa * sigma) + 1, 2 * (int)(_kappa * sigma) + 1), sigma);
		filter = cv::cuda::createGaussianFilter(response[k].type(), response[k].type(), cv::Size( 2 * (int)(3.0 * _kappa * sigma) + 1, 2 * (int)(_kappa * sigma) + 1), 0);
		filter->apply(response[k], response[k]);
		k++;
	}

	for (double sigma = 1.0; sigma <= 8.0; sigma *= 2.0){
		cv::cuda::GpuMat temp;
		//cv::cuda::GaussianBlur(grayGPU, temp, cv::Size(2 * (int)(_kappa * sigma) + 1, 2 * (int)(_kappa * sigma) + 1), sigma);
		cv::Ptr<cv::cuda::Filter> filter = cv::cuda::createGaussianFilter(grayGPU.type(), temp.type(), cv::Size(2 * (int)(_kappa * sigma) + 1, 2 * (int)(_kappa * sigma) + 1), 0);
		filter->apply(grayGPU, temp);
		/*cout << temp.type() << endl;
		cout << response[k].type() << endl;
		cout << grayGPU.type() << endl;*/
		//cv::cuda::Laplacian(temp, response[k], temp.type());

		cv::Ptr<cv::cuda::Filter> filter2 = cv::cuda::createLaplacianFilter(temp.type(), response[k].type());
		filter2->apply(temp, response[k]);

		k++;
	}

	//std::cout << "Calculating 17 gpumats time cost: " << (cv::getTickCount() - t0) / cv::getTickFrequency() << endl;

	//t0 = cv::getTickCount();
	vector<cv::Mat> response_cpu(texton_dim);
	for (int i = 0; i < texton_dim; i++){
		//cout << response[i].type() << endl;//CV_8U
		response[i].download(response_cpu[i]);
	}
	//std::cout << "Downloading 17 gpumats time cost: " << (cv::getTickCount() - t0) / cv::getTickFrequency() << endl;

	Initialization(features, Image);

#pragma omp parallel for
	for (int i = 0; i < image.rows; i+=sampleStep){
		for (int j = 0; j < image.cols; j+=sampleStep){
			FeatVector* textonfeature = NULL;
			if (LabelImage != NULL){
				int label = GetLabel(j,i, LabelImage);
				if (label > 0){
					textonfeature = new FeatVector();
					textonfeature->SetLabel(label);
				}
			}
			else{
				textonfeature = new FeatVector();
			}
			if (textonfeature != NULL){
				textonfeature->SetExtraData(j, i);
				features[i*image.cols + j] = textonfeature;
				for (int k = 0; k < texton_dim; k++){
					float v = static_cast<float>(response_cpu[k].at<uchar>(i, j));
					textonfeature->InsertNewAttribute(v);
				}
			}
		}
	}
	//std::cout << "Allocating 17 gpumats time cost: " << (cv::getTickCount() - t0) / cv::getTickFrequency() << endl;
}


void PixelFeatureExtractor::ExtractionTexton(vector< FeatVector* >& features, IplImage* Image, cv::Mat& samplingMask, int sampleStep, IplImage* LabelImage)
{
	int texton_dim = 17;
	int i, j, k;
	vector< IplImage* > response;

	for (i = 0; i<texton_dim; i++)
	{
		IplImage* ResponseImage = NULL;
		response.push_back(ResponseImage);
	}

	TextonFeature::preprocessImage(response, Image, 1.0);//smoothing

	Initialization(features, Image);

	for (i = 0; i<Image->height; i += sampleStep)
	{
		for (j = 0; j<Image->width; j += sampleStep)
		{
			FeatVector* textonfeature = NULL;
			if (LabelImage != NULL)
			{
				int label = GetLabel(j, i, LabelImage);
				if (label > 0)
				{
					textonfeature = new FeatVector();
					textonfeature->SetLabel(label);
				}
			}
			else
			{
				textonfeature = new FeatVector();
			}

			if (textonfeature != NULL)
			{
				textonfeature->SetExtraData(j, i);
				features[i*Image->width + j] = textonfeature;
				for (k = 0; k<texton_dim; k++)
				{
					textonfeature->InsertNewAttribute(CV_IMAGE_ELEM(response[k], float, i, j));//data.push_back(value);
				}
			}
		}
	}

	for (i = 0; i<texton_dim; i++)
	{
		if (response.at(i) != NULL)
		{
			cvReleaseImage(&response.at(i));
		}
	}

}

FeatureParameter::~FeatureParameter(){
	//if (cluteringTree != NULL){
	//	delete cluteringTree;
	//	//cluteringTree = NULL;
	//}
}

void PixelFeatureExtractor::ExtractionTexton(vector< FeatVector* >& features, IplImage* Image, FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage)
{
	if (featPara.numScales == 1)
	{
		int64 t0 = cv::getTickCount();
		ExtractionTextonGPU(features, Image, samplingMask, featPara.sampleStep, LabelImage);
		std::cout << "The time for extracting Texton is: " << (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000 << " ms. " << endl;
	}
	else if (featPara.numScales > 1)
	{
		ExtractionTexton(features, Image, samplingMask, featPara.sampleStep, LabelImage);

		for (int k = 2; k <= featPara.numScales; k++)
		{
			vector< FeatVector* > scaledFeatures;
			int w = Image->width / pow(2.0, k - 1);
			int h = Image->height / pow(2.0, k - 1);
			IplImage* scaledImage = cvCreateImage(cvSize(w, h), Image->depth, Image->nChannels);
			cvResize(Image, scaledImage);

			ExtractionTexton(scaledFeatures, scaledImage, samplingMask, 1, NULL);

			for (int i = 0; i<Image->height; i++)
			{
				for (int j = 0; j<Image->width; j++)
				{
					int y = min(int(i / pow(2.0, k - 1)), scaledImage->height - 1);
					int x = min(int(j / pow(2.0, k - 1)), scaledImage->width - 1);

					int idx1 = i*Image->width + j;
					int idx2 = y*scaledImage->width + x;

					if (features.at(idx1) != NULL && scaledFeatures.at(idx2) != NULL)
					{
						if (features.at(idx1)->GetFeatDim() == (k - 1)*scaledFeatures.at(idx2)->GetFeatDim())
						{
							for (int n = 0; n<scaledFeatures.at(idx2)->GetFeatDim(); n++)
							{
								float v = scaledFeatures.at(idx2)->GetAttributeValue(n);
								features.at(idx1)->InsertNewAttribute(v);
							}
						}
						else
						{
							delete features.at(idx1);
							//features.at(idx1) = NULL;
						}
					}
					else
					{
						if (features.at(idx1) != NULL)
						{
							delete features.at(idx1);
							//features.at(idx1) = NULL;
						}
					}
				}
			}

			for (unsigned int i = 0; i<scaledFeatures.size(); ++i)
			{
				if (scaledFeatures[i] != NULL) delete scaledFeatures[i];
			}
			scaledFeatures.clear();

			cvReleaseImage(&scaledImage);
		}
	}
}

void PixelFeatureExtractor::ExtractionSIFTGPU(vector< FeatVector* >& features, IplImage* Image, cv::Mat& samplingMask, int sampleStep, IplImage* LabelImage, VlDsiftFilterGPU* SIFTFilter){
	int width = Image->width;
	int height = Image->height;

	IplImage* ImageSmoothed = cvCreateImage(cvGetSize(Image), Image->depth, Image->nChannels);
	cvSmooth(Image, ImageSmoothed, CV_GAUSSIAN, 5);

	IplImage* Image_gray = cvCreateImage(cvGetSize(ImageSmoothed), ImageSmoothed->depth, 1);
	cvCvtColor(Image, Image_gray, CV_BGR2GRAY);

	float* imagedata = new float[width*height];
	int i, j;
	for (i = 0; i<height; i++)
	{
		for (j = 0; j<width; j++)
		{
			imagedata[i*width + j] = ((unsigned char*)Image_gray->imageData)[i*Image_gray->widthStep + j];
		}
	}

	
	_vl_dsift_alloc_buffers_gpu(SIFTFilter);

	int64 t0 = cv::getTickCount();
	vl_dsift_process_gpu(SIFTFilter, imagedata);
	printf("%f ms\n", (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000);

	VlDsiftKeypoint* ptkeypoint = (VlDsiftKeypoint*)vl_dsift_get_keypoints(SIFTFilter);

	float* descriptor = (float*)vl_dsift_get_descriptors(SIFTFilter);

	cout << sizeof(descriptor) / sizeof(float) << endl;

	int numpoints = vl_dsift_get_keypoint_num(SIFTFilter);
	int dim = vl_dsift_get_descriptor_size(SIFTFilter);

	Initialization(features, Image);//feature.size()= image.width*image.height * (FeatVect*)
//#pragma omp parallel for
	for (i = 0; i<numpoints; i++)
	{
		FeatVector* SIFTfeature = NULL;
		if (LabelImage != NULL)/////////////////////////
		{
			int label = GetLabel(ptkeypoint->x, ptkeypoint->y, LabelImage);/////////////////////////
			if (label > 0)/////////////////////////
			{
				SIFTfeature = new FeatVector();/////////////////////////
				SIFTfeature->SetLabel(label);/////////////////////////
			}
		}
		else
		{
			SIFTfeature = new FeatVector();
		}

		if (SIFTfeature != NULL)
		{
			SIFTfeature->SetExtraData(ptkeypoint->x, ptkeypoint->y);//void SetExtraData(int px, int py){ x = px; y = py; }
			features[ptkeypoint->y*Image->width + ptkeypoint->x] = SIFTfeature;
			for (j = 0; j<dim; j++)
			{
				float v = descriptor[i*dim + j];
				SIFTfeature->InsertNewAttribute(v);//data.push_back(v);
			}
			//cout << SIFTfeature->GetFeatDim() << endl;
		}

		ptkeypoint++;
	}
	delete[] imagedata;
	/*delete[] ptkeypoint;
	delete[] descriptor;*/
	cvReleaseImage(&Image_gray);
	cvReleaseImage(&ImageSmoothed);
	
	//vl_dsift_free_gpu(SIFTFilter);
}

void PixelFeatureExtractor::ExtractionSIFT(vector< FeatVector* >& features, IplImage* Image, cv::Mat& samplingMask, int sampleStep, IplImage* LabelImage)
{
	int width = Image->width;
	int height = Image->height;

	IplImage* ImageSmoothed = cvCreateImage(cvGetSize(Image), Image->depth, Image->nChannels);
	cvSmooth(Image, ImageSmoothed, CV_GAUSSIAN, 5);

	IplImage* Image_gray = cvCreateImage(cvGetSize(ImageSmoothed), ImageSmoothed->depth, 1);
	cvCvtColor(Image, Image_gray, CV_BGR2GRAY);

	float* imagedata = new float[width*height];
	int i, j;
	for (i = 0; i<height; i++)
	{
		for (j = 0; j<width; j++)
		{
			imagedata[i*width + j] = ((unsigned char*)Image_gray->imageData)[i*Image_gray->widthStep + j];
		}
	}

	int PatchSize = 8;
	VlDsiftFilter*  SIFTFilter = vl_dsift_new_basic(width, height, sampleStep, PatchSize);
	vl_dsift_process(SIFTFilter, imagedata);

	VlDsiftKeypoint* ptkeypoint = (VlDsiftKeypoint*)vl_dsift_get_keypoints(SIFTFilter);

	float* descriptor = (float*)vl_dsift_get_descriptors(SIFTFilter);

	int numpoints = vl_dsift_get_keypoint_num(SIFTFilter);
	int dim = vl_dsift_get_descriptor_size(SIFTFilter);//128

	Initialization(features, Image);//feature.size()= image.width*image.height * (FeatVect*)

	for (i = 0; i<numpoints; i++)
	{
		FeatVector* SIFTfeature = NULL;
		if (LabelImage != NULL)/////////////////////////
		{
			int label = GetLabel(ptkeypoint->x, ptkeypoint->y, LabelImage);/////////////////////////
			if (label > 0)/////////////////////////
			{
				SIFTfeature = new FeatVector();/////////////////////////
				SIFTfeature->SetLabel(label);/////////////////////////
			}
		}
		else
		{
			SIFTfeature = new FeatVector();
		}

		if (SIFTfeature != NULL)
		{
			SIFTfeature->SetExtraData(ptkeypoint->x, ptkeypoint->y);//void SetExtraData(int px, int py){ x = px; y = py; }
			features[ptkeypoint->y*Image->width + ptkeypoint->x] = SIFTfeature;
			for (j = 0; j<dim; j++)
			{
				float v = descriptor[i*dim + j];
				SIFTfeature->InsertNewAttribute(v);//data.push_back(v);
			}
		}

		ptkeypoint++;
	}
	delete[] imagedata;
	cvReleaseImage(&Image_gray);
	cvReleaseImage(&ImageSmoothed);
	vl_dsift_delete(SIFTFilter);
}

void PixelFeatureExtractor::ExtractionSIFT(vector< FeatVector* >& features, IplImage* Image, FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage, VlDsiftFilterGPU* SIFTFilter)
{
	if (featPara.numScales == 1)
	{
		int64 t0 = cv::getTickCount();
		ExtractionSIFTGPU(features, Image, samplingMask, featPara.sampleStep, LabelImage, SIFTFilter);
		std::cout << "The time for extracting SIFT is: " << (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000 << " ms. " << std::endl;
		
	}
	else if (featPara.numScales > 1)
	{
		ExtractionSIFT(features, Image, samplingMask, featPara.sampleStep, LabelImage);

		for (int k = 2; k <= featPara.numScales; k++)
		{
			vector< FeatVector* > scaledFeatures;
			int w = Image->width / pow(2.0, k - 1);
			int h = Image->height / pow(2.0, k - 1);
			IplImage* scaledImage = cvCreateImage(cvSize(w, h), Image->depth, Image->nChannels);
			cvResize(Image, scaledImage);

			ExtractionSIFT(scaledFeatures, scaledImage, samplingMask, 1, NULL);

			for (int i = 0; i<Image->height; i++)
			{
				for (int j = 0; j<Image->width; j++)
				{
					int y = min(int(i / pow(2.0, k - 1)), scaledImage->height - 1);
					int x = min(int(j / pow(2.0, k - 1)), scaledImage->width - 1);

					int idx1 = i*Image->width + j;
					int idx2 = y*scaledImage->width + x;

					if (features.at(idx1) != NULL && scaledFeatures.at(idx2) != NULL)
					{
						if (features.at(idx1)->GetFeatDim() == (k - 1)*scaledFeatures.at(idx2)->GetFeatDim())
						{
							for (int n = 0; n<scaledFeatures.at(idx2)->GetFeatDim(); n++)
							{
								float v = scaledFeatures.at(idx2)->GetAttributeValue(n);
								features.at(idx1)->InsertNewAttribute(v);
							}
						}
						else
						{
							delete features.at(idx1);
							features.at(idx1) = NULL;
						}
					}
					else
					{
						if (features.at(idx1) != NULL)
						{
							delete features.at(idx1);
							features.at(idx1) = NULL;
						}
					}
				}
			}

			for (unsigned int i = 0; i<scaledFeatures.size(); ++i)
			{
				if (scaledFeatures[i] != NULL) delete scaledFeatures[i];
			}
			scaledFeatures.clear();

			cvReleaseImage(&scaledImage);
		}
	}
}

void PixelFeatureExtractor::ExtractionSparseSIFT(vector< FeatVector* >& features, IplImage* Image,
	FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage)
{
	for (int i = 0; i<Image->height; i++)
	{
		for (int j = 0; j<Image->width; j++)
		{
			features.push_back(NULL);
		}
	}

	int  noctaves = log2(min(Image->width, Image->height));
	int  nlevels = 3;
	int  o_min = -1;

	VlSiftFilt*  Sift = vl_sift_new(Image->width, Image->height, noctaves, nlevels, o_min);

	float* imagedata = new float[Image->width*Image->height];
	for (int i = 0; i<Image->height; i++){
		for (int j = 0; j<Image->width; j++){
			imagedata[i*Image->width + j] = Image->imageData[i*Image->widthStep + j];
		}
	}

	vl_sift_process_first_octave(Sift, imagedata);

	for (int i = o_min; i<o_min + noctaves; i++)
	{
		vl_sift_process_next_octave(Sift);
		vl_sift_detect(Sift);
		const VlSiftKeypoint* keypoint = vl_sift_get_keypoints(Sift);
		int numpoints = vl_sift_get_nkeypoints(Sift);

		double angles[4];
		for (int k = 0; k<numpoints; k++)
		{
			int nangles = vl_sift_calc_keypoint_orientations(Sift, angles, keypoint);
			for (int j = 0; j<nangles; j++)
			{
				float desc[128];
				vl_sift_calc_keypoint_descriptor(Sift, desc, keypoint, angles[j]);

				FeatVector* SIFTfeature = NULL;
				if (LabelImage != NULL)
				{
					int label = GetLabel(keypoint->x, keypoint->y, LabelImage);
					if (label > 0)
					{
						SIFTfeature = new FeatVector();
						SIFTfeature->SetLabel(label);
					}
				}
				else
				{
					SIFTfeature = new FeatVector();
				}

				if (SIFTfeature != NULL)
				{
					features[keypoint->y*Image->width + keypoint->x] = SIFTfeature;
					SIFTfeature->SetExtraData(keypoint->x, keypoint->y);
					for (int s = 0; s<128; s++)
					{
						SIFTfeature->InsertNewAttribute(desc[s]);
					}
				}
			}
			keypoint++;
		}
	}
	delete[] imagedata;
	vl_sift_delete(Sift);
}

// takes a double color image and a bin size
// returns HOG features
void PixelFeatureExtractor::ExtractionSparseHOG(vector< FeatVector* >& features,
	IplImage* Image,
	FeatureParameter& featPara, cv::Mat& samplingMask,
	IplImage* LabelImage)
{
	Initialization(features, Image);
	//AssignLabel(features,LabelImage);

	const double eps = 0.0001;
	// unit vectors used to compute gradient orientation
	double uu[9] = { 1.0000, 0.9397, 0.7660,
		0.500, 0.1736, -0.1736,
		-0.5000, -0.7660, -0.9397 };
	double vv[9] = { 0.0000, 0.3420, 0.6428,
		0.8660, 0.9848, 0.9848,
		0.8660, 0.6428, 0.3420 };

	int sbin = featPara.featPatchSize / 2;
	int dims[2];
	dims[0] = Image->height;
	dims[1] = Image->width;

	// memory for caching orientation histograms & their norms
	int blocks[2];
	blocks[0] = (int)round((double)dims[0] / (double)sbin);
	blocks[1] = (int)round((double)dims[1] / (double)sbin);
	double *hist = new double[blocks[0] * blocks[1] * 18];
	double *norm = new double[blocks[0] * blocks[1]];

	for (int i = 0; i<blocks[0] * blocks[1] * 18; i++)
	{
		hist[i] = 0;
	}

	for (int i = 0; i<blocks[0] * blocks[1]; i++)
	{
		norm[i] = 0;
	}

	// memory for HOG features
	int out[3];
	out[0] = max(blocks[0] - 2, 0);
	out[1] = max(blocks[1] - 2, 0);
	out[2] = 27 + 4 + 1;

	double* feat = new double[out[0] * out[1] * out[2]];
	for (int i = 0; i<out[0] * out[1] * out[2]; i++)
	{
		feat[i] = 0;
	}

	double* im = new double[dims[0] * dims[1] * 3];
	for (int x = 0; x<dims[1]; x++)
	{
		for (int y = 0; y<dims[0]; y++)
		{
			im[x*dims[0] + y] = ((unsigned char*)Image->imageData)[y*Image->widthStep + x*Image->nChannels];
			im[x*dims[0] + y + dims[0] * dims[1]] = ((unsigned char*)Image->imageData)[y*Image->widthStep + x*Image->nChannels + 1];
			im[x*dims[0] + y + dims[0] * dims[1] * 2] = ((unsigned char*)Image->imageData)[y*Image->widthStep + x*Image->nChannels + 2];
		}
	}

	for (int x = 1; x < dims[1] - 1; x++)
	{
		for (int y = 1; y < dims[0] - 1; y++)
		{
			// first color channel
			double *s = im + x*dims[0] + y;
			double dy = *(s + 1) - *(s - 1);
			double dx = *(s + dims[0]) - *(s - dims[0]);
			double v = dx*dx + dy*dy;

			// second color channel
			s += dims[0] * dims[1];
			double dy2 = *(s + 1) - *(s - 1);
			double dx2 = *(s + dims[0]) - *(s - dims[0]);
			double v2 = dx2*dx2 + dy2*dy2;

			// third color channel
			s += dims[0] * dims[1];
			double dy3 = *(s + 1) - *(s - 1);
			double dx3 = *(s + dims[0]) - *(s - dims[0]);
			double v3 = dx3*dx3 + dy3*dy3;

			// pick channel with strongest gradient
			if (v2 > v)
			{
				v = v2;
				dx = dx2;
				dy = dy2;
			}
			if (v3 > v)
			{
				v = v3;
				dx = dx3;
				dy = dy3;
			}

			// snap to one of 18 orientations
			double best_dot = 0;
			int best_o = 17;
			for (int o = 0; o < 9; o++)
			{
				double dot = uu[o] * dx + vv[o] * dy;
				if (dot > best_dot)
				{
					best_dot = dot;
					best_o = o;
				}
				else if (-dot > best_dot)
				{
					best_dot = -dot;
					best_o = o + 9;
				}
			}

			// add to 4 histograms around pixel using linear interpolation
			double xp = ((double)x + 0.5) / (double)sbin - 0.5;
			double yp = ((double)y + 0.5) / (double)sbin - 0.5;
			int ixp = (int)floor(xp);
			int iyp = (int)floor(yp);
			double vx0 = xp - ixp;
			double vy0 = yp - iyp;
			double vx1 = 1.0 - vx0;
			double vy1 = 1.0 - vy0;
			v = sqrt(v);

			if (ixp >= 0 && iyp >= 0)
			{
				*(hist + ixp*blocks[0] + iyp + best_o*blocks[0] * blocks[1]) += vx1*vy1*v;
			}

			if (ixp + 1 < blocks[1] && iyp >= 0)
			{
				*(hist + (ixp + 1)*blocks[0] + iyp + best_o*blocks[0] * blocks[1]) += vx0*vy1*v;
			}

			if (ixp >= 0 && iyp + 1 < blocks[0])
			{
				*(hist + ixp*blocks[0] + (iyp + 1) + best_o*blocks[0] * blocks[1]) += vx1*vy0*v;
			}

			if (ixp + 1 < blocks[1] && iyp + 1 < blocks[0])
			{
				*(hist + (ixp + 1)*blocks[0] + (iyp + 1) + best_o*blocks[0] * blocks[1]) += vx0*vy0*v;
			}
		}
	}

	// compute energy in each block by summing over orientations
	for (int o = 0; o < 9; o++)
	{
		double *src1 = hist + o*blocks[0] * blocks[1];
		double *src2 = hist + (o + 9)*blocks[0] * blocks[1];
		double *dst = norm;
		double *end = norm + blocks[1] * blocks[0];
		while (dst < end)
		{
			*(dst++) += (*src1 + *src2) * (*src1 + *src2);
			src1++;
			src2++;
		}
	}

	// compute features
	for (int x = 0; x < out[1]; x++)
	{
		for (int y = 0; y < out[0]; y++)
		{
			double *dst = feat + x*out[0] + y;
			double *src, *p, n1, n2, n3, n4;

			p = norm + (x + 1)*blocks[0] + y + 1;
			n1 = 1.0 / sqrt(*p + *(p + 1) + *(p + blocks[0]) + *(p + blocks[0] + 1) + eps);
			p = norm + (x + 1)*blocks[0] + y;
			n2 = 1.0 / sqrt(*p + *(p + 1) + *(p + blocks[0]) + *(p + blocks[0] + 1) + eps);
			p = norm + x*blocks[0] + y + 1;
			n3 = 1.0 / sqrt(*p + *(p + 1) + *(p + blocks[0]) + *(p + blocks[0] + 1) + eps);
			p = norm + x*blocks[0] + y;
			n4 = 1.0 / sqrt(*p + *(p + 1) + *(p + blocks[0]) + *(p + blocks[0] + 1) + eps);

			double t1 = 0;
			double t2 = 0;
			double t3 = 0;
			double t4 = 0;

			//int idx = (y+1)*sbin*Image->width+(x+1)*sbin;

			FeatVector* HOGfeature = NULL;
			if (LabelImage != NULL)
			{
				int label = GetLabel((x + 1)*sbin, (y + 1)*sbin, LabelImage);
				if (label > 0)
				{
					HOGfeature = new FeatVector();
					HOGfeature->SetLabel(label);
				}
			}
			else
			{
				HOGfeature = new FeatVector();
			}

			if (HOGfeature != NULL)
			{
				HOGfeature->SetExtraData((x + 1)*sbin, (y + 1)*sbin);
				features[(y + 1)*sbin*Image->width + (x + 1)*sbin] = HOGfeature;
				// contrast-sensitive features
				src = hist + (x + 1)*blocks[0] + (y + 1);
				for (int o = 0; o < 18; o++)
				{
					double h1 = min(*src * n1, 0.2);
					double h2 = min(*src * n2, 0.2);
					double h3 = min(*src * n3, 0.2);
					double h4 = min(*src * n4, 0.2);
					*dst = 0.5 * (h1 + h2 + h3 + h4);
					HOGfeature->InsertNewAttribute(*dst);
					t1 += h1;
					t2 += h2;
					t3 += h3;
					t4 += h4;
					dst += out[0] * out[1];
					src += blocks[0] * blocks[1];
				}

				// contrast-insensitive features
				src = hist + (x + 1)*blocks[0] + (y + 1);
				for (int o = 0; o < 9; o++)
				{
					double sum = *src + *(src + 9 * blocks[0] * blocks[1]);
					double h1 = min(sum * n1, 0.2);
					double h2 = min(sum * n2, 0.2);
					double h3 = min(sum * n3, 0.2);
					double h4 = min(sum * n4, 0.2);
					*dst = 0.5 * (h1 + h2 + h3 + h4);
					HOGfeature->InsertNewAttribute(*dst);
					dst += out[0] * out[1];
					src += blocks[0] * blocks[1];
				}

				// texture features
				*dst = 0.2357 * t1;
				HOGfeature->InsertNewAttribute(*dst);
				dst += out[0] * out[1];
				*dst = 0.2357 * t2;
				HOGfeature->InsertNewAttribute(*dst);
				dst += out[0] * out[1];
				*dst = 0.2357 * t3;
				HOGfeature->InsertNewAttribute(*dst);
				dst += out[0] * out[1];
				*dst = 0.2357 * t4;
				HOGfeature->InsertNewAttribute(*dst);

				// truncation feature
				dst += out[0] * out[1];
				*dst = 0;
				HOGfeature->InsertNewAttribute(*dst);
			}
		}
	}

	delete[] im;
	delete[] hist;
	delete[] norm;
	delete[] feat;
}

void PixelFeatureExtractor::ExtractionHOGGPU(vector< FeatVector* >& features, IplImage* Image, cv::Mat& samplingMask, int sampleStep, int PatchSize, IplImage* LabelImage){
	//int64 t0 = cv::getTickCount();
	Initialization(features, Image);
	//cout << "get initialized takes " << (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000 << "ms" << endl;

	//t0 = cv::getTickCount();
	cv::Mat image = cv::cvarrToMat(Image);
	cv::cuda::GpuMat srcGPU(image);
	//cv::cuda::GpuMat grayGPU;
	//cv::cuda::cvtColor(srcGPU, grayGPU, CV_BGR2GRAY);
	//cout << "upload gpu mat takes " << (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000 << "ms" << endl;
	
	//t0 = cv::getTickCount();
	vector<cv::cuda::GpuMat> matArray;
	cv::cuda::split(srcGPU, matArray);
	//cout << "split into three channels takes " << (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000 << "ms" << endl;

	// unit vectors used to compute gradient orientation
	double uu[9] = { 1.0000, 0.9397, 0.7660,
		0.500, 0.1736, -0.1736,
		-0.5000, -0.7660, -0.9397 };
	double vv[9] = { 0.0000, 0.3420, 0.6428,
		0.8660, 0.9848, 0.9848,
		0.8660, 0.6428, 0.3420 };

	int dims[2];
	dims[0] = Image->height;
	dims[1] = Image->width;

	int sbin = floor(PatchSize / 2);
	double *kernel = new double[sbin*sbin];

	// bilinear interpolation kernel (ideally, it should be gaussian kernel)
	for (int x = 0; x < sbin; x++)
		for (int y = 0; y < sbin; y++)
			*(kernel + x * sbin + y) = double((sbin - x)*(sbin - y)) / (sbin*sbin);

	// memory for caching orientation histograms & their norms
	int blocks[2];
	blocks[0] = dims[0] - 2 * sbin;
	blocks[1] = dims[1] - 2 * sbin;
	//double *hist = new double[blocks[0] * blocks[1] * 18];
	/*double *norm = new double[dims[0] * dims[1]];
	for (int i = 0; i<dims[0] * dims[1]; i++) norm[i] = 0;*/

	// memory for HOG features
	int out[3];
	out[0] = blocks[0] - 2 * sbin;
	out[1] = blocks[1] - 2 * sbin;
	out[2] = 27 + 4;

	//cv::cuda::GpuMat* histGPU;
	//for (int i = 0; i < 18; i++)
	//	histGPU[i] = cv::cuda::GpuMat(dims[1], dims[0], CV_64FC1);
	//	//checkCudaErrors(cudaMalloc(&(histGPU[i]), sizeof(double) * dims[0] * dims[1]));
	//t0 = cv::getTickCount();
	double* res = new double[out[0] * out[1] * 31];
	for (int i = 0; i < out[0] * out[1] * 31; i++) res[i] = 0;
	calcHistonGPU(matArray[0], matArray[1], matArray[2], dims, uu, vv, kernel, sbin, res);
	//cout << "calculate result takes  " << (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000 << "ms" << endl;

	//ofstream fout("result.txt");
	//for (int i = 0; i < blocks[0]; i++){
	//	for (int j = 0; j < blocks[1]; j++){
	//		fout << hist[i*blocks[1] + j] << "\t";
	//	}
	//	fout << endl;
	//}
	//fout.close();

	int bw = floor(sbin * 2.5);
	
	//t0 = cv::getTickCount();
#pragma omp parallel for
	for (int x = 0; x < out[1]; x++)
	{
		for (int y = 0; y < out[0]; y++)
		{
			if (y < out[0] - sbin && x < out[1] - sbin)
			{

				FeatVector* HOGfeature = NULL;
				if (LabelImage != NULL)
				{
					int label = GetLabel(x + bw, y + bw, LabelImage);
					//cout << label << endl;
					if (label > 0)
					{
						HOGfeature = new FeatVector();
						HOGfeature->SetLabel(label);
					}
				}
				else
				{
					HOGfeature = new FeatVector();
				}

				if (HOGfeature != NULL)
				{
					
					HOGfeature->SetExtraData(x + bw, y + bw);
					
					features.at((y + bw)*Image->width + x + bw) = HOGfeature;
					
					vector<float>* ptFeat = features.at((y + bw)*Image->width + x + bw)->GetDataPtr();
					for (int i = 0; i < 31; i++)
						ptFeat->push_back(res[x*out[0] + y + i * out[1] * out[0]]);
				}
			}
		}
	}
	//cout << "total distribution time for hog: " << (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000 << " ms" << endl;
	delete[] kernel;
	delete[] res;
}

//ExtractionHOG(features, Image, samplingMask, featPara.sampleStep, featPara.featPatchSize, LabelImage);
void PixelFeatureExtractor::ExtractionHOG(vector< FeatVector* >& features, IplImage* Image, cv::Mat& samplingMask, int sampleStep, int PatchSize, IplImage* LabelImage)
{
	Initialization(features, Image);

	const double eps = 0.0001;
	// unit vectors used to compute gradient orientation
	double uu[9] = { 1.0000, 0.9397, 0.7660,
		0.500, 0.1736, -0.1736,
		-0.5000, -0.7660, -0.9397 };
	double vv[9] = { 0.0000, 0.3420, 0.6428,
		0.8660, 0.9848, 0.9848,
		0.8660, 0.6428, 0.3420 };

	int dims[2];
	dims[0] = Image->height;
	dims[1] = Image->width;

	int sbin = floor(PatchSize / 2);
	double *kernel = new double[sbin*sbin];

	// bilinear interpolation kernel (ideally, it should be gaussian kernel)
	for (int x = 0; x<sbin; x++)
	{
		for (int y = 0; y<sbin; y++)
		{
			//kernel[x][y]
			*(kernel + x*sbin + y) = double((sbin - x)*(sbin - y)) / (sbin*sbin);
		}
	}

	// memory for caching orientation histograms & their norms
	int blocks[2];
	blocks[0] = dims[0] - 2 * sbin;
	blocks[1] = dims[1] - 2 * sbin;
	double *hist = new double[blocks[0] * blocks[1] * 18];

	double *norm = new double[blocks[0] * blocks[1]];
	for (int i = 0; i<blocks[0] * blocks[1] * 18; i++)
	{
		hist[i] = 0;
	}

	for (int i = 0; i<blocks[0] * blocks[1]; i++)
	{
		norm[i] = 0;
	}

	// memory for HOG features
	int out[3];
	out[0] = blocks[0] - 2 * sbin;
	out[1] = blocks[1] - 2 * sbin;
	out[2] = 27 + 4;

	double* im = new double[dims[0] * dims[1] * 3];
	for (int x = 0; x<dims[1]; x++)
	{
		for (int y = 0; y<dims[0]; y++)
		{
			im[x*dims[0] + y] = ((unsigned char*)Image->imageData)[y*Image->widthStep + x*Image->nChannels];
			im[x*dims[0] + y + dims[0] * dims[1]] = ((unsigned char*)Image->imageData)[y*Image->widthStep + x*Image->nChannels + 1];
			im[x*dims[0] + y + dims[0] * dims[1] * 2] = ((unsigned char*)Image->imageData)[y*Image->widthStep + x*Image->nChannels + 2];
		}
	}

	for (int x = 1; x < dims[1] - 1; x++)
	{
		for (int y = 1; y < dims[0] - 1; y++)
		{
			// first color channel
			double *s = im + x*dims[0] + y;
			double dy = *(s + 1) - *(s - 1);
			double dx = *(s + dims[0]) - *(s - dims[0]);
			double v = dx*dx + dy*dy;

			// second color channel
			s += dims[0] * dims[1];
			double dy2 = *(s + 1) - *(s - 1);
			double dx2 = *(s + dims[0]) - *(s - dims[0]);
			double v2 = dx2*dx2 + dy2*dy2;

			// third color channel
			s += dims[0] * dims[1];
			double dy3 = *(s + 1) - *(s - 1);
			double dx3 = *(s + dims[0]) - *(s - dims[0]);
			double v3 = dx3*dx3 + dy3*dy3;

			// pick channel with strongest gradient
			if (v2 > v)
			{
				v = v2;
				dx = dx2;
				dy = dy2;
			}
			if (v3 > v)
			{
				v = v3;
				dx = dx3;
				dy = dy3;
			}

			// snap to one of 18 orientations
			double best_dot = 0;
			int best_o = 17;
			for (int o = 0; o < 9; o++)
			{
				double dot = uu[o] * dx + vv[o] * dy;
				if (dot > best_dot)
				{
					best_dot = dot;
					best_o = o;
				}
				else if (-dot > best_dot)
				{
					best_dot = -dot;
					best_o = o + 9;
				}
			}

			// add to surrounding bins using weights from the precomputed kernels
			v = sqrt(v);
			for (int dx = -(sbin - 1); dx <= (sbin - 1); dx++)
			{
				int xdx = x + dx;
				if (sbin <= xdx && xdx < dims[1] - sbin)
				{
					int adx = abs(dx);
					xdx = xdx - sbin;
					for (int dy = -(sbin - 1); dy <= (sbin - 1); dy++)
					{
						int ydy = y + dy;
						if (sbin <= ydy && ydy < dims[0] - sbin)
						{
							int ady = abs(dy);
							ydy = ydy - sbin;
							*(hist + xdx*blocks[0] + ydy + best_o*blocks[0] * blocks[1]) += v * (*(kernel + adx*sbin + ady));
						}
					}
				}
			}
		}
	}

	// compute energy in each block by summing over orientations
	for (int o = 0; o < 9; o++)
	{
		double *src1 = hist + o*blocks[0] * blocks[1];
		double *src2 = hist + (o + 9)*blocks[0] * blocks[1];
		double *dst = norm;
		double *end = norm + blocks[1] * blocks[0];

		while (dst < end)
		{
			*(dst++) += (*src1 + *src2) * (*src1 + *src2);
			src1++;
			src2++;
		}
	}

	int sbin_blocks0 = sbin*blocks[0];
	int sbin_blocks0_sbin = sbin*blocks[0] + sbin;

	vector< vector< vector<float> > > HOGFeat;
	for (int x = 0; x < out[1]; x++)
	{
		vector< vector<float> > rowFeat;
		HOGFeat.push_back(rowFeat);
		for (int y = 0; y < out[0]; y++)
		{
			vector<float> feat;
			HOGFeat[x].push_back(feat);
		}
	}

	int bw = floor(sbin * 2.5);
	for (int x = 0; x < out[1]; x++)
	{
		for (int y = 0; y < out[0]; y++)
		{
			double *src, *p, n1, n2, n3, n4;
			if (y < out[0] - sbin && x < out[1] - sbin)
			{
				FeatVector* HOGfeature = NULL;
				if (LabelImage != NULL)
				{
					int label = GetLabel(x + bw, y + bw, LabelImage);
					if (label > 0)
					{
						HOGfeature = new FeatVector();
						HOGfeature->SetLabel(label);
					}
				}
				else
				{
					HOGfeature = new FeatVector();
				}

				if (HOGfeature != NULL)
				{
					HOGfeature->SetExtraData(x + bw, y + bw);
					features.at((y + bw)*Image->width + x + bw) = HOGfeature;
					vector<float>* ptFeat = features.at((y + bw)*Image->width + x + bw)->GetDataPtr();

					p = norm + x*blocks[0] + y;
					n4 = 1.0 / sqrt(*p + *(p + sbin) + *(p + sbin_blocks0) + *(p + sbin_blocks0_sbin) + eps);

					p = p + sbin;
					n3 = 1.0 / sqrt(*p + *(p + sbin) + *(p + sbin_blocks0) + *(p + sbin_blocks0_sbin) + eps);

					p = p + sbin*blocks[0];
					n1 = 1.0 / sqrt(*p + *(p + sbin) + *(p + sbin_blocks0) + *(p + sbin_blocks0_sbin) + eps);

					p = p - sbin;
					n2 = 1.0 / sqrt(*p + *(p + sbin) + *(p + sbin_blocks0) + *(p + sbin_blocks0_sbin) + eps);

					double t1 = 0;
					double t2 = 0;
					double t3 = 0;
					double t4 = 0;

					// contrast-sensitive features
					src = hist + (x + sbin)*blocks[0] + (y + sbin);
					for (int o = 0; o < 18; o++)
					{
						double h1 = min(*src * n1, 0.2);
						double h2 = min(*src * n2, 0.2);
						double h3 = min(*src * n3, 0.2);
						double h4 = min(*src * n4, 0.2);
						double v = 0.5 * (h1 + h2 + h3 + h4);

						ptFeat->push_back(v);

						t1 += h1;
						t2 += h2;
						t3 += h3;
						t4 += h4;
						src += blocks[0] * blocks[1];
					}

					// contrast-insensitive features
					src = hist + (x + sbin)*blocks[0] + (y + sbin);
					for (int o = 0; o < 9; o++)
					{
						double sum = *src + *(src + 9 * blocks[0] * blocks[1]);
						double h1 = min(sum * n1, 0.2);
						double h2 = min(sum * n2, 0.2);
						double h3 = min(sum * n3, 0.2);
						double h4 = min(sum * n4, 0.2);
						double v = 0.5 * (h1 + h2 + h3 + h4);

						ptFeat->push_back(v);

						src += blocks[0] * blocks[1];
					}

					// texture features
					ptFeat->push_back(0.2357 * t1);
					ptFeat->push_back(0.2357 * t2);
					ptFeat->push_back(0.2357 * t3);
					ptFeat->push_back(0.2357 * t4);
				}
			}
		}
	}

	delete[] im;
	delete[] hist;
	delete[] norm;
	delete[] kernel;
}

// takes a double color image and a bin size
// returns HOG features
//ExtractionHOG(features, Image, featParameter[0], samplingMask, LabelImage);
void PixelFeatureExtractor::ExtractionHOG(vector< FeatVector* >& features, IplImage* Image, FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage)
{
	if (featPara.numScales == 1)
	{
		int64 t0 = cv::getTickCount();
		//ExtractionHOG(features, Image, samplingMask, featPara.sampleStep, featPara.featPatchSize, LabelImage);
		ExtractionHOGGPU(features, Image, samplingMask, featPara.sampleStep, featPara.featPatchSize, LabelImage);
		cout << "The time for extracting HOG is: " << (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000 <<" ms."<< endl;
	}
	else if (featPara.numScales > 1)
	{
		ExtractionHOG(features, Image, samplingMask, featPara.sampleStep, featPara.featPatchSize, LabelImage);

		for (int k = 2; k <= featPara.numScales; k++)
		{
			vector< FeatVector* > scaledFeatures;
			int w = Image->width / pow(2.0, k - 1);
			int h = Image->height / pow(2.0, k - 1);
			IplImage* scaledImage = cvCreateImage(cvSize(w, h), Image->depth, Image->nChannels);
			cvResize(Image, scaledImage);

			ExtractionHOG(scaledFeatures, scaledImage, samplingMask, 1, featPara.featPatchSize, NULL);

			for (int i = 0; i<Image->height; i++)
			{
				for (int j = 0; j<Image->width; j++)
				{
					int y = min(int(i / pow(2.0, k - 1)), scaledImage->height - 1);
					int x = min(int(j / pow(2.0, k - 1)), scaledImage->width - 1);

					int idx1 = i*Image->width + j;
					int idx2 = y*scaledImage->width + x;

					if (features.at(idx1) != NULL && scaledFeatures.at(idx2) != NULL)
					{
						if (features.at(idx1)->GetFeatDim() == (k - 1)*scaledFeatures.at(idx2)->GetFeatDim())
						{
							for (int n = 0; n<scaledFeatures.at(idx2)->GetFeatDim(); n++)
							{
								float v = scaledFeatures.at(idx2)->GetAttributeValue(n);
								features.at(idx1)->InsertNewAttribute(v);
							}
						}
						else
						{
							delete features.at(idx1);
							features.at(idx1) = NULL;
						}
					}
					else
					{
						if (features.at(idx1) != NULL)
						{
							delete features.at(idx1);
							features.at(idx1) = NULL;
						}
					}
				}
			}

			for (unsigned int i = 0; i<scaledFeatures.size(); ++i)
			{
				if (scaledFeatures[i] != NULL) delete scaledFeatures[i];
			}
			scaledFeatures.clear();

			cvReleaseImage(&scaledImage);
		}
	}
}

void PixelFeatureExtractor::ExtractionLBP(vector< FeatVector* >& features, IplImage* Image, FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage)
{
#ifdef COMPUTE_GPU
	Initialization(features, Image);
	cv::Mat image = cv::cvarrToMat(Image);
	cv::Mat gray;
	cv::cvtColor(image, gray, CV_BGR2GRAY);
	cv::cuda::GpuMat imageGPU(gray);
	LBPFeature lbp;
	cv::Mat response = lbp.computeGPU(imageGPU, 7, 8);
	//std::cout << response.rows << " * " << response.cols << endl;

	int featDim = 256;
	int patchSize = 14;
#pragma omp parallel for
	for (int i = patchSize/2; i<image.rows -1- patchSize/2;i++){
		for ( int j = patchSize/2; j<image.cols - 1 - patchSize/2; j++){
			FeatVector* LBPfeature = NULL;
			if (LabelImage != NULL){
				int label = GetLabel(j,i,LabelImage);
				if (label > 0){
					LBPfeature = new FeatVector();
					LBPfeature->SetLabel(label);
				}
			}else{
				LBPfeature = new FeatVector();
			}

			if (LBPfeature != NULL){
				LBPfeature->SetExtraData(j, i);
				features[i * image.cols + j] = LBPfeature;
				for (int k = 0; k < featDim; k++){
					LBPfeature->InsertNewAttribute(0);
				}

				for (int m = -patchSize / 2; m <= patchSize / 2; m++){
					for (int n = -patchSize / 2; n <= patchSize / 2; n++){
						int x = j + n;
						int y = i + m;
						uchar r = response.at<uchar>(y, x);
						float v = static_cast<float>(LBPfeature->GetAttributeValue(r) + 1);
						LBPfeature->SetAttributeValue(r, v);
					}
				}
			}
		}
	}

#else
	Initialization(features, Image);

	IplImage* ImageGray = cvCreateImage(cvGetSize(Image), Image->depth, 1);
	cvCvtColor(Image, ImageGray, CV_BGR2GRAY);

	int sampleStep = featPara.sampleStep;

	float weightTemplate[8];
	for (int i = 0; i<8; i++) weightTemplate[i] = pow(2.0, i);

	cv::Mat response(Image->height, Image->width, CV_32SC1, cv::Scalar(0));
	for (int i = 1; i<Image->height - 1; i++)
	{
		for (int j = 1; j<Image->width - 1; j++)
		{
			int r = 0;
			CvScalar s = cvGet2D(ImageGray, i, j);

			CvScalar s1 = cvGet2D(ImageGray, i - 1, j - 1);
			if (s1.val[0] > s.val[0]) r += weightTemplate[0];

			s1 = cvGet2D(ImageGray, i - 1, j);
			if (s1.val[0] > s.val[0]) r += weightTemplate[1];

			s1 = cvGet2D(ImageGray, i - 1, j + 1);
			if (s1.val[0] > s.val[0]) r += weightTemplate[2];

			s1 = cvGet2D(ImageGray, i, j + 1);
			if (s1.val[0] > s.val[0]) r += weightTemplate[3];

			s1 = cvGet2D(ImageGray, i + 1, j + 1);
			if (s1.val[0] > s.val[0]) r += weightTemplate[4];

			s1 = cvGet2D(ImageGray, i + 1, j);
			if (s1.val[0] > s.val[0]) r += weightTemplate[5];

			s1 = cvGet2D(ImageGray, i + 1, j - 1);
			if (s1.val[0] > s.val[0]) r += weightTemplate[6];

			s1 = cvGet2D(ImageGray, i, j - 1);
			if (s1.val[0] > s.val[0]) r += weightTemplate[7];

			response.at<int>(i, j) = r;
		}
	}
	int featDim = 256;
	int patchSize = featPara.featPatchSize;

	for (int i = patchSize / 2; i<Image->height - 1 - patchSize / 2; i += sampleStep)
	{
		for (int j = patchSize / 2; j<Image->width - 1 - patchSize / 2; j += sampleStep)
		{
			FeatVector* LBPfeature = NULL;
			if (LabelImage != NULL)
			{
				int label = GetLabel(j, i, LabelImage);
				if (label > 0)
				{
					LBPfeature = new FeatVector();
					LBPfeature->SetLabel(label);
				}
			}
			else
			{
				LBPfeature = new FeatVector();
			}

			if (LBPfeature != NULL)
			{
				LBPfeature->SetExtraData(j, i);
				features[i*Image->width + j] = LBPfeature;
				for (int k = 0; k<featDim; k++)
				{
					LBPfeature->InsertNewAttribute(0);
				}

				for (int m = -patchSize / 2; m <= patchSize / 2; m++)
				{
					for (int n = -patchSize / 2; n <= patchSize / 2; n++)
					{
						int x = j + n;
						int y = i + m;
						int r = response.at<int>(y, x);
						float v = LBPfeature->GetAttributeValue(r) + 1;
						LBPfeature->SetAttributeValue(r, v);
					}
				}
			}
		}
	}

	cvReleaseImage(&ImageGray);
#endif
}

void PixelFeatureExtractor::ExtractionColor(vector< FeatVector* >& features, IplImage* Image,
	FeatureParameter& featPara, cv::Mat& samplingMask, IplImage* LabelImage)
{
	Initialization(features, Image);

	IplImage *imgCIELab = cvCreateImage(cvGetSize(Image), IPL_DEPTH_32F, 3);
	cvConvertScale(Image, imgCIELab, 1.0 / 255.0, 0.0);
	cvCvtColor(imgCIELab, imgCIELab, CV_BGR2Lab);

	IplImage *imgHSV = cvCreateImage(cvGetSize(Image), IPL_DEPTH_32F, 3);
	cvConvertScale(Image, imgHSV, 1.0 / 255.0, 0.0);
	cvCvtColor(imgHSV, imgHSV, CV_BGR2HSV);

	for (int i = 0; i<Image->height; i += featPara.sampleStep)
	{
		for (int j = 0; j<Image->width; j += featPara.sampleStep)
		{
			FeatVector* colorfeature = NULL;
			if (LabelImage != NULL)
			{
				int label = GetLabel(j, i, LabelImage);
				if (label > 0)
				{
					colorfeature = new FeatVector();
					colorfeature->SetLabel(label);
				}
			}
			else
			{
				colorfeature = new FeatVector();
			}

			if (colorfeature != NULL)
			{
				colorfeature->SetExtraData(j, i);
				features[i*Image->width + j] = colorfeature;

				for (int r = 4; r >= 2; r = r - 2)
				{
					CvScalar s;
					s = cvGet2D(Image, i, j);
					for (int k = 0; k<Image->nChannels; k++)
					{
						float totalSum = 0;
						int   totalCount = 0;
						for (int m = -r; m <= r; m++)
						{
							for (int n = -r; n <= r; n++)
							{
								if (m + i >= 0 && m + i < Image->height && j + n >= 0 && j + n < Image->width)
								{
									totalSum += s.val[k];
									totalCount++;
								}
							}
						}
						float meanV = totalSum / totalCount;
						colorfeature->InsertNewAttribute(meanV);

						totalSum = 0;
						for (int m = -r; m <= r; m++)
						{
							for (int n = -r; n <= r; n++)
							{
								if (m + i >= 0 && m + i < Image->height && j + n >= 0 && j + n < Image->width)
								{

									totalSum += (s.val[k] - meanV)*(s.val[k] - meanV);
								}
							}
						}

						float variance = totalSum / totalCount;
						colorfeature->InsertNewAttribute(variance);
					}

					s = cvGet2D(imgCIELab, i, j);
					for (int k = 0; k<imgCIELab->nChannels; k++)
					{
						float totalSum = 0;
						int   totalCount = 0;
						for (int m = -r; m <= r; m++)
						{
							for (int n = -r; n <= r; n++)
							{
								if (m + i >= 0 && m + i < Image->height && j + n >= 0 && j + n < Image->width)
								{
									totalSum += s.val[k];
									totalCount++;
								}
							}
						}
						float meanV = totalSum / totalCount;
						colorfeature->InsertNewAttribute(meanV);

						totalSum = 0;
						for (int m = -r; m <= r; m++)
						{
							for (int n = -r; n <= r; n++)
							{
								if (m + i >= 0 && m + i < Image->height && j + n >= 0 && j + n < Image->width)
								{

									totalSum += (s.val[k] - meanV)*(s.val[k] - meanV);
								}
							}
						}

						float variance = totalSum / totalCount;
						colorfeature->InsertNewAttribute(variance);
					}

					s = cvGet2D(imgHSV, i, j);
					for (int k = 0; k<imgHSV->nChannels; k++)
					{
						float totalSum = 0;
						int   totalCount = 0;
						for (int m = -r; m <= r; m++)
						{
							for (int n = -r; n <= r; n++)
							{
								if (m + i >= 0 && m + i < Image->height && j + n >= 0 && j + n < Image->width)
								{
									totalSum += s.val[k];
									totalCount++;
								}
							}
						}
						float meanV = totalSum / totalCount;
						colorfeature->InsertNewAttribute(meanV);

						totalSum = 0;
						for (int m = -r; m <= r; m++)
						{
							for (int n = -r; n <= r; n++)
							{
								if (m + i >= 0 && m + i < Image->height && j + n >= 0 && j + n < Image->width)
								{

									totalSum += (s.val[k] - meanV)*(s.val[k] - meanV);
								}
							}
						}

						float variance = totalSum / totalCount;
						colorfeature->InsertNewAttribute(variance);
					}
				}
			}
		}
	}

	cvReleaseImage(&imgCIELab);
	cvReleaseImage(&imgHSV);
}

void PixelFeatureExtractor::AnalysisPCA(vector< FeatVector* >& features, string imageName, string flag)
{
	cout << "feature PCA ..." << endl;
	int featurenum = 0;
	int featuredim = 0;

	vector<int> featureIndexSet;
	for (unsigned int i = 0; i<features.size(); i++)
	{
		FeatVector* ptvwfeav = features.at(i);
		if (ptvwfeav != NULL)
		{
			if (ptvwfeav->GetFeatDim()>0)
			{
				featuredim = ptvwfeav->GetFeatDim();
				featureIndexSet.push_back(i);
			}
		}
	}

	featurenum = featureIndexSet.size();
	if (featurenum == 0)
	{
		cout << "No input feature vector..." << endl;
		return;
	}

	cv::Mat data(featurenum, featuredim, CV_32FC1);
	cv::Mat mean;

	int index = 0;
	for (int i = 0; i<featurenum; i++)
	{
		FeatVector* ptvwfeav = features.at(featureIndexSet.at(i));
		if (ptvwfeav != NULL)
		{
			if (ptvwfeav->GetFeatDim() != featuredim)
			{
				cout << ptvwfeav->GetFeatDim() << " " << featuredim << endl;
			}
			assert(ptvwfeav->GetFeatDim() == featuredim);
			for (int k = 0; k<featuredim; k++)
			{
				data.at<float>(index, k) = ptvwfeav->GetAttributeValue(k);
			}
			if (ptvwfeav->GetFeatDim()>0) index++;
		}
	}

	const int subspacedim = 3;
	cv::PCA dataPCA;
	dataPCA(data, mean, CV_PCA_DATA_AS_ROW, subspacedim);

	cv::Mat projectedData(featurenum, subspacedim, CV_32FC1);
	dataPCA.project(data, projectedData);

	float max[subspacedim];
	float min[subspacedim];
	for (int i = 0; i<subspacedim; i++)
	{
		max[i] = -1e6;
		min[i] = 1e6;
	}

	cv::Mat colorProjectedData(featurenum, subspacedim, CV_32FC1);
	for (int i = 0; i<featurenum; i++)
	{
		float v0 = projectedData.at<float>(i, 0);
		float v1 = projectedData.at<float>(i, 1);
		float v2 = projectedData.at<float>(i, 2);
		// R G B
		colorProjectedData.at<float>(i, 0) = (2 * v0 + 3 * v1 + 2 * v2) / 6.0;
		colorProjectedData.at<float>(i, 1) = (2 * v0 - 3 * v1 + 2 * v2) / 6.0;
		colorProjectedData.at<float>(i, 2) = (v0 - 2 * v2) / 6.0;

		for (int j = 0; j<subspacedim; j++)
		{
			if (max[j]<colorProjectedData.at<float>(i, j))
				max[j] = colorProjectedData.at<float>(i, j);
			if (min[j]>colorProjectedData.at<float>(i, j))
				min[j] = colorProjectedData.at<float>(i, j);
		}
	}

	for (int i = 0; i<featurenum; i++)
	{
		for (int j = 0; j<subspacedim; j++)
		{
			colorProjectedData.at<float>(i, j) = (colorProjectedData.at<float>(i, j) - min[j]) / (max[j] - min[j]) * 254;
		}
	}

	/*visualize PCA*/
	IplImage* featurePCAMap = cvLoadImage(imageName.c_str());
	cvSetZero(featurePCAMap);

	int x, y;
	for (int i = 0; i<featurenum; i++)
	{
		FeatVector* ptvwfeav = features.at(featureIndexSet.at(i));
		if (ptvwfeav != NULL)
		{
			ptvwfeav->GetExtraData(x, y);

			int r = colorProjectedData.at<float>(i, 0);
			int g = colorProjectedData.at<float>(i, 1);
			int b = colorProjectedData.at<float>(i, 2);
			cvSet2D(featurePCAMap, y, x, cvScalar(b, g, r));
		}
	}

	string PCAfilename = TestConfiguration.featurePCAdir + ExtractFileName(imageName, false) + "_" + flag + "_PCA.png";

	cvSaveImage(PCAfilename.c_str(), featurePCAMap);
	cvReleaseImage(&featurePCAMap);
}

void TextonFeature::resizeInPlace(IplImage **image, int height, int width, int interpolation)
{
	if (((*image)->height == height) && ((*image)->width == width))
		return;
	IplImage *tmpImage = cvCreateImage(cvSize(width, height),
		(*image)->depth, (*image)->nChannels);
	cvResize(*image, tmpImage, interpolation);
	cvReleaseImage(image);
	*image = tmpImage;
}

void TextonFeature::preprocessImage(vector<IplImage*> &responses, const IplImage *cimg, float scale)
{
	IplImage *img = cvCreateImage(cvGetSize(cimg), cimg->depth, cimg->nChannels);
	int n = kernelSize(scale);
	cvSmooth(cimg, img, CV_GAUSSIAN, n, n, scale);

	// resize image (locations resized in extractWithScales)
	resizeInPlace(&img, (int)(img->height / scale), (int)(img->width / scale));
	filter(img, responses);
	cvReleaseImage(&img);
}

void TextonFeature::filter(IplImage *img, vector<IplImage *> &response)
{
	const int NUM_FILTERS = 17;
	double _kappa = 1.0;
	// check input
	assert(img != NULL);
	if (response.empty())
	{
		response.resize(NUM_FILTERS, NULL);
	}

	assert((int)response.size() == NUM_FILTERS);
	assert((img->nChannels == 3) && (img->depth == IPL_DEPTH_8U));
	for (int i = 0; i < NUM_FILTERS; i++)
	{
		if (response[i] == NULL)
		{
			response[i] = cvCreateImage(cvGetSize(img), IPL_DEPTH_32F, 1);
		}
		else if (response[i]->width != img->width || response[i]->height != img->height)
		{
			cvReleaseImage(&response[i]);
			response[i] = cvCreateImage(cvGetSize(img), IPL_DEPTH_32F, 1);
		}
		assert((response[i] != NULL) && (response[i]->nChannels == 1) &&
			(response[i]->depth == IPL_DEPTH_32F));
	}

	int k = 0;
	// color convert
	IplImage *imgCIELab = cvCreateImage(cvGetSize(img), IPL_DEPTH_32F, 3);
	cvConvertScale(img, imgCIELab, 1.0 / 255.0, 0.0);
	cvCvtColor(imgCIELab, imgCIELab, CV_BGR2Lab);

	IplImage *greyImg = cvCreateImage(cvGetSize(img), IPL_DEPTH_32F, 1);
	cvSetImageCOI(imgCIELab, 1);
	cvCopy(imgCIELab, greyImg);
	cvSetImageCOI(imgCIELab, 0);

	// gaussian filter on all color channels
	IplImage *gImg32f = cvCreateImage(cvGetSize(img), IPL_DEPTH_32F, 3);
	for (double sigma = 1.0; sigma <= 4.0; sigma *= 2.0)
	{
		cvSmooth(imgCIELab, gImg32f, CV_GAUSSIAN, 2 * (int)(_kappa * sigma) + 1);
		for (int c = 1; c <= 3; c++)
		{
			cvSetImageCOI(gImg32f, c);
			cvCopy(gImg32f, response[k++]);
		}
		cvSetImageCOI(gImg32f, 0);
	}
	cvReleaseImage(&gImg32f);

	// derivatives of gaussians on just greyscale image
	for (double sigma = 2.0; sigma <= 4.0; sigma *= 2.0)
	{
		// x-direction
		cvSobel(greyImg, response[k++], 1, 0, 1);
		cvSmooth(response[k - 1], response[k - 1], CV_GAUSSIAN,
			2 * (int)(_kappa * sigma) + 1, 2 * (int)(3.0 * _kappa * sigma) + 1);

		// y-direction
		cvSobel(greyImg, response[k++], 0, 1, 1);
		cvSmooth(response[k - 1], response[k - 1], CV_GAUSSIAN,
			2 * (int)(3.0 * _kappa * sigma) + 1, 2 * (int)(_kappa * sigma) + 1);
	}

	// laplacian of gaussian on just greyscale image
	IplImage *tmpImg = cvCreateImage(cvGetSize(greyImg), IPL_DEPTH_32F, 1);
	for (double sigma = 1.0; sigma <= 8.0; sigma *= 2.0)
	{
		cvSmooth(greyImg, tmpImg, CV_GAUSSIAN, 2 * (int)(_kappa * sigma) + 1);
		cvLaplace(tmpImg, response[k++]);
	}

	// free memory
	cvReleaseImage(&tmpImg);
	cvReleaseImage(&greyImg);
	cvReleaseImage(&imgCIELab);
}
