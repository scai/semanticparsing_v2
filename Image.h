#ifndef IMAGE_H_
#define IMAGE_H_

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include "cv.h"
#include "cxcore.h"
#include "cvaux.h"
#include "highgui.h"
#include "feature.h"
#include "BasicAPI.h"
#include "GCO/GCoptimization.h"
//#include "ANN.h"

using namespace std;

class BasicImage
{
public:
	BasicImage(){};
	~BasicImage(){};

	BasicImage(const BasicImage& T)
	{
		filenameImage = T.filenameImage;             // image file name
		filenameLabelMask = T.filenameLabelMask;         // annotation file name
		filenameSuperPixel = T.filenameSuperPixel;
		filenameFeature3d = T.filenameFeature3d;         // 3d feature file name
	}

	BasicImage& operator=(const BasicImage& T)
	{
		if (this != &T)
		{
			filenameImage = T.filenameImage;             // image file name
			filenameLabelMask = T.filenameLabelMask;         // annotation file name
			filenameSuperPixel = T.filenameSuperPixel;
			filenameFeature3d = T.filenameFeature3d;         // 3d feature file name
		}
		return *this;
	}

	BasicImage(string filenameInput)
	{
		filenameImage = filenameInput;
	}

public:
	string filenameImage;             // image file name
	string filenameLabelMask;         // annotation file name
	string filenameSuperPixel;        // super pixel file name
	string filenameFeature3d;         // 3d feature file name
};

class GraphParameters;

class PixelImage : public BasicImage
{
public:
	PixelImage();
	virtual ~PixelImage();

	PixelImage(const BasicImage& T) :BasicImage(T){}
	PixelImage(const PixelImage& T);
	PixelImage(string filenameInput);
	PixelImage(IplImage* Image);

public:
	void Initialization(int scale = 1);
	void ReleaseContent();

	void PixelMRFInference(cv::Mat& probability, cv::Mat& probability_index);

	void ViewPixelConfidenceMap(cv::Mat& probability, cv::Mat& probability_index);

	void SaveSegmentationResult();
	bool SegmentationValidation(cv::Mat& confusionMatrix, cv::Mat* activemask = NULL);
	bool SaveConfidenceMap(cv::Mat& prob, cv::Mat& probIndex);

	bool SaveBOWMap();
	bool LoadBOWMap();

	bool LoadConfidenceMap(cv::Mat*& prob, cv::Mat*& probIndex);
	bool LoadConfidenceMapALE(cv::Mat*& prob, cv::Mat*& probIndex);

	//void BagOfWordProjection(cv::Mat*& featBOWMap,vector<FeatVector>& features,ANNkd_tree* ClusteringTree);

public:
	int width;                        // width of image
	int height;                       // height of image

	vector<FeatVector*> features_2d;   // appearance feature
	vector<string> classexistFlag;    // class existence flag

	IplImage* ptImage;                // point to image
	IplImage* segResult;              // segmentation result
	IplImage* segResultObject;    // segmentation object result
	IplImage* labelMask;              // ground truth label mask

	vector<cv::Mat*> bowMap;          // the bag-of-word projection map,corresponding to different features
	FeatVector* imageLevelFeatures;   // image level feature, one for each kind of feature

	cv::Mat* labelMap;        // the label map
	cv::Mat* prob;            // predicted prob of different categories, for pixels
	cv::Mat* probIndex;       // predicted prob of different categories, for pixels position

	static vector< multimap<double, int> > knnLabelCandidates;
};

#endif /* IMAGE_H_ */
