#ifndef IMAGESEQUENCE_H_
#define IMAGESEQUENCE_H_

#include <vector>
#include <iostream>
#include "cv.h"
#include "cxcore.h"
#include "cvaux.h"
#include "highgui.h"

#include "Global.h"
#include "SuperPixelImage.h"
using namespace std;

class ImageSequence {
public:
	ImageSequence();
	virtual ~ImageSequence();

public:
	void DebugTesting(const int DEBUG);
public:
	vector<SuperPixelImage> ImageSet;
};

#endif /* IMAGESEQUENCE_H_ */
