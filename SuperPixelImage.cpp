#include "SuperPixelImage.h"
#include "Global.h"
#include "GraphModel.h"
using namespace BasicAPI;

const int errorValue = 5;

SuperPixelImage::SuperPixelImage() {
	// TODO Auto-generated constructor stub
	maxIndex = 0;
	numClassLabel = 0;
	numSuperPixel = 0;

	superPixelMap = NULL;
	activeMask = NULL;
}

SuperPixelImage::~SuperPixelImage() {
	// TODO Auto-generated destructor stub
	ReleaseContent();
}

void SuperPixelImage::ReleaseContent() {

	// TODO Auto-generated destructor stub
	if (ptImage != NULL)
	{
		cvReleaseImage(&ptImage);
		ptImage = NULL;
	}

	if (segResult != NULL)
	{
		cvReleaseImage(&segResult);
		segResult = NULL;
	}
	if (segResultObject != NULL)
	{
		cvReleaseImage(&segResultObject);
		segResultObject = NULL;
	}
	if (labelMask != NULL)
	{
		cvReleaseImage(&labelMask);
		labelMask = NULL;
	}

	if (superPixelMap != NULL) delete superPixelMap;
	if (activeMask != NULL) delete activeMask;
	if (labelMap != NULL) delete labelMap;

	superPixelMap = NULL;
	activeMask = NULL;
	labelMap = NULL;

	for (unsigned int i = 0; i<priorMaps.size(); i++)
	{
		delete priorMaps.at(i);
	}
	priorMaps.clear();

	superPixels.clear();
	superPixelLink.clear();

	/*for (unsigned int i = 0; i<features_2d.size(); ++i)
	{
		if (features_2d[i] != NULL) delete features_2d[i];
	}
	features_2d.clear();*/
}

// Initialization: super pixel i: pixels contained
//                                super pixel index
//                                adjacent super pixels
//                 Image size
//                 number of super pixels
bool SuperPixelImage::Initialization(cv::Mat* superpixelMat)
{
	// index for each super-pixel is from 1 ot N, and 0 for boundary
	assert(superpixelMat->cols == width);
	assert(superpixelMat->rows == height);

	//SLIC oversegmentor;

	//int* klabels = new int[width*height];
	//int  numlabels;
	//int  superpixelSize = 250;
	//int  K = width*height / superpixelSize + 1;
	//double m = 40;

	//oversegmentor.DoSuperpixelSegmentation_ForGivenK(ptImage,
	//	width, height,
	//	klabels, numlabels, K, m);

	int* klabels = new int[width*height];
	int  superpixelSize = 400;
	int superpixels = width * height / superpixelSize +1; //400; //desired number of superpixels
	int numberOfBins = 5; // number of bins used for color histogram
	int neighborhoodSize = 1; //neighborhood size used for smoothing prior
	float minimumConfidence = 0.1; //minimum confidence used for block update
	float spatialWeight = 0.25; // spatial weight
	int iterations = 5;//2; // iterations at each level
	SEEDSRevisedMeanPixels seeds(cv::cvarrToMat(ptImage), superpixels, numberOfBins, neighborhoodSize, minimumConfidence, spatialWeight);
	seeds.initialize();
	seeds.iterate(iterations);
	int bgr[] = { 0, 0, 204 };
	int ** labelsArray = seeds.getLabels();
	for (int i = 0; i < height; i++){
		for (int j = 0; j < width; j++){
			klabels[i*width + j] = labelsArray[i][j];
		}
	}
	cv::Mat temp(cv::Size(width, height), CV_8UC1);
	for (int i = 0; i < height; i++){
		for (int j = 0; j < width; j++){
			temp.at<uchar>(i, j) = klabels[i*width + j];
		}
	}
	//memcpy(temp, klabels, width*height);

	int i, j, k;
	int maxSPIndex = 0;
	for (unsigned int i = 0; i<height; i++)
	{
		for (int j = 0; j<width; j++)
		{
			superpixelMat->at<int>(i, j) = klabels[i*width + j] + 1;
			if (maxSPIndex < superpixelMat->at<int>(i, j))
				maxSPIndex = superpixelMat->at<int>(i, j);
		}
	}

	//for (int i = 0; i < height; i++) delete[] labelsArray[i];
	//delete[] labelsArray;

	delete[] klabels;

	if (maxSPIndex == 0)
	{
		cout << "wrong super-pixel Matrix" << endl;
		return false;
	}

	for (i = 0; i<maxSPIndex; i++)
	{
		SuperPixel sp;
		sp.index = i + 1;
		superPixels.push_back(sp);
	}

	/*insert pixels within the super pixels*/
	for (i = 0; i<height; i++)
	{
		for (j = 0; j<width; j++)
		{
			int index = superpixelMat->at<int>(i, j) - 1;
			superPixels.at(index).pixels.push_back(cvPoint(j, i));
		}
	}

	for (i = 0; i<superPixels.size(); i++)
	{
		superPixels.at(i).ComputeCenter();

		list<int> adjSPIndexSet;
		for (j = 0; j<superPixels.at(i).pixels.size(); j++)
		{
			int x = superPixels.at(i).pixels.at(j).x;
			int y = superPixels.at(i).pixels.at(j).y;
			int SPIndex = superpixelMat->at<int>(y, x);

			for (int m = -1; m <= 1; m++)
			{
				for (int n = -1; n <= 1; n++)
				{
					if (x + n >= 0 && x + n<superpixelMat->cols &&
						y + m >= 0 && y + m<superpixelMat->rows)
					{
						int adjSPIndex = superpixelMat->at<int>(y + m, x + n);
						if (adjSPIndex != SPIndex) adjSPIndexSet.push_back(adjSPIndex);
					}
				}
			}
		}
		adjSPIndexSet.sort();
		adjSPIndexSet.unique();

		for (list<int>::iterator iter = adjSPIndexSet.begin(); iter != adjSPIndexSet.end(); iter++)
			superPixels.at(i).adjIndex.push_back(*iter);
	}

	numClassLabel = TestConfiguration.numOfClass;
	//numSuperPixel = numlabels;
	numSuperPixel = superPixels.size();
	return true;
}

bool SuperPixelImage::Initialization(BasicImage* Image, string testLevel)
{
	filenameImage = Image->filenameImage;         // image file name
	filenameLabelMask = Image->filenameLabelMask;     // annotation file name
	filenameSuperPixel = Image->filenameSuperPixel;

	ptImage = cvLoadImage(filenameImage.c_str());          // input image

	if (ptImage == NULL)
	{
		cout << "Error load Image: " << filenameImage << " failed" << endl;
		return false;
	}
	else /*resize image*/
	{
		int scaleFactor = TestConfiguration.imageScaleFactor;
		if (scaleFactor != 1)
		{
			IplImage* temp = cvCreateImage(cvSize(ptImage->width / scaleFactor, ptImage->height / scaleFactor),
				ptImage->depth, ptImage->nChannels);
			cvResize(ptImage, temp);
			cvReleaseImage(&ptImage);
			ptImage = temp;
		}
	}

	labelMask = cvLoadImage(filenameLabelMask.c_str());      // ground truth label mask
	if (labelMask == NULL)
	{
		cout << "Warning Load Mask file: " << filenameLabelMask << " failed" << endl;
	}
	else /*resize label mask*/
	{
		if (labelMask->width != ptImage->width || labelMask->height != ptImage->height)
		{
			IplImage* temp = cvCreateImage(cvSize(ptImage->width, ptImage->height), labelMask->depth, labelMask->nChannels);
			cvResize(labelMask, temp, CV_INTER_NN);
			cvReleaseImage(&labelMask);
			labelMask = temp;
		}
	}

	segResult = cvCreateImage(cvGetSize(ptImage), ptImage->depth, ptImage->nChannels);
	cvSetZero(segResult);

	segResultObject = cvCreateImage(cvGetSize(ptImage), ptImage->depth, ptImage->nChannels);
	cvSetZero(segResultObject);

	/*assign label map*/
	if (labelMask != NULL)
	{
		labelMap = new cv::Mat(labelMask->height, labelMask->width, CV_32SC1, cv::Scalar(-1));
		for (int i = 0; i<labelMask->height; i++)
		{
			for (int j = 0; j<labelMask->width; j++)
			{
				CvScalar s = cvGet2D(labelMask, i, j);
				int label = TestConfiguration.colorTable.FindLabel(s);
				labelMap->at<int>(i, j) = label;
			}
		}
	}

	width = ptImage->width;
	height = ptImage->height;

	/*load super pixel*/
	if (testLevel == "superpixel" || testLevel == "pixel&superpixel")
	{
		superPixelMap = new cv::Mat(height, width, CV_32SC1, cv::Scalar(0)); // super pixel map
		if (Initialization(superPixelMap) == false) 
			delete superPixelMap;
		LabelSuperPixel();
	}

	return true;
}

void SuperPixelImage::PnModelMRFInference(cv::Mat& probability, cv::Mat& probability_index)
{
	GraphModel PnModelCRF(TestConfiguration.numOfClass);
	PnModelCRF.SetupGraph(this, &probability, &probability_index, NULL, NULL, NULL, NULL);
	PnModelCRF.MAPInference();

	for (unsigned int i = 0; i<PnModelCRF.Layer.size() && i<width*height; i++)
	{
		int x = PnModelCRF.Layer.at(i)->x;
		int y = PnModelCRF.Layer.at(i)->y;
		int label = PnModelCRF.Layer.at(i)->predictLabel;
		if (label < TestConfiguration.numOfClass && label >= 0)
		{
			cvSet2D(segResult, y, x, TestConfiguration.colorTable.GetCategoryColor(label));
		}
	}
}

void SuperPixelImage::PixelMRFInference(cv::Mat& probability, cv::Mat& probability_index)
{
	GraphModel pixelCRF(TestConfiguration.numOfClass);
	if (GraphModel::initialized == false)
		GraphModel::SetConfiguration(NULL);

	pixelCRF.SetupGraph(this, &probability, &probability_index, NULL, NULL, NULL, NULL);
	pixelCRF.MAPInference(false);

#pragma omp parallel for
	for (int i = 0; i<pixelCRF.Layer.size(); i++)
	{
		int x = pixelCRF.Layer[i]->x;
		int y = pixelCRF.Layer[i]->y;
		int label = pixelCRF.Layer.at(i)->predictLabel;
		if (label >= 0)
		{
			cvSet2D(segResult, y, x, TestConfiguration.colorTable.GetCategoryColor(label));
		}
	}
}


bool SuperPixelImage::SegmentMRFinference()
{
	int numclass = TestConfiguration.numOfClass;
	cv::Mat prob(superPixels.size(), TestConfiguration.numOfClass, CV_32FC1, cv::Scalar(0));
	cv::Mat probIndex(superPixels.size(), 1, CV_32SC1, cv::Scalar(0));

	for (unsigned int i = 0; i<superPixels.size(); i++)
	{
		for (int k = 0; k<numclass; k++)
		{
			prob.at<float>(i, k) = superPixels.at(i).confidenceValue.at(k);
		}
		probIndex.at<int>(i, 0) = superPixels.at(i).index;
		assert(superPixels.at(i).index == i + 1);
	}

	GraphModel segmentCRF(TestConfiguration.numOfClass);
	if (GraphModel::initialized == false) GraphModel::SetConfiguration(NULL);
	segmentCRF.SetupGraph(this, NULL, NULL, &prob, &probIndex, NULL, NULL);
	segmentCRF.MAPInference();

	vector<int> labels;
	for (unsigned int i = 0; i<segmentCRF.upLayer.size(); i++)
	{
		int label = segmentCRF.upLayer.at(i)->predictLabel;
		labels.push_back(label);
	}
	MRFParsing(labels);
	return true;
}

void SuperPixelImage::MRFParsing(vector<int>& labelset, bool saveResult)
{
	if (segResult != NULL) cvReleaseImage(&segResult);
	segResult = cvCreateImage(cvGetSize(ptImage), ptImage->depth, ptImage->nChannels);
	cvSetZero(segResult);

	for (unsigned i = 0; i<superPixels.size(); i++)
	{
		SuperPixel* sp = &superPixels.at(i);
		sp->label = labelset.at(i);
		if (labelset.at(i) >= 0)
		{
			for (unsigned j = 0; j<sp->pixels.size(); j++)
			{
				int x = sp->pixels.at(j).x;
				int y = sp->pixels.at(j).y;
				cvSet2D(segResult, y, x, TestConfiguration.colorTable.GetCategoryColor(labelset.at(i)));
			}
		}
		else
		{
			for (unsigned j = 0; j<sp->pixels.size(); j++)
			{
				int x = sp->pixels.at(j).x;
				int y = sp->pixels.at(j).y;
				cvSet2D(segResult, y, x, cvScalar(0, 0, 0));
			}
		}
	}
}

/*Get the label for each super-pixel from the label mask*/
void SuperPixelImage::LabelSuperPixel()
{
	int i, j;
	int numclass = TestConfiguration.numOfClass;
	int labelCount = 0;

	if (labelMap != NULL)
	{
		int* count = new int[numclass];
		int x, y;
		for (i = 0; i<superPixels.size(); i++)
		{
			superPixels.at(i).label = -1;
			for (j = 0; j<numclass; j++) count[j] = 0;
			for (j = 0; j<superPixels.at(i).pixels.size(); j++)
			{
				x = superPixels.at(i).pixels.at(j).x;
				y = superPixels.at(i).pixels.at(j).y;
				int label = labelMap->at<int>(y, x);
				if (label>0) count[label - 1]++;
			}

			int maxcount = 0;
			for (j = 0; j<numclass; j++)
			{
				if (maxcount<count[j])
				{
					maxcount = count[j];
					if (maxcount > superPixels.at(i).pixels.size()*GraphModel::robustPnAllowedRatio)
					{
						superPixels.at(i).label = j + 1;
						labelCount++;
					}
				}
			}
		}
		delete[] count;
	}

	cout << "Super Pixel With Label: (" << labelCount << "/" << superPixels.size() << ")" << endl;
}

bool SuperPixelImage::LoadConfidenceMapSP()
{
	string typeFalg;
	typeFalg = "2d";

	if (TestConfiguration.withSPLevelfeatures == true)
	{
		typeFalg = typeFalg + "_BOW_SP_";
		if (TestConfiguration.withImageLevelfeatures == true)
		{
			typeFalg = typeFalg + "IMAGE_";
		}
		typeFalg = typeFalg + TestConfiguration.bowFeatureFlag;
	}

	string confidenceFilename =
		TestConfiguration.confidencedir + ExtractFileName(filenameImage, false) + "_confidence_" +
		typeFalg + "_superpixel" + ".txt";

	ifstream input;
	input.open(confidenceFilename.c_str());

	if (!input)
	{
		input.close();
		cout << "load file: " << confidenceFilename << " failed" << endl;
		return false;
	}

	int lineLength = 1024;
	char temp[1024];

	/*check image size is consistent*/
	input.getline(temp, lineLength);
	if (temp[0] == '*')
	{
		string textLine(temp);
		vector<string> textElement;
		TextlineSplit(textLine, textElement, ' ');
		int w = atoi(textElement.at(1).c_str());
		int h = atoi(textElement.at(2).c_str());
		if (w != ptImage->width || h != ptImage->height)
		{
			cout << "inconsistent image size" << endl;
			return false;
		}
	}

	input.getline(temp, lineLength);
	if (temp[0] == '*')
	{
		string textLine(temp);
		vector<string> textElement;
		TextlineSplit(textLine, textElement, ' ');
		for (unsigned int i = 0; i<TestConfiguration.colorTable.GetNumberOfCategory(); i++)
		{
			if (textElement.at(i + 1) != TestConfiguration.colorTable.GetCategoryName(i))
			{
				cout << "inconsistent category order" << endl;
				return false;
			}
		}
	}

	int numOfItems = 0;
	input.getline(temp, lineLength);
	if (temp[0] == '*')
	{
		string textLine(temp);
		vector<string> textElement;
		TextlineSplit(textLine, textElement, ' ');
		numOfItems = atoi(textElement.at(1).c_str());
	}

	input.getline(temp, lineLength); // skip one line
	while (!input.eof())
	{
		input.getline(temp, lineLength);
		string textLine(temp);
		if (textLine.size()>2)
		{
			vector<string> textElement;
			TextlineSplit(textLine, textElement, ' ');

			int x = atof(textElement.at(0).c_str());
			int y = atof(textElement.at(1).c_str());
			int spIndex = superPixelMap->at<int>(y, x) - 1;

			SuperPixel* sp = &superPixels.at(spIndex);
			sp->confidenceValue.clear();

			for (unsigned int i = 0; i<TestConfiguration.numOfClass; i++)
			{
				sp->confidenceValue.push_back(atof(textElement.at(i + 2).c_str()));
			}
		}
	}
	input.close();

	return true;
}

void SuperPixelImage::SaveConfidenceMapSP()
{
	string typeFalg;
	typeFalg = "2d";

	if (TestConfiguration.withSPLevelfeatures == true)
	{
		typeFalg = typeFalg + "_BOW_SP_";
		if (TestConfiguration.withImageLevelfeatures == true)
		{
			typeFalg = typeFalg + "IMAGE_";
		}
		typeFalg = typeFalg + TestConfiguration.bowFeatureFlag;
	}

	string confidenceFilename =
		TestConfiguration.confidencedir + ExtractFileName(filenameImage, false) + "_confidence_" +
		typeFalg + "_superpixel" + ".txt";

	ofstream out;
	out.open(confidenceFilename.c_str());

	/*header of confidence file*/
	out << "*image-size: " << ptImage->width << " " << ptImage->height << endl;
	out << "*category-order: ";
	for (unsigned int i = 0; i<TestConfiguration.colorTable.GetNumberOfCategory(); i++)
	{
		out << TestConfiguration.colorTable.GetCategoryName(i) << " ";
	}
	out << endl;
	out << "*num-lines " << superPixels.size() << endl;
	out << "*format: <x,y of superpixel center> <probabilty>" << endl;

	int i, j, k;
	for (i = 0; i<superPixels.size(); i++)
	{
		SuperPixel* sp = &superPixels.at(i);
		int x = sp->pixels.at(sp->pixels.size() / 2).x;
		int y = sp->pixels.at(sp->pixels.size() / 2).y;

		out << x << " " << y << " ";
		for (int k = 0; k<TestConfiguration.numOfClass; k++)
		{
			out << sp->confidenceValue.at(k) << " ";
		}
		out << endl;
	}
	out.close();
}

void SuperPixelImage::CleanLabelMaskBoundary(IplImage* mask)
{
	if (mask != NULL)
	{
		IplImage* tempMask = cvCreateImage(cvGetSize(mask), mask->depth, mask->nChannels);
		cvCopy(mask, tempMask);
		/*shrink edges*/
		int numShrinkedEdge = 2;
		for (int k = 0; k<numShrinkedEdge; k++)
		{
			for (int i = 1; i<mask->height - 1; i++)
			{
				for (int j = 1; j<mask->width - 1; j++)
				{
					cv::Scalar s = cvGet2D(tempMask, i, j);
					int label = TestConfiguration.colorTable.FindLabel(s);
					if (label > 0 && label <= TestConfiguration.colorTable.GetNumberOfCategory())
					{
						for (int m = -1; m <= 1; m++)
						{
							for (int n = -1; n <= 1; n++)
							{
								cv::Scalar s1 = cvGet2D(tempMask, i + m, j + n);
								int adjLabel = TestConfiguration.colorTable.FindLabel(s1);
								if (label != adjLabel)
								{
									cvSet2D(mask, i, j, cv::Scalar(0, 0, 0));
								}
							}
						}
					}
				}
			}
			cvCopy(mask, tempMask);
			//            string testPath = "H:/Research/projects/StructualParsing-Folder/test/test_label.png";
			//			cvSaveImage(testPath.c_str(),mask);
		}
		cvReleaseImage(&tempMask);
	}
}

/* joint 2d and 3d feature extraction*/
/* features_2d3d store all combined 2d3d feature
/* for pixel without 3d feature,2d feature will
* be extracted and stored in features_2d
* extractType: train / test, for train only some features of one type are extracted
* for test, both type of features with and without 3d feature are extracted*/

/*if extractType = train, then only features with label are extracted from images*/
void SuperPixelImage::FeatureExtraction(VlDsiftFilterGPU* SIFTFilter, string extractType)
{
	cout << "2D feature extraction ..." << endl;
	for (int i = 0; i<features_2d.size(); ++i)
	{
		if (features_2d[i] != NULL){
			delete features_2d[i];
			//features_2d[i] = NULL;
		}
	}
	features_2d.clear();

	if (TestConfiguration.withPixelLevelfeatures == true)
	{
		PixelFeatureExtractor featExtractor(TestConfiguration.featParameters);
		if (extractType == "train")
		{
			CleanLabelMaskBoundary(labelMask);
			featExtractor.Extraction(SIFTFilter, features_2d, ptImage, labelMask);
		}
		else featExtractor.Extraction(SIFTFilter, features_2d, ptImage, NULL);

		/*insert position feature*/
		if (TestConfiguration.withPositionFeature == true)
		{
			for (unsigned int i = 0; i<features_2d.size(); i++)
			{
				if (features_2d[i] != NULL)
				{
					if (features_2d[i]->GetFeatDim()>0)
					{
						int x, y;
						features_2d[i]->GetExtraData(x, y);
						features_2d[i]->InsertNewAttribute(float(x) / float(width));
						features_2d[i]->InsertNewAttribute(float(y) / float(height));
					}
				}
			}
		}

		int feature_count_2d = 0;
		int feature_dim_2d = 0;

		for (unsigned int i = 0; i<features_2d.size(); i++)
		{
			if (features_2d[i] != NULL)
			{
				if (features_2d[i]->GetFeatDim()>0)
				{
					feature_count_2d++;
					feature_dim_2d = features_2d[i]->GetFeatDim();
					//cout << "feature Dimension: " << feature_dim_2d << endl;
				}
			}
		}
		cout << "done!" << endl << endl;;
		cout << "# of pixel feature 2d: " << feature_count_2d << "\t feature dimension(" << feature_dim_2d << ")" << endl;
	}
}

void SuperPixelImage::ConfidenceEstimation(cv::Mat& probability, cv::Mat& probability_index)
{
	int i, j, k;
	vector<int> superpixel_sample_count;
	int numclass = TestConfiguration.numOfClass;

	for (i = 0; i<superPixels.size(); i++)
	{
		superPixels.at(i).confidenceValue.clear();
		for (j = 0; j<numclass; j++)
		{
			superPixels.at(i).confidenceValue.push_back(0);
		}
		superpixel_sample_count.push_back(0);
	}

	int radius = 1;
	for (j = 0; j<probability_index.rows; j++)
	{
		int x = probability_index.at<float>(j, 0);
		int y = probability_index.at<float>(j, 1);
		int idx = superPixelMap->at<int>(y, x) - 1;
		if (idx >= 0)
		{
			/*avoid boundary pixel*/
			bool NearBoundary = false;
			for (int m = -radius; m <= radius; m++)
			{
				for (int n = -radius; n <= radius; n++)
				{
					if ((x + m) >= 0 && (x + m)< superPixelMap->cols &&
						(y + n) >= 0 && (y + n)< superPixelMap->rows)
					{
						if (superPixelMap->at<int>(y + n, x + m) <= 0)
						{
							NearBoundary = true;
							break;
						}
					}
				}
			}

			/*avoid boundary pixel*/
			if (NearBoundary == false)
			{
				for (k = 0; k<numclass; k++)
				{
					superPixels.at(idx).confidenceValue.at(k) += probability.at<float>(j, k);
				}
			}
			superpixel_sample_count.at(idx)++;
		}
	}

	for (i = 0; i<superPixels.size(); i++)
	{
		if (superpixel_sample_count.at(i)>0)
		{
			for (j = 0; j<numclass; j++)
			{
				superPixels.at(i).confidenceValue.at(j) = superPixels.at(i).confidenceValue.at(j) /
					superpixel_sample_count.at(i);
			}
		}
	}
}

bool SuperPixelImage::LoadSuperPixels()
{
	return BasicAPI::LoadMatrixFromFile(*superPixelMap, filenameSuperPixel.c_str());
}

void SuperPixelImage::ViewSegmentConfidenceMap()
{
	cout << "Generate confidence map..." << endl;
	vector<IplImage*> confidenceMap;
	int numclass = TestConfiguration.numOfClass;

	int i, j, k;
	for (k = 0; k<numclass; k++)
	{
		IplImage* temp = cvCreateImage(cvGetSize(ptImage), ptImage->depth, ptImage->nChannels);
		cvSetZero(temp);
		confidenceMap.push_back(temp);
	}

	string ptImagefile = TestConfiguration.interresultdir + "/" + ExtractFileName(filenameImage, true);
	cvSaveImage(ptImagefile.c_str(), ptImage);

	for (unsigned i = 0; i<superPixels.size(); i++)
	{
		for (k = 0; k<numclass; k++)
		{
			float w = superPixels.at(i).confidenceValue.at(k);
			int label = TestConfiguration.colorTable.GetCategoryLabel(k);
			int r = TestConfiguration.colorTable.GetCategoryColor(label).val[0] * w;
			int g = TestConfiguration.colorTable.GetCategoryColor(label).val[1] * w;
			int b = TestConfiguration.colorTable.GetCategoryColor(label).val[2] * w;

			for (j = 0; j<superPixels.at(i).pixels.size(); j++)
			{
				int x = superPixels.at(i).pixels.at(j).x;
				int y = superPixels.at(i).pixels.at(j).y;
				cvSet2D(confidenceMap[k], y, x, cvScalar(b, g, r));
			}
		}
	}

	for (k = 0; k<numclass; k++)
	{
		string confidenceMapName = TestConfiguration.interresultdir +
			ExtractFileName(filenameImage, false) + "_" +
			TestConfiguration.featureFlag + "_" +
			TestConfiguration.colorTable.GetCategoryName(k) + "_sp.png";

		cvSaveImage(confidenceMapName.c_str(), confidenceMap[k]);
		cvReleaseImage(&confidenceMap[k]);
	}
}

void SuperPixelImage::SegmentationEdgeRefine()
{
	cv::Mat segmentMap(segResult->height, segResult->width, CV_32SC1, cv::Scalar(0));
	for (int i = 0; i<segResult->height; i++)
	{
		for (int j = 0; j<segResult->width; j++)
		{
			cv::Scalar s = cvGet2D(segResult, i, j);
			int label = TestConfiguration.colorTable.FindLabel(s);
			if (label > 0)
			{
				segmentMap.at<int>(i, j) = label;
			}
		}
	}

	cv::Mat imageInput= cv::cvarrToMat(ptImage);
	/*edge refinement with watershed algorithm*/
	ShrinkEdge(segmentMap, 4); /*shrink edge*/
	RefineEdge(imageInput, segmentMap); /*expand edge with watershed*/

	cvSetZero(segResult);
	vector< vector<cv::Point> > connectedComponents;

	/*small region removal with watershed algorithm*/
	BasicAPI::ExtractConnectedComponent(segmentMap, 0, connectedComponents, false);
	for (unsigned int i = 0; i<connectedComponents.size(); i++)
	{
		for (unsigned int j = 0; j<connectedComponents[i].size(); j++)
		{
			int x = connectedComponents[i][j].x;
			int y = connectedComponents[i][j].y;
			int label = segmentMap.at<int>(y, x);
			cv::Scalar s = TestConfiguration.colorTable.GetCategoryColor(label);
			cvSet2D(segResult, y, x, s);
		}
	}
}

void SuperPixelImage::RefineEdge(cv::Mat& imageInput, cv::Mat& segmentMap)
{
	cv::Mat img = imageInput;
	cv::Mat markers(img.rows, img.cols, CV_32SC1, cv::Scalar(0));

	int r = 4;
	for (int i = 0; i < markers.rows; i += r)
	{
		for (int j = 0; j < markers.cols; j += r)
		{
			markers.at<int>(i, j) = segmentMap.at<int>(i, j);
		}
	}

	cv::watershed(img, markers);
	segmentMap = cv::Scalar(0);

	// paint the watershed image
	for (int i = 0; i < markers.rows; i++)
	{
		for (int j = 0; j < markers.cols; j++)
		{
			int index = markers.at<int>(i, j);
			if (index >= 0)
			{
				segmentMap.at<int>(i, j) = index;
			}
		}
	}
}

void SuperPixelImage::ShrinkEdge(cv::Mat& segmentMap, int bandW)
{
	for (int k = 0; k<bandW; k++)
	{
		for (int i = 1; i<segmentMap.rows - 1; i++)
		{
			for (int j = 1; j<segmentMap.cols - 1; j++)
			{
				int idx = segmentMap.at<int>(i, j);
				if (idx > 0)
				{
					for (int m = 0; m <= 1; m++)
					{
						for (int n = 0; n <= 1; n++)
						{
							int adjidx = segmentMap.at<int>(i + m, j + n);
							if (idx != adjidx)
							{
								segmentMap.at<int>(i, j) = 0;
								break;
							}
						}
					}
				}
			}
		}
	}
}

/*remove the small holes in the segmentation result*/
int SuperPixelImage::SegmentationPostProcessing(vector<int>& sizeConstraint)
{
	vector< vector<cv::Point> > Regions;
	vector< int > RegionLabels;

	cv::Mat flagMask(ptImage->height, ptImage->width, CV_32SC1, cv::Scalar(0));
	/*connected components*/
	vector<CvPoint> Stack;
	for (int i = 0; i<segResult->height; i++)
	{
		for (int j = 0; j<segResult->width; j++)
		{
			if (flagMask.at<int>(i, j) == 0)
			{
				Stack.clear();
				vector<cv::Point> Region;
				Stack.push_back(cv::Point(j, i));
				flagMask.at<int>(i, j) = 1;

				while (Stack.size()>0)
				{
					cv::Point p = Stack.at(Stack.size() - 1);
					Stack.pop_back();
					Region.push_back(p);

					for (int m = -1; m <= 1; m++)
					{
						for (int n = -1; n <= 1; n++)
						{
							if ((p.y + m) >= 0 && (p.y + m)<segResult->height &&
								(p.x + n) >= 0 && (p.x + n)<segResult->width)
							{
								if (flagMask.at<int>(p.y + m, p.x + n) == 0)
								{
									bool InsertFlag = true;
									for (int k = 0; k<segResult->nChannels; k++)
									{
										if (segResult->imageData[p.y*segResult->widthStep + p.x*segResult->nChannels + k] !=
											segResult->imageData[(p.y + m)*segResult->widthStep + (p.x + n)*segResult->nChannels + k])
										{
											InsertFlag = false;
											break;
										}
									}

									if (InsertFlag == true)
									{
										Stack.push_back(cv::Point(p.x + n, p.y + m));
										flagMask.at<int>(p.y + m, p.x + n) = 1;
									}
								}
							}
						}
					}
				}

				cv::Scalar s = cvGet2D(segResult, Region.at(Region.size() - 1).y, Region.at(Region.size() - 1).x);
				int regionLabel = TestConfiguration.colorTable.FindLabel(s);
				Regions.push_back(Region);
				RegionLabels.push_back(regionLabel);
			}
		}
	}

	cv::Mat segmentaionMap(ptImage->height, ptImage->width, CV_32SC1, cv::Scalar(0));
	for (int i = 0; i<Regions.size(); i++)
	{
		for (int j = 0; j<Regions.at(i).size(); j++)
		{
			int x = Regions.at(i).at(j).x;
			int y = Regions.at(i).at(j).y;
			segmentaionMap.at<int>(y, x) = i + 1;
		}
	}

	/*find adjacent regions*/
	vector< list<int> > adjRegions;
	for (unsigned int i = 0; i<Regions.size(); i++)
	{
		list<int> adjRegion;
		adjRegions.push_back(adjRegion);
	}

	for (unsigned int i = 0; i<ptImage->height; i++)
	{
		for (int j = 0; j<ptImage->width; j++)
		{
			int clusterIdx = segmentaionMap.at<int>(i, j);
			for (int m = -1; m <= 1; m++)
			{
				for (int n = -1; n <= 1; n++)
				{
					if (i + m >= 0 && i + m < ptImage->height && j + n >= 0 && j + n < ptImage->width)
					{
						int adjClusterIdx = segmentaionMap.at<int>(i + m, j + n);
						if (clusterIdx != adjClusterIdx)
						{
							adjRegions.at(clusterIdx - 1).push_back(adjClusterIdx);
						}
					}
				}
			}
		}
	}

	for (unsigned int i = 0; i<adjRegions.size(); i++)
	{
		adjRegions.at(i).sort();
		adjRegions.at(i).unique();
	}

	/*smooth small regions which is contained in another region*/
	for (unsigned int i = 0; i<Regions.size(); i++)
	{
		int selectedRegionIndex = -1;
		int numCandidateRegions = 0;
		int regionLabel = RegionLabels.at(i) - 1;
		int adjLabel = -1;
		if (regionLabel >= 0 && regionLabel < sizeConstraint.size())
		{
			if (Regions.at(i).size()< min(250, sizeConstraint.at(regionLabel)))
			{
				for (list<int>::iterator iter = adjRegions.at(i).begin(); iter != adjRegions.at(i).end(); iter++)
				{
					int idx = (*iter) - 1;
					adjLabel = RegionLabels.at(idx) - 1;
					/*a region that will be preserved*/
					if (Regions.at(idx).size() > sizeConstraint.at(adjLabel))
					{
						numCandidateRegions++;
						selectedRegionIndex = idx;
					}
				}

				/*if there is only one candidate for merging*/
				if (numCandidateRegions == 1)
				{
					for (int j = 0; j<Regions.at(i).size(); j++)
					{
						int x = Regions.at(i).at(j).x;
						int y = Regions.at(i).at(j).y;
						Regions.at(selectedRegionIndex).push_back(cv::Point(x, y));
					}
					Regions.at(i).clear();
				}
			}
		}
	}

	/*set object map*/
	vector<cv::Scalar> regionColorset;
	BasicAPI::GenerateRandomColorSet(Regions.size(), regionColorset);

	int numUpdatedRegions = 0;
	for (unsigned int i = 0; i<Regions.size(); i++)
	{
		int regionLabel = RegionLabels.at(i) - 1;
		if (regionLabel >= 0 && regionLabel < sizeConstraint.size())
		{
			if (Regions.at(i).size()< sizeConstraint.at(regionLabel))
			{
				for (int j = 0; j<Regions.at(i).size(); j++)
				{
					cvSet2D(segResult, Regions.at(i).at(j).y, Regions.at(i).at(j).x, cv::Scalar(255, 255, 255));
				}
			}
			else
			{
				cv::Scalar s = TestConfiguration.colorTable.GetCategoryColor(regionLabel + 1);
				for (int j = 0; j<Regions.at(i).size(); j++)
				{
					cvSet2D(segResultObject, Regions.at(i).at(j).y, Regions.at(i).at(j).x, regionColorset.at(i));
					cvSet2D(segResult, Regions.at(i).at(j).y, Regions.at(i).at(j).x, s);
				}
			}
		}
	}

	return numUpdatedRegions;
}

void SuperPixelImage::ViewSuperPixel()
{
	IplImage* viewImage = cvCreateImage(cvGetSize(ptImage), ptImage->depth, ptImage->nChannels);
	cvCopy(ptImage, viewImage);

	/*bow projection view*/
	vector< CvScalar > viewColorTable;
	int bit_per_channle = ceil(log2(numSuperPixel * 2 + 1) / 3.0);
	int bits_total = 3 * bit_per_channle;
	int color_num = 1 << bits_total;
	for (int c = 0; c < color_num; c++)
	{
		int r = 0;
		int g = 0;
		int b = 0;
		for (int k = 0; k < bits_total;)
		{
			b = (b << 1) + ((c >> k++) & 1);
			g = (g << 1) + ((c >> k++) & 1);
			r = (r << 1) + ((c >> k++) & 1);
		}
		r = r << (8 - bit_per_channle);
		g = g << (8 - bit_per_channle);
		b = b << (8 - bit_per_channle);

		cv::Scalar t;
		t.val[0] = b;
		t.val[1] = g;
		t.val[2] = r;
		viewColorTable.push_back(t);
	}

	double w = 0.75;

	for (int x = 0; x<ptImage->width; x++)
	{
		for (int y = 0; y<ptImage->height; y++)
		{
			int spIndex = superPixelMap->at<int>(y, x);
			CvScalar s1 = cvGet2D(viewImage, y, x);
			s1.val[0] = s1.val[0] * w + viewColorTable.at(spIndex - 1).val[0] * (1 - w);
			s1.val[1] = s1.val[1] * w + viewColorTable.at(spIndex - 1).val[1] * (1 - w);
			s1.val[2] = s1.val[2] * w + viewColorTable.at(spIndex - 1).val[2] * (1 - w);
			cvSet2D(viewImage, y, x, s1);
		}
	}

	string filename = TestConfiguration.rootpath + "/superpixel/" + ExtractFileName(filenameImage, false) + "_sp_seeds.jpg";
	cv::Mat image = cv::cvarrToMat(viewImage);
	
	cvSaveImage(filename.c_str(), viewImage);
	cout << filename << endl;

	cvReleaseImage(&viewImage);
}
