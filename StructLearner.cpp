#include "StructLearner.h"
#include <omp.h>
#include "Global.h"
using namespace BasicAPI;
//const double smallValue = 0.0001;
/*the lower bound of parametes value*/
double minValue = 1e-6;

StructLearner::StructLearner(int  setNumClass)
{
	// TODO Auto-generated constructor stub
	numClass = setNumClass;
	clearBoundary = false;
	loadLearnedWeight = true;
	RTClassifier = NULL;

	StructSVM_C = 1.0;
	SubGradient_C = 1.0;
	SubGradient_stepsize = 0.0;
	OnlinePA_C = 1.0;
	OnlinePA_initialC = 1.0;

	iterationCount = TestConfiguration.maxIterations;
	iterationIter = 0;
	convergedIter = -1;

	parameterFlag = "";
}

StructLearner::~StructLearner()
{
	// TODO Auto-generated destructor stub
	if (RTClassifier != NULL){
		delete RTClassifier;
		//RTClassifier = NULL;
	}

	//for (int i = 0; i<trainSamples.size(); i++){
	//	if (trainSamples[i] != NULL){
	//		delete trainSamples[i];
	//		//trainSamples[i] = NULL;
	//	}
	//}
	//trainSamples.clear();
}

double StructLearner::EmpericalRisk(vector<double>& sumPredictedStructureFeature,
	vector<double>& sumTrueStructureFeature,
	GraphParameters& learnedParameters)
{
	vector<double> weight;
	vector<weightType> weightFlag;
	learnedParameters.GetParameters(weight, weightFlag);

	double sumWeights = 0;
	double risk = 0;
	for (int i = 0; i<weight.size(); i++)
	{
		double d = sumTrueStructureFeature.at(i) - sumPredictedStructureFeature.at(i);
		risk += (weight.at(i) * d);
		sumWeights += (weight.at(i) *weight.at(i));
	}

	double a = 0;
	if (TestConfiguration.GraphModelTrainerType == TypeOnlinePA)
	{
		a = TestConfiguration.OnlinePA_C;
	}
	else if (TestConfiguration.GraphModelTrainerType == TypeSubGradient)
	{
		a = TestConfiguration.SubGradient_C;
	}
	else if (TestConfiguration.GraphModelTrainerType == TypeStructSVM)
	{
		a = TestConfiguration.StructSVM_C;
	}

	return a*risk + sumWeights / 2;
}

void StructLearner::Train()
{
	/*constraint format: w*x + slack >= b => [w,slack] * trans([x,1]) >= b*/
	/*numWeights + 1 for [w1,w2,...,wn,slack]*/
	/*add positive constraint*/

	parametersSets.clear();
	vector<double> weight;
	vector<weightType> weightFlag;
	learnedParameters.GetParameters(weight, weightFlag);
	numWeights = weight.size();

	parametersSets.push_back(learnedParameters);

	/*store the confusion matrix for each updated weight during the iteration*/
	vector<cv::Mat*> confusionMatrixSet;
	double sumDeltaLoss = 0;
	vector<double> sumPredictedStructureFeature;
	vector<double> sumTrueStructureFeature;

	vector<double> objectiveValues;
	for (int k = 0; k <= iterationCount; k++)
	{
		iterationIter = k;
		if (k<iterationCount) cout << endl << "Iteration " << k + 1 << endl;
		cv::Mat* confusionMatrix = new cv::Mat(numClass, numClass, CV_32SC1, cv::Scalar(0));
		GraphModel::AssignGraphParameters(learnedParameters);

		if (k<iterationCount) learnedParameters.Print();

		/*after the first iteration, the confidence saved*/
		if (k>0)
		{
			TestConfiguration.saveConfidenceMap = false;
			TestConfiguration.loadConfidenceMap = true;
		}

		/*generation new constraint*/
		FindNewConstraint(sumPredictedStructureFeature,
			sumTrueStructureFeature,
			sumDeltaLoss,
			confusionMatrix);

		/*store performance of current parameters*/
		confusionMatrixSet.push_back(confusionMatrix);

		double objectiveValue = EmpericalRisk(sumPredictedStructureFeature, sumTrueStructureFeature, learnedParameters) + sumDeltaLoss;
		objectiveValues.push_back(objectiveValue);

		if (TestConfiguration.localGraphTraining == false)
			RecordIteration(k, confusionMatrix, objectiveValue);

		/*the iteration: iterationCount will not included*/
		if (k<iterationCount)
		{
			PrintFeature(sumPredictedStructureFeature, sumTrueStructureFeature);

			/*update parameters*/
			bool changed = UpdateWeight(sumPredictedStructureFeature,
				sumTrueStructureFeature,
				sqrt(sumDeltaLoss));

			/*no update, then stop the learning process*/
			if (changed == false)
			{
				cout << "Learning converged ... " << endl;
				break;
			}
		}
	}

	ExtractOptimalWeight(confusionMatrixSet, objectiveValues);

	for (int k = 0; k<confusionMatrixSet.size(); k++)
	{
		delete confusionMatrixSet.at(k);
	}
}

void StructLearner::FindNewConstraint(GraphModel* singleTrainingSample,
	int sampleIndex,
	cv::Mat* confusionMatrix,
	vector< vector<double> >& predictedStructureFeatureSet,
	vector< vector<double> >& trueStructureFeatureSet,
	vector< double >& deltaLossSet)
{
	/*To reduce memory cost, the graph will be constucted when perform inference*/
	char temp[128];
	sprintf(temp, "%d", iterationIter);
	string tag(temp);

	if (TestConfiguration.GraphModelTrainerType == TypeOnlinePA)
		tag = tag + "_OnlinePA";
	else if (TestConfiguration.GraphModelTrainerType == TypeStructSVM)
		tag = tag + "_StructSVM";
	else if (TestConfiguration.GraphModelTrainerType == TypeSubGradient)
		tag = tag + "_SubGradient";

	bool validTrainingSample = true;

	/*to reduce the memory cost, the graph will be built and release for each single example*/
	if (withLowMemoryCost == true)
	{
		SuperPixelImage testImage;
		validTrainingSample = testImage.Initialization(&singleTrainingSample->inputImage,
			TestConfiguration.testLevel);
		if (validTrainingSample == true)
		{
			cv::Mat* prob = NULL;
			cv::Mat* probIndex = NULL;

			if (TestConfiguration.classifierName == "RT")
			{
				RTClassifier->TestBasicClassifiers(&testImage, prob, probIndex);
			}

			/*exclude the unlabeled region & boundary between categories from training process for evaluation*/
			if (clearBoundary == true)
			{
				/*clear image border*/
				for (int i = 0; i<testImage.labelMask->height; i++)
				{
					testImage.labelMap->at<int>(i, 0) = 0;
					testImage.labelMap->at<int>(i, testImage.labelMask->width - 1) = 0;
				}
				/*clear image border*/
				for (int j = 0; j<testImage.labelMask->width; j++)
				{
					testImage.labelMap->at<int>(0, j) = 0;
					testImage.labelMap->at<int>(testImage.labelMask->height - 1, j) = 0;
				}

				/*clear edges between different categories*/
				for (int i = 1; i<testImage.labelMask->height - 1; i++)
				{
					for (int j = 1; j<testImage.labelMask->width - 1; j++)
					{
						int label = testImage.labelMap->at<int>(i, j);
						if (label > 0)
						{
							for (int m = -1; m <= 1; m++)
							{
								for (int n = -1; n <= 1; n++)
								{
									if (testImage.labelMap->at<int>(i + m, j + n) > 0 &&
										testImage.labelMap->at<int>(i + m, j + n) != label)
									{
										testImage.labelMap->at<int>(i, j) = 0;
										break;
									}
								}

								if (testImage.labelMap->at<int>(i, j) == 0) break;
							}
						}
					}
				}
			}

			singleTrainingSample->SetupGraph(&testImage, prob, probIndex, NULL, NULL, NULL, NULL);

			if (prob != NULL) delete prob;
			if (probIndex != NULL) delete probIndex;
		}

		assert(validTrainingSample == true);
	}

	/*inference on training sample*/
	if (validTrainingSample == true)
	{
		if (TestConfiguration.parallel == true)
		{
#pragma omp critical
			FindConstraintLabel(singleTrainingSample);
		}
		else FindConstraintLabel(singleTrainingSample);

		ViewFoundConstraintLabel(singleTrainingSample, tag);

		/*if unlabeled region is actived
		*(GraphModel::withUnLabelRegion == true),
		*then all nodes will be active
		*and the extracted feature will include unlabeled region
		*then the unlabeled region should also be included in the label dependent
		*feature extraction for truth label*/
		vector<double> predictedStructureFeature;
		ExtractStructureFeature(singleTrainingSample, predictedStructureFeature, true);

		/*assign predicted label to the unlabeled region*/
		if (GraphModel::withUnLabelRegion == true)
			ProcessUnlabeledRegion(singleTrainingSample, true);

		vector<double> trueStructureFeature;
		ExtractStructureFeature(singleTrainingSample, trueStructureFeature, false);

		/*recover the unlabeled region*/
		if (GraphModel::withUnLabelRegion == true)
			ProcessUnlabeledRegion(singleTrainingSample, false);

		double deltaLoss = 0;
		deltaLoss = ComputeLoss(singleTrainingSample, confusionMatrix);

		deltaLossSet.at(sampleIndex) = deltaLoss;
		predictedStructureFeatureSet.at(sampleIndex) = predictedStructureFeature;
		trueStructureFeatureSet.at(sampleIndex) = trueStructureFeature;

		if (withLowMemoryCost == true)
		{
			singleTrainingSample->Release();
		}
	}
}

void StructLearner::FindNewConstraint(vector<double>& sumPredictedStructureFeature,
	vector<double>& sumTrueStructureFeature,
	double& sumDeltaLoss,
	cv::Mat* confusionMatrix)
{
	sumDeltaLoss = 0;
	sumPredictedStructureFeature.clear();
	sumTrueStructureFeature.clear();

	vector< vector<double> > predictedStructureFeatureSet;
	vector< vector<double> > trueStructureFeatureSet;
	vector< double > deltaLossSet;

	int i = 0;
	for (i = 0; i<trainSamples.size(); i++)
	{
		vector<double> v;
		predictedStructureFeatureSet.push_back(v);
		trueStructureFeatureSet.push_back(v);
		deltaLossSet.push_back(0);
	}

	if (TestConfiguration.parallel == true)
	{
		omp_set_num_threads(TestConfiguration.numThreads);
#pragma omp parallel for
		for (i = 0; i<trainSamples.size(); i++)
		{
			cout << "train sample:" << i + 1 << "/" << trainSamples.size() << endl;
			FindNewConstraint(trainSamples.at(i), i, confusionMatrix,
				predictedStructureFeatureSet,
				trueStructureFeatureSet,
				deltaLossSet);
		}
	}
	else
	{
		for (i = 0; i<trainSamples.size(); i++)
		{
			cout << "train sample:" << i + 1 << "/" << trainSamples.size() << endl;
			FindNewConstraint(trainSamples.at(i), i, confusionMatrix,
				predictedStructureFeatureSet,
				trueStructureFeatureSet,
				deltaLossSet);
		}
	}

	for (i = 0; i<trainSamples.size(); i++)
	{
		sumDeltaLoss += deltaLossSet.at(i);
		if (sumPredictedStructureFeature.size() == 0 &&
			sumTrueStructureFeature.size() == 0)
		{
			sumPredictedStructureFeature = predictedStructureFeatureSet.at(i);
			sumTrueStructureFeature = trueStructureFeatureSet.at(i);
		}
		else
		{
			for (int j = 0; j<trueStructureFeatureSet.at(i).size(); j++)
			{
				sumTrueStructureFeature.at(j) += trueStructureFeatureSet.at(i).at(j);
			}

			for (int j = 0; j<predictedStructureFeatureSet.at(i).size(); j++)
			{
				sumPredictedStructureFeature.at(j) += predictedStructureFeatureSet.at(i).at(j);
			}
		}
	}
}


/*assign the unlabeled region with prediction, if assignPrediction == true*/
void StructLearner::ProcessUnlabeledRegion(GraphModel* sample, bool assignPrediction)
{
	for (unsigned int i = 0; i<sample->Layer.size(); i++)
	{
		if (sample->Layer.at(i)->withLabel == false)
		{
			if (assignPrediction == true)
				sample->Layer.at(i)->trueLabel = sample->Layer.at(i)->predictLabel;
			else
				sample->Layer.at(i)->trueLabel = -1;
		}
	}

	for (unsigned int i = 0; i<sample->upLayer.size(); i++)
	{
		if (sample->upLayer.at(i)->withLabel == false)
		{
			if (assignPrediction == true)
				sample->upLayer.at(i)->trueLabel = sample->upLayer.at(i)->predictLabel;
			else
				sample->upLayer.at(i)->trueLabel = -1;
		}
	}
}

void StructLearner::Evaluation(cv::Mat* confusionMatrix,
	double& recall,
	double& averagePrecision,
	double& averageRecall)
{
	/*overall recall*/
	double sampleCount = 0;
	double recallCount = 0;
	for (unsigned int i = 0; i<numClass; i++)
	{
		for (int j = 0; j<numClass; j++)
		{
			sampleCount += confusionMatrix->at<int>(i, j);
		}
		recallCount += confusionMatrix->at<int>(i, i);
	}
	recall = recallCount / sampleCount;

	/*category average recall*/
	averageRecall = 0;
	for (unsigned int i = 0; i<numClass; i++)
	{
		double categorySampleCount = 0;
		double categoryrecallCount = 0;
		for (int j = 0; j<numClass; j++)
		{
			categorySampleCount += confusionMatrix->at<int>(i, j);
		}
		recallCount = confusionMatrix->at<int>(i, i);

		averageRecall += (recallCount / (categorySampleCount + 1e-8));
	}
	averageRecall /= numClass;

	/*category average precision*/
	averagePrecision = 0;
	for (unsigned int i = 0; i<numClass; i++)
	{
		double categorySampleCount = 0;
		double categoryrecallCount = 0;
		for (int j = 0; j<numClass; j++)
		{
			categorySampleCount += confusionMatrix->at<int>(j, i);
		}
		recallCount = confusionMatrix->at<int>(i, i);

		averagePrecision += (recallCount / (categorySampleCount + 1e-8));
	}
	averagePrecision /= numClass;

	cout << endl << "evaluation: " << endl;
	cout << "overall recall: " << recall << endl;
	cout << "category average recall: " << averageRecall << endl;
	cout << "category average precision: " << averagePrecision << endl;
}

/* generate structure samples from images*/
/* partially labeled training samples: the unlabeled region will be excluded*/
/* testing samples: include all regions*/
void StructLearner::ConstructStructExample(SuperPixelImage* image,
	GraphModel* structExample,
	cv::Mat* layerProb,
	cv::Mat* layerProbIndex,
	cv::Mat* uplayerProb,
	cv::Mat* uplayerProbIndex,
	cv::Mat* superlayerProb,
	cv::Mat* superlayerProbIndex)
{
	structExample->SetupGraph(image, layerProb, layerProbIndex,
		uplayerProb, uplayerProbIndex,
		superlayerProb, superlayerProbIndex);
}

void StructLearner::Validate(GraphModel* sample)
{
	FindConstraintLabel(sample);
}

void StructLearner::Initialization()
{
	if (TestConfiguration.classifierName == "RT")
	{
		RTClassifier = new ImageParser();
	}

	if (TestConfiguration.loadConfidenceMap == false)
	{
		if (TestConfiguration.classifierName == "RT")
		{
			RTClassifier->TestInitialization();
		}
	}

	InitialParameters();
}

void StructLearner::InitialParameters()
{
	StructLearner::loadLearnedWeight = TestConfiguration.loadLearnedWeight;    // load initialization
	StructLearner::withLowMemoryCost = true;

	initialParameters.SetDefaultParameters();
	learnedParameters = initialParameters;

	if (StructLearner::loadLearnedWeight == true)
	{
		string weight_file_name = ModelParameterFilename();
		cout << weight_file_name << endl;
		assert(LoadLearnedWeight(weight_file_name) == true);/*also update the learnedParameters*/
	}

	GraphModel::AssignGraphParameters(initialParameters);
}

double StructLearner::ComputeLoss(GraphModel* sample, cv::Mat* confusionMatrix)
{
	double lossValue = 0;
	if (GraphModel::withLayerUnary == true)
	{
		for (unsigned int i = 0; i<sample->Layer.size(); i++)
		{
			if (sample->Layer.at(i)->isActive == true && sample->Layer.at(i)->trueLabel > 0)
			{
				if (sample->Layer.at(i)->trueLabel != sample->Layer.at(i)->predictLabel)
				{
					lossValue += UnitLossCost;
				}

				if (confusionMatrix != NULL)
				{
					if (sample->Layer.at(i)->predictLabel>0)
					{
						confusionMatrix->at<int>(sample->Layer.at(i)->trueLabel - 1,
							sample->Layer.at(i)->predictLabel - 1) += 1;
					}
				}
			}
		}
	}

	if (GraphModel::withLayerUnary == true)
	{
		for (unsigned int i = 0; i<sample->upLayer.size(); i++)
		{
			if (sample->upLayer.at(i)->trueLabel > 0 && sample->upLayer.at(i)->isActive == true)
			{
				for (int j = 0; j<sample->upLayer.at(i)->layerlinks.size(); j++)
				{
					int subLabel = sample->upLayer.at(i)->layerlinks.at(j).linkedNode->predictLabel;
					if (sample->upLayer.at(i)->trueLabel != subLabel)
					{
						lossValue += UnitLossCost;
					}
				}
			}
		}
	}

	return lossValue + 1e-6;
}

double StructLearner::ComputeLoss(vector<GraphModel*>& sampleSet)
{
	double lossValue = 0;
	for (int k = 0; k<sampleSet.size(); k++)
	{
		for (unsigned int i = 0; i<sampleSet.at(k)->Layer.size(); i++)
		{
			if (sampleSet.at(k)->Layer.at(i)->isActive == true)
			{
				if (sampleSet.at(k)->Layer.at(i)->trueLabel != sampleSet.at(k)->Layer.at(i)->predictLabel &&
					sampleSet.at(k)->Layer.at(i)->trueLabel > 0)
				{
					lossValue += 1;
				}
			}
		}
	}
	return lossValue + 1e-6;
}

void StructLearner::PrintFeature(vector<double>& predictedStructureFeature,
	vector<double>& trueStructureFeature)
{
	double unary = 0;
	double smooth = 0;

	vector<double> weight;
	vector<weightType> weightFlag;
	learnedParameters.GetParameters(weight, weightFlag);

	int s = learnedParameters.LayerNodesWeights.GetSize() +
		learnedParameters.upLayerNodesWeights.GetSize() +
		learnedParameters.superLayerNodesWeights.GetSize();

	for (unsigned int i = 0; i<s; i++)
	{
		unary += weight.at(i)*predictedStructureFeature.at(i);
	}

	for (unsigned int i = s; i<weight.size(); i++)
	{
		smooth += weight.at(i)*predictedStructureFeature.at(i);
	}
	cout << "energy : predict-unary " << unary << " / predict-smooth " << smooth << endl;

	cout << endl << "Debug Print: prediction/truth feature vector " << endl;

	int startIndex = 0;
	if (learnedParameters.LayerNodesWeights.GetSize()>0)
	{
		learnedParameters.LayerNodesWeights.PrintStatus("layer unary : ");
		for (int j = 0; j<learnedParameters.LayerNodesWeights.GetSize(); j++) cout << predictedStructureFeature.at(j + startIndex) << " ";
		cout << endl;
		for (int j = 0; j<learnedParameters.LayerNodesWeights.GetSize(); j++) cout << trueStructureFeature.at(j + startIndex) << " ";
		cout << endl;

		startIndex += learnedParameters.LayerNodesWeights.GetSize();
	}

	if (learnedParameters.upLayerNodesWeights.GetSize()>0)
	{
		learnedParameters.upLayerNodesWeights.PrintStatus("uplayer unary :");
		for (int j = 0; j<learnedParameters.upLayerNodesWeights.GetSize(); j++) cout << predictedStructureFeature.at(j + startIndex) << " ";
		cout << endl;
		for (int j = 0; j<learnedParameters.upLayerNodesWeights.GetSize(); j++) cout << trueStructureFeature.at(j + startIndex) << " ";
		cout << endl;
		startIndex += learnedParameters.upLayerNodesWeights.GetSize();
	}

	if (learnedParameters.superLayerNodesWeights.GetSize()>0)
	{
		learnedParameters.superLayerNodesWeights.PrintStatus("super layer unary : ");
		for (int j = 0; j<learnedParameters.superLayerNodesWeights.GetSize(); j++) cout << predictedStructureFeature.at(j + startIndex) << " ";
		cout << endl;
		for (int j = 0; j<learnedParameters.superLayerNodesWeights.GetSize(); j++) cout << trueStructureFeature.at(j + startIndex) << " ";
		cout << endl;

		startIndex += learnedParameters.superLayerNodesWeights.GetSize();
	}

	if (learnedParameters.LayerLinksWeights.GetSize()>0)
	{
		learnedParameters.LayerLinksWeights.PrintStatus("layer link :");
		for (int j = 0; j<learnedParameters.LayerLinksWeights.GetSize(); j++) cout << predictedStructureFeature.at(j + startIndex) << " ";
		cout << endl;
		for (int j = 0; j<learnedParameters.LayerLinksWeights.GetSize(); j++) cout << trueStructureFeature.at(j + startIndex) << " ";
		cout << endl;
		startIndex += learnedParameters.LayerLinksWeights.GetSize();
	}

	if (learnedParameters.upLayerLinksWeights.GetSize()>0)
	{
		learnedParameters.upLayerLinksWeights.PrintStatus("uplayer link : ");
		for (int j = 0; j<learnedParameters.upLayerLinksWeights.GetSize(); j++) cout << predictedStructureFeature.at(j + startIndex) << " ";
		cout << endl;
		for (int j = 0; j<learnedParameters.upLayerLinksWeights.GetSize(); j++) cout << trueStructureFeature.at(j + startIndex) << " ";
		cout << endl;

		startIndex += learnedParameters.upLayerLinksWeights.GetSize();
	}

	if (learnedParameters.constraintWeights.GetSize()>0)
	{
		learnedParameters.constraintWeights.PrintStatus("constraint weight : ");

		for (int j = 0; j<learnedParameters.constraintWeights.GetSize(); j++) cout << predictedStructureFeature.at(j + startIndex) << " ";
		cout << endl;
		for (int j = 0; j<learnedParameters.constraintWeights.GetSize(); j++) cout << trueStructureFeature.at(j + startIndex) << " ";
		cout << endl;

		startIndex += learnedParameters.constraintWeights.GetSize();
	}

	if (learnedParameters.RobustPnWeights.GetSize()>0)
	{
		learnedParameters.constraintWeights.PrintStatus("RobustPn weight : ");

		for (int j = 0; j<learnedParameters.RobustPnWeights.GetSize(); j++) cout << predictedStructureFeature.at(j + startIndex) << " ";
		cout << endl;
		for (int j = 0; j<learnedParameters.RobustPnWeights.GetSize(); j++) cout << trueStructureFeature.at(j + startIndex) << " ";
		cout << endl;
	}
}

void StructLearner::ExtractUnaryFeature(vector<GraphNode*>& layer,
	cv::Mat& feat_unary,
	bool usePrediction)
{
	/*layer unary potential*/
	for (unsigned int i = 0; i<layer.size(); i++)
	{
		int label = layer.at(i)->trueLabel - 1;
		if (usePrediction == true) label = layer.at(i)->predictLabel - 1;

		/*only consider active nodes*/
		if (label >= 0 && layer.at(i)->isActive == true)
		{
			if (feat_unary.cols == numClass)
				feat_unary.at<float>(0, label) += layer.at(i)->featVector.at(label);
			else if (feat_unary.cols == 1)
				feat_unary.at<float>(0, 0) += layer.at(i)->featVector.at(label);
		}
	}
}

void StructLearner::ExtractRobustPnFeature(vector<GraphNode*>& layer,
	cv::Mat& feat_RobustPn,
	bool usePrediction)
{
	vector<NodeLink>* ptNodeLink;

	/*pairwise potential*/
	for (unsigned int i = 0; i<layer.size(); i++)
	{
		int label = layer.at(i)->trueLabel - 1;
		if (usePrediction == true) label = layer.at(i)->predictLabel - 1;

		if (layer.at(i)->isActive == true)
		{
			/*unary term*/
			if (label > GraphModel::numClass)
			{
				feat_RobustPn.at<float>(0, 0) += layer.at(i)->size;
			}
			else
			{
				feat_RobustPn.at<float>(0, 0) += layer.at(i)->size * (1 - GraphModel::robustPnAllowedRatio);
			}

			if (label >= 0 && label < GraphModel::numClass)
			{
				ptNodeLink = &layer.at(i)->layerlinks;
				for (int j = 0; j<ptNodeLink->size(); j++)
				{
					int adjLabel = ptNodeLink->at(j).linkedNode->trueLabel - 1;
					if (usePrediction == true) adjLabel = ptNodeLink->at(j).linkedNode->predictLabel - 1;
					double v = GraphModel::robustPnAllowedRatio / 2;

					if (label != adjLabel && adjLabel >= 0 &&
						ptNodeLink->at(j).linkedNode->isActive == true)
					{
						feat_RobustPn.at<float>(0, 0) += v;
					}
				}
			}
		}
	}
}

void StructLearner::ExtractConstraintFeature(vector<GraphNode*>& layer,
	cv::Mat& feat_constraint,
	bool usePrediction)
{
	vector<NodeLink>* ptNodeLink;
	/*pairwise potential*/
	for (unsigned int i = 0; i<layer.size(); i++)
	{
		int label = layer.at(i)->trueLabel - 1;
		if (usePrediction == true) label = layer.at(i)->predictLabel - 1;

		if (label >= 0 && layer.at(i)->isActive == true)
		{
			ptNodeLink = &layer.at(i)->constraintLayerlinks;

			for (int j = 0; j<ptNodeLink->size(); j++)
			{
				int adjLabel = ptNodeLink->at(j).linkedNode->trueLabel - 1;
				if (usePrediction == true) adjLabel = ptNodeLink->at(j).linkedNode->predictLabel - 1;
				double v = ptNodeLink->at(j).w / 2;

				if (label != adjLabel && adjLabel >= 0 &&
					ptNodeLink->at(j).linkedNode->isActive == true)
				{
					assert(GraphModel::constraintTable->at<int>(label, adjLabel) == 1 ||
						GraphModel::constraintTable->at<int>(label, adjLabel) == 0);

					if (learnedParameters.constraintWeights.paraSizeCol == 1)
					{
						feat_constraint.at<float>(0, 0) +=
							(1 - GraphModel::constraintTable->at<int>(label, adjLabel))*v;
						feat_constraint.at<float>(0, 0) +=
							(1 - GraphModel::constraintTable->at<int>(label, adjLabel))*v;
					}
					else if (learnedParameters.constraintWeights.paraSizeCol == numClass)
					{
						feat_constraint.at<float>(0, label) +=
							(1 - GraphModel::constraintTable->at<int>(label, adjLabel))*v;
						feat_constraint.at<float>(0, adjLabel) +=
							(1 - GraphModel::constraintTable->at<int>(label, adjLabel))*v;
					}
				}
			}
		}
	}
}

void StructLearner::ExtractPairwiseFeature(vector<GraphNode*>& layer,
	cv::Mat& feat_pairwise,
	int linkType, bool usePrediction)
{
	vector<NodeLink>* ptNodeLink;
	ptNodeLink = new vector<NodeLink>();

	int c = 0;
	/*pairwise potential*/
	for (unsigned int i = 0; i<layer.size(); i++)
	{
		int label = layer.at(i)->trueLabel - 1;
		if (usePrediction == true) label = layer.at(i)->predictLabel - 1;

		if (label >= 0 && layer.at(i)->isActive == true)
		{
			if (linkType == TypeLayerNode) ptNodeLink = &layer.at(i)->layerlinks;
			else if (linkType == TypeUpLayerNode) ptNodeLink = &layer.at(i)->upLayerlinks;
			else if (linkType == TypeSuperLayerNode) ptNodeLink = &layer.at(i)->superLayerlinks;

			int paraSizeRow = 0;
			if (linkType == TypeLayerNode)
				paraSizeRow = learnedParameters.LayerLinksWeights.paraSizeRow;
			else if (linkType == TypeUpLayerNode)
				paraSizeRow = learnedParameters.upLayerLinksWeights.paraSizeRow;
			else if (linkType == TypeSuperLayerNode)
				paraSizeRow = learnedParameters.superLayerLinksWeights.paraSizeRow;

			for (int j = 0; j<ptNodeLink->size(); j++)
			{
				int adjLabel = ptNodeLink->at(j).linkedNode->trueLabel - 1;
				if (usePrediction == true) adjLabel = ptNodeLink->at(j).linkedNode->predictLabel - 1;
				double v = ptNodeLink->at(j).w / 2.0;

				if (label != adjLabel && adjLabel >= 0 &&
					ptNodeLink->at(j).linkedNode->isActive == true)
				{
					if (paraSizeRow == numClass)
					{
						feat_pairwise.at<float>(label, adjLabel) += v;
						feat_pairwise.at<float>(adjLabel, label) += v;
					}
					else if (paraSizeRow == 1)
					{
						feat_pairwise.at<float>(0, adjLabel) += v;
						feat_pairwise.at<float>(0, label) += v;
					}
				}
			}
		}
	}
}

void StructLearner::ExtractStructureFeature(GraphModel* sample, vector<double>& structureFeature,
	bool usePrediction)
{
	/*accumulate feature vector of different types of potentials*/
	/*must be consistence with the order of parameters*/
	structureFeature.clear();

	if (learnedParameters.LayerNodesWeights.GetSize()>0)
	{
		cv::Mat feat_Layer_unary(learnedParameters.LayerNodesWeights.paraSizeRow,
			learnedParameters.LayerNodesWeights.paraSizeCol,
			CV_32FC1, cv::Scalar(0));

		ExtractUnaryFeature(sample->Layer, feat_Layer_unary, usePrediction);

		for (int i = 0; i<feat_Layer_unary.rows; i++)
		{
			for (int j = 0; j<feat_Layer_unary.cols; j++)
			{
				structureFeature.push_back(feat_Layer_unary.at<float>(i, j));
			}
		}
	}

	if (learnedParameters.upLayerNodesWeights.GetSize()>0)
	{
		cv::Mat feat_upLayer_unary(learnedParameters.upLayerNodesWeights.paraSizeRow,
			learnedParameters.upLayerNodesWeights.paraSizeCol,
			CV_32FC1, cv::Scalar(0));

		ExtractUnaryFeature(sample->upLayer, feat_upLayer_unary, usePrediction);

		for (int i = 0; i<feat_upLayer_unary.rows; i++)
		{
			for (int j = 0; j<feat_upLayer_unary.cols; j++)
			{
				structureFeature.push_back(feat_upLayer_unary.at<float>(i, j));
			}
		}
	}

	if (learnedParameters.superLayerNodesWeights.GetSize()>0)
	{
		cv::Mat feat_superLayer_unary(learnedParameters.superLayerNodesWeights.paraSizeRow,
			learnedParameters.superLayerNodesWeights.paraSizeCol,
			CV_32FC1, cv::Scalar(0));

		ExtractUnaryFeature(sample->superLayer, feat_superLayer_unary, usePrediction);

		for (int i = 0; i<feat_superLayer_unary.rows; i++)
		{
			for (int j = 0; j<feat_superLayer_unary.cols; j++)
			{
				structureFeature.push_back(feat_superLayer_unary.at<float>(i, j));
			}
		}
	}

	if (learnedParameters.LayerLinksWeights.GetSize()>0)
	{
		cv::Mat feat_Layer_pairwise(learnedParameters.LayerLinksWeights.paraSizeRow,
			learnedParameters.LayerLinksWeights.paraSizeCol,
			CV_32FC1, cv::Scalar(0));

		ExtractPairwiseFeature(sample->Layer, feat_Layer_pairwise, TypeLayerNode, usePrediction);
		for (int i = 0; i<feat_Layer_pairwise.rows; i++)
		{
			for (int j = 0; j<feat_Layer_pairwise.cols; j++)
			{
				structureFeature.push_back(feat_Layer_pairwise.at<float>(i, j));
			}
		}
	}

	if (learnedParameters.upLayerLinksWeights.GetSize()>0)
	{
		cv::Mat feat_upLayer_pairwise(learnedParameters.upLayerLinksWeights.paraSizeRow,
			learnedParameters.upLayerLinksWeights.paraSizeCol,
			CV_32FC1, cv::Scalar(0));

		ExtractPairwiseFeature(sample->upLayer, feat_upLayer_pairwise, TypeUpLayerNode, usePrediction);
		for (int i = 0; i<feat_upLayer_pairwise.rows; i++)
		{
			for (int j = 0; j<feat_upLayer_pairwise.cols; j++)
			{
				structureFeature.push_back(feat_upLayer_pairwise.at<float>(i, j));
			}
		}
	}

	if (learnedParameters.constraintWeights.GetSize()>0)
	{
		cv::Mat feat_constraint(learnedParameters.constraintWeights.paraSizeRow,
			learnedParameters.constraintWeights.paraSizeCol,
			CV_32FC1, cv::Scalar(0));

		if (usePrediction == true) ExtractConstraintFeature(sample->Layer, feat_constraint, usePrediction);

		for (int i = 0; i<feat_constraint.rows; i++)
		{
			for (int j = 0; j<feat_constraint.cols; j++)
			{
				structureFeature.push_back(feat_constraint.at<float>(i, j));
			}
		}
	}

	if (learnedParameters.RobustPnWeights.GetSize()>0)
	{
		cv::Mat feat_RobustPn(learnedParameters.RobustPnWeights.paraSizeRow,
			learnedParameters.RobustPnWeights.paraSizeCol,
			CV_32FC1, cv::Scalar(0));

		ExtractRobustPnFeature(sample->upLayer, feat_RobustPn, usePrediction);

		for (int i = 0; i<feat_RobustPn.rows; i++)
		{
			for (int j = 0; j<feat_RobustPn.cols; j++)
			{
				structureFeature.push_back(feat_RobustPn.at<float>(i, j));
			}
		}
	}
}

void StructLearner::FindConstraintLabel(GraphModel* sample)
{
	/*using ground truth as the initialization*/
	if (TestConfiguration.GraphModelTrainerType == TypeOnlinePA)
		sample->MAPInference(true, false);
	else
		sample->MAPInference(false, true);
}

void StructLearner::ViewFoundConstraintLabel(GraphModel* sample, string tag)
{
	int width = sample->width;
	int height = sample->height;

	if (GraphModel::withLayerUnary == true)
	{
		IplImage* testView = cvCreateImage(cvSize(width + 1, height + 1), 8, 3);
		cvSetZero(testView);
		for (unsigned int i = 0; i<sample->Layer.size(); i++)
		{
			int label = sample->Layer.at(i)->predictLabel;
			if (label > 0)
			{
				cvSet2D(testView, sample->Layer.at(i)->y, sample->Layer.at(i)->x, TestConfiguration.colorTable.GetCategoryColor(label));
			}
		}

		string imagefilename = TestConfiguration.rootpath + "/test/" + ExtractFileName(sample->inputImage.filenameImage, false) + "_" + tag + "_parsing.jpg";
		cvSaveImage(imagefilename.c_str(), testView);

		if (tag == "0")
		{
			cvSetZero(testView);
			for (unsigned int i = 0; i<sample->Layer.size(); i++)
			{
				int label = sample->Layer.at(i)->trueLabel;
				if (label > 0)
				{
					cvSet2D(testView, sample->Layer.at(i)->y, sample->Layer.at(i)->x, TestConfiguration.colorTable.GetCategoryColor(label));
				}
			}
			imagefilename = "test/" + ExtractFileName(sample->inputImage.filenameImage, false) + "_groundtruth.jpg";
			cvSaveImage(imagefilename.c_str(), testView);
		}
		cvReleaseImage(&testView);
	}

	if (GraphModel::withUpLayerUnary == true)
	{
		IplImage* testView = cvCreateImage(cvSize(width + 1, height + 1), 8, 3);
		cvSetZero(testView);
		for (unsigned int i = 0; i<sample->upLayer.size(); i++)
		{
			int label = sample->upLayer.at(i)->predictLabel;
			if (label > 0)
			{
				//				for(int j=0;j<sample->upLayer.at(i)->subNodes.size();j++)
				//				{
				//					int x = sample->upLayer.at(i)->subNodes.at(j).x;
				//					int y = sample->upLayer.at(i)->subNodes.at(j).y;
				//					cvSet2D(testView,y,x,cvScalar(b,g,r));
				//				}
			}
		}

		string imagefilename = TestConfiguration.rootpath + "/test/" + ExtractFileName(sample->inputImage.filenameImage, false) + "_" + tag + "_sp_parsing.jpg";
		cvSaveImage(imagefilename.c_str(), testView);

		if (tag == "0")
		{
			cvSetZero(testView);
			for (unsigned int i = 0; i<sample->upLayer.size(); i++)
			{
				int label = sample->upLayer.at(i)->trueLabel;
				if (label > 0)
				{
					//					for(int j=0;j<sample->upLayer.at(i)->subNodes.size();j++)
					//					{
					//						int x = sample->upLayer.at(i)->subNodes.at(j).x;
					//						int y = sample->upLayer.at(i)->subNodes.at(j).y;
					//						cvSet2D(testView,y,x,cvScalar(b,g,r));
					//					}
				}
			}
			imagefilename = "test/" + ExtractFileName(sample->inputImage.filenameImage, false) +
				"_sp_groundtruth.jpg";
			cvSaveImage(imagefilename.c_str(), testView);
		}

		cvReleaseImage(&testView);
	}
}

void  StructLearner::SaveLearnedWeight(string weight_file_name, double objectiveValue)
{
	ofstream out;
	out.open(weight_file_name.c_str());

	/*header of confidence file*/
	out << "*model multi-class: " << numClass << endl;
	out << "*category-order: ";
	for (unsigned int i = 0; i<TestConfiguration.colorTable.GetNumberOfCategory(); i++)
	{
		out << TestConfiguration.colorTable.GetCategoryName(i) << " ";
	}
	out << endl;

	out << "*training initialization: ";
	if (TestConfiguration.randomInitialization == true)
		out << "random" << endl;
	else out << "normal" << endl;
	//out<<"with-unlabel: "<<TestConfiguration.withUnLabelRegion<<endl;
	//out<<"*C= "<<initialC<<endl;

	out << "*training converged: " << convergedIter << "/" << iterationCount << endl;
	out << "*training recall: " << initialRecall << "/" << bestRecall << endl;
	out << "*training average recall: " << initialAverageRecall << "/" << bestAverageRecall << endl;
	out << "*value of objective function: " << objectiveValue << endl;
	GraphModel::weights.WriteToFile(out);
	out.close();
}

string StructLearner::ModelParameterFilename(string extraTag)
{
	string learnerTag = "";
	std::ostringstream ss;
	//ss.setf(ios::fixed, ios::floatfield);
	if (TestConfiguration.GraphModelTrainerType == TypeOnlinePA)
	{
		learnerTag = "OnlinePA";
		ss << TestConfiguration.OnlinePA_C;
		string para = ss.str();
		learnerTag = learnerTag + "_C=" + para;
	}
	else if (TestConfiguration.GraphModelTrainerType == TypeStructSVM)
	{
		learnerTag = "StructSVM";
		ss << TestConfiguration.StructSVM_C;
		string para = ss.str();
		learnerTag = learnerTag + "_C=" + para;
	}
	else if (TestConfiguration.GraphModelTrainerType == TypeSubGradient)
	{
		std::ostringstream ss1;
		std::ostringstream ss2;
		learnerTag = "SubGradient";
		ss1 << TestConfiguration.SubGradient_stepsize;
		string para1 = ss1.str();
		ss2 << TestConfiguration.SubGradient_C;
		string para2 = ss2.str();
		learnerTag = learnerTag + "_S=" + para1 + "_C=" + para2;
	}

	/*max number of iterations*/
	char temp[64];
	sprintf(temp, "%d", TestConfiguration.maxIterations);
	string str_maxIterations(temp);
	learnerTag = learnerTag + '_' + str_maxIterations;

	if (extraTag != "") learnerTag = learnerTag + '_' + extraTag;

	if (TestConfiguration.localGraphTraining == true)
	{
		learnerTag = learnerTag + '_' + parameterFlag;
	}

	string typeFlag = TestConfiguration.GetFeatureTag(false);
	string weight_file_name = TestConfiguration.modeldir +
		TestConfiguration.datasetName + "_" +
		TestConfiguration.classifierName + "_" +
		typeFlag + "_" + learnerTag + "_CRF.weight";

	return weight_file_name;
}

bool StructLearner::LoadLearnedWeight(string weight_file_name)
{
	ifstream input;
	input.open(weight_file_name.c_str());

	if (!input)
	{
		input.close();
		cout << "load file: " << weight_file_name << " failed" << endl;
		return false;
	}

	int lineLength = 1024;
	char temp[1024];

	/*load CRF mode: binary / multi-class*/
	input.getline(temp, lineLength);
	string textLine(temp);
	if (temp[0] == '*')
	{
		vector<string> textElement;
		TextlineSplit(textLine, textElement, ' ');
		assert(numClass == atoi(textElement.at(2).c_str()));
	}

	input.getline(temp, lineLength);
	while (temp[0] == '*')
	{
		input.getline(temp, lineLength);
	}

	//    input.getline(temp,lineLength);
	//    input.getline(temp,lineLength);
	//    /*load learning parameter*/
	//    if(temp[0] == '*')
	//    {
	//    	vector<string> textElement;
	//    	TextlineSplit(textLine,textElement,' ');
	//    	initialC = atof(textElement.at(1).c_str());
	//    }
	//    input.getline(temp,lineLength);
	//    input.getline(temp,lineLength);
	//    input.getline(temp,lineLength);

	initialParameters.LoadFromFile(input);
	learnedParameters = initialParameters;
	learnedParameters.Print();

	return true;
}

void StructLearner::RecordIteration(int iterationIndex, cv::Mat* confusionMatrix, double objectiveValue)
{
	char temp[128];
	sprintf(temp, "%d", iterationIter);
	string tag(temp);

	/*evaluation & save current parameters*/
	double recall;
	double averagePrecision;
	double averageRecall;
	Evaluation(confusionMatrix, recall, averagePrecision, averageRecall);

	if (iterationIndex == 0)
	{
		initialRecall = recall;
		initialAverageRecall = averageRecall;
	}

	bestRecall = recall;
	bestAverageRecall = averageRecall;
	convergedIter = iterationIndex;

	string weight_file_name = ModelParameterFilename(tag);
	SaveLearnedWeight(weight_file_name, objectiveValue);
}

void StructLearner::ExtractOptimalWeight(vector<cv::Mat*>& confusionMatrixSet, vector<double>& risks)
{
	int bestWeightIndex = 0;
	if (TestConfiguration.GraphModelTrainerType == TypeOnlinePA)
	{
		vector<double> recallSet;
		vector<double> averagePrecisionSet;
		vector<double> averageRecallSet;

		double recall = 0;
		double averagePrecision;
		double averageRecall;
		for (int k = 0; k<confusionMatrixSet.size(); k++)
		{
			cout << endl << k << "th evaluation: " << endl;
			Evaluation(confusionMatrixSet.at(k), recall, averagePrecision, averageRecall);
			cout << "risk: " << risks.at(k) << endl;

			/*overall recall*/
			recallSet.push_back(recall);

			/*category average recall*/
			averageRecallSet.push_back(averageRecall);

			/*category average precision*/
			averagePrecisionSet.push_back(averagePrecision);
		}

		/*find the best weight*/
		bestRecall = 0;
		bestAverageRecall = 0;

		initialRecall = recallSet.at(0);
		initialAverageRecall = averageRecallSet.at(0);

		if (TestConfiguration.optimizeAverageRecall == true)
		{
			for (unsigned int i = 0; i<averageRecallSet.size(); i++)
			{
				if (bestAverageRecall < averageRecallSet.at(i) + 0.001)
				{
					bestRecall = recallSet.at(i);
					bestAverageRecall = averageRecallSet.at(i);

					bestWeightIndex = i;
					convergedIter = bestWeightIndex;
				}
			}
		}
		else
		{
			double evaluation = 0;
			for (unsigned int i = 0; i<recallSet.size(); i++)
			{
				if (evaluation < recallSet.at(i) + averageRecallSet.at(i) + 0.001)
				{
					evaluation = recallSet.at(i) + averageRecallSet.at(i);
					bestRecall = recallSet.at(i);
					bestAverageRecall = averageRecallSet.at(i);

					bestWeightIndex = i;
					convergedIter = bestWeightIndex;
				}
			}
		}
	}
	else if (TestConfiguration.GraphModelTrainerType == TypeStructSVM)
	{
		bestWeightIndex = parametersSets.size() - 1;
	}
	else if (TestConfiguration.GraphModelTrainerType == TypeSubGradient)
	{
		bestWeightIndex = parametersSets.size() - 1;
	}

	cout << endl << "best weight index: " << bestWeightIndex << " " << bestRecall << endl;

	learnedParameters = parametersSets.at(bestWeightIndex);
	GraphModel::AssignGraphParameters(learnedParameters);

	string weight_file_name = ModelParameterFilename();
	SaveLearnedWeight(weight_file_name, risks.at(bestWeightIndex));
}

OnlinePA::OnlinePA(int  setNumClass) : StructLearner(setNumClass)
{
	piecewiseTraining = true;
	maxChangeRatio = 0.8;
	OnlinePA_C = TestConfiguration.OnlinePA_C;
	OnlinePA_initialC = OnlinePA_C;
}

OnlinePA::~OnlinePA()
{
}

double OnlinePA::ObjectiveFunciotn(double alpha,
	vector<double>& weight,
	vector<double>& diffFeatureVector,
	vector<weightType> weightFlag,
	double deltaLoss,
	multimap<double, int>& indexMapping)
{
	double functionValue = 0;
	for (multimap<double, int>::iterator Iter = indexMapping.begin();
		Iter != indexMapping.end();
		Iter++)
	{
		int idx = (*Iter).second;
		if (weightFlag.at(idx) == active)
		{
			functionValue += (pow(weight.at(idx), 2));
		}
	}

	functionValue /= 2;

	for (multimap<double, int>::iterator Iter = indexMapping.begin();
		Iter != indexMapping.end();
		Iter++)
	{
		int idx = (*Iter).second;
		if (weightFlag.at(idx) == active && (*Iter).first <= 0)
		{
			double d = diffFeatureVector.at(idx);
			functionValue += (pow(alpha*d - weight.at(idx), 2));
		}
	}

	/*sorted already*/
	for (multimap<double, int>::iterator Iter = indexMapping.begin();
		Iter != indexMapping.end();
		Iter++)
	{
		if ((*Iter).first > alpha)
		{
			int idx = (*Iter).second;
			if (weightFlag.at(idx) == active)
			{
				double d = diffFeatureVector.at(idx);
				functionValue += (pow(alpha*d - weight.at(idx), 2));
			}
		}
	}
	functionValue = -0.5*functionValue + alpha*deltaLoss;

	return functionValue;
}

void OnlinePA::OptimizeSinglePieceWiseFunction(double optimalPointsCandidate,
	double leftBoundaryPoint,
	double rightBoundaryPoint,
	vector<double>& diffFeatureVector,
	double deltaLoss,
	vector<double>& solutions,
	vector<double>& optimalValues,
	multimap<double, int>& indexMapping)
{
	vector<double> weight;
	vector<weightType> weightFlag;
	learnedParameters.GetParameters(weight, weightFlag);

	double sqrtWeight = 0;
	for (unsigned int i = 0; i<weight.size(); i++)
	{
		sqrtWeight += (weight.at(i)*weight.at(i));
	}

	double OptimalValue = -1e8;
	/*optimal values can be reached as three points for each piecewise function*/
	if (optimalPointsCandidate>leftBoundaryPoint && optimalPointsCandidate<rightBoundaryPoint)
	{
		OptimalValue = ObjectiveFunciotn(optimalPointsCandidate,
			weight, diffFeatureVector,
			weightFlag, deltaLoss, indexMapping);
	}

	double leftOptimalValue = ObjectiveFunciotn(leftBoundaryPoint,
		weight, diffFeatureVector,
		weightFlag, deltaLoss, indexMapping);

	double rightOptimalValue = ObjectiveFunciotn(rightBoundaryPoint,
		weight, diffFeatureVector,
		weightFlag, deltaLoss, indexMapping);

	if (optimalPointsCandidate>leftBoundaryPoint && optimalPointsCandidate<rightBoundaryPoint)
	{
		/*check the correctness of the solution*/
		if (OptimalValue < rightOptimalValue)
		{
			cout << "error" << endl;
			cout << OptimalValue << " " << rightOptimalValue << endl;
			cout << optimalPointsCandidate << " " << rightBoundaryPoint << endl;
		}
		if (OptimalValue < leftOptimalValue)
		{
			cout << "error" << endl;
			cout << OptimalValue << " " << leftOptimalValue << endl;
			cout << optimalPointsCandidate << " " << leftBoundaryPoint << endl;
		}

		assert(OptimalValue >= leftOptimalValue - 1e-6);
		assert(OptimalValue >= rightOptimalValue - 1e-6);

		solutions.push_back(optimalPointsCandidate);
		optimalValues.push_back(OptimalValue);
	}
	else
	{
		if (leftOptimalValue>rightOptimalValue)
		{
			solutions.push_back(leftBoundaryPoint);
			optimalValues.push_back(leftOptimalValue);
		}
		else
		{
			solutions.push_back(rightBoundaryPoint);
			optimalValues.push_back(rightOptimalValue);
		}
	}
}

double OnlinePA::OptimizePieceWiseFunction(vector<double>& diffFeatureVector,
	double deltaLoss,
	multimap<double, int>& indexMapping)
{
	/*compute candidates of optimal point*/
	vector<double> optimalPointsCandidates;
	vector<double> leftBoundaryPoints;
	vector<double> rightBoundaryPoints;

	vector<double> weight;
	vector<weightType> weightFlag;
	learnedParameters.GetParameters(weight, weightFlag);

	double fixed_margin = 0;
	double fixed_sumDistance = 0;

	multimap<double, int>::iterator iter = indexMapping.begin();

	/*range < 0*/
	double max_bound_point = (*iter).first;
	for (iter = indexMapping.begin(); iter != indexMapping.end(); iter++)
	{
		if ((*iter).first <= 0)
		{
			int idx = (*iter).second;
			if (weightFlag.at(idx) == active)
			{
				double d = diffFeatureVector.at(idx);
				fixed_margin += (weight.at(idx)*d);
				fixed_sumDistance += (d*d);
			}
		}
		if ((*iter).first >= max_bound_point)
			max_bound_point = (*iter).first;
	}
	fixed_margin += deltaLoss;

	/*piecewise domain*/
	leftBoundaryPoints.push_back(0);
	for (iter = indexMapping.begin(); iter != indexMapping.end(); iter++)
	{
		if ((*iter).first > 0)
		{
			int idx = (*iter).second;
			if (weightFlag.at(idx) == active)
			{
				leftBoundaryPoints.push_back((*iter).first);
				rightBoundaryPoints.push_back((*iter).first);
			}
		}
	}
	rightBoundaryPoints.push_back(1e8);

	/*optimal points candidates for each piecewise function*/
	for (unsigned int i = 0; i<leftBoundaryPoints.size(); i++)
	{
		double margin = 0;
		double sumDistance = 0;
		for (multimap<double, int>::iterator iter = indexMapping.begin(); iter != indexMapping.end(); iter++)
		{
			if ((*iter).first>leftBoundaryPoints.at(i))
			{
				int idx = (*iter).second;
				if (weightFlag.at(idx) == active)
				{
					double d = diffFeatureVector.at(idx);
					margin += (weight.at(idx)*d);
					sumDistance += (d*d);
				}
			}
		}

		sumDistance += fixed_sumDistance;
		margin += fixed_margin;
		/*out of range*/
		if (abs(sumDistance)<1e-8)
		{
			optimalPointsCandidates.push_back(-1); /*skip the value*/
		}
		else optimalPointsCandidates.push_back(margin / sumDistance);
	}

	/*adaptive set OnlinePA_C*/
	cout << endl << "Debug Print: piecewise domain & optimal point candidate " << endl;

	double maxCandidates = 0;
	for (unsigned int i = 0; i<optimalPointsCandidates.size(); i++)
	{
		/*upper bound check*/
		if (rightBoundaryPoints.at(i) > OnlinePA_C) rightBoundaryPoints.at(i) = OnlinePA_C;
		if (leftBoundaryPoints.at(i)  > OnlinePA_C) leftBoundaryPoints.at(i) = OnlinePA_C;

		if (maxCandidates < leftBoundaryPoints.at(i))
			maxCandidates = leftBoundaryPoints.at(i);

		/*update the maximun rightBoundaryPoints*/
		/*case 1: the optimal point is in the range*/
		/*case 2: the optimal point is not in the range*/
		if (optimalPointsCandidates.at(i) > leftBoundaryPoints.at(i) &&
			optimalPointsCandidates.at(i) < rightBoundaryPoints.at(i))
		{
			if (maxCandidates < optimalPointsCandidates.at(i))
				maxCandidates = optimalPointsCandidates.at(i);
		}

		cout << "Domain " << i << " ";
		cout << "[" << leftBoundaryPoints.at(i) << " ";
		cout << rightBoundaryPoints.at(i) << "] ";
		cout << optimalPointsCandidates.at(i) << " ";
		cout << endl;
	}

	rightBoundaryPoints.at(rightBoundaryPoints.size() - 1) = OnlinePA_C;
	cout << "C = " << OnlinePA_C << endl;

	/*compute piecewise optimzal value*/
	vector<double> solutions;
	vector<double> optimalValues;

	for (unsigned int i = 0; i<optimalPointsCandidates.size(); i++)
	{
		OptimizeSinglePieceWiseFunction(optimalPointsCandidates.at(i),
			leftBoundaryPoints.at(i),
			rightBoundaryPoints.at(i),
			diffFeatureVector,
			deltaLoss,
			solutions,
			optimalValues,
			indexMapping);
	}

	cout << endl << "Debug Print: piecewise function optimal points " << endl;
	for (unsigned int i = 0; i<optimalPointsCandidates.size(); i++)
	{
		cout << "Domain " << i << " ";
		cout << "[" << leftBoundaryPoints.at(i) << " ";
		cout << rightBoundaryPoints.at(i) << "] ";
		cout << solutions.at(i) << " " << optimalValues.at(i);
		cout << endl;
	}

	double alpha = solutions.at(0);
	double optimalValue = optimalValues.at(0);
	for (unsigned int i = 1; i<optimalValues.size(); i++)
	{
		if (optimalValue<optimalValues.at(i))
		{
			alpha = solutions.at(i);
			optimalValue = optimalValues.at(i);
		}
	}

	return alpha;
}

bool OnlinePA::UpdateWeight(vector<double>& predictedStructureFeature,
	vector<double>& trueStructureFeature,
	double deltaLoss)
{
	multimap<double, int> indexMapping;
	vector<double> diffFeatureVector;

	vector<double> weight;
	vector<weightType> weightFlag;
	learnedParameters.GetParameters(weight, weightFlag);

	// sort w/d with increase order
	for (int i = 0; i<trueStructureFeature.size(); i++)
	{
		double d = trueStructureFeature.at(i) - predictedStructureFeature.at(i);
		assert(weightFlag.at(i) != nonactive);
		diffFeatureVector.push_back(d);

		double v = 0;
		if (weightFlag.at(i) == active)
		{
			if (abs(d)>1e-8) v = weight.at(i) / d;
		}

		pair<double, int> p;
		p.first = v;
		p.second = i;
		indexMapping.insert(p);
	}

	/*test the optimization for constraint label finding*/
	double s = 0;
	for (unsigned int i = 0; i<weight.size(); i++)
	{
		double d = diffFeatureVector.at(i);
		s += (weight.at(i)*d);
	}
	if (s<0)
	{
		cout << "wrong in optimization of constraint label finding" << endl;
	}

	positiveWeight = true;
	if (positiveWeight == true)
	{
		cout << endl << "Debug Print: alpha domain vector " << endl;
		for (map<double, int>::iterator iter = indexMapping.begin(); iter != indexMapping.end(); iter++)
		{
			cout << (*iter).first << " ";
		}
		cout << endl;
	}

	cout << "loss " << deltaLoss << endl;
	for (unsigned int i = 0; i<weightFlag.size(); i++)
	{
		if (weightFlag.at(i) == staticType)
		{
			double d = diffFeatureVector.at(i);
			deltaLoss += (weight.at(i)*d);
		}
	}

	vector<double>  updatedweight = weight;
	double alpha = 0;

	OnlinePA_C = OnlinePA_initialC / sqrt(iterationIter + 1);
	/*whether change some parameters*/
	bool changeState = UpdateWeight(diffFeatureVector, alpha, weight, weightFlag,
		updatedweight, deltaLoss, indexMapping);

	/*if the paramters are not changed, reset the up bound of alpha C*/
	if (changeState == false)
	{
		double originalC = OnlinePA_C;
		multimap<double, int>::iterator iter = indexMapping.begin();

		vector<double> positiveValues;
		for (iter = indexMapping.begin(); iter != indexMapping.end(); iter++)
		{
			if ((*iter).first >0)
			{
				positiveValues.push_back((*iter).first);
			}
		}

		/*reset the boundary C*/
		OnlinePA_C = positiveValues.at(positiveValues.size()*maxChangeRatio);

		cout << endl << "Warning: weights are not changed this iteration ..." << endl <<
			"         automatically adjust C in objective function ... " << endl <<
			"         from: " << originalC << " to " << OnlinePA_C << endl;

		UpdateWeight(diffFeatureVector, alpha, weight, weightFlag,
			updatedweight, deltaLoss, indexMapping);

		OnlinePA_C = originalC;
		changeState = true;
	}

	if (changeState == true)
	{
		learnedParameters.SetParameters(updatedweight, weightFlag);
		for (unsigned int i = 0; i<parametersSets.size(); i++)
		{
			//learnedParameters.Print();
			//parametersSets.at(i).Print();
			if (learnedParameters == parametersSets.at(i))
			{
				return false;
			}
		}
		parametersSets.push_back(learnedParameters);
	}

	return changeState;
}

bool OnlinePA::UpdateWeight(vector<double>& diffFeatureVector,
	double& alpha,
	vector<double>& weight, vector<weightType>& weightFlag,
	vector<double>& updatedweight,
	double deltaLoss,
	multimap<double, int>& indexMapping)
{
	updatedweight = weight;
	alpha = OptimizePieceWiseFunction(diffFeatureVector, deltaLoss, indexMapping);
	if (alpha < 1e-8) alpha = 1e-8;
	cout << "alpha: " << alpha << endl;

	int smallValueCount = 0;
	int valueCount = 0;

	bool changeState = false;
	if (alpha > 0)
	{
		for (unsigned int i = 0; i<weight.size(); i++)
		{
			if (weightFlag.at(i) == active)
			{
				valueCount++;
				double d = diffFeatureVector.at(i);
				if (weight.at(i) - alpha*d >= 0)
				{
					double w = weight.at(i) - alpha*d + minValue;
					updatedweight.at(i) = w;
				}
				else
				{
					updatedweight.at(i) = minValue;
					smallValueCount++;
				}
			}
		}
	}

	/*if more than half paramters go out of bound, then change the bound C correspondingly*/
	if (smallValueCount > valueCount * maxChangeRatio) changeState = false;
	else changeState = true;

	return changeState;
}
