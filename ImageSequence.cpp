#include "ImageSequence.h"
#include "feature.h"
#include "GCO/GCoptimization.h"
#include "StructureParsingLearning.h"

ImageSequence::ImageSequence() {
	// TODO Auto-generated constructor stub

}

ImageSequence::~ImageSequence() {
	// TODO Auto-generated destructor stub
}

/*load image sequence*/
void ImageSequence::DebugTesting(const int DEBUG)
{
	for (unsigned int i = 0; i<TestConfiguration.validateImages.size(); i++)
	{
		TestConfiguration.testImages.push_back(TestConfiguration.validateImages.at(i));
	}

	for (unsigned int i = 0; i<TestConfiguration.testImages.size(); i++)
	{
		SuperPixelImage Image;
		cout << Image.filenameImage << endl;

		if (DEBUG == DEBUG_VIEWSUPERPIXEL)
		{
			
			Image.Initialization(&TestConfiguration.testImages[i], "superpixel");
			Image.ViewSuperPixel();
		}
		else if (DEBUG == DEBUG_VIEWFEATUREPCA)
		{
			Image.Initialization(&TestConfiguration.testImages[i], "pixel");
			Image.FeatureExtraction(NULL, "test");

			if (TestConfiguration.withPixelLevelfeatures == true &&
				TestConfiguration.withSPLevelfeatures == false)
			{
				cout << TestConfiguration.featureFlag << endl;
				PixelFeatureExtractor::AnalysisPCA(Image.features_2d, Image.filenameImage, TestConfiguration.featureFlag);
			}

			if (TestConfiguration.withSPLevelfeatures == true)
			{
				vector< FeatVector > features;
				for (int j = 0; j<Image.superPixels.size(); j++)
				{
					for (int k = 0; k<Image.superPixels.at(j).pixels.size(); k++)
					{
						FeatVector feat;
						feat.SetExtraData(Image.superPixels.at(j).pixels.at(k).x, Image.superPixels.at(j).pixels.at(k).y);
						feat.UpdateData(Image.superPixels.at(j).feature_2d.GetDataPtr());
						features.push_back(feat);
					}
				}

				//				for(int j=0;j<Image.features_2d.size();j++)
				//				{
				//					Image.features_2d.at(j).data.clear();
				//					if(Image.features_2d.at(j).spLevelFeat != NULL)
				//					{
				//						Image.features_2d.at(j).data =
				//								Image.features_2d.at(j).spLevelFeat->data;
				//					}
				//				}
				//				PixelFeature::AnalysisPCA(features,Image.filenameImage,TestConfiguration.bowFeatureFlag);
			}
		}
	}
}
