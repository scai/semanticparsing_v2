#ifndef DATASET_H_
#define DATASET_H_

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <list>

#include "cv.h"
#include "cxcore.h"
#include "cvaux.h"
#include "highgui.h"

#include "Image.h"
#include "feature.h"
using namespace std;

enum weightType{ staticType, active, nonactive };

enum structLearnerType{ TypeStructSVM, TypeSubGradient, TypeOnlinePA };

/*color to category*/
struct ColorToCategory{
	int classLabel;
	int r, g, b;
	string className;
	ColorToCategory() : classLabel(-1), r(0), g(0), b(0){}
	ColorToCategory(const ColorToCategory& c){
		classLabel = c.classLabel;
		className = c.className;
		r = c.r;
		g = c.g;
		b = c.b;
	}
	ColorToCategory& operator=(const ColorToCategory& c){
		if (this != &c){
			classLabel = c.classLabel;
			className = c.className;
			r = c.r;
			g = c.g;
			b = c.b;
		}
		return *this;
	}
};

//color to category table
//Note the label starts from one, instead of zero
//one label can be shared by different color
//class ColorCategoryTable{
//public:
//	ColorCategoryTable(){}
//	~ColorCategoryTable(){}
//
//	void LoadFromFile(string colorTableFile);
//
//	int FindLabel(int r, int g, int b){
//		int rgb = r*1e6 + g* 1e3 + b;
//		multimap<int, int>::iterator iter;
//		iter = colorCategoryMapping.find(rgb);
//		if (iter == colorCategoryMapping.end()) return -1;
//		else return iter->second;
//	}
//
//	int FindLabel(CvScalar& s){
//		int r = s.val[0];
//		int g = s.val[1];
//		int b = s.val[2];
//		int rgb = r*1e6 + g*1e3 + b;
//		multimap<int, int>::iterator iter;
//	}
//
//private:
//	vector< list<int> > categoryGrouping; /*each element for one category that could contain multiple new labels*/
//	vector<ColorToCategory> colorCategorySet;
//	multimap<int, int> colorCategoryMapping; /*color-to-label map*/
//};


class ColorCategoryTable
{
public:
	ColorCategoryTable(){}
	~ColorCategoryTable(){}

	void LoadFromFile(string colorTableFile);
	//void LoadFromFile2(string colorTableFile);

	int FindLabel(int r, int g, int b)
	{
		int rgb = r*1e6 + g*1e3 + b;
		multimap<int, int>::iterator iter;
		iter = colorCategoryMapping.find(rgb);
		if (iter == colorCategoryMapping.end()) return -1;
		else return iter->second;
	}

	/*be careful of the order of the r,g,b channel*/
	int FindLabel(CvScalar& s)
	{
		int r = s.val[2];
		int g = s.val[1];
		int b = s.val[0];
		return FindLabel(r, g, b);
	}

	/*be careful of the order of the r,g,b channel*/
	int FindLabel(cv::Scalar& s)
	{
		int r = s.val[2];
		int g = s.val[1];
		int b = s.val[0];
		return FindLabel(r, g, b);
	}

	/*get category label by its name, please note the label set is sorted */
	int GetCategoryLabel(string category)
	{
		int label = -1;
		for (unsigned int j = 0; j<colorCategorySet.size(); j++)
		{
			if (colorCategorySet.at(j).className == category)
			{
				label = colorCategorySet.at(j).classLabel;
				break;
			}
		}

		return label;
	}

	void Initialization()
	{
		colorCategorySet.clear();
		colorCategoryMapping.clear();
	}

	/*get category color*/
	cv::Scalar GetCategoryColor(int label)
	{
		if (label > 0)
		{
			int idx = -1;
			for (unsigned int j = 0; j<colorCategorySet.size(); j++)
			{
				if (colorCategorySet.at(j).classLabel == label)
				{
					idx = j;
					break;
				}
			}
			int r = colorCategorySet.at(idx).r;
			int g = colorCategorySet.at(idx).g;
			int b = colorCategorySet.at(idx).b;
			return cv::Scalar(b, g, r);
		}
		else return cv::Scalar(0, 0, 0);
	}

	void GetCategoryColor(int label, int& r, int& g, int& b)
	{
		cv::Scalar s = GetCategoryColor(label);
		r = s.val[2];
		g = s.val[1];
		b = s.val[0];
	}

	/*get category name of the idx element*/
	string GetCategoryName(int idx)
	{
		assert(idx >= 0 && idx < colorCategorySet.size());
		return colorCategorySet[idx].className;
	}

	int GetCategoryLabel(int idx)
	{
		assert(idx >= 0 && idx < colorCategorySet.size());
		return colorCategorySet[idx].classLabel;
	}

	int GetNumberOfCategory()
	{
		return colorCategorySet.size();
	}

	void InsertCategory(ColorToCategory& category)
	{
		colorCategorySet.push_back(category);
	}

	void InsertColorCategoryMapping(pair<int, int>& mapping)
	{
		colorCategoryMapping.insert(mapping);
	}

	/* return the unique label for one given label*/
	/* as each color is assigned an label, one category consisting of multiple colors will contain multiple labels*/
	/* this function will output the minimum sorted label for the category*/
	/* the input should be the new label*/
	int UniqueLabel(int label)
	{
		assert(label > 0 && label <= colorCategorySet.size());
		list<int>::iterator iter = categoryGrouping[label - 1].begin();
		return (*iter);
	}

private:
	vector< list<int> > categoryGrouping; /*each element for one category that could contain multiple new labels*/
	vector<ColorToCategory> colorCategorySet;
	multimap<int, int> colorCategoryMapping; /*color-to-label map*/
};

/*dataset*/
class GeneralConfiguration
{
public:
	GeneralConfiguration()
	{
		rootpath = "F:/ImageParsingProject";
		dirend = "/";                                                                          // system directory symbol
		resultdir = rootpath + dirend + "Result" + dirend;                // segmentation result
		interresultdir = rootpath + dirend + "interresult" + dirend;  // some step result
		filedir = rootpath + dirend + "File" + dirend;                         // the data list file and color table file
		featurePCAdir = rootpath + dirend + "FeaturePCA" + dirend;       // feature PCA analysis for visualization
		modeldir = rootpath + dirend + "classifier" + dirend;              // clustering tree and matching model
		configuredir = rootpath + dirend + "configure" + dirend;        // training and test configuration for extreme random forest
		parameterFile = configuredir + "ParameterSetting.txt";         // the parameter file
		confidencedir = resultdir + "confidence" + dirend;

		classifierFlag = "local";                                       // local or global classifiertype
		operation = -1;
		numofMaxTrainSamples = -1;
		numofMaxClusteringSamples = -1;
		numOfClass = -1;
		visualwordDim = -1;
		timeCost = 0;

		classifierName = "randomforest";
		classifierType = "multiClass";

		loadConfidenceMap = false;         // load saved confidence directly
		saveConfidenceMap = false;         // save confidence map from matching

		withPixelLevelfeatures = true;
		withSPLevelfeatures = false;       // add super-pixel level feature for each patch level feature or not
		withImageLevelfeatures = false;    // add image level feature for each patch level feature or not
		with3dFeature = false;
		imageScaleFactor = 1;

		withPositionFeature = false;       // with 2D position feature or not
		testLevel = "pixel";

		layerUnary = staticType;
		layerPariwise = active;
		uplayerUnary = nonactive;
		uplayerPariwise = nonactive;
		superlayerUnary = nonactive;
		superlayerPariwise = nonactive;
		constraintlayer = nonactive;
		RobustPn = nonactive;
		up_layer_Pariwise = nonactive;
		super_up_layerPariwise = nonactive;
		super_layer_Pariwise = nonactive;
		randomInitialization = false;
		optimizeAverageRecall = false;
		useShapeFilter = false;
		parallel = false;

		graphParameters = NULL;
		withLossCostStructureTraining = true;
		withUnLabelRegion = true;
		loadLearnedWeight = false;
		localGraphTraining = false;

		/*parameter of learning formulation*/
		OnlinePA_C = 1.0;
		StructSVM_C = 0.001;
		SubGradient_C = 1.0;
		SubGradient_stepsize = 0.01;
		maxIterations = 10;
		numThreads = 1;

		GraphModelTrainerType = TypeOnlinePA;
	}
	virtual ~GeneralConfiguration();
public:

	void Configuration();
	void LoadColorTable();
	//void LoadColorTableArray();
	void LoadParameter();
	void LoadGistFeature();
	void LoadImageList(string dataList, const int IMAGETYPE);
	void RegularizeMask();

	string GetFeatureTag(bool with3DFlag);
	string GetClassifierName(bool with3DFlag = false);

	void SetRootPath(string newRootPath);

	/*performance evaluation saving*/
	void SaveTestingResult(string resultfile);
public:
	vector<FeatureParameter> featParameters;    // pixel feature extraction parameter, multiple features
	vector<FeatureParameter> BowFeatParameters; // bow feature extraction parameter, multiple features
	string parameterFile;  // the file setting parameters
	string colorTableFile; // the file setting color category mapping

	ColorCategoryTable colorTable;

	vector<BasicImage> allImages;
	vector<BasicImage> trainImages;
	vector<BasicImage> testImages;
	vector<BasicImage> validateImages;

	string imageLevelFeaturesFilename; /*the filename of image level features*/
	map<string, vector<float> > ImageLevelFeatures; /*store Image level features, like GIST*/
	/*parameter setting*/

	string rootpath;            // the root path for data, result
	string dirend;              // system directory symbol
	string resultdir;           // segmentation result
	string interresultdir;      // some step result
	string filedir;             // the data list file and color table file
	string featurePCAdir;       // feature PCA analysis for visualization
	string modeldir;            // clustering tree and matching model
	string configuredir;        // training and test configuration for extreme random forest

	string datasetName;         // the searching path prefix for visual word and tranfer model
	string trainList;           // training list
	string validateList;        // validation list
	string testList;            // testing list

	string colortableFilename;            // color table

	string clusteringTreeFile;            // unsupervised clustering tree
	string property;                      // property: feature, dimension
	string confidencedir;                 // saved confidence path
	string classifierFlag;                // local or global classifiertype
	string featureFlag;                   // the feature flag
	string bowFeatureFlag;                // the feature flag
	string classifierType;                // the type of classifier: multi-class or multiple binary classifiers
	string classifierName;                // the name of used classifier

	int operation;
	int numofMaxClusteringSamples;        // the maximum number of samples for clustering tree construction
	int numofMaxTrainSamples;             // the maximum number of samples for training classifiers
	int numOfClass;                       // the number of categories
	int visualwordDim;                    // the visual word dimension
	int numThreads;

	bool loadConfidenceMap;         // load saved confidence directly
	bool saveConfidenceMap;         // save confidence map from matching
	bool withPositionFeature;       // with 2D position feature or not

	bool  withPixelLevelfeatures;    // add pixel level feature
	bool  withSPLevelfeatures;       // add super-pixel level feature for each patch level feature or not
	bool  withImageLevelfeatures;    // add image level feature for each patch level feature or not
	bool  with3dFeature;             // train and test with 3d feature or not
	bool  useShapeFilter;
	bool  parallel;
	int   imageScaleFactor;         // scale factor for image parsing

	GraphParameters* graphParameters;
	/*Graphical Model training setting*/
	weightType layerUnary;
	weightType layerPariwise;
	weightType uplayerUnary;
	weightType uplayerPariwise;
	weightType superlayerUnary;
	weightType superlayerPariwise;
	weightType constraintlayer;
	weightType RobustPn;
	weightType up_layer_Pariwise;
	weightType super_up_layerPariwise;
	weightType super_layer_Pariwise;

	structLearnerType GraphModelTrainerType;

	bool localGraphTraining;
	bool withLossCostStructureTraining;  // include loss cost for structure training or not;
	bool withUnLabelRegion;
	bool loadLearnedWeight;
	bool randomInitialization;

	/*parameter of learning formulation*/
	double OnlinePA_C;
	double StructSVM_C;
	double SubGradient_C;
	double SubGradient_stepsize;
	int maxIterations;

	bool optimizeAverageRecall; // use -averagerecall as optimization criterion
	cv::Mat confusionMatrix;      // the confusion matrix for testing
	double  timeCost;                  // the total processing time cost for testing
	string testLevel;                    // the test level: on superpixel or pixel
};

class DataSample {
public:
	DataSample(int setSampleIndex = -1, int setLabel = -1);
	virtual ~DataSample();

	int  GetLabel(){ return label; }
	void SetLabel(int setLabel){ label = setLabel; }

	void SetSampleIndex(int setSampleIndex){ sampleIndex = setSampleIndex; }
	int  GetSampleIndex(){ return sampleIndex; }

	void   SetData(int index, double value){ feat->SetAttributeValue(index, value); }
	double GetData(int index) { return feat->GetAttributeValue(index); }

	void SetData(FeatVector* setFeat)
	{
		feat = setFeat;
	}

	int GetFeatDim() { return feat->GetFeatDim(); }

	void ClearData()
	{
		if (feat != NULL) feat->ClearData();
	}

private:
	FeatVector* feat;
	int    label;
	int    sampleIndex;
};

class DataSampleSet
{
public:
	DataSampleSet();
	DataSampleSet(int setSampleDataLength, int setDatasetType, vector<int>& setLabelSet);

	DataSampleSet(DataSampleSet* inputDataset);
	virtual ~DataSampleSet();

	void ReleaseData()
	{
		for (unsigned int i = 0; i<samples.size(); i++)
		{
			samples.at(i)->ClearData();
		}

		for (unsigned int i = 0; i<samples.size(); i++) delete samples.at(i);
		samples.clear();
	}

	int GetNumSamples(){ return samples.size(); }
	int GetNumLabels(){ return labelSet.size(); }
	int GetLabel(int index){ return labelSet.at(index); }
	int GetLabelIndex(int label)
	{
		/*label mapping*/
		if (labelMapping == NULL)
		{
			labelMapping = new map<int, int>;
			for (unsigned int i = 0; i<labelSet.size(); i++)
			{
				pair<int, int> p;
				p.first = labelSet.at(i);
				p.second = i;
				labelMapping->insert(p);
			}
		}

		map<int, int>::iterator iter = labelMapping->find(label);
		return (*iter).second;
	}

	int GetDatasetType(){ return datasetType; }
	static int GetSampleDataLength(){ return sampleDataLength; }
	static void SetSampleDataLength(int setSampleDataLength){ sampleDataLength = setSampleDataLength; }

	bool LoadSamples(string filename);

	void InsertSample(DataSample* sample)
	{
		sample->SetSampleIndex(samples.size());
		samples.push_back(sample);
	}

	DataSample* GetSample(int sampleIndex){ return samples.at(sampleIndex); }

	/*estimate the best purity of a sample set*/
	void EstimatePurity(bool update = false);
	double EstimateEntropy(bool updateDistribution = false);

	void ReleaseSample();
	void RandomSample(DataSampleSet*& sampledDataset, double sampleRatio);

	void SearchActiveAttributes(vector<int>& activeAttributesIndex);
	void BuildAttributesRange(vector<int>& attributeIndexs, vector< pair<double, double> >& featValuesRange);

public:
	vector<int> labelSet;                                    /*label set*/
	vector<DataSample*> samples;                             /*samples*/
	double* categoryDist;                                    /*the category distribution of samples*/
	vector< pair<double, double> > featValuesRange;           /*the value range of each attribute*/

private:
	int datasetType;
	static int sampleDataLength;
	map<int, int>* labelMapping;
};

#endif /* DATASET_H_ */
