#include "Image.h"
#include "Global.h"
#include "GraphModel.h"
#include "GCO/GCoptimization.h"
using namespace BasicAPI;

vector< multimap<double, int> > PixelImage::knnLabelCandidates;

GCoptimization::EnergyTermType NewSmoothTermCost(int node_i, int node_j, int i, int j)
{
	multimap<double, int>::iterator iterI = PixelImage::knnLabelCandidates.at(node_i).begin();
	multimap<double, int>::iterator iterJ = PixelImage::knnLabelCandidates.at(node_j).begin();
	for (int c = 0; c<i; c++) iterI++;
	for (int c = 0; c<j; c++) iterJ++;

	if ((*iterI).second == (*iterJ).second)
	{
		return 0;
	}
	else return 1;
}

PixelImage::PixelImage() {
	// TODO Auto-generated constructor stub
	width = 0;
	height = 0;

	ptImage = NULL;
	segResult = NULL;
	segResultObject = NULL;
	labelMask = NULL;
	imageLevelFeatures = NULL;

	labelMap = NULL;
	prob = NULL;
	probIndex = NULL;
}

PixelImage::~PixelImage() {
	// TODO Auto-generated destructor stub
	ReleaseContent();
}

PixelImage::PixelImage(IplImage* Image)
{
	width = Image->width;
	height = Image->height;
	ptImage = cvCreateImage(cvGetSize(Image), Image->depth, Image->nChannels);
	cvCopy(Image, ptImage);
	segResult = NULL;
	segResultObject = NULL;
	labelMask = NULL;
	labelMap = NULL;
	prob = NULL;
	probIndex = NULL;
}

PixelImage::PixelImage(const PixelImage& T)
{
	filenameImage = T.filenameImage;             // image file name
	filenameLabelMask = T.filenameLabelMask;         // annotation file name
	filenameSuperPixel = T.filenameSuperPixel;
	width = T.width;                        // width of image
	height = T.height;                       // height of image

	features_2d.clear();
	for (unsigned int i = 0; i<T.features_2d.size(); i++)
	{
		features_2d.push_back(T.features_2d.at(i));
	}

	classexistFlag = T.classexistFlag;     // class existence flag

	ptImage = NULL;
	segResult = NULL;
	segResultObject = NULL;
	labelMask = NULL;

	if (T.ptImage != NULL)
	{
		ptImage = cvCreateImage(cvGetSize(T.ptImage), T.ptImage->depth, T.ptImage->nChannels);
		cvCopy(T.ptImage, ptImage);
	}

	if (T.segResultObject != NULL)
	{
		segResultObject = cvCreateImage(cvGetSize(T.segResultObject), T.segResultObject->depth, T.segResultObject->nChannels);
		cvCopy(T.segResultObject, segResultObject);
	}

	if (T.segResult != NULL)
	{
		segResult = cvCreateImage(cvGetSize(T.segResult), T.segResult->depth, T.segResult->nChannels);
		cvCopy(T.segResult, segResult);
	}

	if (T.labelMask != NULL)
	{
		labelMask = cvCreateImage(cvGetSize(T.labelMask), T.labelMask->depth, T.labelMask->nChannels);
		cvCopy(T.labelMask, labelMask);
	}

	if (prob != NULL) prob->release();
	if (probIndex != NULL) probIndex->release();

	if (T.prob != NULL && T.probIndex != NULL)
	{
		prob = new cv::Mat(T.prob->rows, T.prob->cols, T.prob->type(), cv::Scalar(0));
		probIndex = new cv::Mat(T.probIndex->rows, T.probIndex->cols, T.probIndex->type(), cv::Scalar(0));
		T.prob->copyTo(*prob);
		T.probIndex->copyTo(*probIndex);
	}
}

PixelImage::PixelImage(string filenameInput)
{
	filenameImage = filenameInput;             // image file name
	width = 0;
	height = 0;

	ptImage = NULL;
	segResult = NULL;              // segmentation result
	segResultObject = NULL;   // segmentation result
	labelMask = NULL;              // ground truth label mask
	labelMap = NULL;

	prob = NULL;
	probIndex = NULL;
}

void PixelImage::Initialization(int scale)
{
	if (scale == 1)
	{
		ptImage = cvLoadImage(filenameImage.c_str());
		height = ptImage->height;
		width = ptImage->width;
	}
	else
	{
		IplImage* Data = cvLoadImage(filenameImage.c_str());
		ptImage = cvCreateImage(cvSize(Data->width / scale, Data->height / scale), Data->depth, Data->nChannels);
		cvResize(Data, ptImage);
		cvReleaseImage(&Data);
		height = ptImage->height;
		width = ptImage->width;
	}
}

void PixelImage::ReleaseContent()
{
	//for (int i = 0; i<features_2d.size(); i++){
	//	if (features_2d[i] != NULL) {
	//		delete features_2d[i];
	//		//features_2d[i] = NULL;
	//	}
	//}
	//features_2d.clear();

	if (ptImage != NULL) cvReleaseImage(&ptImage);
	if (segResult != NULL) cvReleaseImage(&segResult);
	if (segResultObject != NULL) cvReleaseImage(&segResultObject);
	if (labelMask != NULL) cvReleaseImage(&labelMask);

	ptImage = NULL;
	segResult = NULL;              // segmentation result
	segResultObject = NULL;
	labelMask = NULL;              // ground truth label mask

	
	if (prob != NULL) prob->release();
	if (probIndex != NULL) probIndex->release();

	for (unsigned int i = 0; i<bowMap.size(); i++)
	{
		if (bowMap.at(i) != NULL) bowMap.at(i)->release();
	}
	//bowMap.clear();

	if (labelMap != NULL) labelMap->release();

	if (imageLevelFeatures != NULL) {
		delete imageLevelFeatures;
		//imageLevelFeatures = NULL;
	}

	width = 0;
	height = 0;
}

bool PixelImage::SegmentationValidation(cv::Mat& confusionMatrix, cv::Mat* activeMask)
{
	float AverageCorrectRate = 0;
	int numclass = TestConfiguration.numOfClass;

	if (labelMask == NULL)
	{
		cout << "No ground truth Mask..." << endl;
		return false;
	}

	if (segResult == NULL)
	{
		cout << "No segmentation result..." << endl;
		return false;
	}

	int i, j;
	int edgewidth = 1;
	int* local_class_count = new int[numclass];
	int* local_correct_class_count = new int[numclass];

	for (i = 0; i<numclass; i++)
	{
		local_class_count[i] = 0;
		local_correct_class_count[i] = 0;
	}

	// overall statistics
	int* current_class_count = new int[numclass];
	int* current_correct_class_count = new int[numclass];

	for (i = 0; i<numclass; i++)
	{
		current_class_count[i] = 0;
		for (j = 0; j<numclass; j++)
			current_class_count[i] += confusionMatrix.at<float>(i, j);
		current_correct_class_count[i] = confusionMatrix.at<float>(i, i);
	}

	for (i = edgewidth; i<segResult->height - edgewidth; i++)
	{
		for (j = edgewidth; j<segResult->width - edgewidth; j++)
		{
			int y = floor(float(labelMask->height) / float(segResult->height)*i);
			int x = floor(float(labelMask->width) / float(segResult->width)*j);

			if (activeMask == NULL)
			{
				int label = labelMap->at<int>(y, x);
				if (label>0)
				{
					local_class_count[label - 1]++;
					int r_b = ((unsigned char*)segResult->imageData)[i*segResult->widthStep + j*segResult->nChannels];
					int r_g = ((unsigned char*)segResult->imageData)[i*segResult->widthStep + j*segResult->nChannels + 1];
					int r_r = ((unsigned char*)segResult->imageData)[i*segResult->widthStep + j*segResult->nChannels + 2];

					int recognized_label = TestConfiguration.colorTable.FindLabel(r_r, r_g, r_b);
					if (recognized_label == label)
						local_correct_class_count[label - 1]++;

					if (recognized_label>0)
						confusionMatrix.at<float>(label - 1, recognized_label - 1)++;
				}
			}
			else if (activeMask->at<int>(y, x) == 1)
			{
				int label = labelMap->at<int>(y, x);
				if (label>0)
				{
					local_class_count[label - 1]++;
					int r_b = ((unsigned char*)segResult->imageData)[i*segResult->widthStep + j*segResult->nChannels];
					int r_g = ((unsigned char*)segResult->imageData)[i*segResult->widthStep + j*segResult->nChannels + 1];
					int r_r = ((unsigned char*)segResult->imageData)[i*segResult->widthStep + j*segResult->nChannels + 2];
					int recognized_label = TestConfiguration.colorTable.FindLabel(r_r, r_g, r_b);

					if (recognized_label == label)
						local_correct_class_count[label - 1]++;

					if (recognized_label>0)
						confusionMatrix.at<float>(label - 1, recognized_label - 1)++;
				}
			}
		}
	}

	float total_local_count = 0;
	float total_correct_local_count = 0;

	cout << "current test sample accuracy: " << endl;
	for (i = 0; i<numclass; i++)
	{
		total_local_count += local_class_count[i];
		if (local_class_count[i]>0)
		{
			total_correct_local_count += local_correct_class_count[i];
			float rate = float(local_correct_class_count[i]) / float(local_class_count[i]);
			cout << TestConfiguration.colorTable.GetCategoryName(i) << ": " << rate << endl;
			float class_correct_ration = float(current_correct_class_count[i] + local_correct_class_count[i]) / float(current_class_count[i] + local_class_count[i]);

		}
	}

	BasicAPI::AnalysisConfusionMap(confusionMatrix);

	delete[] local_class_count;
	delete[] local_correct_class_count;

	delete[] current_class_count;
	delete[] current_correct_class_count;

	return true;
}

void PixelImage::SaveSegmentationResult()
{
	string featureType;
	featureType = TestConfiguration.featureFlag + "_2d";

	/*save segmentation result*/
	string prefix = TestConfiguration.resultdir + TestConfiguration.datasetName + TestConfiguration.dirend + ExtractFileName(filenameImage, false);
	string Overlayfilename = prefix + "_" + featureType + "_overlay_RT.jpg";

	float alpha = 0.5;
	IplImage* image_blending = cvCreateImage(cvGetSize(ptImage), ptImage->depth, ptImage->nChannels);
	cvSetZero(image_blending);

	cvAddWeighted(ptImage, alpha, segResult, 1 - alpha, 0, image_blending);
	cvSaveImage(Overlayfilename.c_str(), image_blending);
	cvReleaseImage(&image_blending);

	string segfilename = prefix + "_seg.png";
	cvSaveImage(segfilename.c_str(), segResult);

	string segObjectsfilename = prefix + "_seg_objects.png";
	cvSaveImage(segObjectsfilename.c_str(), segResultObject);

	string imagefilename = prefix + ".jpg";
	cvSaveImage(imagefilename.c_str(), ptImage);

	cout << "save segmentation result to " << segfilename << endl;
}

bool PixelImage::SaveConfidenceMap(cv::Mat& probability, cv::Mat& probability_index)
{
	string typeFlag = TestConfiguration.GetFeatureTag(false);
	string confidenceFilename = TestConfiguration.confidencedir +
		ExtractFileName(filenameImage, false) + "_confidence_" +
		typeFlag + "_pixel" + ".txt";

	ofstream out;
	out.open(confidenceFilename.c_str());

	/*header of confidence file*/
	out << "*image-size: " << ptImage->width << " " << ptImage->height << endl;
	out << "*category-order: ";
	for (unsigned int i = 0; i<TestConfiguration.colorTable.GetNumberOfCategory(); i++)
	{
		out << TestConfiguration.colorTable.GetCategoryName(i) << " ";
	}
	out << endl;
	out << "*num-lines " << probability.rows << endl;
	out << "*format: <x,y> <probabilty>" << endl;

	for (int j = 0; j<probability_index.rows; j++)
	{
		int x = probability_index.at<float>(j, 0);
		int y = probability_index.at<float>(j, 1);
		out << x << " " << y << " ";
		for (int k = 0; k<TestConfiguration.numOfClass; k++)
		{
			out << probability.at<float>(j, k) << " ";
		}
		out << endl;
	}
	out.close();

	return true;
}

bool PixelImage::LoadConfidenceMapALE(cv::Mat*& prob, cv::Mat*& probIndex)
{
	string confidenceFilename = "Result/import/" + TestConfiguration.datasetName + "/" +
		BasicAPI::ExtractFileName(filenameImage, false) + ".dns";

	FILE* f = fopen(confidenceFilename.c_str(), "rb");
	if (f == NULL)
	{
		cout << "load file: " << confidenceFilename << " failed ... " << endl;
		return false;
	}

	int w, h, c;
	fread(&w, sizeof(int), 1, f);
	fread(&h, sizeof(int), 1, f);
	fread(&c, sizeof(int), 1, f);

	assert(w == width);
	assert(h == height);
	assert(c == TestConfiguration.numOfClass);

	double* p = new double[width * height * c];

	fread(p, sizeof(double), width * height * c, f);
	fclose(f);

	for (unsigned int i = 0; i<width * height; i++)
	{
		double probSum = 0;
		for (int j = 0; j<c; j++)
		{
			probSum += exp(p[i*c + j]);
		}

		for (int j = 0; j<c; j++)
		{
			p[i*c + j] = exp(p[i*c + j]) / probSum;
		}
	}

	if (prob != NULL) prob->release();
	if (probIndex != NULL) probIndex->release();
	prob = new cv::Mat(width * height, TestConfiguration.numOfClass, CV_32FC1, cv::Scalar(0));
	probIndex = new cv::Mat(width * height, 2, CV_32FC1, cv::Scalar(0));
	delete[] p;

	return true;
}

bool PixelImage::LoadConfidenceMap(cv::Mat*& probability, cv::Mat*& probability_index)
{
	string typeFlag = TestConfiguration.GetFeatureTag(false);

	string confidenceFilename =
		TestConfiguration.confidencedir + ExtractFileName(filenameImage, false) + "_confidence_" +
		typeFlag + "_pixel" + ".txt";

	ifstream input;
	input.open(confidenceFilename.c_str());

	if (!input)
	{
		input.close();
		cout << "load file: " << confidenceFilename << " failed" << endl;
		return false;
	}

	int lineLength = 1024;
	char temp[1024];

	/*check image size is consistent*/
	input.getline(temp, lineLength);
	if (temp[0] == '*')
	{
		string textLine(temp);
		vector<string> textElement;
		TextlineSplit(textLine, textElement, ' ');
		int w = atoi(textElement.at(1).c_str());
		int h = atoi(textElement.at(2).c_str());
		if (w != ptImage->width || h != ptImage->height)
		{
			cout << "inconsistent image size" << endl;
			return false;
		}
	}

	input.getline(temp, lineLength);
	if (temp[0] == '*')
	{
		string textLine(temp);
		vector<string> textElement;
		TextlineSplit(textLine, textElement, ' ');

		for (unsigned int i = 0; i<TestConfiguration.colorTable.GetNumberOfCategory(); i++)
		{
			if (textElement.at(i + 1) != TestConfiguration.colorTable.GetCategoryName(i))
			{
				cout << "inconsistent category order" << endl;
				return false;
			}
		}
	}

	int numOfItems = 0;
	input.getline(temp, lineLength);
	if (temp[0] == '*')
	{
		string textLine(temp);
		vector<string> textElement;
		TextlineSplit(textLine, textElement, ' ');
		numOfItems = atoi(textElement.at(1).c_str());
	}

	if (probability != NULL) probability->release();
	if (probability_index != NULL) probability_index->release();
	probability = new cv::Mat(numOfItems, TestConfiguration.numOfClass, CV_32FC1, cv::Scalar(0));
	probability_index = new cv::Mat(numOfItems, 2, CV_32FC1, cv::Scalar(0));

	int idx = 0;
	input.getline(temp, lineLength); // skip one line
	while (!input.eof())
	{
		input.getline(temp, lineLength);
		string textLine(temp);
		if (textLine.size()>2)
		{
			vector<string> textElement;
			TextlineSplit(textLine, textElement, ' ');

			probability_index->at<float>(idx, 0) = atof(textElement.at(0).c_str());
			probability_index->at<float>(idx, 1) = atof(textElement.at(1).c_str());

			for (unsigned int i = 0; i<TestConfiguration.numOfClass; i++)
			{
				probability->at<float>(idx, i) = atof(textElement.at(i + 2).c_str());
			}
			idx++;
		}
	}
	input.close();

	return true;
}

void PixelImage::ViewPixelConfidenceMap(cv::Mat& probability, cv::Mat& probability_index)
{
	cout << "Generate confidence map..." << endl;
	vector<IplImage*> confidenceMap;

	int j, k;
	int numclass = TestConfiguration.numOfClass;
	for (k = 0; k<numclass; k++)
	{
		IplImage* temp = cvCreateImage(cvGetSize(ptImage), ptImage->depth, ptImage->nChannels);
		cvSetZero(temp);
		confidenceMap.push_back(temp);
	}
	string originalImagefile = TestConfiguration.interresultdir + "/" + ExtractFileName(filenameImage, true);
	cvSaveImage(originalImagefile.c_str(), ptImage);

	assert(probability_index.type() == CV_32FC1);
	assert(probability.type() == CV_32FC1);

	for (k = 0; k<numclass; k++)
	{
		for (j = 0; j<probability_index.rows; j++)
		{
			int x = probability_index.at<float>(j, 0);
			int y = probability_index.at<float>(j, 1);
			float prob = probability.at<float>(j, k);
			int label = TestConfiguration.colorTable.GetCategoryLabel(k);

			int r = TestConfiguration.colorTable.GetCategoryColor(label).val[0] * prob;
			int g = TestConfiguration.colorTable.GetCategoryColor(label).val[1] * prob;
			int b = TestConfiguration.colorTable.GetCategoryColor(label).val[2] * prob;
			cvSet2D(confidenceMap[k], y, x, cvScalar(b, g, r));
		}
	}

	for (k = 0; k<numclass; k++)
	{
		string confidenceMapName = TestConfiguration.interresultdir + ExtractFileName(filenameImage, false) + "_" +
			TestConfiguration.featureFlag + "_" +
			TestConfiguration.colorTable.GetCategoryName(k) + "_pixel.png";
		cvSaveImage(confidenceMapName.c_str(), confidenceMap[k]);
		cvReleaseImage(&confidenceMap[k]);
	}
}

void PixelImage::PixelMRFInference(cv::Mat& probability, cv::Mat& probability_index)
{
	GraphModel pixelCRF(TestConfiguration.numOfClass);
	if (GraphModel::initialized == false)
		GraphModel::SetConfiguration(NULL);

	pixelCRF.SetupGraph(this, &probability, &probability_index);
	pixelCRF.MAPInference(false);

	for (unsigned int i = 0; i<pixelCRF.Layer.size(); i++)
	{
		int x = pixelCRF.Layer.at(i)->x;
		int y = pixelCRF.Layer.at(i)->y;
		int label = pixelCRF.Layer.at(i)->predictLabel;

		if (label>0)
		{
			cvSet2D(segResult, y, x, TestConfiguration.colorTable.GetCategoryColor(label));
		}
	}
}
