#ifndef STRUCTUREPARSINGLEARNING_H_
#define STRUCTUREPARSINGLEARNING_H_

#include "ClassificationModel.h"
#include "StructLearner.h"

class StructureParsing
{
public:
	StructureParsing();
	virtual ~StructureParsing();

public:
	void Train(vector<BasicImage>& inputTrainImages);
	/*Train parameters in graph with local image set*/
	void Train(vector<BasicImage>& testImages, vector<BasicImage>& inputTrainImages);
	void Train(vector<BasicImage>& testImages, vector< vector<int> >& localTrainImagesIndex);

	//void SplitTrainData(vector<BasicImage>& inputTrainImages);
	void TrainClassifier();

	/*Train global parameters in graph*/
	void TrainStructureModel();

	/*Train parameters in graph with local image set*/
	void TrainStructureModel(StructLearner* graphMoldeTrainer, vector<BasicImage>& localTrainImages);

	/*Test with a global parameters*/
	void Test(vector<BasicImage>& testImages);

	vector<BasicImage> testImages;     // test images
	vector<BasicImage> trainImages;    // for classifier learning
	vector<BasicImage> validateImages; // for structure model learning

	bool adaptiveLearning;             // whether the parameters are adaptive
	// learned for each test image
};

#endif /* STRUCTUREPARSINGLEARNING_H_ */
