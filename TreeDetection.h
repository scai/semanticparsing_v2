#ifndef TREEDETECTION_H_
#define TREEDETECTION_H_

#include "cv.h"
#include "cxcore.h"
#include "highgui.h"
#include "BasicAPI.h"
#include "Image.h"
#include "FibonacciHeap/fibheap.h"

#include <iostream>
#include <fstream>
#include <cmath>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <algorithm>
#include <vector>

using namespace std;

struct TreePosition
{
	int x, y;
	int radius;
	float probabilty;
	TreePosition() :x(-1), y(-1), radius(-1), probabilty(0)
	{
	}
};

class HeapNode : public FibHeapNode
{
public:
	float Key;
	int x, y;
	int matching_index_source;
	int matching_index_target;
	int destImageIndex;
	int radius;

	HeapNode()
	{
		Key = 0;
		matching_index_source = -1;
		matching_index_target = -1;
		destImageIndex = -1;
		x = -1;
		y = -1;
		radius = -1;
	};

	virtual~HeapNode(){};

	float GetKeyValue() { return Key; };
	void SetKeyValue(float inkey) { Key = inkey; };
};

void AerialTreeExtraction(IplImage* SegmentationMask, IplImage* InputImage, string SaveFileName);

#endif /* TREEDETECTION_H_ */
