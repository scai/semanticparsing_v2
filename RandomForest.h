#ifndef BASICELEMENTS_H_
#define BASICELEMENTS_H_

#include <iostream>
#include <assert.h>
#include <omp.h>
#include <sstream>
#include "Dataset.h"

using namespace std;

const int NODE_INTERNAL = 1;
const int NODE_LEAF = 2;

const int SPLITOR_Stump = 1;
const int SPLITOR_HyperPlane = 2;

class ForestTrainingParameters
{
public:
	ForestTrainingParameters()
	{
		numThreads = 1;
		numTrees = 50;               /*the number of trees*/
		treeSampleRatio = 1;         /*random sample ratio for training a single tree*/
		paralleTraining = true;      /*paralle training*/
		paralleTesting = false;     /*paralle testing*/
	}
	~ForestTrainingParameters(){};

public:
	int numThreads;
	int numTrees;                 /*the number of trees*/
	double treeSampleRatio;       /*random sample ratio for training a single tree*/
	bool paralleTraining;         /*paralle training*/
	bool paralleTesting;          /*paralle Testing*/
};

class TreeTrainingParameters
{
public:
	TreeTrainingParameters()
	{
		maxDepth = 32; /*the maximun depth of a tree*/
		minLeafSize = 1;
		ratioPosTry = 0.33;
		numSplitTry = 10; /*the number of trying different split values on one position*/
		numPosTry = 10; /*the number of trying different positions*/
		splitorType = SPLITOR_Stump;
	}

	~TreeTrainingParameters(){};

public:
	int splitorType; /*the type of the splitor*/
	int maxDepth; /*the maximun depth of a tree*/
	int minLeafSize; /*the minimum number of training samples that support a leaf*/

	int numSplitTry; /*the number of trying different split values on one position*/
	int numPosTry; /*the number of trying different positions*/
	float ratioPosTry; /*the ratio of tried position among all attributes*/
};

/*splitor for a internal node: hyperplane ; binary partition*/

class TreeNodeSplitor {
public:
	TreeNodeSplitor();
	virtual ~TreeNodeSplitor();

	/*return -1/+1 label only, for left and right tree*/
	inline int ClassifySample(DataSample* sample);

	void SaveTreeNodeSplitor(ofstream& out);
	bool LoadTreeNodeSplitor(ifstream& input);

	/*class specific*/
	void ConstructSplitor(DataSampleSet* samples);

	/*class specific*/
	void ConstructSplitor(int setSplitPosition, int setSplitValue);

public:
	/*stump*/
	int splitPosition;
	double splitValue;

	static int featDim;
};

class TreeNode {
public:
	TreeNode();
	TreeNode(int setNodeIndex);

	virtual ~TreeNode();

	int  GetNodeLevel(){ return level; }
	void SetNodeLevel(int setNodeLevel){ level = setNodeLevel; }

	int  GetNodeIndex(){ return nodeIndex; }
	void SetNodeIndex(int setNodeIndex){ nodeIndex = setNodeIndex; }

	int  GetNodeType(){ return nodeType; }
	void SetNodeType(int setNodeType){ nodeType = setNodeType; }

	int  GetLeaveLabel(){
		if (nodeType == NODE_LEAF) return leafLabel;
		else {
			cout << "Error: the node is not a leave ." << endl;
			return -1;
		}
	}

	void SetLeaveLabel(int setLeaveLabel){
		if (nodeType == NODE_LEAF) leafLabel = setLeaveLabel;
		else {
			cout << "Error: the node is not a leave ." << endl;
		}
	}

	bool IsLeaf()
	{
		if (nodeType == NODE_LEAF) return true;
		else return false;
	}

	void SaveNode(ofstream& out);
	bool LoadNode(ifstream& input);

	void ConstructSplitor(DataSampleSet* samples,
		DataSampleSet*& leftTreeSamples,
		DataSampleSet*& rightTreeSamples);

	double EvaluateSplitor(TreeNodeSplitor* testedSplitor,
		DataSampleSet*  dataSamples,
		DataSampleSet*& leftTreeSamples,
		DataSampleSet*& rightTreeSamples);

	/*binary partition of label space, for a binary SVM training*/
	void GenerateLabelPartition(DataSampleSet* dataSamples, vector<int>& binarySequence, int k);

public:
	int numSamples; /*number of training samples to train the node*/
	int level;                 /* the level of the node in the tree*/
	int nodeIndex;     /* the index of the node in the tree*/
	int nodeType;      /* type of the node*/
	int leafLabel;      /* the label of the leaf, if it is a leaf; else , -1*/

	int leftChildIndex;
	int rightChildIndex;

	TreeNodeSplitor* splitor;
	TreeNode* leftChild;
	TreeNode* rightChild;

public:
	int labelPartition; /*binary partition of label space*/
	double purity;
	double objectivePurity;
};

class RandomTree
{
public:
	RandomTree();
	RandomTree(int setTreeIndex)
	{
		treeIndex = setTreeIndex;
	}

	virtual ~RandomTree();

	void SaveTree(ofstream& out);
	bool LoadTree(ifstream& input);

	int    ClassifySample(DataSample* sample);
	void ClassifySamples(DataSampleSet* samples);
	void ConstructTree(DataSampleSet* samples, int level = 0);

	void TrainingAnalysis();

public:
	vector<TreeNode *> nodes;
	int numSamples;
	int treeIndex;

	static TreeTrainingParameters trainingParameter;
};

class RandomForest
{
public:
	RandomForest();
	virtual ~RandomForest();

	int ClassifySample(DataSample* sample);
	void ClassifySamples(DataSampleSet* samples);

	void SaveForest(string modefilename);
	bool LoadForest(string modefilename);

	void PredictClassProb(DataSample* sample, vector<float>& prob);
	void ConstructForest(DataSampleSet* samples);

public:
	vector<RandomTree*> trees;
	vector<int> labelSet;
	int numSamples;

	static int numSplitSegments; /*the number of partitions for each attribute*/

	//static vector< pair<double,double> > featValuesRange;
	static  ForestTrainingParameters trainingParameter;
	static vector<int> activeAttributesIndex;
};
#endif /* BASICELEMENTS_H_ */
