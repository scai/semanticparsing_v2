//#include "reference_calc.cpp"
#include "LBP.h"
#include "cuda.h"
#include "utils.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <cuda.h>
#include <cuda_runtime.h>
#include <string>
#include <fstream>
//#include "opencv2\gpu\gpu.hpp"

__global__ void lbp(const cv::cuda::PtrStepSz<uchar> d_src, cv::cuda::PtrStepSz<uchar> d_dst, int numRows, int numCols, int radius, int neighbors, const float* const d_array)
{
	//int* fx, int* fy, int* cx, int* cy, float *w1, float* w2, float* w3, float* w4){
	const int c = blockIdx.x * blockDim.x + threadIdx.x;
	const int r = blockIdx.y * blockDim.y + threadIdx.y;

	//int neighbors = 8, radius = 7;
	if (c >= numCols  || r >= numRows  || c < 0 || r < 0){
		return;
	}
	else if (c >= numCols - radius || r >= numRows - radius || c < radius || r < radius){
		d_dst(r, c) = 0;
	}

	int code = 0;
	for (int n = 0; n < neighbors; n++) {
		float t = d_array[n * 8 + 4] * d_src(r + d_array[n * 8 + 1], c + d_array[n * 8 + 0]) + d_array[n * 8 + 5] * d_src(r + d_array[n * 8 + 1], c + d_array[n * 8 + 2])
			+ d_array[n * 8 + 6] * d_src(r + d_array[n * 8 + 3], c + d_array[n * 8 + 0]) + d_array[n * 8 + 7] * d_src(r + d_array[n * 8 + 3], c + d_array[n * 8 + 2]);

		code += (((t > d_src(r, c)) && (abs(t - d_src(r, c)) > 1.192093e-07)) << n);

	}
	d_dst(r, c) = code;
}

void LBPFeature::your_lbp(const cv::cuda::PtrStepSz<uchar>& d_src, cv::cuda::PtrStepSz<uchar> d_dst, const size_t numRows, const size_t numCols, float* h_array)
{
	const dim3 blockSize(16, 16);
	dim3 gridSize((d_src.cols + blockSize.x - 1) / blockSize.x, (d_src.rows + blockSize.y - 1) / blockSize.y);

	if (_d_array == NULL)
		checkCudaErrors(cudaMalloc(&_d_array, sizeof(float) * 8 * 8));
	checkCudaErrors(cudaMemcpy(_d_array, h_array, sizeof(float) * 8 * 8, cudaMemcpyHostToDevice));

	lbp << <gridSize, blockSize >> >(d_src, d_dst, d_src.rows, d_src.cols, 7, 8, _d_array);
	cudaDeviceSynchronize();
	checkCudaErrors(cudaGetLastError());
}

