#include "RandomForest.h"
#include "Global.h"

TreeTrainingParameters RandomTree::trainingParameter;
ForestTrainingParameters RandomForest::trainingParameter;

int RandomForest::numSplitSegments;
vector<int> RandomForest::activeAttributesIndex;
int TreeNodeSplitor::featDim;

/*convert decimal number to binary number of length k*/
void DecimalToBinary(int d, int k, vector<bool>& binary)
{
	assert(d<pow(float(2), k));
	binary.clear();
	for (int j = 0; j<k; j++)
	{
		if (d % 2 == 0)binary.push_back(false);
		else binary.push_back(true);
		d /= 2;
	}
}

/*convert decimal number to binary number of length k*/
void BinaryToDecimal(vector<bool>& binary, int& d)
{
	d = 0;
	for (int j = 0; j<binary.size(); j++)
	{
		if (binary.at(j) == true) d += pow(float(2), j);
	}
}

TreeNodeSplitor::TreeNodeSplitor()
{
	splitPosition = -1;
	splitValue = -1;
}

TreeNodeSplitor::~TreeNodeSplitor()
{

}

inline int TreeNodeSplitor::ClassifySample(DataSample* sample)
{
	return (sample->GetData(splitPosition) <= splitValue) ? -1 : 1;
}

bool TreeNodeSplitor::LoadTreeNodeSplitor(ifstream& input)
{
	const int filelength = 1024 * 10;
	char temp[filelength];
	if (RandomTree::trainingParameter.splitorType == SPLITOR_Stump)
	{
		input.getline(temp, filelength);
		string textLine(temp);

		vector<string> dataElement;
		BasicAPI::TextlineSplit(textLine, dataElement, ' ');

		std::istringstream stm2;
		stm2.str(dataElement.at(1).c_str());
		stm2 >> splitPosition;

		std::istringstream stm3;
		stm3.str(dataElement.at(2).c_str());
		stm3 >> splitValue;
	}
	return true;
}

void TreeNodeSplitor::SaveTreeNodeSplitor(ofstream& out)
{
	if (RandomTree::trainingParameter.splitorType == SPLITOR_Stump)
		out << "Splitor " << splitPosition << " " << splitValue << endl;
}

TreeNode::TreeNode()
{
	numSamples = 0; /*number of training samples to train the node*/
	level = -1;                 /* the level of the node in the tree*/
	nodeIndex = -1;     /* the index of the node in the tree*/

	nodeType = NODE_INTERNAL;
	leafLabel = -1;

	leftChildIndex = -1;
	rightChildIndex = -1;

	purity = 0;
	objectivePurity = 0.95;
	splitor = NULL;
	leftChild = NULL;
	rightChild = NULL;
}

TreeNode::TreeNode(int setNodeIndex)
{
	numSamples = 0; /*number of training samples to train the node*/
	level = -1;                 /* the level of the node in the tree*/
	nodeIndex = -1;     /* the index of the node in the tree*/

	nodeType = NODE_INTERNAL;
	leafLabel = -1;

	leftChildIndex = -1;
	rightChildIndex = -1;

	purity = 0;
	objectivePurity = 0.95;
	splitor = NULL;
	leftChild = NULL;
	rightChild = NULL;

	nodeIndex = setNodeIndex;
}

TreeNode::~TreeNode()
{
	if (splitor != NULL){
		delete splitor;
		splitor = NULL;
	}
	//if (leftChild != NULL){
	//	delete leftChild;
	//	//leftChild = NULL;
	//}

	//if (rightChild != NULL){
	//	delete rightChild;
	//	//rightChild = NULL;
	//}
}

/*construct Tree Node splitor*/
void TreeNode::ConstructSplitor(DataSampleSet* dataSamples,
	DataSampleSet*& leftTreeSamples,
	DataSampleSet*& rightTreeSamples)
{
	/*distribution of samples*/
	if (dataSamples->categoryDist == NULL) dataSamples->EstimatePurity();
	double maxInfoGain = 0;

	/*initialization of left&right sub tree*/
	assert(leftTreeSamples == NULL);
	assert(rightTreeSamples == NULL);

	leftTreeSamples = new DataSampleSet(dataSamples);
	rightTreeSamples = new DataSampleSet(dataSamples);

	DataSampleSet* tempLeftTreeSamples = new DataSampleSet(dataSamples);
	DataSampleSet* tempRightTreeSamples = new DataSampleSet(dataSamples);

	assert(RandomTree::trainingParameter.splitorType == SPLITOR_Stump);

	if (RandomTree::trainingParameter.splitorType == SPLITOR_Stump)
	{
		vector<int> selectedAttributeIndexs;
		vector<int> selectedIndexs;

		vector< pair<double, double> > featValuesRange; /*the value range of each attribute*/
		/*Generate random sequence between a range*/
		BasicAPI::GenerateRandomSequence(0, RandomForest::activeAttributesIndex.size() - 1,
			selectedIndexs,
			RandomTree::trainingParameter.numPosTry);

		for (int i = 0; i<selectedIndexs.size(); i++)
			selectedAttributeIndexs.push_back(RandomForest::activeAttributesIndex.at(selectedIndexs.at(i)));

		dataSamples->BuildAttributesRange(selectedAttributeIndexs, featValuesRange);

		if (splitor != NULL) delete splitor;
		splitor = new TreeNodeSplitor();

		TreeNodeSplitor* tempSplitor = new TreeNodeSplitor();
		for (int i = 0; i<selectedAttributeIndexs.size(); i++)
		{
			double range = featValuesRange.at(i).second - featValuesRange.at(i).first;
			if (range > 1e-6) /*valid range*/
			{
				tempSplitor->splitPosition = selectedAttributeIndexs.at(i);
				double stepSize = (featValuesRange.at(i).second -
					featValuesRange.at(i).first) / (RandomForest::numSplitSegments);

				for (int j = 0; j<RandomForest::numSplitSegments; j++)
				{
					tempSplitor->splitValue = featValuesRange.at(i).first + j * stepSize;

					/*evaluate with classification accuracy*/
					double infoGain = EvaluateSplitor(tempSplitor, dataSamples, tempLeftTreeSamples, tempRightTreeSamples);
					//cout<<"info gain: "<<infoGain<<"("<<tempLeftTreeSamples->samples.size()<<","<<tempRightTreeSamples->samples.size()<<")"<<endl;

					/*the update of the splitor subject to two conditions: 1 the increase of information gain; 2 the size of the two  splited sub sets must be more than a threshold*/
					if (maxInfoGain < infoGain) /*check info gain*/
					{
						maxInfoGain = infoGain;
						splitor->splitPosition = tempSplitor->splitPosition;
						splitor->splitValue = tempSplitor->splitValue;
					}
				}
			}
		}
		if (tempSplitor != NULL) delete tempSplitor;
	}

	if (maxInfoGain > 0)  EvaluateSplitor(splitor, dataSamples, leftTreeSamples, rightTreeSamples);
	else
	{
		delete leftTreeSamples;
		delete rightTreeSamples;
		rightTreeSamples = leftTreeSamples = NULL;
	}

	if (tempLeftTreeSamples != NULL) delete tempLeftTreeSamples;
	if (tempRightTreeSamples != NULL) delete tempRightTreeSamples;
}

/*compute information gain*/
double TreeNode::EvaluateSplitor(TreeNodeSplitor* testedSplitor, DataSampleSet*  dataSamples,
	DataSampleSet*& leftTreeSamples, DataSampleSet*& rightTreeSamples)
{
	leftTreeSamples->samples.clear();
	rightTreeSamples->samples.clear();
	for (int i = 0; i<dataSamples->GetNumSamples(); i++)
	{
		double prediction = testedSplitor->ClassifySample(dataSamples->samples.at(i));

		if (prediction >= 0) rightTreeSamples->InsertSample(dataSamples->samples.at(i));
		else leftTreeSamples->InsertSample(dataSamples->samples.at(i));
	}

	double infoGain = 0;
	if (rightTreeSamples->GetNumSamples() > RandomTree::trainingParameter.minLeafSize &&
		leftTreeSamples->GetNumSamples() > RandomTree::trainingParameter.minLeafSize)
	{
		double entropy = dataSamples->EstimateEntropy(false);
		double p = double(leftTreeSamples->GetNumSamples()) / double(dataSamples->GetNumSamples());
		double leftEntropy = 0;
		double rightEntropy = 0;

		if (p < 0.5)
		{
			leftEntropy = leftTreeSamples->EstimateEntropy(true);
			if (rightTreeSamples->categoryDist == NULL)
				rightTreeSamples->categoryDist = new double[dataSamples->GetNumLabels()];

			for (int i = 0; i<dataSamples->GetNumLabels(); i++)
			{
				rightTreeSamples->categoryDist[i] = (dataSamples->categoryDist[i] * dataSamples->GetNumSamples() -
					leftTreeSamples->categoryDist[i] * leftTreeSamples->GetNumSamples());

				rightTreeSamples->categoryDist[i] /= rightTreeSamples->GetNumSamples();
				if (rightTreeSamples->categoryDist[i]>0)
					rightEntropy += (-rightTreeSamples->categoryDist[i] * log2(rightTreeSamples->categoryDist[i]));
			}
		}
		else
		{
			rightEntropy = rightTreeSamples->EstimateEntropy(true);
			if (leftTreeSamples->categoryDist == NULL)
				leftTreeSamples->categoryDist = new double[dataSamples->GetNumLabels()];

			for (int i = 0; i<dataSamples->GetNumLabels(); i++)
			{
				leftTreeSamples->categoryDist[i] = (dataSamples->categoryDist[i] * dataSamples->GetNumSamples() -
					rightTreeSamples->categoryDist[i] * rightTreeSamples->GetNumSamples());

				leftTreeSamples->categoryDist[i] /= leftTreeSamples->GetNumSamples();
				if (leftTreeSamples->categoryDist[i]>0)
					leftEntropy += (-leftTreeSamples->categoryDist[i] * log2(leftTreeSamples->categoryDist[i]));
			}
		}

		infoGain = entropy - p*leftEntropy - (1 - p)*rightEntropy;
	}

	return infoGain;
}

/*binary partition of label space*/
void TreeNode::GenerateLabelPartition(DataSampleSet* dataSamples, vector<int>& binarySequence, int k)
{
	int numLabels = dataSamples->GetNumLabels();
	srand(time(NULL));

	vector<int> activeCategoryIndex;
	/*exclude the labels that do not exist*/
	for (int i = 0; i<numLabels; i++)
	{
		if (dataSamples->categoryDist[i] > 0) activeCategoryIndex.push_back(i);
	}

	int reducedNumLabels = activeCategoryIndex.size();
	int startIndex = 1;
	int endIndex = pow(float(2), reducedNumLabels) - 2;
	k = min(endIndex - startIndex + 1, k);

	if (reducedNumLabels < numLabels)
	{
		vector<int> tempBinarySequence;
		BasicAPI::GenerateRandomSequence(startIndex, endIndex, tempBinarySequence, k);

		binarySequence.clear();
		for (int i = 0; i<tempBinarySequence.size(); i++)
		{
			vector<bool> tempBinary;
			DecimalToBinary(tempBinarySequence.at(i), reducedNumLabels, tempBinary);

			vector<bool> binary;
			for (int j = 0; j<numLabels; j++) binary.push_back(false);
			for (int j = 0; j<activeCategoryIndex.size(); j++)
			{
				binary.at(activeCategoryIndex.at(j)) = tempBinary.at(j);
			}

			int s = 0;
			for (int j = 0; j<numLabels; j++)
			{
				if (binary.at(j) == true) s += pow(float(2), j);
			}

			DecimalToBinary(s, numLabels, tempBinary);

			for (int j = 0; j<numLabels; j++)
			{
				assert(binary.at(j) == tempBinary.at(j));
			}

			binarySequence.push_back(s);
		}
	}
	else BasicAPI::GenerateRandomSequence(startIndex, endIndex, binarySequence, k);
}

/*save nodes of RTSVM tree*/
void TreeNode::SaveNode(ofstream& out)
{
	out << "Node " << nodeIndex << " " << level << " " << nodeType << " " << leafLabel << " " << " " << purity << " " << numSamples << endl;
	if (IsLeaf() == false)
	{
		out << "Link " << nodeIndex << " " << leftChild->GetNodeIndex() << " " << rightChild->GetNodeIndex() << endl;
		/*save split hyperplane*/
		splitor->SaveTreeNodeSplitor(out);
	}
}

bool TreeNode::LoadNode(ifstream& input)
{
	const int filelength = 1024 * 10;
	char temp[filelength];

	input.getline(temp, filelength);
	string textLine(temp);

	vector<string> textElement;
	BasicAPI::TextlineSplit(textLine, textElement, ' ');

	nodeIndex = atoi(textElement.at(1).c_str());
	level = atoi(textElement.at(2).c_str());
	nodeType = atoi(textElement.at(3).c_str());
	leafLabel = atoi(textElement.at(4).c_str());

	std::istringstream stm;
	stm.str(textElement.at(5).c_str());
	stm >> purity;

	if (nodeType != NODE_LEAF)
	{
		/*load link*/
		input.getline(temp, filelength);
		textLine = string(temp);

		BasicAPI::TextlineSplit(textLine, textElement, ' ');

		leftChildIndex = atoi(textElement.at(2).c_str());
		rightChildIndex = atoi(textElement.at(3).c_str());

		if (splitor != NULL) delete splitor;
		splitor = new TreeNodeSplitor();
		splitor->LoadTreeNodeSplitor(input);
	}
	return true;
}

RandomTree::RandomTree()
{
	numSamples = 0;
	treeIndex = -1;
}

RandomTree::~RandomTree()
{

	for (int i = 0; i<nodes.size(); i++)
	{
		delete nodes.at(i);
	}
	nodes.clear();
}

void RandomTree::ClassifySamples(DataSampleSet* dataSamples)
{
	int numLabels = dataSamples->GetNumLabels();

	double** confusionMatrix = new double*[numLabels];
	double* categoryCount = new double[numLabels];

	for (int i = 0; i<numLabels; i++)
	{
		categoryCount[i] = 0;
		confusionMatrix[i] = new double[numLabels];
		for (int j = 0; j<numLabels; j++)
		{
			confusionMatrix[i][j] = 0;
		}
	}

	for (int i = 0; i<dataSamples->GetNumSamples(); i++)
	{
		int predictedLabel = ClassifySample(dataSamples->GetSample(i));
		int trueLabel = dataSamples->GetSample(i)->GetLabel();

		int predictedLabelIndex = dataSamples->GetLabelIndex(predictedLabel);
		int trueLabelIndex = dataSamples->GetLabelIndex(trueLabel);

		confusionMatrix[trueLabelIndex][predictedLabelIndex]++;
		categoryCount[trueLabelIndex]++;
	}

	cout << "Testing result: " << endl;
	for (int i = 0; i<numLabels; i++)
	{
		for (int j = 0; j<numLabels; j++)
		{
			cout << confusionMatrix[i][j] << " ";
		}
		cout << endl;
	}

	for (int i = 0; i<numLabels; i++) delete[] confusionMatrix[i];
	delete[] confusionMatrix;
	delete[] categoryCount;
}

/*save trained RTSVM tree*/
void RandomTree::SaveTree(ofstream& out)
{
	/*save tree*/
	out << "& treeIndex: " << treeIndex << endl;
	out << "& number-of-training-samples: " << numSamples << endl;
	out << "& number-of-nodes: " << nodes.size() << endl;

	for (int i = 0; i<nodes.size(); i++)
	{
		assert(nodes.at(i)->GetNodeIndex() == i);
		nodes.at(i)->SaveNode(out);
	}
}

bool RandomTree::LoadTree(ifstream& input)
{
	const int filelength = 1024 * 10;
	char temp[filelength];

	input.getline(temp, filelength);
	string textLine(temp);
	vector<string> textElement;
	BasicAPI::TextlineSplit(textLine, textElement, ' ');
	treeIndex = atoi(textElement.at(2).c_str());

	input.getline(temp, filelength);
	textLine = string(temp);
	BasicAPI::TextlineSplit(textLine, textElement, ' ');
	numSamples = atoi(textElement.at(2).c_str());

	/*load number of nodes*/
	input.getline(temp, filelength);
	textLine = string(temp);
	BasicAPI::TextlineSplit(textLine, textElement, ' ');
	int numNodes = atoi(textElement.at(2).c_str());

	nodes.clear();
	for (int i = 0; i<numNodes; i++)
	{
		TreeNode* newNode = new TreeNode();
		nodes.push_back(newNode);
	}

	for (int i = 0; i<nodes.size(); i++)
	{
		nodes.at(i)->LoadNode(input);
		assert(nodes.at(i)->nodeIndex == i);
	}

	/*setup links*/
	for (int i = 0; i<nodes.size(); i++)
	{
		if (nodes.at(i)->IsLeaf() == false)
		{
			nodes.at(i)->leftChild = nodes.at(nodes.at(i)->leftChildIndex);
			nodes.at(i)->rightChild = nodes.at(nodes.at(i)->rightChildIndex);
		}
	}
	return true;
}

void RandomTree::ConstructTree(DataSampleSet* samples, int level)
{
	/*compute initial purity*/
	if (samples->categoryDist == NULL) samples->EstimatePurity();
	if (level == 0) numSamples = samples->GetNumSamples();

	//    cout<<endl<<"sample size        : "<<samples->GetNumSamples()<<endl;
	//    cout<<"sample distribution: ";
	//	for(int i=0;i<samples->GetNumLabels();i++)
	//	{
	//		cout<<samples->categoryDist[i]<<" ";
	//	}
	//	cout<<endl;

	/*compute the best purity*/
	double bestPurity = 0;
	int bestLabel = 0;
	for (int i = 0; i<samples->GetNumLabels(); i++)
	{
		if (bestPurity < samples->categoryDist[i])
		{
			bestPurity = samples->categoryDist[i];
			bestLabel = i; /*the index of the true label*/
			//bestLabel  = samples->GetLabel(i);
		}
	}

	/*node*/
	TreeNode * ptNode = new TreeNode(nodes.size());
	ptNode->SetNodeLevel(level);
	ptNode->numSamples = samples->GetNumSamples();
	ptNode->purity = bestPurity;
	nodes.push_back(ptNode);

	if (bestPurity < ptNode->objectivePurity) /*if not achieve the objective purity*/
	{
		DataSampleSet* leftTreeSamples = NULL;
		DataSampleSet* rightTreeSamples = NULL;

		ptNode->ConstructSplitor(samples, leftTreeSamples, rightTreeSamples);

		if (leftTreeSamples != NULL && rightTreeSamples != NULL) /*spliting of the dataset is successful*/
		{
			/*left sub-tree*/
			ptNode->leftChildIndex = nodes.size();
			ConstructTree(leftTreeSamples, level + 1);
			//cout<<ptNode->nodeIndex<<" "<<ptNode->leftChildIndex<<" "<<nodes.size()<<endl;
			ptNode->leftChild = nodes.at(ptNode->leftChildIndex);
			assert(ptNode->leftChildIndex == ptNode->leftChild->nodeIndex);
			delete leftTreeSamples;
			leftTreeSamples = NULL;

			/*right sub-tree*/
			ptNode->rightChildIndex = nodes.size();
			ConstructTree(rightTreeSamples, level + 1);
			//cout<<ptNode->nodeIndex<<" "<<ptNode->rightChildIndex<<" "<<nodes.size()<<endl;
			ptNode->rightChild = nodes.at(ptNode->rightChildIndex);
			assert(ptNode->rightChildIndex == ptNode->rightChild->nodeIndex);
			delete rightTreeSamples;
			rightTreeSamples = NULL;
		}
		else /*build a leaf, when no further split is possible*/
		{
			ptNode->SetNodeType(NODE_LEAF);
			ptNode->SetLeaveLabel(bestLabel);
			//cout<<"end"<<endl;
		}
	}
	else /*build a leaf*/
	{
		ptNode->SetNodeType(NODE_LEAF);
		ptNode->SetLeaveLabel(bestLabel);
		//cout<<"leaf"<<endl;
	}
}

void RandomTree::TrainingAnalysis()
{
	cout << "Total number of samples: " << numSamples << endl;

	for (int i = 0; i<nodes.size(); i++)
	{
		if (nodes.at(i)->IsLeaf() == true)
		{
			cout << endl;
			cout << "node:  " << nodes.at(i)->numSamples << endl;
			cout << "level: " << nodes.at(i)->level << endl;
			cout << "target-label: " << nodes.at(i)->leafLabel << endl;
			cout << "purity: " << nodes.at(i)->purity << endl;
		}
	}
}

int RandomTree::ClassifySample(DataSample* sample)
{
	assert(nodes.size() > 0);
	TreeNode* ptNode = nodes.at(0);
	while (ptNode->IsLeaf() == false)
	{
		if (ptNode->splitor->ClassifySample(sample) == -1) ptNode = ptNode->leftChild;
		else ptNode = ptNode->rightChild;
	}

	return ptNode->GetLeaveLabel();
}

RandomForest::RandomForest()
{
	numSamples = 0;
}

RandomForest::~RandomForest()
{
	for (int i = 0; i<trees.size(); i++)
	{
		delete trees.at(i);
	}
	trees.clear();
}

void RandomForest::PredictClassProb(DataSample* sample, vector<float>& predictionProb)
{
	predictionProb.clear();
	assert(labelSet.size() > 0);
	for (int i = 0; i<labelSet.size(); i++) predictionProb.push_back(0);

	if (trainingParameter.paralleTesting == false)
	{
		for (int i = 0; i<trees.size(); i++)
		{
			int predictedLabelIndex = trees.at(i)->ClassifySample(sample);
			if (predictedLabelIndex >= 0) predictionProb[predictedLabelIndex]++;
		}
	}
	else
	{
		omp_set_num_threads(TestConfiguration.numThreads);
#pragma omp parallel for
		for (int i = 0; i<trees.size(); i++)
		{
			int predictedLabelIndex = trees.at(i)->ClassifySample(sample);
#pragma omp critical
			{
				if (predictedLabelIndex >= 0) predictionProb[predictedLabelIndex]++;
			}
		}
	}

	for (int i = 0; i<labelSet.size(); i++)
	{
		predictionProb[i] /= trees.size();
	}
}

int RandomForest::ClassifySample(DataSample* sample)
{
	vector<float> predictionProb;
	PredictClassProb(sample, predictionProb);

	float maxProb = 0;
	int predictedLabel = -1;
	for (int i = 0; i<labelSet.size(); i++)
	{
		if (maxProb < predictionProb[i])
		{
			maxProb = predictionProb[i];
			predictedLabel = labelSet.at(i);
		}
	}

	return predictedLabel;
}

void RandomForest::ClassifySamples(DataSampleSet* samples)
{
	int numLabels = samples->GetNumLabels();
	double** confusionMatrix = new double*[numLabels];
	double* categoryCount = new double[numLabels];

	for (int i = 0; i<numLabels; i++)
	{
		categoryCount[i] = 0;
		confusionMatrix[i] = new double[numLabels];
		for (int j = 0; j<numLabels; j++)
		{
			confusionMatrix[i][j] = 0;
		}
	}

	for (int i = 0; i<samples->GetNumSamples(); i++)
	{
		int predictedLabel = ClassifySample(samples->GetSample(i));
		int trueLabel = samples->GetSample(i)->GetLabel();

		int predictedLabelIndex = samples->GetLabelIndex(predictedLabel);
		int trueLabelIndex = samples->GetLabelIndex(trueLabel);

		if (trueLabelIndex >= 0)
		{
			confusionMatrix[trueLabelIndex][predictedLabelIndex]++;
			categoryCount[trueLabelIndex]++;
		}
	}

	double correctSamples = 0;
	cout << "Testing result: " << endl;
	for (int i = 0; i<numLabels; i++)
	{
		for (int j = 0; j<numLabels; j++)
		{
			cout << confusionMatrix[i][j] << " ";
		}
		cout << endl;
		correctSamples += confusionMatrix[i][i];
	}

	if (samples->GetNumSamples() > 0)
		cout << "Overall Accuracy: " << float(correctSamples) / float(samples->GetNumSamples()) << endl;

	for (int i = 0; i<numLabels; i++) delete[] confusionMatrix[i];
	delete[] confusionMatrix;
	delete[] categoryCount;
}

void RandomForest::ConstructForest(DataSampleSet* samples)
{
	samples->SearchActiveAttributes(activeAttributesIndex);

	numSamples = samples->GetNumSamples();
	labelSet = samples->labelSet;
	TreeNodeSplitor::featDim = samples->GetSampleDataLength();

	RandomTree::trainingParameter.numPosTry = int(TreeNodeSplitor::featDim * RandomTree::trainingParameter.ratioPosTry);
	numSplitSegments = RandomTree::trainingParameter.numSplitTry + 1;

	srand(time(NULL));
	if (trainingParameter.paralleTraining == false)
	{
		for (int i = 0; i<trainingParameter.numTrees; i++)
		{
			cout << "Construct the " << i + 1 << "th tree" << endl;
			DataSampleSet* sampledDataset = NULL;
			samples->RandomSample(sampledDataset, trainingParameter.treeSampleRatio);
			RandomTree* ptTree = new RandomTree(i);
			ptTree->ConstructTree(sampledDataset);
			trees.push_back(ptTree);
			if (sampledDataset != NULL)  delete sampledDataset;
		}
	}
	else
	{
		omp_set_num_threads(TestConfiguration.numThreads);
#pragma omp parallel for
		for (int i = 0; i<trainingParameter.numTrees; i++)
		{
			cout << "Construct the " << i + 1 << "th tree" << endl;
			DataSampleSet* sampledDataset = NULL;
			samples->RandomSample(sampledDataset, trainingParameter.treeSampleRatio);
			RandomTree* ptTree = new RandomTree(i);
			ptTree->ConstructTree(sampledDataset);
#pragma omp critical
			{
				trees.push_back(ptTree);
			}
			if (sampledDataset != NULL)  delete sampledDataset;
		}
	}
}

void RandomForest::SaveForest(string modefilename)
{
	ofstream out;
	out.open(modefilename.c_str());
	out << "& number-of-training-samples: " << numSamples << endl;
	out << "& label-set: ";
	for (int i = 0; i<labelSet.size(); i++) out << labelSet.at(i) << " ";
	out << endl;
	out << "& feature-dim: " << TreeNodeSplitor::featDim << endl;

	/*save training parameters*/
	out << "& number-of-tree: " << RandomForest::trainingParameter.numTrees << endl;
	out << "& tree-sample-ratio: " << RandomForest::trainingParameter.treeSampleRatio << endl;
	out << "& tree-max-depth: " << RandomTree::trainingParameter.maxDepth << endl;
	out << "& min-leaf-size: " << RandomTree::trainingParameter.minLeafSize << endl;
	out << "& splitor-type: " << RandomTree::trainingParameter.splitorType << endl;

	for (int i = 0; i<trees.size(); i++)
	{
		trees.at(i)->SaveTree(out);
	}
	out.close();
}

bool RandomForest::LoadForest(string modefilename)
{
	ifstream input;
	input.open(modefilename.c_str());

	if (!input)
	{
		cout << "load model from file failed: " << modefilename << endl;
		return false;
	}

	const int filelength = 1024 * 10;
	char temp[filelength];

	input.getline(temp, filelength);
	string textLine(temp);
	vector<string> textElement;
	BasicAPI::TextlineSplit(textLine, textElement, ' ');
	numSamples = atoi(textElement.at(2).c_str());

	labelSet.clear();
	input.getline(temp, filelength);
	textLine = string(temp);
	BasicAPI::TextlineSplit(textLine, textElement, ' ');
	for (int i = 2; i<textElement.size(); i++)
	{
		labelSet.push_back(atoi(textElement.at(i).c_str()));
	}

	input.getline(temp, filelength);
	textLine = string(temp);
	BasicAPI::TextlineSplit(textLine, textElement, ' ');
	TreeNodeSplitor::featDim = atoi(textElement.at(2).c_str());

	input.getline(temp, filelength);
	textLine = string(temp);
	BasicAPI::TextlineSplit(textLine, textElement, ' ');
	RandomForest::trainingParameter.numTrees = atoi(textElement.at(2).c_str());

	input.getline(temp, filelength);
	textLine = string(temp);
	BasicAPI::TextlineSplit(textLine, textElement, ' ');
	RandomForest::trainingParameter.treeSampleRatio = atof(textElement.at(2).c_str());

	input.getline(temp, filelength);
	textLine = string(temp);
	BasicAPI::TextlineSplit(textLine, textElement, ' ');
	RandomTree::trainingParameter.maxDepth = atoi(textElement.at(2).c_str());

	input.getline(temp, filelength);
	textLine = string(temp);
	BasicAPI::TextlineSplit(textLine, textElement, ' ');
	RandomTree::trainingParameter.minLeafSize = atoi(textElement.at(1).c_str());

	input.getline(temp, filelength);
	textLine = string(temp);
	BasicAPI::TextlineSplit(textLine, textElement, ' ');
	RandomTree::trainingParameter.splitorType = atoi(textElement.at(2).c_str());

	trees.clear();
	for (int i = 0; i<trainingParameter.numTrees; i++)
	{
		RandomTree* ptTree = new RandomTree();
		ptTree->LoadTree(input);
		trees.push_back(ptTree);
	}

	input.close();
	return true;
}
