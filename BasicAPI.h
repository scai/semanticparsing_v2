#ifndef BASIC2DAPI_H_
#define BASIC2DAPI_H_

#include <string>
#include <vector>
#include "cv.h"
#include "cxcore.h"
#include "cvaux.h"
#include "highgui.h"
#include <fstream>
#include <iostream>
#include "GCO/GCoptimization.h"

using namespace std;

namespace BasicAPI
{
	void ViewMat(cv::Mat& Matrix, string SaveFileName);
	void ViewMatJetColor(cv::Mat& Matrix, string SaveFileName, cv::Mat* Mask = NULL);

	void SavingMatrixToFile(cv::Mat& Matrix, const char* filename);
	bool LoadMatrixFromFile(cv::Mat& Matrix, const char* filename);

	void ComputeImageGradient(IplImage* Image, cv::Mat& Gradient, bool normalization = true);
	void ComputeImageGradient(IplImage* Image, cv::Mat& Gradient_x, cv::Mat& Gradient_y, bool normalization = true);

	void ExtractConnectedComponent(IplImage* ptInputImage, cv::Scalar const& markColor, vector< vector<cv::Point> >& connectedComponents, bool checkBoundaryOnly);
	void ExtractConnectedComponent(cv::Mat& inputImage, int markIndex, vector< vector<cv::Point> >& connectedComponents, bool checkBoundaryOnly);

	string ExtractFileName(string file, bool withExtension);
	string ExtractFileExtention(string file);
	string ExtractPath(string filename);

	string NumberToString(int v);
	string NumberToString(float v);
	string NumberToString(double v);

	bool FileExistCheck(const char* filename, ifstream& inputfile);
	bool FileExistCheck(const char* filename);
	void TextlineSplit(string textLine, vector<string>& textElement, char splitFlag);
	//void TextlineSplitW(string textLine, vector<string>& textElement);

	void Normalization(vector<float>& vec, string type = "L2");
	void AnalysisConfusionMap(cv::Mat& confusionMatrix);

	string ExtractFileName(string filename, bool withExtension);
	string ExtractPath(string filename);

	void GenerateRandomColorSet(int numColors, vector<cv::Scalar>& colorset);

	void GenerateRandomSequence(int s, int e, vector<int>& seq, int length);
}
#endif /* BASIC2DAPI_H_ */
