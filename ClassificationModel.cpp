#include "ClassificationModel.h"
#include "BasicAPI.h"
#include "GraphModel.h"
#include "StructLearner.h"
//#include "FacadePartition.h"
using namespace BasicAPI;

DataExtractor::DataExtractor() {
	// TODO Auto-generated constructor stub

}

DataExtractor::~DataExtractor() {
	// TODO Auto-generated destructor stub
}

ImageParser::ImageParser() {
	// TODO Auto-generated constructor stub

	pixelRTrees = NULL;
	superpixelRTrees = NULL;

	classifierLoaded = false;
}

ImageParser::~ImageParser() {
	// TODO Auto-generated destructor stub

	if (pixelRTrees != NULL){
		delete pixelRTrees;
	}
	if (superpixelRTrees != NULL){
		delete superpixelRTrees;
	}
}

/*only extract super pixel feature*/
void DataExtractor::FeatureExtractionTraining(vector<FeatVector>& sampleFeatureSet, SuperPixelImage* trainImage,
	int* trainSampleStat, int numFeatures)
{
	/*only extract one type of features*/
	assert(TestConfiguration.withSPLevelfeatures != TestConfiguration.withPixelLevelfeatures);
	int featureNum = 0; // the number of valid features
	int idx = 0;

	vector<int> sampleFeatureSetIndex;
	vector<int> tempfeatures;
	if (TestConfiguration.withSPLevelfeatures == true)
	{
		ExtractFeatureSamples(trainImage, sampleFeatureSetIndex, false, true);
		tempfeatures = sampleFeatureSetIndex;

		featureNum = tempfeatures.size();
		numFeatures = min(numFeatures, featureNum);

		if (numFeatures>0)
		{
			vector<int> selectedSamples;
			BasicAPI::GenerateRandomSequence(0, featureNum - 1, selectedSamples, numFeatures);
			for (unsigned int i = 0; i<selectedSamples.size(); i++)
			{
				idx = tempfeatures.at(selectedSamples.at(i));
				FeatVector feat;
				sampleFeatureSet.push_back(feat);
				int featIndex = sampleFeatureSet.size() - 1;

				sampleFeatureSet.at(featIndex).SetLabel(trainImage->superPixels.at(idx).feature_2d.GetLabel());
				sampleFeatureSet.at(featIndex).UpdateData(trainImage->superPixels.at(idx).feature_2d.GetDataPtr());

				/*add image level feature*/
				if (TestConfiguration.withImageLevelfeatures == true)
				{
					for (int j = 0; j<trainImage->superPixels.at(idx).feature_2d.imageLevelFeat->GetFeatDim(); j++)
					{
						double featv = trainImage->superPixels.at(idx).feature_2d.imageLevelFeat->GetAttributeValue(j);
						sampleFeatureSet.at(featIndex).InsertNewAttribute(featv);
					}
				}

				if (trainSampleStat != NULL)
				{
					trainSampleStat[sampleFeatureSet.at(featIndex).GetLabel() - 1]++;
				}
			}
		}
	}
	else
	{
		ExtractFeatureSamples(trainImage, sampleFeatureSetIndex, true, true);
		tempfeatures = sampleFeatureSetIndex;
		featureNum = tempfeatures.size();
		numFeatures = min(numFeatures, featureNum);

		if (numFeatures>0)
		{
			vector<int> selectedSamples;
			BasicAPI::GenerateRandomSequence(0, featureNum - 1, selectedSamples, numFeatures);

			for (unsigned int i = 0; i<selectedSamples.size(); i++)
			{
				idx = tempfeatures.at(selectedSamples.at(i));
				if (trainImage->features_2d.at(idx) != NULL)
				{
					FeatVector feat;
					sampleFeatureSet.push_back(feat);
					int featIndex = sampleFeatureSet.size() - 1;

					sampleFeatureSet.at(featIndex).SetLabel(trainImage->features_2d.at(idx)->GetLabel());
					sampleFeatureSet.at(featIndex).UpdateData(trainImage->features_2d.at(idx)->GetDataPtr());

					if (trainSampleStat != NULL)
					{
						trainSampleStat[sampleFeatureSet.at(featIndex).GetLabel() - 1]++;
					}
				}
			}
		}

		for (unsigned int i = 0; i<trainImage->features_2d.size(); ++i)
		{
			if (trainImage->features_2d[i] != NULL) delete trainImage->features_2d[i];
		}
		trainImage->features_2d.clear();
	}

	if (sampleFeatureSet.size()>0)
	{
		int feavdim = sampleFeatureSet.at(0).GetFeatDim();
		cout << "number of training samples: " << sampleFeatureSet.size() << "(" << feavdim << ")" << endl;
	}
	else
	{
		cout << "warning: no training samples " << endl;
	}
}

/*training random forest with training feature vectors*/
void ImageParser::Train(vector<BasicImage>& TrainImages)
{
	RTFileName = TestConfiguration.GetClassifierName(false);
	cout << "Classifier Name: " << RTFileName << endl;
	vector<FeatVector> SampleFeatureSet;
	extractor.FeatureExtractionTraining(SampleFeatureSet, TrainImages);
	TrainClassifier(SampleFeatureSet);
}

void DataExtractor::FeatureExtractionTraining(vector<FeatVector>& SampleFeatureSet, vector<BasicImage>& TrainImages)
{
	if (TrainImages.size() <= 0)
	{
		cout << "no training images loaded..." << endl;
		return;
	}

	VlDsiftFilterGPU* SIFTFilter;
	int PatchSize = 8, sampleStep = 1;
	SIFTFilter = (VlDsiftFilterGPU*)vl_dsift_new_basic(widthO, heightO, sampleStep, PatchSize);

	assert(TestConfiguration.withPixelLevelfeatures == false || TestConfiguration.withSPLevelfeatures == false);

	int numoftrainimages = TrainImages.size();
	int numoftrainsamples_per = TestConfiguration.numofMaxTrainSamples / numoftrainimages;

	int i;
	int numclass = TestConfiguration.numOfClass;

	/*training samples statistics*/
	int* train_sample_stat = new int[numclass];
	for (i = 0; i<numclass; i++) train_sample_stat[i] = 0;
	/*when training, adjust the sample step*/
	for (unsigned int i = 0; i<TestConfiguration.featParameters.size(); i++)
	{
		if (TestConfiguration.featParameters.at(i).sampleStep < 4)
		{
			TestConfiguration.featParameters.at(i).sampleStep =
				TestConfiguration.featParameters.at(i).featPatchSize / 2;
			cout << TestConfiguration.featParameters.at(i).strFeatName << endl;
			cout << "sample step for classifier training ... ";
			cout << "adjust from " << TestConfiguration.featParameters.at(i).sampleStep << " to " <<
				TestConfiguration.featParameters.at(i).featPatchSize / 2;
			cout << endl;
		}
	}

	if (TestConfiguration.parallel == true)
	{
		omp_set_num_threads(TestConfiguration.numThreads);
#pragma omp parallel for
		for (int k = 0; k<TrainImages.size(); k++)
		{
			if (TestConfiguration.withPixelLevelfeatures == true)
			{
				cout << "feature :" << TestConfiguration.featureFlag << " extraction..." << endl;
				cout << k + 1 << "/" << TrainImages.size() << " " << TrainImages.at(k).filenameImage << endl;
				cout << k + 1 << "/" << TrainImages.size() << " " << TrainImages.at(k).filenameLabelMask << endl;

				SuperPixelImage trainImage;
				if (trainImage.Initialization(&TrainImages.at(k), TestConfiguration.testLevel) == true)
				{
					trainImage.FeatureExtraction(SIFTFilter);
#pragma omp critical
					{
						FeatureExtractionTraining(SampleFeatureSet, &trainImage, train_sample_stat, numoftrainsamples_per);
					}
				}
			}
		}
	}
	else
	{
		for (unsigned int k = 0; k<TrainImages.size(); k++)
		{
			if (TestConfiguration.withPixelLevelfeatures == true)
			{
				cout << "feature :" << TestConfiguration.featureFlag << " extraction..." << endl;
				cout << k + 1 << "/" << TrainImages.size() << " " << TrainImages.at(k).filenameImage << endl;
				cout << k + 1 << "/" << TrainImages.size() << " " << TrainImages.at(k).filenameLabelMask << endl;

				SuperPixelImage trainImage;
				if (trainImage.Initialization(&TrainImages.at(k), TestConfiguration.testLevel) == true)
				{
					trainImage.FeatureExtraction(SIFTFilter);
					FeatureExtractionTraining(SampleFeatureSet, &trainImage, train_sample_stat, numoftrainsamples_per);
				}
			}
		}
	}

	for (int i = 0; i<numclass; i++) train_sample_stat[i] = 0;
	/*super pixel level feature based classifier*/
	for (unsigned int k = 0; k<TrainImages.size(); k++)
	{
		if (TestConfiguration.withSPLevelfeatures == true)
		{
			cout << "sp feature :" << TestConfiguration.featureFlag << " extraction..." << endl;
			cout << k + 1 << "/" << TrainImages.size() << " " << TrainImages.at(k).filenameImage << endl;
			cout << k + 1 << "/" << TrainImages.size() << " " << TrainImages.at(k).filenameLabelMask << endl;

			SuperPixelImage trainImage;
			if (trainImage.Initialization(&TrainImages.at(k), TestConfiguration.testLevel) == true)
			{
				trainImage.FeatureExtraction(SIFTFilter);
				FeatureExtractionTraining(SampleFeatureSet, &trainImage, train_sample_stat, numoftrainsamples_per);
			}
		}
	}
	vl_dsift_delete_gpu(SIFTFilter);
}

/*extract the valid feature set for the image*/
/* set withLabel for training*/
/* set isPixelFeature for pixel level feature or super pixel level feature*/
//extractor.ExtractFeatureSamples(testImage, SampleFeatureSet, true, false);
void DataExtractor::ExtractFeatureSamples(SuperPixelImage* testImage,
	vector<int>& SampleFeatureSet,//record the pixels that have concatenated features
	bool isPixelFeature,
	bool withLabel)
{
	SampleFeatureSet.clear();
	if (isPixelFeature == true)
	{
		for (unsigned int i = 0; i<testImage->features_2d.size(); i++)
		{
			if (testImage->features_2d.at(i) != NULL)
			{
				if (testImage->features_2d.at(i)->GetFeatDim() > 0)
				{
					SampleFeatureSet.push_back(i);
				}
			}
		}

		if (withLabel == true)
		{
			vector<int> tempSampleFeatureSet;
			for (unsigned int i = 0; i<SampleFeatureSet.size(); i++)
			{
				int idx = SampleFeatureSet.at(i);
				if (testImage->features_2d.at(idx) != NULL)
				{
					if (testImage->features_2d.at(idx)->GetLabel()>0)
					{
						tempSampleFeatureSet.push_back(idx);
					}
				}
			}
			SampleFeatureSet = tempSampleFeatureSet;
		}
	}
	else
	{
		for (unsigned int i = 0; i<testImage->superPixels.size(); i++)
		{
			/*note that the two features will not exist at the same time*/
			if (testImage->superPixels.at(i).feature_2d.GetFeatDim()>0)
			{
				if (TestConfiguration.withImageLevelfeatures == true)
				{
					if (testImage->superPixels.at(i).feature_2d.imageLevelFeat != NULL)
					{
						SampleFeatureSet.push_back(i);
					}
				}
				else SampleFeatureSet.push_back(i);
			}
		}

		if (withLabel == true)
		{
			vector<int> tempSampleFeatureSet;
			for (unsigned int i = 0; i<SampleFeatureSet.size(); i++)
			{
				int idx = SampleFeatureSet.at(i);
				if (testImage->superPixels.at(idx).feature_2d.GetLabel()>0)
				{
					tempSampleFeatureSet.push_back(idx);
				}
			}
			SampleFeatureSet = tempSampleFeatureSet;
		}
	}
}

void ImageParser::UpdateGraphModel(BasicImage& TestImage)
{
	StructLearner trainer(TestConfiguration.numOfClass);
	trainer.initialParameters.SetDefaultParameters();
	trainer.learnedParameters = trainer.initialParameters;
	trainer.parameterFlag = BasicAPI::ExtractFileName(TestImage.filenameImage, false);

	string weight_file_name = trainer.ModelParameterFilename();
	cout << weight_file_name << endl;
	trainer.LoadLearnedWeight(weight_file_name);
	GraphModel::AssignGraphParameters(trainer.initialParameters);
}

/*the main function for testing*/
void ImageParser::Test(vector<BasicImage>& TestImages)
{
	TestInitialization();//loading the classifier file
	if (TestImages.size() == 0)
	{
		cout << "No testing images..." << endl;
		return;
	}

	int k = 0;
	/*Do not support parallel processing for testing with local parameters now*/
	if (TestConfiguration.localGraphTraining == true)
	{
		for (k = 0; k<TestImages.size(); k++)
		{
			cout << "process " << ExtractFileName(TestImages.at(k).filenameImage, true) << endl;
			cout << k + 1 << "/" << TestImages.size() << endl;
			UpdateGraphModel(TestImages.at(k));
			TestBasicClassifiers(TestImages.at(k), TestConfiguration.confusionMatrix);
		}
	}
	else
	{
		if (TestConfiguration.parallel == true)
		{
			omp_set_num_threads(TestConfiguration.numThreads);
#pragma omp parallel for
			for (k = 0; k<TestImages.size(); k++)
			{
				cout << "process " << ExtractFileName(TestImages.at(k).filenameImage, true) << endl;
				cout << k + 1 << "/" << TestImages.size() << endl;
				TestBasicClassifiers(TestImages.at(k), TestConfiguration.confusionMatrix);
			}
		}
		else
		{
			//omp_set_num_threads(TestConfiguration.numThreads);
//#pragma omp parallel for
			for (k = 0; k<TestImages.size(); k++)
			{
				cout << endl << endl;
				cout << "process " << ExtractFileName(TestImages[k].filenameImage, true) << endl;
				cout << k + 1 << "/" << TestImages.size() << endl;
				int64 t0 = cv::getTickCount();
				TestBasicClassifiers(TestImages[k], TestConfiguration.confusionMatrix);
				cout << "Whole Time: " << (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000 << " ms." << endl;
			}
		}
	}

	string resultfile = TestConfiguration.resultdir + TestConfiguration.datasetName + "_seg_result.txt";
	//cout <<"resultdir: "<< TestConfiguration.resultdir << "\t datasetName: " << TestConfiguration.datasetName << endl;
	TestConfiguration.SaveTestingResult(resultfile);
}

/*training random forest API*/
/*model_file_name the path to save the trained models*/
void ImageParser::TrainClassifier(vector<FeatVector>& sampleFeatureSet)
{
	DataSampleSet trainingSet;
	for (int i = 0; i<TestConfiguration.colorTable.GetNumberOfCategory(); i++)
	{
		trainingSet.labelSet.push_back(TestConfiguration.colorTable.GetCategoryLabel(i));
	}

	int numSamples = sampleFeatureSet.size();
	trainingSet.SetSampleDataLength(sampleFeatureSet.at(sampleFeatureSet.size() / 2).GetFeatDim());

	for (int i = 0; i<numSamples; i++)
	{
		int sampleLabel = sampleFeatureSet.at(i).GetLabel();
		sampleLabel = TestConfiguration.colorTable.UniqueLabel(sampleLabel);
		int sampleIndex = i;
		DataSample* sample = new DataSample(sampleIndex, sampleLabel);
		sample->SetData(&sampleFeatureSet.at(i));
		trainingSet.samples.push_back(sample);
	}

	int numclass = TestConfiguration.numOfClass;
	int feavdim = sampleFeatureSet.at(0).GetFeatDim();
	int* train_sample_stat = new int[numclass];

	for (int i = 0; i<numclass; i++) train_sample_stat[i] = 0;
	/*label range : 0 - number-of-class - 1 */
	for (unsigned int i = 0; i<sampleFeatureSet.size(); i++)
	{
		//cout << "check sampleFeatureSize: " << sampleFeatureSet.at(i).GetFeatDim()<< endl;
		assert(sampleFeatureSet.at(i).GetLabel()>0 && sampleFeatureSet.at(i).GetLabel() <= TestConfiguration.numOfClass);
		assert(sampleFeatureSet.at(i).GetFeatDim() == feavdim);
		int sampleLabel = sampleFeatureSet.at(i).GetLabel();
		sampleLabel = TestConfiguration.colorTable.UniqueLabel(sampleLabel);
		train_sample_stat[sampleLabel - 1]++;
	}

	cout << "Training samples statistics" << "(" << feavdim << ")" << ":" << endl;
	for (int i = 0; i<numclass; i++)
	{
		cout << TestConfiguration.colorTable.GetCategoryName(i) << ": " << train_sample_stat[i] << " " << float(train_sample_stat[i]) / float(sampleFeatureSet.size()) * 100 << "%" << endl;
	}
	delete[] train_sample_stat;

	RandomForest rtrees;
	rtrees.trainingParameter.paralleTraining = TestConfiguration.parallel;
	rtrees.ConstructForest(&trainingSet);
	rtrees.SaveForest(RTFileName);
	rtrees.ClassifySamples(&trainingSet);
	cout << "classifier stored!" << endl;
	trainingSet.ReleaseData();
}

/*for super pixel testing*/
void ImageParser::TestBasicClassifiers(SuperPixelImage* testImage, cv::Mat*& probability, cv::Mat*& probability_index)
{
	/*load confidence from file*/
	bool loadFlag = false;
	if (TestConfiguration.withPixelLevelfeatures == true)
	{
		if (TestConfiguration.loadConfidenceMap == true)
		{
			loadFlag = testImage->LoadConfidenceMap(probability, probability_index);
		}
	}

	if (TestConfiguration.withSPLevelfeatures == true)
	{
		if (TestConfiguration.loadConfidenceMap == true)
		{
			loadFlag = testImage->LoadConfidenceMapSP();
		}
	}

	if (loadFlag == false)
	{
		vector<int> SampleFeatureSet;
		int numclass = TestConfiguration.numOfClass;

		testImage->FeatureExtraction(NULL, "test");
		/**************************************/
		if (TestConfiguration.withPixelLevelfeatures == true)
		{
			extractor.ExtractFeatureSamples(testImage, SampleFeatureSet, true, false);
			if (SampleFeatureSet.size()>0)
			{
				probability = new cv::Mat(SampleFeatureSet.size(), numclass, CV_32FC1, cv::Scalar(0));
				probability_index = new cv::Mat(SampleFeatureSet.size(), 2, CV_32FC1, cv::Scalar(0));
				int idx = SampleFeatureSet.at(0);
				int x, y;
				DataSample testSample;
				vector<float> predictionProb;
//#pragma omp parallel for
				for (int i = 0; i<SampleFeatureSet.size(); i++)
				{
					idx = SampleFeatureSet.at(i);
					if (testImage->features_2d.at(idx) != NULL)
					{
						testSample.SetData(testImage->features_2d.at(idx));
						testImage->features_2d.at(idx)->GetExtraData(x, y);
						probability_index->at<float>(i, 0) = x;
						probability_index->at<float>(i, 1) = y;
						pixelRTrees->PredictClassProb(&testSample, predictionProb);

						for (int j = 0; j<numclass; j++)
						{
							probability->at<float>(i, j) = predictionProb[j];
							//cout<<prob[j]<<" ";
						}
						//cout<<endl;
					}
				}
			}

			/*release memory for the feature extraction*/
			//cout << testimage->features_2d.size() << endl;
			for (unsigned int i = 0; i<testImage->features_2d.size(); i++)
			{
				//cout << i << endl;
				if (testImage->features_2d[i] != NULL) delete testImage->features_2d[i];
			}
			testImage->features_2d.clear();
		}

		if (TestConfiguration.withSPLevelfeatures == true)
		{
			for (unsigned int i = 0; i<testImage->superPixels.size(); i++)
			{
				testImage->superPixels.at(i).confidenceValue.clear();
				for (int j = 0; j<TestConfiguration.numOfClass; j++)
				{
					testImage->superPixels.at(i).confidenceValue.push_back(0);
				}
			}

			extractor.ExtractFeatureSamples(testImage, SampleFeatureSet, false, false);
			if (SampleFeatureSet.size()>0)
			{
				int idx = SampleFeatureSet.at(0);
				int feavdim = testImage->superPixels.at(idx).feature_2d.GetFeatDim();

				if (TestConfiguration.withImageLevelfeatures == true)
				{
					feavdim += testImage->superPixels.at(idx).feature_2d.imageLevelFeat->GetFeatDim();
				}

				DataSample testSample;
				for (unsigned int i = 0; i<SampleFeatureSet.size(); i++)
				{
					idx = SampleFeatureSet.at(i);
					testSample.SetData(&testImage->superPixels.at(idx).feature_2d);
					superpixelRTrees->PredictClassProb(&testSample, testImage->superPixels.at(idx).confidenceValue);
				}
			}
		}
		//testImage->ViewConfidenceMap();
		/**************************************/

		if (TestConfiguration.saveConfidenceMap == true)
		{
			if (TestConfiguration.withPixelLevelfeatures == true)
				testImage->SaveConfidenceMap(*probability, *probability_index);

			if (TestConfiguration.withSPLevelfeatures == true)
				testImage->SaveConfidenceMapSP();
		}
	}
}

void ImageParser::TestBasicClassifiers(BasicImage& Image, cv::Mat& confusionMatrix)
{
	SuperPixelImage testImage;
	cv::Mat* probability = NULL;
	cv::Mat* probability_index = NULL;

	if (testImage.Initialization(&Image, TestConfiguration.testLevel) == false) return;
	TestBasicClassifiers(&testImage, probability, probability_index);

	//if(TestConfiguration.withPixelLevelfeatures == true)
	//	testImage.ViewPixelConfidenceMap(*probability,*probability_index);
	//else if(TestConfiguration.withSPLevelfeatures == true)
	//	testImage.ViewSegmentConfidenceMap();

	if (TestConfiguration.withPixelLevelfeatures == true)
		testImage.PixelMRFInference(*probability, *probability_index);
	else if (TestConfiguration.withSPLevelfeatures == true)
		testImage.SegmentMRFinference();

	vector<int> categorySizeConstraint;
	for (int i = 0; i<TestConfiguration.colorTable.GetNumberOfCategory(); i++)
	{
		categorySizeConstraint.push_back(0);
	}


	cout << "segmentation post processing" << endl;

	testImage.SegmentationPostProcessing(categorySizeConstraint);
	testImage.SegmentationEdgeRefine();
	testImage.SaveSegmentationResult();

	/*tree detection*/
	//	if(TestConfiguration.datasetName == "Map3D")
	//	{
	//	    string prefix = TestConfiguration.resultdir+TestConfiguration.datasetName+
	//					     TestConfiguration.dirend+ExtractFileName(testImage.filenameImage,false);
	//	    string savedFilename = prefix+"_"+"tree_detection.jpg";
	//	    AerialTreeExtraction(testImage.segResult,testImage.ptImage,savedFilename);
	//	}

	/*if (testImage.labelMask != NULL)
	{
		testImage.SegmentationValidation(confusionMatrix, testImage.activeMask);
	}*/

	if (probability != NULL){
		delete probability;
		//probability = NULL;
	}
	if (probability_index != NULL) {
		delete probability_index;
		//probability_index = NULL;
	}
}

void ImageParser::TestInitialization()
{
	TestConfiguration.confusionMatrix = cv::Scalar(0);

	assert(TestConfiguration.classifierName == "RT");

	/*reload the classifier*/
	if (classifierLoaded == false)
	{
		if (pixelRTrees != NULL) { 
			delete pixelRTrees; 
			//pixelRTrees = NULL;
		}
		if (superpixelRTrees != NULL){
			delete superpixelRTrees; 
			//superpixelRTrees = NULL;
		}
	}

	bool tempwithPixelLevelfeatures = TestConfiguration.withPixelLevelfeatures;
	bool tempwithSPLevelfeatures = TestConfiguration.withSPLevelfeatures;

	if (classifierLoaded == false)/*reload the classifier*/
	{
		if (TestConfiguration.withPixelLevelfeatures == true)
		{
			TestConfiguration.withSPLevelfeatures = false;
			string ERT_Model_file_2d = TestConfiguration.GetClassifierName(false);
			LoadRandomForest(pixelRTrees, ERT_Model_file_2d);
		}

		TestConfiguration.withSPLevelfeatures = tempwithSPLevelfeatures;
		if (TestConfiguration.withSPLevelfeatures == true)
		{
			TestConfiguration.withPixelLevelfeatures = false;
			string ERT_Model_file_2d = TestConfiguration.GetClassifierName(false);
			LoadRandomForest(superpixelRTrees, ERT_Model_file_2d);
		}
		TestConfiguration.withPixelLevelfeatures = tempwithPixelLevelfeatures;

		classifierLoaded = true;
	}

	/*pixel level testing setting*/
	for (unsigned int i = 0; i<TestConfiguration.featParameters.size(); i++)
	{
		TestConfiguration.featParameters[i].sampleStep = 1;
	}
}

bool ImageParser::LoadRandomForest(RandomForest*& RTrees, string modelFileName)
{
	if (RTrees != NULL) delete RTrees;

	ifstream inputfile;
	if (BasicAPI::FileExistCheck(modelFileName.c_str(), inputfile) == true)
	{
		RTrees = new RandomForest();
		RTrees->LoadForest(modelFileName);
	}
	inputfile.close();

	if (RTrees == NULL)
	{
		cout << "Load Classifier failed from " << modelFileName << endl;
		return false;
	}
	return true;
}
