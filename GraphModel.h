
#ifndef GRAPHMODEL_H_
#define GRAPHMODEL_H_

#include <vector>
#include "SuperPixelImage.h"
#include "cv.h"
#include "cxcore.h"
#include "cvaux.h"
#include "highgui.h"
#include "Image.h"
#include "Global.h"
#include "GraphElements.h"
using namespace std;

class PotentialParameters
{
public:
	PotentialParameters();

	PotentialParameters(int setParaSizeRow, int setParaSizeCol, double intialValue, weightType status);

	PotentialParameters& operator=(const PotentialParameters& T);

	bool operator==(const PotentialParameters& T);

	void SetParameter(int setParaSizeRow, int setParaSizeCol,
		double intialValue, weightType status);

	void SetStatus(weightType status);

	void WriteWeight(ofstream& out, string potentialType);
	void LoadWeight(ifstream& in);

	void PrintWeight();
	void PrintStatus(string potentialType);
	void Print(string potentialType);

	void GetParameters(vector<double>& weight, vector<weightType>& weightStatus);
	void SetParameters(vector<double>& weight, vector<weightType>& weightStatus, int& startIndex);

	int GetSize();

public:
	int paraSizeRow;
	int paraSizeCol;
	vector< vector<double> > paras;
	vector< vector<weightType> > parasStatus;
};

class GraphModel;

class GraphParameters{
public:
	GraphParameters();
	GraphParameters(const GraphParameters& T);

	bool operator==(const GraphParameters& T);
	GraphParameters& operator=(const GraphParameters& T);

	void SetDefaultParameters();
	void GetParameters(vector<double>& weight, vector<weightType>& weightStatus);
	void SetParameters(vector<double>& weight, vector<weightType>& weightStatus);

	void Print();

	void WriteToFile(ofstream& out);
	void LoadFromFile(ifstream& in);

public:
	/*unary potential weights*/
	PotentialParameters LayerNodesWeights;
	PotentialParameters upLayerNodesWeights;
	PotentialParameters superLayerNodesWeights;

	/*pairwise potential weights*/
	PotentialParameters LayerLinksWeights;
	PotentialParameters upLayerLinksWeights;
	PotentialParameters superLayerLinksWeights;

	/*across layer pairwise potential weights*/
	PotentialParameters upLayer_LayerLinksWeights;
	PotentialParameters superLayer_LayerLinksWeights;
	PotentialParameters superLayer_upLayerLinksWeights;

	PotentialParameters constraintWeights;
	PotentialParameters RobustPnWeights;
};

/* undirected graph model*/
class GraphModel {
public:
	GraphModel(int setNumClass);
	GraphModel();

	virtual ~GraphModel();
public:

	void SetupGraph(BasicImage& image);
	void SetupGraph(PixelImage* image, cv::Mat* layerProb, cv::Mat* layerProbIndex);

	void SetupGraph(SuperPixelImage* image,
		cv::Mat* layerProb, cv::Mat* layerProbIndex,
		cv::Mat* upLayerProb, cv::Mat* upLayerProbIndex,
		cv::Mat* superLayerProb, cv::Mat* superLayerProbIndex);

	void SetupLayer(PixelImage* image, cv::Mat* layerProb, cv::Mat* layerProbIndex);
	void SetupLayer(SuperPixelImage* image, cv::Mat* layerProb, cv::Mat* layerProbIndex);
	void SetupUpLayer(SuperPixelImage* image, cv::Mat* upLayerProb, cv::Mat* upLayerProbIndex);
	void SetupSuperLayer(SuperPixelImage* image, cv::Mat* superLayerProb, cv::Mat* superLayerProbIndex);

	void SetupConstraintLayer();

	static void AssignGraphParameters(GraphParameters& setWeight)
	{
		/*unary potential weights */
		weights = setWeight;
	}

	static void SetupConstraintTable(cv::Mat& priorConstraintTable)
	{
		assert(priorConstraintTable.cols == TestConfiguration.numOfClass &&
			priorConstraintTable.rows == TestConfiguration.numOfClass);

		if (constraintTable != NULL) constraintTable->release();
		constraintTable = new cv::Mat(priorConstraintTable.rows,
			priorConstraintTable.cols,
			CV_32SC1, cv::Scalar(0));

		priorConstraintTable.copyTo(*constraintTable);

		int availabelCount = 0;
		for (int i = 0; i<constraintTable->rows; i++)
		{
			for (int j = 0; j<constraintTable->cols; j++)
			{
				if (constraintTable->at<int>(i, j)>0 ||
					constraintTable->at<int>(j, i)>0)
				{
					availabelCount++;
				}
			}
		}
		cout << "available constraint label: " << availabelCount << "/" << TestConfiguration.numOfClass*TestConfiguration.numOfClass << endl;
	}

	void ViewGraphLabel();
	/*map inference with graph cut*/
	void MAPInference(bool withInitializaiton = false, bool withLossCost = false);
	/*map inference with loopy belief propagation*/
	void MAPInferenceBP(bool withInitializaiton = false);
	void WriteWeights();
	void LoadWeights();

	/*assign the loss cost for inference in the structure learning*/
	void AssignLossCost(GCoptimization::EnergyTermType* dCost,
		int numLabels,
		int startIndex,
		vector<int>& activeNodeIndexs,
		vector<GraphNode*>& inputLayer,
		int NodeType,
		int *initialLabeling = NULL);

	void AssignUnaryCost(GCoptimization::EnergyTermType* dCost,
		int numLabels,
		int startIndex,
		vector<int>& activeNodeIndexs,
		vector<GraphNode*>& inputLayer,
		int NodeType,
		int *initialLabeling = NULL);

	void AssignPairwiseCost(GCoptimizationGeneralGraph* mrf,
		int startIndex,
		vector<int>& activeNodeIndexs,
		vector<GraphNode*>& inputLayer,
		int NodeType);

	void AssignAcrossLayerPairwiseCost(GCoptimizationGeneralGraph* mrf,
		int srcStartIndex,
		int dstStartIndex,
		vector<int>& activeNodeIndexs,
		vector<GraphNode*>& inputLayer,
		int LinkType);

	void AssignRobustPnUnaryCost(GCoptimization::EnergyTermType* dCost,
		int numLabels,
		int startIndex,
		vector<int>& activeNodeIndexs,
		vector<GraphNode*>& inputLayer,
		int NodeType,
		int *initialLabeling);

	void AssignRobustPnPairwiseCost(GCoptimizationGeneralGraph* mrf,
		int srcStartIndex,
		int dstStartIndex,
		vector<int>& activeNodeIndexs,
		vector<GraphNode*>& inputLayer,
		int LinkType);

	void Release();

	/*must be called before use graph model*/
	static void SetConfiguration(GraphParameters* loadedParameters)
	{
		if (GraphModel::initialized == false)
		{
			withLayerUnary = false;
			withUpLayerUnary = false;
			withSuperLayerUnary = false;

			withRobustPn = false;

			withLayerPairwise = false;
			withUpLayerPairwise = false;
			withSuperLayerPairwise = false;

			withUpLayer_LayerPairwise = false;
			withSuperLayer_upLayerPairwise = false;
			withSuperLayer_LayerPairwise = false;

			withConstraintLayer = false;
			GraphModel::withUnLabelRegion = TestConfiguration.withUnLabelRegion;
			GraphModel::numClass = TestConfiguration.numOfClass;

			if (TestConfiguration.layerUnary != nonactive) withLayerUnary = true;
			if (TestConfiguration.uplayerUnary != nonactive) withUpLayerUnary = true;
			if (TestConfiguration.superlayerUnary != nonactive) withSuperLayerUnary = true;

			if (TestConfiguration.layerPariwise != nonactive) withLayerPairwise = true;
			if (TestConfiguration.uplayerPariwise != nonactive) withUpLayerPairwise = true;
			if (TestConfiguration.superlayerPariwise != nonactive) withSuperLayerPairwise = true;

			if (TestConfiguration.layerPariwise != nonactive) withLayerPairwise = true;
			if (TestConfiguration.layerPariwise != nonactive) withLayerPairwise = true;
			if (TestConfiguration.layerPariwise != nonactive) withLayerPairwise = true;

			if (TestConfiguration.constraintlayer != nonactive) withConstraintLayer = true;
			if (TestConfiguration.RobustPn != nonactive) withRobustPn = true;

			if (TestConfiguration.up_layer_Pariwise != nonactive) withUpLayer_LayerPairwise = true;
			if (TestConfiguration.super_layer_Pariwise != nonactive) withSuperLayer_LayerPairwise = true;
			if (TestConfiguration.super_up_layerPariwise != nonactive) withSuperLayer_upLayerPairwise = true;

			constraintLinkStep = 8;

			if (loadedParameters != NULL) weights = (*loadedParameters);
			else weights.SetDefaultParameters();

			GraphModel::initialized = true;
		}
	}

	/*layer of different layer*/
	vector<GraphNode*> Layer;
	vector<GraphNode*> upLayer;
	vector<GraphNode*> superLayer;

	//string imageFileName;
	BasicImage inputImage;
	int width;
	int height;
	int LayerSize;
	int upLayerSize;
	int superLayerSize;

	float initialEnergy;
	float finalEnergy;

	static bool withRobustPn;
	static bool withConstraintLayer;
	/*link configuration*/
	static bool withLayerUnary;
	static bool withUpLayerUnary;
	static bool withSuperLayerUnary;

	static bool withLayerPairwise;
	static bool withUpLayerPairwise;
	static bool withSuperLayerPairwise;

	static bool withUpLayer_LayerPairwise;
	static bool withSuperLayer_upLayerPairwise;
	static bool withSuperLayer_LayerPairwise;

	/*initialization flag of graph model*/
	static bool initialized;

	static const float robustPnAllowedRatio; /*the max ratio of allowed different labels in the cliques*/
	static const float LargePenalty;
	static int constraintLinkStep;
	static GraphParameters weights;
	static cv::Mat*   constraintTable;    // store valid constraint combination
	static int numClass;

	static GCoptimization::EnergyTermType SmoothTermCost(int node_i, int node_j, int label_i, int label_j, void* extraData);
	static GCoptimization::EnergyTermType SmoothTermCostGeneral(int node_i, int node_j, int i, int j, void* extraData);

	static bool withUnLabelRegion;    // include unlabeled region or not
	// for training, if it is true, then exclude unlabeled region for testing, always be true

};
#endif /* GRAPHMODEL_H_ */
