#ifndef STRUCTURELEARNING_H_
#define STRUCTURELEARNING_H_

#include <vector>
#include <iostream>
#include <stdio.h>
#include "BasicAPI.h"
#include "GraphModel.h"
#include "Global.h"
#include "ClassificationModel.h"
#include "StructLearner.h"
#include "Dataset.h"

//#include "CGAL/basic.h"
//#include "CGAL/QP_models.h"
//#include "CGAL/QP_functions.h"
//#include "CGAL/Gmpzf.h"
//typedef CGAL::Gmpzf ET;

using namespace std;

/*structure learning of CRF with linear interaction of parameter  and feature*/

class StructLearner{
public:

	StructLearner(int  setNumClass);
	virtual ~StructLearner();

	void Initialization(); /*Initialization of struct learner*/

	void InitialParameters();

	void Train();

	/*Find a new constraint for structure learning, usually the most violated*/
	void FindNewConstraint(GraphModel* singleTrainingSample,
		int sampleIndex,
		cv::Mat* confusionMatrix,
		vector< vector<double> >& predictedStructureFeatureSet,
		vector< vector<double> >& trueStructureFeatureSet,
		vector< double >& deltaLossSet);

	/*Find a new constraint for structure learning, for one slack formulation*/
	void FindNewConstraint(vector<double>& sumPredictedStructureFeature,
		vector<double>& sumTrueStructureFeature,
		double& sumDeltaLoss,
		cv::Mat* confusionMatrix);

	/*build a graphical model for an image*/
	void ConstructStructExample(SuperPixelImage* image,
		GraphModel* structExample,
		cv::Mat* layerProb, cv::Mat* layerProbIndex,
		cv::Mat* uplayerProb, cv::Mat* uplayerProbIndex,
		cv::Mat* superlayerProb, cv::Mat* superlayerProbIndex);

	double ComputeLoss(GraphModel* sample, cv::Mat* confusionMatrix = NULL);
	double ComputeLoss(vector<GraphModel*>& sampleSet);

	/*extract feature for structure sample*/
	void ExtractStructureFeature(GraphModel* sample, vector<double>& structureFeature, bool usePrediction);
	void ExtractUnaryFeature(vector<GraphNode*>& layer, cv::Mat& feat_unary, bool usePrediction);
	void ExtractPairwiseFeature(vector<GraphNode*>& layer, cv::Mat& feat_pairwise, int linkType, bool usePrediction);
	void ExtractConstraintFeature(vector<GraphNode*>& layer, cv::Mat& feat_constraint, bool usePrediction);
	void ExtractRobustPnFeature(vector<GraphNode*>& layer, cv::Mat& feat_RobustPn, bool usePrediction);

	/*extract the best parameters with evaluation score*/
	void ExtractOptimalWeight(vector<cv::Mat*>& confusionMatrixSet, vector<double>& risks);

	/*find constraint to be inserted from a graphical model*/
	void FindConstraintLabel(GraphModel* sample);

	void PrintFeature(vector<double>& predictedStructureFeature, vector<double>& trueStructureFeature);

	void ProcessUnlabeledRegion(GraphModel* sample, bool assignPrediction);

	double EmpericalRisk(vector<double>& sumPredictedStructureFeature,
		vector<double>& sumTrueStructureFeature,
		GraphParameters& learnedParameters);

	string ModelParameterFilename(string extraTag = "");

	void SaveLearnedWeight(string weight_file_name, double objectiveValue);
	bool LoadLearnedWeight(string weight_file_name);

	void Validate(GraphModel* sample);
	void ViewFoundConstraintLabel(GraphModel* sample, string tag = "");

	void Evaluation(cv::Mat* confusionMatrix, double& recall, double& averagePrecision, double& averageRecall);

	void RecordIteration(int iterationIndex, cv::Mat* confusionMatrix, double objectiveValue);

	virtual bool UpdateWeight(vector<double>& predictedStructureFeature,
		vector<double>& trueStructureFeature,
		double deltaLoss)
	{
		cerr << "Error: This update function should never be called ... " << endl;
		return false;
	}

public:
	bool clearBoundary;
	bool positiveWeight;                 // the learned weights are positive or not
	int numClass;                        // the number of categories
	int iterationCount;                  // the max number of iterations
	int convergedIter;                   // the iteration index of convergence
	int numWeights;                      // the number of weights

	double bestRecall;
	double bestAverageRecall;
	double initialRecall;
	double initialAverageRecall;

	GraphParameters learnedParameters;
	GraphParameters initialParameters;

	/*parameter for the learning formulation*/
	double StructSVM_C;
	double SubGradient_C;
	double SubGradient_stepsize;
	double OnlinePA_C;
	double OnlinePA_initialC;

	bool loadLearnedWeight;         // using loaded weight as initial
	bool withLowMemoryCost;     // to reduce memory cost, load unary feature from file real time, with more I/O operation

	/*training samples with ground truth label*/
	vector<GraphModel*> trainSamples;

	/*training samples with orginal label*/
	vector< GraphParameters > parametersSets;

	ImageParser* RTClassifier;
	int iterationIter;
	string parameterFlag; /*set an extra flag for the parameters to be learned, for local training*/
};

/*This class implement the one-slacl Struct SVM*/
/*1 : QP solver from the CGAL library*/
/*2 : non-negative QP for non-negative constraints for parameters to be learned*/

//class StructSVM : public StructLearner
//{
//public:
//	StructSVM(int setNumClass);
//	virtual ~StructSVM();
//
//	bool UpdateWeight(vector<double>& predictedStructureFeature,
//                      vector<double>& trueStructureFeature,
//                      double deltaLoss);
//
//	void InsertConstraints(vector<double>& predictedStructureFeature,
//	                       vector<double>& trueStructureFeature,
//	                       double deltaLoss);
//
//	bool SetQPConstraints(CGAL::Quadratic_program<double>& QP);
//
//public:
//	vector< vector<float> > constraints;  /*store the constraint*/
//    double slack;
//};
//
///*sub-gradient algorithm*/
//class SubGradient : public StructLearner
//{
//public:
//	SubGradient(int setNumClass);
//	virtual ~SubGradient();
//
//	/*update parameters with single constraint*/
//	bool UpdateWeight(vector<double>& predictedStructureFeature,
//                      vector<double>& trueStructureFeature,
//                      double deltaLoss);
//
//	bool UpdateWeight(vector<double>& diffFeatureVector,
//	                  vector<double>& weight,vector<weightType>& weightFlag,
//	                  vector<double>& updatedweight,
//	                  double deltaLoss,multimap<double,int>& indexMapping);
//
//public:
//};

/*online passive-aggressive algorithm*/
class OnlinePA : public StructLearner
{
public:
	OnlinePA(int  setNumClass);
	virtual ~OnlinePA();

	double OptimizePieceWiseFunction(vector<double>& diffFeatureVector,
		double deltaLoss,
		multimap<double, int>& indexMapping);

	void OptimizeSinglePieceWiseFunction(double optimalPointsCandidate,
		double leftBoundaryPoint,
		double rightBoundaryPoint,
		vector<double>& diffFeatureVector,
		double deltaLoss,
		vector<double>& solutions,
		vector<double>& optimalValues,
		multimap<double, int>& indexMapping);

	double ObjectiveFunciotn(double alpha,
		vector<double>& weight,
		vector<double>& diffFeatureVector,
		vector<weightType> weightFlag,
		double deltaLoss,
		multimap<double, int>& indexMapping);

	/*update parameters with single constraint*/
	bool UpdateWeight(vector<double>& predictedStructureFeature,
		vector<double>& trueStructureFeature,
		double deltaLoss);

	bool UpdateWeight(vector<double>& diffFeatureVector, double& alpha,
		vector<double>& weight, vector<weightType>& weightFlag,
		vector<double>& updatedweight,
		double deltaLoss, multimap<double, int>& indexMapping);

public:
	bool piecewiseTraining;        // using piecewise training or not
	double maxChangeRatio;      /*the maximum change ratio of projecting out of bound value to initial value*/

};
#endif /* STRUCTURELEARNING_H_ */
