#include "StructureParsingLearning.h"
#include "BasicAPI.h"

StructureParsing::StructureParsing() {
	// TODO Auto-generated constructor stub

	adaptiveLearning = false;
}

StructureParsing::~StructureParsing() {
	// TODO Auto-generated destructor stub
}

/*Parsing with parameters loaded from file*/
void StructureParsing::Test(vector<BasicImage>& testImages)
{
	TestConfiguration.classifierType = "multiClass";
	TestConfiguration.withUnLabelRegion = true; /*for test images, this information is unknown*/

	GraphModel::SetConfiguration(NULL);

	StructLearner* graphMoldeTrainer = NULL;
	if (TestConfiguration.GraphModelTrainerType == TypeOnlinePA)
		graphMoldeTrainer = new OnlinePA(TestConfiguration.numOfClass);

	graphMoldeTrainer->Initialization();
	if (TestConfiguration.classifierName == "RT")
	{
		if (TestConfiguration.loadConfidenceMap == true)
			graphMoldeTrainer->RTClassifier->classifierLoaded = true;
		graphMoldeTrainer->RTClassifier->Test(testImages);
	}

	delete graphMoldeTrainer;
}

/*Parsing with adaptive parameters*/
void StructureParsing::Train(vector<BasicImage>& testImages,
	vector< vector<int> >& localTrainImagesIndex)
{
	GraphModel::SetConfiguration(NULL);
	StructLearner* graphMoldeTrainer = NULL;
	if (TestConfiguration.GraphModelTrainerType == TypeOnlinePA)
		graphMoldeTrainer = new OnlinePA(TestConfiguration.numOfClass);

	graphMoldeTrainer->Initialization();
	if (TestConfiguration.classifierName == "RT")
	{
		vector<BasicImage> localTrainImages;
		/*train local parameters*/
		for (int i = 0; i<testImages.size(); i++)
		{
			localTrainImages.clear();
			for (int j = 0; j<localTrainImagesIndex.at(i).size(); j++)
			{
				int idx = localTrainImagesIndex.at(i).at(j);
				localTrainImages.push_back(validateImages.at(idx));
			}

			graphMoldeTrainer->parameterFlag = BasicAPI::ExtractFileName(testImages.at(i).filenameImage, false);
			TrainStructureModel(graphMoldeTrainer, localTrainImages);
		}
	}

	delete graphMoldeTrainer;
}

void StructureParsing::Train(vector<BasicImage>& inputTrainImages)
{
	validateImages.clear();
	for (int i = 0; i<inputTrainImages.size(); i++)
	{
		validateImages.push_back(inputTrainImages.at(i));
	}

	//if(TestConfiguration.loadConfidenceMap == false) TrainClassifier();

	TrainStructureModel();
}

void StructureParsing::Train(vector<BasicImage>& testImages, vector<BasicImage>& inputTrainImages)
{
	validateImages.clear();
	for (int i = 0; i<inputTrainImages.size(); i++)
	{
		validateImages.push_back(inputTrainImages.at(i));
	}

	if (TestConfiguration.loadConfidenceMap == false) TrainClassifier();
}

//void StructureParsing::SplitTrainData(vector<BasicImage>& inputTrainImages)
//{
//	trainImages.clear();
//	validateImages.clear();
//
//    int step = inputTrainImages.size();
//
//    if(TestConfiguration.trainRatio > 0.5)
//    {
//    	cout<<"too high ratio for training graphilca model ... ";
//    	cout<<"adjust to 0.5 automatically ... "<<endl;
//    	TestConfiguration.trainRatio = 0.5;
//    }
//    else if(TestConfiguration.trainRatio < 0.05)
//    {
//    	cout<<"too low ratio for training graphilca model ... ";
//    	cout<<"adjust to 0.1 automatically ... "<<endl;
//    	TestConfiguration.trainRatio = 0.1;
//    }
//
//	step = int(max(1.0,1.0/TestConfiguration.trainRatio));
//
//	for(unsigned int i=0;i<inputTrainImages.size();i++)
//	{
//		if(i%step == 0)
//		{
//			validateImages.push_back(inputTrainImages.at(i));
//		}
//		else
//		{
//			trainImages.push_back(inputTrainImages.at(i));
//		}
//	}
//
//	/*output for ALE*/
//	ofstream out1;
//	out1.open("Train.txt");
//	for(unsigned int i=0;i<trainImages.size();i++)
//	{
//		string s = "image " + trainImages.at(i).filenameImage;
//		out1<<s<<endl;
//		s = "label " + trainImages.at(i).filenameLabelMask;
//		out1<<s<<endl;
//	}
//	out1.close();
//
//	ofstream out3;
//	out3.open("Validate.txt");
//	for(unsigned int i=0;i<validateImages.size();i++)
//	{
//		string s = "image " + validateImages.at(i).filenameImage;
//		out3<<s<<endl;
//		s = "label " + validateImages.at(i).filenameLabelMask;
//		out3<<s<<endl;
//	}
//	out3.close();
//
////	ofstream out2;
////	out2.open("Test.txt");
////	for(unsigned int i=0;i<validateImages.size();i++)
////	{
////		out2<<BasicAPI::ExtractFileName(validateImages.at(i).filenameImage,true)<<endl;
////	}
////	for(unsigned int i=0;i<TestConfiguration.validateImages.size();i++)
////	{
////		out2<<BasicAPI::ExtractFileName(TestConfiguration.validateImages.at(i).filenameImage,true)<<endl;
////	}
////	out2.close();
//}

void StructureParsing::TrainClassifier()
{
	TestConfiguration.classifierType = "multiClass";

	if (TestConfiguration.classifierName == "RT")
	{
		ImageParser RfClassifier;
		RfClassifier.Train(trainImages);
	}
}

void StructureParsing::TrainStructureModel()
{
	int i, j, k;
	TestConfiguration.classifierType = "multiClass";

	GraphModel::SetConfiguration(NULL);
	StructLearner* graphMoldeTrainer = NULL;

	if (TestConfiguration.GraphModelTrainerType == TypeOnlinePA)
		graphMoldeTrainer = new OnlinePA(TestConfiguration.numOfClass);

	graphMoldeTrainer->Initialization();

	//    if(TestConfiguration.loadConfidenceMap == false)
	//    {
	//    	TestConfiguration.saveConfidenceMap = true;
	//        for(int k=0;k<validateImages.size();k++)
	//        {
	//        	cout<<"process "<<BasicAPI::ExtractFileName(validateImages.at(k).filenameImage,true)<<endl;
	//        	cout<<k+1<<"/"<<validateImages.size()<<endl;
	//
	//        	SuperPixelImage testImage;
	//        	if(testImage.Initialization(&validateImages.at(k),TestConfiguration.testLevel) == true)
	//        	{
	//            	cv::Mat* prob = NULL;
	//            	cv::Mat* probIndex = NULL;
	//
	//            	if(TestConfiguration.classifierName == "RT")
	//            	{
	//            		graphMoldeTrainer->RTClassifier->TestBasicClassifiers(&testImage,prob,probIndex);
	//            	}
	//
	//            	if(prob!=NULL) delete prob;
	//            	if(probIndex!=NULL) delete probIndex;
	//        	}
	//        }
	//    }

	TestConfiguration.loadConfidenceMap = true;
	TestConfiguration.saveConfidenceMap = false;

	/*initialize structure training samples*/
	for (k = 0; k<validateImages.size(); k++)
	{
		cout << "process " << BasicAPI::ExtractFileName(validateImages.at(k).filenameImage, true) << endl;
		cout << k + 1 << "/" << validateImages.size() << endl;

		if (graphMoldeTrainer->withLowMemoryCost == false)
		{
			SuperPixelImage testImage;
			if (testImage.Initialization(&validateImages.at(k), TestConfiguration.testLevel) == true)
			{
				GraphModel* sample = new GraphModel();
				graphMoldeTrainer->trainSamples.push_back(sample);

				cv::Mat* prob = NULL;
				cv::Mat* probIndex = NULL;

				if (TestConfiguration.classifierName == "RT")
				{
					graphMoldeTrainer->RTClassifier->TestBasicClassifiers(&testImage, prob, probIndex);
				}

				sample->SetupGraph(&testImage, prob, probIndex, NULL, NULL, NULL, NULL);
				if (prob != NULL) delete prob;
				if (probIndex != NULL) delete probIndex;
			}
		}
		else
		{
			GraphModel* sample = new GraphModel();
			graphMoldeTrainer->trainSamples.push_back(sample);
			sample->SetupGraph(validateImages.at(k));
		}
	}

	/*structure training*/
	graphMoldeTrainer->Train();
	for (unsigned int i = 0; i<graphMoldeTrainer->trainSamples.size(); i++)
	{
		delete graphMoldeTrainer->trainSamples.at(i);
	}

	delete graphMoldeTrainer;
}


void StructureParsing::TrainStructureModel(StructLearner* graphMoldeTrainer, vector<BasicImage>& localTrainImages)
{
	int i, j, k;
	TestConfiguration.classifierType = "multiClass";

	TestConfiguration.localGraphTraining = false;
	graphMoldeTrainer->InitialParameters();
	TestConfiguration.localGraphTraining = true;
	for (k = 0; k<localTrainImages.size(); k++)
	{
		cout << "process " << BasicAPI::ExtractFileName(localTrainImages.at(k).filenameImage, true) << endl;
		cout << k + 1 << "/" << localTrainImages.size() << endl;

		if (graphMoldeTrainer->withLowMemoryCost == false)
		{
			SuperPixelImage testImage;
			if (testImage.Initialization(&localTrainImages.at(k), TestConfiguration.testLevel) == true)
			{
				GraphModel* sample = new GraphModel();
				graphMoldeTrainer->trainSamples.push_back(sample);

				cv::Mat* prob = NULL;
				cv::Mat* probIndex = NULL;

				if (TestConfiguration.classifierName == "RT")
				{
					graphMoldeTrainer->RTClassifier->TestBasicClassifiers(&testImage, prob, probIndex);
				}

				sample->SetupGraph(&testImage, prob, probIndex, NULL, NULL, NULL, NULL);
				if (prob != NULL) delete prob;
				if (probIndex != NULL) delete probIndex;
			}
		}
		else
		{
			GraphModel* sample = new GraphModel();
			graphMoldeTrainer->trainSamples.push_back(sample);
			sample->SetupGraph(localTrainImages.at(k));
		}
	}

	graphMoldeTrainer->Train();
	for (unsigned int i = 0; i<graphMoldeTrainer->trainSamples.size(); i++)
	{
		delete graphMoldeTrainer->trainSamples.at(i);
	}
	graphMoldeTrainer->trainSamples.clear();
}
