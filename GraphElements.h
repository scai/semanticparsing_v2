#ifndef GRAPHELEMENTS_H_
#define GRAPHELEMENTS_H_

#include <vector>
using namespace std;

class NodeLink;

class GraphNode
{
public:
	GraphNode();
	GraphNode(const GraphNode& T);
	~GraphNode(){};

	GraphNode& operator=(const GraphNode& T);

	void InsertLayerlinks(GraphNode* T, double setw = 0);
	void InsertUpLayerlinks(GraphNode* T, double setw = 0);
	void InsertSuperLayerlinks(GraphNode* T, double setw = 0);
	void InsertConstraintLayerlinks(GraphNode* T, double setw = 0);

public:
	/* link & link-wieght*/
	vector< NodeLink >   layerlinks;           // the link in the same layers
	vector< NodeLink >   upLayerlinks;         // the link across different layers
	vector< NodeLink >   superLayerlinks;      // the link across different layers
	vector< NodeLink >   constraintLayerlinks; // the link to constraint nodes

	vector<float> featVector;           // the feature of the node

	int index;                          // the index of nodes
	int activeIndex;                    // the index of nodes active
	int trueLabel;                      // ground truth label
	int predictLabel;                   // predicted label

	int x;                              // position
	int y;
	int size;                           // the size of the node
	bool isActive;                      // active flag
	bool withLabel;                     // is the node with ground truth label
};

class NodeLink{
public:
	NodeLink();
	~NodeLink();
	NodeLink(const NodeLink& t);
	NodeLink& operator=(const NodeLink& t);

public:
	float w;                /*link weight*/
	GraphNode* linkedNode;  /*linked node*/
};

#endif /* GRAPHELEMENTS_H_ */
